FROM ubuntu:latest

RUN apt update -y

RUN apt install git -y
RUN apt install openjdk-21-jdk -y
RUN apt install maven -y
RUN apt install python3 python3-pip -y

RUN apt install build-essential 
RUN apt install zlib1g-dev


# Install MARCO GMUS Solver
WORKDIR /usr/local/bin
RUN git clone https://github.com/liffiton/MARCO
WORKDIR /usr/local/bin/MARCO/src/pyminisolvers
RUN make
WORKDIR /usr/local/bin
RUN mv ./MARCO/* .
WORKDIR /
