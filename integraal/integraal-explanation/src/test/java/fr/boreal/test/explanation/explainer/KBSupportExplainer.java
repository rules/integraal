package fr.boreal.test.explanation.explainer;

import fr.boreal.explanation.ruleFactories.StaticGRIRuleTransformer;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.KnowledgeBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.KnowledgeBaseImpl;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;
import fr.boreal.test.explanation.TestData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

public class KBSupportExplainer {
    fr.boreal.explanation.explainers.KBSupportExplainer explainer;
    StaticGRIRuleTransformer staticGRIRuleTransformer = StaticGRIRuleTransformer.instance();

    Predicate REL = staticGRIRuleTransformer.REL;

    @BeforeEach
    public void setUp() {
    }

    @Test
    public void KBSupportExplainerTest3() {
        Atom query = TestData.qa;

        FactBase factBase = new SimpleInMemoryGraphStore(List.of(TestData.saa, TestData.sbc, TestData.tab));
        RuleBase ruleBase = new RuleBaseImpl(List.of(TestData.r6, TestData.r7, TestData.r8));
        KnowledgeBase kb = new KnowledgeBaseImpl(factBase, ruleBase);

        explainer = new fr.boreal.explanation.explainers.KBSupportExplainer(kb);
        Set<KnowledgeBase> explanations = explainer.explain(query);

        FactBase expectedFactBase = new SimpleInMemoryGraphStore(Set.of(TestData.saa, TestData.sbc, TestData.tab));
        RuleBase expectedRuleBase = new RuleBaseImpl(Set.of(TestData.r6,TestData.r8));
        KnowledgeBase expectedExpl = new KnowledgeBaseImpl(expectedFactBase, expectedRuleBase);

        Assertions.assertTrue(explanations.contains(expectedExpl), "(Completeness issue) Explanation missing: " + expectedExpl);
        Assertions.assertEquals(1, explanations.size(), "(Soundness issue) identify non-explanation as explanation");

    }

    @Test
    public void KBSupportExplainerTest2() {
        Atom query = TestData.qa;

        FactBase factBase = new SimpleInMemoryGraphStore(List.of(TestData.saa, TestData.sbc, TestData.tab));
        RuleBase ruleBase = new RuleBaseImpl(List.of(TestData.r6, TestData.r7, TestData.r8));
        KnowledgeBase kb = new KnowledgeBaseImpl(factBase, ruleBase);

        explainer = new fr.boreal.explanation.explainers.KBSupportExplainer(kb);

        try{
        Set<KnowledgeBase> explanations = explainer.explain(query);

        FactBase expectedFactBase = new SimpleInMemoryGraphStore(Set.of(TestData.saa, TestData.sbc, TestData.tab));
        RuleBase expectedRuleBase = new RuleBaseImpl(Set.of(TestData.r6,TestData.r8));
        KnowledgeBase expectedExpl = new KnowledgeBaseImpl(expectedFactBase, expectedRuleBase);

        Assertions.assertTrue(explanations.contains(expectedExpl), "(Completeness issue) Explanation missing: " + expectedExpl);
        Assertions.assertEquals(1, explanations.size(), "(Soundness issue) identify non-explanation as explanation");
        } catch (RuntimeException e) {
            System.out.println("MARCO Execution failed");
        }
    }

    @Test
    public void KBSupportExplainerTest1() {
        Atom query = TestData.qa;

        FactBase factBase = new SimpleInMemoryGraphStore(List.of(TestData.pa, TestData.ta));
        RuleBase ruleBase = new RuleBaseImpl(List.of(TestData.r1, TestData.r4));
        KnowledgeBase kb = new KnowledgeBaseImpl(factBase, ruleBase);

        explainer = new fr.boreal.explanation.explainers.KBSupportExplainer(kb);

        try {
            Set<KnowledgeBase> explanations = explainer.explain(query);

            FactBase expectedFactBase = new SimpleInMemoryGraphStore(Set.of(TestData.pa));
            RuleBase expectedRuleBase = new RuleBaseImpl(Set.of(TestData.r1));
            KnowledgeBase expectedExpl = new KnowledgeBaseImpl(expectedFactBase, expectedRuleBase);


            Assertions.assertTrue(explanations.contains(expectedExpl), "(Completeness issue) Explanation missing: " + expectedExpl);
            Assertions.assertEquals(1, explanations.size(), "(Soundness issue) identify non-explanation as explanation");
        } catch (RuntimeException e) {
            System.out.println("MARCO Execution failed");
        }
    }

}
