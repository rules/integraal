package fr.boreal.test.explanation.ruletransformation;

import fr.boreal.explanation.ruleFactories.StaticGRIRuleTransformer;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.KnowledgeBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.KnowledgeBaseImpl;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.*;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.logicalElements.impl.functionalTerms.GroundFunctionalTermImpl;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.rule.impl.FORuleImpl;
import fr.boreal.redundancy.Redundancy;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;
import fr.boreal.test.explanation.TestData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

public class StaticGRIRuleTransformerTest {
    private final StaticGRIRuleTransformer factory = StaticGRIRuleTransformer.instance();
    public static PredicateFactory predicateFactory = SameObjectPredicateFactory.instance();
    public static TermFactory termFactory = SameObjectTermFactory.instance();
    public static FOFormulaFactory formulaFactory = FOFormulaFactory.instance();
    FactBase factBase = new SimpleInMemoryGraphStore(Collections.singleton(TestData.pa));
    RuleBase ruleBase = new RuleBaseImpl(Collections.singleton(TestData.r1));
    KnowledgeBase kb = new KnowledgeBaseImpl(factBase, ruleBase);

    @BeforeEach
    public void setUp() {
        // to do
    }

    @Test
    public void getFnSymbolFromPredicateTest() {
        String actual = factory.getFnSymbolFromPredicate(TestData.p1);
        String expected = "f_p1";
        Assertions.assertEquals(actual, expected, "Wrong functional symbol created");
    }

    @Test
    public void getPredicateWithIdentifierTest() {
        Predicate actual = factory.getPredicateWithIdentifier(TestData.p1);
        Predicate expected = predicateFactory.createOrGetPredicate("p1+",2);
        Assertions.assertEquals(actual, expected, "Wrong predicate with identifier created");
    }

    @Test
    public void createAtomWithStoredFnTermIdentifierTest() {
        Atom actual = factory.createAtomWithStoredFnTermIdentifier(TestData.pa);

        var identifier = new GroundFunctionalTermImpl("f_p1", List.of(TestData.a));

        Atom expected = new AtomImpl(factory.getPredicateWithIdentifier(TestData.p1), List.of(TestData.a, identifier));

        Assertions.assertEquals(actual, expected, "Wrong atom with functional identifier");
    }
    @Test
    public void createAtomWithVariableIdentifierTest() {
        Atom actual = factory.createAtomWithVariableIdentifier(TestData.pa);

        Variable identifier = termFactory.createOrGetFreshVariable();

        Atom expected = new AtomImpl(factory.getPredicateWithIdentifier(TestData.p1), List.of(TestData.a, identifier));

        Assertions.assertTrue(TestData.isIsomorphic(actual,expected), "Wrong atom with functional identifier");
    }

    @Test
    public void createGRINodeBuildingRuleTest() {
        FORule actual = factory.createGRINodeBuildingRule(TestData.p1);
        Atom bodyAtom = TestData.px;
        Atom headAtom = new AtomImpl(factory.getPredicateWithIdentifier(TestData.p1),
                List.of(TestData.x, factory.createFnTermIdentifier(TestData.px)));
        FORule expected = new FORuleImpl(bodyAtom, headAtom);
        Assertions.assertTrue(Redundancy.isRuleIsomorphism(actual,expected),
                String.format("Wrong GRI node building rule: %n  actual: %s, %n expected: %s", actual, expected));

    }

    @Test
    public void createGRIEdgeBuildingRuleTest() {
        FORule actual = factory.createGRIEdgeBuildingRule(TestData.r1);

        Atom bodyAtom = factory.createAtomWithVariableIdentifier(TestData.px);
        Atom headAtom = factory.createAtomWithStoredFnTermIdentifier(TestData.qx);

        List<Atom> headAtoms = new ArrayList<>(List.of(headAtom));
        headAtoms.add(new AtomImpl(factory.getOrGenerateEdgePredicateFromRule(TestData.r1),
                List.of(bodyAtom.getTerm(1),headAtom.getTerm(1))));

        FOFormula body = formulaFactory.createOrGetConjunction(bodyAtom);
        FOFormula head = formulaFactory.createOrGetConjunction(headAtoms);
        FORule expected = new FORuleImpl(TestData.r1.getLabel(), body, head);

        Assertions.assertTrue(Redundancy.isRuleIsomorphism(actual,expected),
                        String.format("Wrong GRI edge building rule: %n  actual: %s, %n expected: %s", actual, expected));
    }

    @Test
    public void createGRIEdgeBuildingRuleTest2() {
        FORule actual = factory.createGRIEdgeBuildingRule(TestData.r2);

        Atom bodyAtom = factory.createAtomWithVariableIdentifier(TestData.px);
        Atom headAtom1 = factory.createAtomWithStoredFnTermIdentifier(TestData.qx);
        Atom headAtom2 = factory.createAtomWithStoredFnTermIdentifier(TestData.tx);

        List<Atom> headAtoms = new ArrayList<>(List.of(headAtom1,headAtom2));
        headAtoms.add(new AtomImpl(factory.getOrGenerateEdgePredicateFromRule(TestData.r2),
                List.of(bodyAtom.getTerm(1),headAtom1.getTerm(1))));
        headAtoms.add(new AtomImpl(factory.getOrGenerateEdgePredicateFromRule(TestData.r2),
                List.of(bodyAtom.getTerm(1),headAtom2.getTerm(1))));

        FOFormula body = formulaFactory.createOrGetConjunction(bodyAtom);
        FOFormula head = formulaFactory.createOrGetConjunction(headAtoms);
        FORule expected = new FORuleImpl(TestData.r2.getLabel(), body, head);

        Assertions.assertTrue(Redundancy.isRuleIsomorphism(actual,expected),
                String.format("Wrong GRI edge building rule: %n  actual: %s, %n expected: %s", actual, expected));
    }

    @Test
    public void createGRIEdgeBuildingRuleTest3() {
        FORule actual = factory.createGRIEdgeBuildingRule(TestData.r3);

        Atom bodyAtom = factory.createAtomWithVariableIdentifier(TestData.px);
        Atom headAtom1 = factory.createAtomWithStoredFnTermIdentifier(TestData.qx);
        Atom headAtom2 = factory.createAtomWithStoredFnTermIdentifier(TestData.txy);

        List<Atom> headAtoms = new ArrayList<>(List.of(headAtom1,headAtom2));
        headAtoms.add(new AtomImpl(factory.getOrGenerateEdgePredicateFromRule(TestData.r3),
                List.of(bodyAtom.getTerm(1),headAtom1.getTerm(1))));
        headAtoms.add(new AtomImpl(factory.getOrGenerateEdgePredicateFromRule(TestData.r3),
                List.of(bodyAtom.getTerm(1),headAtom2.getTerm(2))));

        FOFormula body = formulaFactory.createOrGetConjunction(bodyAtom);
        FOFormula head = formulaFactory.createOrGetConjunction(headAtoms);
        FORule expected = new FORuleImpl(TestData.r3.getLabel(), body, head);

        Assertions.assertTrue(Redundancy.isRuleIsomorphism(actual,expected),
                String.format("Wrong GRI edge building rule: %n  actual: %s, %n expected: %s", actual, expected));
    }

    @Test
    public void createRelPropagationRuleTest() {
        FORule actual = factory.createRelPropagationRule(TestData.r1);

        Predicate edgePred = factory.getOrGenerateEdgePredicateFromRule(TestData.r1);
        Atom edge_rxy = new AtomImpl(edgePred, List.of(TestData.x, TestData.y));
        Atom rel_y = new AtomImpl(factory.REL, TestData.y);

        FOFormula body = formulaFactory.createOrGetConjunction(List.of(edge_rxy, rel_y));

        Atom rel_x = new AtomImpl(factory.REL, TestData.x);
        Atom relEdge_rxy = new AtomImpl(factory.getOrGenerateRelEdgePredicateFromRule(TestData.r1), List.of(TestData.x,TestData.y));

        FOFormula head = formulaFactory.createOrGetConjunction(rel_x, relEdge_rxy);

        FORule expected = new FORuleImpl(TestData.r1.getLabel(), body, head);

        Assertions.assertTrue(Redundancy.isRuleIsomorphism(actual,expected),
                String.format("Wrong GRI edge building rule: %n  actual: %s, %n expected: %s", actual, expected));


    }

}
