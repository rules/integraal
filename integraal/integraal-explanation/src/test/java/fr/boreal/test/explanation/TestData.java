package fr.boreal.test.explanation;

import fr.boreal.forward_chaining.api.ForwardChainingAlgorithm;
import fr.boreal.forward_chaining.chase.ChaseBuilder;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.KnowledgeBase;
import fr.boreal.model.logicalElements.api.*;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.logicalElements.impl.functionalTerms.SpecializableLogicalFunctionalTermImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.rule.impl.FORuleImpl;
import fr.boreal.query_evaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.storage.natives.DefaultInMemoryAtomSet;

import java.util.List;
import java.util.Set;

public class TestData {
        private static final FOQueryEvaluator<FOFormula> evaluator = GenericFOQueryEvaluator.defaultInstance();
        public static PredicateFactory predicateFactory = SameObjectPredicateFactory.instance();
        public static TermFactory termFactory = SameObjectTermFactory.instance();
        public static FOFormulaFactory formulaFactory = FOFormulaFactory.instance();

        public static Predicate p1 = predicateFactory.createOrGetPredicate("p1", 1);
        public static Predicate q1 = predicateFactory.createOrGetPredicate("q1", 1);
        public static Predicate t1 = predicateFactory.createOrGetPredicate("t1", 1);

        public static Predicate s2 = predicateFactory.createOrGetPredicate("s2", 2);
        public static Predicate t2 = predicateFactory.createOrGetPredicate("t2", 2);

        public static Constant a = termFactory.createOrGetConstant("a");
        public static Constant b = termFactory.createOrGetConstant("b");
        public static Constant c = termFactory.createOrGetConstant("c");

        public static Variable x = termFactory.createOrGetVariable("X");
        public static Variable y = termFactory.createOrGetVariable("Y");


        public static Atom px = new AtomImpl(p1, x);
        public static Atom py = new AtomImpl(p1, y);
        public static Atom qx = new AtomImpl(q1, x);
        public static Atom tx = new AtomImpl(t1, x);

        public static Atom txy = new AtomImpl(t2, List.of(x,y));
        public static Atom sxx = new AtomImpl(s2, List.of(x,x));
        public static Atom sxy = new AtomImpl(s2, List.of(x,y));

        public static Atom pa = new AtomImpl(p1, a);
        public static Atom pb = new AtomImpl(p1, b);
        public static Atom qa = new AtomImpl(q1, a);
        public static Atom ta = new AtomImpl(t1, a);

        public static Atom saa = new AtomImpl(s2, List.of(a,a));
        public static Atom sbc = new AtomImpl(s2, List.of(b,c));
        public static Atom tay = new AtomImpl(t2, List.of(a,y));
        public static Atom tab = new AtomImpl(t2, List.of(a,b));

        public static FOFormula qxtx = formulaFactory.createOrGetConjunction(qx,tx);
        public static FOFormula qxtxy = formulaFactory.createOrGetConjunction(qx,txy);
        public static FOFormula pxpytxy = formulaFactory.createOrGetConjunction(px,py,txy);

        public static FORule r1 = new FORuleImpl("r1",
                formulaFactory.createOrGetConjunction(px),
                formulaFactory.createOrGetConjunction(qx));
        public static FORule r2 = new FORuleImpl("r2", px, qxtx);
        public static FORule r3 = new FORuleImpl("r3", px, qxtxy);
        public static FORule r4 = new FORuleImpl("r4", qx, txy);
        public static FORule r5 = new FORuleImpl("r5", qxtx, px);
        public static FORule r6 = new FORuleImpl("r6", sxy, px);
        public static FORule r7 = new FORuleImpl("r7", sxx, px);
        public static FORule r8 = new FORuleImpl("r8", pxpytxy, qx);

        public static SpecializableLogicalFunctionalTerm f_pa = new SpecializableLogicalFunctionalTermImpl("f_p", List.of(a));

        public static boolean isIsomorphic(FactBase fb1, FactBase fb2) {
                FOQuery<?> q1 = FOQueryFactory.instance().createOrGetQuery(
                        FOFormulaFactory.instance().createOrGetConjunction(fb1),
                        List.of(),
                        null);
                FOQuery<?> q2 = FOQueryFactory.instance().createOrGetQuery(
                        FOFormulaFactory.instance().createOrGetConjunction(fb2),
                        List.of(),
                        null);

                return fb1.size() == fb2.size() && evaluator.existHomomorphism(q1, fb2) && evaluator.existHomomorphism(q2, fb1);
        }

        public static void chase(KnowledgeBase kb) {
                ForwardChainingAlgorithm chase = ChaseBuilder.defaultBuilder(kb.getFactBase(), kb.getRuleBase())
                        .useObliviousChecker()
                        .useNaiveComputer()
                        .useNaiveRuleScheduler()
                        .build().get();
                chase.execute();
        }
        public static boolean isIsomorphic(Atom a1, Atom a2) {
                return isIsomorphic(new DefaultInMemoryAtomSet(Set.of(a1)),
                        new DefaultInMemoryAtomSet(Set.of(a2)));
        }

}
