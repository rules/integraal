package fr.boreal.explanation.solving_enumerating;

import com.google.common.collect.BiMap;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.KnowledgeBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.KnowledgeBaseImpl;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;

import java.util.HashSet;
import java.util.Set;

public class GMUSTranslator {
    private static final GMUSTranslator INSTANCE = new GMUSTranslator();
    public static GMUSTranslator instance() {
        return INSTANCE;
    }

    public Set<KnowledgeBase> translateGMUSes(
            Set<String> gmuses,
            BiMap<Integer, Atom> factIDMap,
            BiMap<Integer, FORule> ruleIDMap
    ) {

        Set<KnowledgeBase> explanations = new HashSet<>();
        for (String gmus : gmuses) {
            explanations.add(gmusToExpl(gmus, factIDMap, ruleIDMap));
        }

        return explanations;
    }


    public KnowledgeBase gmusToExpl(
            String gmus,
            BiMap<Integer, Atom> factIDMap,
            BiMap<Integer, FORule> ruleIDMap) {

        FactBase explFB = new SimpleInMemoryGraphStore();
        RuleBase explRB = new RuleBaseImpl();

        for (String clause : gmus.split(" ")) {

            int nbGroup = Integer.parseInt(clause);

            if (nbGroup > 0) {
                if (factIDMap.containsKey(nbGroup)) {
                    explFB.add(factIDMap.get(nbGroup));
                } else if (ruleIDMap.containsKey(nbGroup)) {
                    explRB.add(ruleIDMap.get(nbGroup));
                }
            }
        }

        KnowledgeBase expl = new KnowledgeBaseImpl(explFB, explRB);

        return expl;
    }
}
