package fr.boreal.explanation.GRI.staticProcessing;

import fr.boreal.explanation.configuration.DefaultChaseForExplanations;
import fr.boreal.explanation.ruleFactories.StaticGRIRuleTransformer;
import fr.boreal.model.kb.api.KnowledgeBase;
import fr.boreal.model.kb.impl.KnowledgeBaseImpl;
import org.apache.commons.lang3.tuple.Pair;

public class GRIBuilder {

    public static Pair<KnowledgeBase, StaticGRIRuleTransformer> buildGRI(KnowledgeBase kb) {
        StaticGRIRuleTransformer staticRuleTransformerFactory =
                new StaticGRIRuleTransformer();

        // transform the ruleset to be able to build the GRI
        var transformedRB = staticRuleTransformerFactory.createTransformedRB(kb);

        KnowledgeBase gri = new KnowledgeBaseImpl(kb.getFactBase(), transformedRB);

        DefaultChaseForExplanations.chase(gri);

        return Pair.of(gri, staticRuleTransformerFactory);
    }

}
