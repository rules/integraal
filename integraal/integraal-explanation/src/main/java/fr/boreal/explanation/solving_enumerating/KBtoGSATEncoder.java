package fr.boreal.explanation.solving_enumerating;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import fr.boreal.explanation.ruleFactories.StaticGRIRuleTransformer;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.*;
import fr.boreal.model.logicalElements.impl.functionalTerms.GroundFunctionalTermImpl;
import fr.boreal.model.rule.api.FORule;

import java.util.*;

/**
 * This class computes an encoding of explanations as GCNF formulas that can later be processed by a MUS solver
 * <p>
 * Ground atoms are represented as propositional variables
 * <p>
 * <p>
 * <p>
 * <p>
 * 1. propVarIDmap maps a propositional variable to its corresponding (int) ID starting at 1
 * 2. clauses is a list of list of integers.
 * A clause take the following form:
 * nbGroup, i for every atom a_i in the chase
 * nbGroup, -i1, -i2, ..., i for every grounded rule a_i1 ^ a_i2 ... -> a_i
 * where each integer i is the ID of a propositional variable (as stored in propVarID map)
 * 3. nbGroup tracks the latest group number (starting at 0)
 * <p>
 * At the same time we are also computing 2 extra structures which are used in translating the GMUSes back
 * to KB:
 * 1. factIDmap (nbGroup - atom)
 * 2. ruleIDmap (nbGroup - ungrounded rule)
 */
public class KBtoGSATEncoder implements GMUSEncoder{

    StaticGRIRuleTransformer staticGRIRuleTransformer;
    Predicate REL = StaticGRIRuleTransformer.REL;
    public BiMap<Term, Integer> propVarIDMap = HashBiMap.create();
    public BiMap<Integer, Atom> factIDMap = HashBiMap.create();
    public BiMap<Integer, FORule> ruleIDMap = HashBiMap.create();
    public List<List<Integer>> clauses = new ArrayList<>();
    public int nbGroup = 1;

    public KBtoGSATEncoder(StaticGRIRuleTransformer staticGRIRuleTransformer) {
        this.staticGRIRuleTransformer = staticGRIRuleTransformer;
    }

    /**
     * This method takes a (filtered) KB and the query and return the set of explanations.
     *
     * @param filteredGRI
     * @param query
     * @return set of explanations
     */

    public EncodingResult encode(
            FactBase filteredGRI,
            Atom query) {
        assignDefaultGroupNumberAndCreateClauseForStartQuery(query);
        assignGroupNumbersAndComputeClausesForRELEdges(filteredGRI, query);

        return new EncodingResult(propVarIDMap,factIDMap,ruleIDMap,clauses, nbGroup);
    }

    /**
     * This method assigns id 1 to the query in propvarIDMap and add the query clause of group 0
     * It modifies:
     * 1. propvarIDMap
     * 2. clauses
     *
     * @param query
     */
    public void assignDefaultGroupNumberAndCreateClauseForStartQuery(Atom query) {
        Term queryFnTermIdentifier = staticGRIRuleTransformer.createFnTermIdentifier(query);
        propVarIDMap.put(queryFnTermIdentifier, propVarIDMap.size() + 1);
        clauses.add(List.of(0, propVarIDMap.get(queryFnTermIdentifier) * -1));
    }

    /**
     * This method assigns id every atom marked with REL and add a singleton group for each to clauses
     * For every *relvant* REL atom a :
     * (1) create a group number [nbgroup_a] for a
     * (2) creates a (singleoton) clause nbgroup id_a
     * It modifies:
     * 1. propvarIDMap
     * 2. clauses
     * 3. nbGroup
     * <p>
     * And in the meantime it records the id of each atom to factIDMap
     *
     * @param filteredGRI
     */

    public void assignGroupNumbersAndCreateClauseForEachSupRelAtom(FactBase filteredGRI, Atom query) {
        var it = filteredGRI.getAtomsByPredicate(REL);
        while (it.hasNext()) {
            var relatom = it.next();
            LogicalFunctionalTerm fnTermForAtom = (LogicalFunctionalTerm) relatom.getTerm(0);
            Atom atom = staticGRIRuleTransformer.getAtomFromFnTerm(fnTermForAtom);

            if (atom.equals(query)) {
                continue;
            }

            propVarIDMap
                    .putIfAbsent(fnTermForAtom, propVarIDMap.size() + 1);
            //make a group with just the atom
            // TODO we may simplify this and have a single map linking the atom, the term, and the group

            if (filteredGRI.contains(atom)) {
                factIDMap.put(nbGroup, atom);
                clauses.add(List.of(nbGroup, propVarIDMap.get(fnTermForAtom)));
            }

            nbGroup++;
        }
    }


    /**
     * For every *relvant* rule r :
     * (1) Gives a group number [nbgroup] to every *relvant* rule r and hence to every REL_EDGE_r predicate
     * (r is relevant if it is used to compute SupRel and marked by the presence of a REL_EDGE_r atom).
     * <p>
     * (2) Computes a set of clauses encoding the usage of r.
     * Every clause is derived from an atom of the form
     * REL_EDGE_r(id_grounded_body1,id_grounded_body2...,id_grounded_head)
     * Every clause is encoded as a list of integers the structure
     * nbgroup -id_grounded_body1 -id_grounded_body2 ... id_grounded_head
     *
     * @param filteredGRI
     */
    public void assignGroupNumbersAndComputeClausesForRELEdges(FactBase filteredGRI, Atom query) {
        Set<Predicate> allPredicatesAfterTracing = new HashSet<>();

        //retrieve all predicates used to compute SupRelevant produced by the GRIBuilding and RelTracer
        filteredGRI.getPredicates().forEachRemaining(predicate -> allPredicatesAfterTracing.add(predicate));

        BiMap<Predicate, FORule> relEdgePredicateToRule = staticGRIRuleTransformer.getRuleToRelEdgePredicateMap().inverse();

        // We compute a SET of clauses for every REL_EDGE_r *predicate*
        for (Predicate relEdgePred : relEdgePredicateToRule.keySet()) {
            if (allPredicatesAfterTracing.contains(relEdgePred)) {
                // we compute a clause for each REL_EDGE_r *atom*
                // for REL_EDGE_r(f_p(a),f_q(b)...,f_t(a)) is in SupRel we make a Horn propositional clause
                // NOT f_p(a) OR NOT f_q(b) ... OR f_t(a)
                // where every ground atom is a propositional variable represented by an integer
                // NOT 1 OR NOT 2 ... OR K
                // this is represented as a list of integers <-1 ,-2,...,K>
                // and, note that as a convention, we add the group number G in the first position <G, -1 ,-2,...,K>


                // records the id of each rule to ruleIDMap
                ruleIDMap.put(nbGroup, relEdgePredicateToRule.get(relEdgePred));
                var it = filteredGRI.getAtomsByPredicate(relEdgePred);
                while (it.hasNext()) {
                    Atom relEdge = it.next();
                    Term[] atomsFnIdentifierInGroundedRule = relEdge.getTerms();
                    List<Integer> clause = new ArrayList<>();

                    clause.addFirst(nbGroup);

                            // adding tales
                    for (int i = 0; i < atomsFnIdentifierInGroundedRule.length - 1; i++) {
                        LogicalFunctionalTerm fnTermForAtom = (GroundFunctionalTermImpl) atomsFnIdentifierInGroundedRule[i];
                        Atom atom = staticGRIRuleTransformer.getAtomFromFnTerm(fnTermForAtom);
                        if (atom.equals(query)) {
                            continue;
                        }

                        propVarIDMap
                                .putIfAbsent(fnTermForAtom, propVarIDMap.size() + 1);

                        // TODO we may simplify this and have a single map linking the atom, the term, and the group

                        if (filteredGRI.contains(atom)) {

                            nbGroup++;
                            factIDMap.inverse().putIfAbsent(atom, nbGroup);
                            clauses.add(List.of(nbGroup, propVarIDMap.get(fnTermForAtom)));
                        }
                        clause.add(propVarIDMap.get(atomsFnIdentifierInGroundedRule[i]) * -1);
                    }

                    // adding tip
                    // TODO you have to add head atom to the dictionaries as well!
                    clause.add(propVarIDMap.get(atomsFnIdentifierInGroundedRule[atomsFnIdentifierInGroundedRule.length - 1])
                    );


                    clauses.add(clause);
                }
            }
        }
    }
}
