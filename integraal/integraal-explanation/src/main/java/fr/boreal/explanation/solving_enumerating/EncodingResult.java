package fr.boreal.explanation.solving_enumerating;

import com.google.common.collect.BiMap;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.rule.api.FORule;

import java.util.List;

public record EncodingResult(BiMap<Term, Integer> propVarIDMap, BiMap<Integer, Atom> factIDMap,
                             BiMap<Integer, FORule> ruleIDMap, List<List<Integer>>clauses, int nbGroup) {}

