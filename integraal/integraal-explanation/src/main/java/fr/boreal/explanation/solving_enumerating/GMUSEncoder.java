package fr.boreal.explanation.solving_enumerating;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;

public interface GMUSEncoder {

    EncodingResult encode(FactBase filteredGRI, Atom query);
}
