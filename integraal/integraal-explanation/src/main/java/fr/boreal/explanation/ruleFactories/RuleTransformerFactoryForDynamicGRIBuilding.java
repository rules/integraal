package fr.boreal.explanation.ruleFactories;

import fr.boreal.model.kb.api.KnowledgeBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.logicalElements.api.*;

/**
 * This class implements a rule transformation for : dynamically computing the GRI depending on a given query
 */
public class RuleTransformerFactoryForDynamicGRIBuilding {

    // default instance
    private static final RuleTransformerFactoryForDynamicGRIBuilding INSTANCE = new RuleTransformerFactoryForDynamicGRIBuilding();

    /**
     * @return the default instance of the rule transformation factory
     */
    public static RuleTransformerFactoryForDynamicGRIBuilding instance() {
        return INSTANCE;
    }

    public RuleBase createTransformedRB(KnowledgeBase kb, Atom a) {
        //TODO
        return null;
    }

}
