package fr.boreal.explanation.explainers;

import fr.boreal.explanation.api.AtomicQueryExplanationEnumerator;
import fr.boreal.explanation.ruleFactories.StaticGRIRuleTransformer;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.logicalElements.api.Atom;

import java.util.Set;

/**
 * Computes rule-support explanations for a knowledge base and a ground atomic query
 */
public class RuleSupportExplainer implements AtomicQueryExplanationEnumerator<RuleBase> {
    static StaticGRIRuleTransformer staticRuleTransformerFactory = StaticGRIRuleTransformer.instance();

    public Set<RuleBase> explain(Atom query) {

        return Set.of();
    }
}
