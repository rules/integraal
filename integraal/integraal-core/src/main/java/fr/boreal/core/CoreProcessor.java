package fr.boreal.core;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Variable;

import java.util.HashSet;
import java.util.Set;

/**
 * Compute the core of a fact base.
 * The computed core is a fact base without redundant atoms;
 * 'core' corresponds to the notion of graph theory (the set of atoms being viewed as a bipartite (predicate/term) graph).
 *
 * @author Guillaume Pérution-Kihli
 *
 */

public interface CoreProcessor {
    /**
     * Compute the core of a fact base by removing all the redundant atoms in it
     *
     * @param fb the fact base on which we want to compute the core
     */
    default void computeCore(FactBase fb) {
        computeCore(fb, new HashSet<>());
    }

    /**
     * Compute the core of a fact base by removing all the redundant atoms in it, in considering the frozen variables as constants,
     * these variables must belong to the core.
     *
     * @param fb the fact base on which we want to compute the core
     * @param frozenVariables Variables that will be treated as constants
     */
    void computeCore(FactBase fb, Set<Variable> frozenVariables);
}
