package fr.boreal.core;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.query_evaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;
import fr.lirmm.boreal.util.PiecesSplitter;

import java.util.*;

import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;


/**
 * Compute the core of a fact base with a multithreading algorithm that computes
 * the core by retracting pieces.
 * In the exhaustive variant, for each piece, we search all the homomorphisms
 * that retract the piece into the atom set, and we keep the homomorphism that
 * minimizes the number of remaining variables in the piece.
 * In the "by specialization" variant, we specialize the piece when we find
 * a homomorphism h that reduces the number of the variables in the piece (the
 * specialization is done by sending using h on the piece and then search new
 * homomorphisms that retract more variables of the initial piece).
 * In the "by deletion" variant, when we found such a homomorphism, we remove
 * the retracted variables from the piece and the atom set and search new
 * homomorphisms on the result.
 *
 * @author Guillaume Pérution-Kihli
 *
 */

public class MultiThreadsByPieceCoreProcessor implements CoreProcessor {
    private final FOQueryEvaluator<FOFormula> evaluator;
    Set<Variable> deletedVariables; // Set of variables that will be deleted at the end of the execution
    Set<Variable> frozenVariables; // Set of variables that will not be retracted
    Set<Variable> notFrozenVariables; // Complementary of frozenVariables
    FactBase target; // Fact base on which we compute the core

    Queue<SimpleInMemoryGraphStore> piecesQueue; // Queue to send pieces to the threads
    List<Thread> threads; // List of the threads
    final long limitNbThreads; // Limit of the number of threads
    Semaphore lockVariableDeleting; // Lock the access to the attribute deletedVariables
    AtomicBoolean stop; // True if we are waiting the end of the execution of the threads, false otherwise
    final Variant variant; // Variant of the algorithm to use

    public enum Variant {
        EXHAUSTIVE,
        BY_SPECIALISATION,
        BY_DELETION
    }

    /**
     * @param evaluator Query evaluator
     * @param limitNbThreads Maximum number of threads
     * @param variant Variant of the algorithm
     */
    public MultiThreadsByPieceCoreProcessor(FOQueryEvaluator<FOFormula> evaluator, long limitNbThreads, Variant variant) {
        this.evaluator = evaluator;
        this.variant = variant;
        this.limitNbThreads = limitNbThreads;
    }

    /**
     * @param limitNbThreads Maximum number of threads
     * @param variant Variant of the algorithm
     */
    public MultiThreadsByPieceCoreProcessor(long limitNbThreads, Variant variant) {
        this.evaluator = GenericFOQueryEvaluator.defaultInstance();
        this.variant = variant;
        this.limitNbThreads = limitNbThreads;
    }

    /**
     * @param limitNbThreads Maximum number of threads
     * @param evaluator Query evaluator
     */
    public MultiThreadsByPieceCoreProcessor(long limitNbThreads, FOQueryEvaluator<FOFormula> evaluator) {
        this(evaluator, limitNbThreads, Variant.BY_DELETION);
    }

    public MultiThreadsByPieceCoreProcessor() {
        this(32, GenericFOQueryEvaluator.defaultInstance());
    }

    @Override
    public void computeCore(FactBase fb, Set<Variable> frozenVariables) {
        // We initialize the attributes of the object
        init(fb, frozenVariables);

        // We compute the pieces induced by the variables that are not frozen
        // We don't want grounded atoms, so we put "false" in the first parameter
        {
            PiecesSplitter splitter = new PiecesSplitter(false, notFrozenVariables);
            Collection<Collection<Atom>> pieces = splitter.split(fb.getAtomsInMemory());

            // We put the pieces into the queue to the threads
            for (Collection<Atom> piece : pieces) {
                SimpleInMemoryGraphStore p = new SimpleInMemoryGraphStore(piece);
                this.piecesQueue.offer(p);
                synchronized (this.piecesQueue) {
                    // We notify a waiting thread that we put an element into the queue
                    this.piecesQueue.notify();
                }
            }
        }

        // Now, we stop the threads
        try {
            this.waitThreads();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        // We delete the variables from the fact base
        for (Variable v : deletedVariables) {
            List<Atom> toRemove = new ArrayList<>();
            fb.getAtomsByTerm(v).forEachRemaining(toRemove::add);
            fb.removeAll(toRemove);
        }

        // Finally, we clean the attributes
        this.clean();
    }

    /**
     * Initialize the attributes of the object to compute the core
     *
     * @param fb the fact base which we compute the core
     * @param frozenVariables Set of variables that will not be retracted
     */
    private void init(FactBase fb, Set<Variable> frozenVariables) {
        // We need to compute the set of all variables that are not frozen
        // The pieces of "a" will be computed with these variables
        notFrozenVariables = new HashSet<>();
        notFrozenVariables.addAll(fb.getVariables()
                .parallel()
                .filter(var -> !frozenVariables.contains(var))
                .toList());

        // We create the semaphore to access the attribute deletedVariables
        lockVariableDeleting = new Semaphore(1);

        this.frozenVariables = frozenVariables;
        target = fb;

        deletedVariables = new HashSet<>();
        piecesQueue = new ConcurrentLinkedQueue<>();
        this.stop = new AtomicBoolean(false);

        // We create and start the threads
        threads = new ArrayList<>();
        for (int i = 0; i < this.limitNbThreads; ++i) {
            Thread t = new Thread(this::retractPieces);
            threads.add(t);
            t.start();
        }
    }

    /**
     * Clean the attributes to free the memory and prepare the object for the next computation
     */
    private void clean() {
        lockVariableDeleting = null;
        deletedVariables = null;
        frozenVariables = null;
        target = null;
        threads = null;
        piecesQueue = null;
        notFrozenVariables = null;
    }

    /**
     * Compute the retractions of the pieces
     * This method is executed by the threads
     */
    private void retractPieces () {
        try {
            // While we are not waiting for the end of the thread or there are some remaining pieces to treat
            while (!stop.get() || this.piecesQueue.peek() != null) {
                // Get a piece from the queue
                SimpleInMemoryGraphStore p = this.piecesQueue.poll();

                if (p == null) {
                    // If there weren't any piece in the queue, we wait
                    synchronized (this.piecesQueue) {
                        this.piecesQueue.wait(1);
                    }
                } else {
                    // Otherwise, we got a piece

                    // We create a substitution to fix the frozen variables in the computing of the homomorphism
                    Substitution frozenVariablesSubstitution = new SubstitutionImpl();
                    for (Variable fv : frozenVariables) {
                        frozenVariablesSubstitution.add(fv, fv);
                    }

                    switch (variant) {
                        case BY_DELETION:
                            this.retractPiecesByDeletion(p, frozenVariablesSubstitution);
                            break;
                        case BY_SPECIALISATION:
                            this.retractPiecesBySpecialisation(p, frozenVariablesSubstitution);
                            break;
                        case EXHAUSTIVE:
                            this.retractPiecesExhaustive(p, frozenVariablesSubstitution);
                            break;
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Iterator<Substitution> computeHomomorphismsToFactBase(
            SimpleInMemoryGraphStore p, Substitution frozenVariablesSubstitution) {
        FOQuery<FOFormulaConjunction> q = FOQueryFactory.instance().createOrGetQuery(
                FOFormulaFactory.instance().createOrGetConjunction(p), null);
        return evaluator.homomorphism(q, target, frozenVariablesSubstitution);
    }

    private boolean checkValidityRetraction (
            Set<Term> variablesToCheck, Set<Variable> deletedVars, boolean unlock) throws InterruptedException {
        // Get the access to the deletedVariables attribute
        this.lockVariableDeleting.acquire();

        // Check that the retraction does not retract a variable to a deleted variable
        for (Term t : variablesToCheck) {
            if (this.deletedVariables.contains(t)) {
                this.lockVariableDeleting.release();
                return false;
            }
        }

        // Otherwise, add the variables to the set of deleted variables and release the locking
        this.deletedVariables.addAll(deletedVars);
        if (unlock) {
            this.lockVariableDeleting.release();
        }

        return true;
    }

    private void retractPiecesExhaustive (SimpleInMemoryGraphStore p, Substitution frozenVariablesSubstitution)
            throws InterruptedException {
        // We get an iterator on the retractions of the piece into the atom set target
        Iterator<Substitution> it = computeHomomorphismsToFactBase(p, frozenVariablesSubstitution);

        // Variables of p
        Set<Variable> pVars = p.getVariables().collect(Collectors.toSet());

        // We count the variables in the piece that are not frozen
        long nbNotFrozenVariables = p.getVariables()
                .filter(v -> !frozenVariables.contains(v))
                .count();
        // At the beginning, no variable has been retracted
        long maxNbVariablesRetracted = 0;
        // We initialize the set of the variables that we have deleted
        Set<Variable> alreadyDeletedVars = new HashSet<>();

        // While there is a retraction to treat
        while (it.hasNext()) {
            Substitution sub = it.next(); // Get the retraction
            sub.removeIdentity();
            if (sub.isEmpty()) {
                continue;
            }

            Set<Variable> deletedVars = new HashSet<>(sub.keys());
            long nbVariablesRetracted = deletedVars.size();

            // If we can remove more variables than a precedent retraction
            if (nbVariablesRetracted > maxNbVariablesRetracted) {

                // Computes the variables that we have to check if there are not deleted
                Set<Term> variablesToCheck = sub.rangeTerms().stream()
                        .filter(v -> v instanceof Variable)
                        .filter(v -> !pVars.contains(v))
                        .filter(v -> !frozenVariables.contains(v))
                        .filter(v -> !alreadyDeletedVars.contains(v))
                        .collect(Collectors.toSet());

                if (checkValidityRetraction(variablesToCheck, deletedVars, true)) {
                    // Update the set of the already deleted variables of the piece
                    alreadyDeletedVars.addAll(deletedVars);

                    // update the maximum number of deleted variables
                    maxNbVariablesRetracted = nbVariablesRetracted;
                    // If all the variables are retracted, stop the search of retractions
                    if (maxNbVariablesRetracted == nbNotFrozenVariables) {
                        break;
                    }
                }
            }
        }
    }

    private void retractPiecesByDeletion (SimpleInMemoryGraphStore p, Substitution frozenVariablesSubstitution)
            throws InterruptedException {
        // We get an iterator on the retractions of the piece into the atom set target
        Iterator<Substitution> it = computeHomomorphismsToFactBase(p, frozenVariablesSubstitution);

        // Variables of p
        Set<Variable> pVars = p.getVariables().collect(Collectors.toSet());

        // We count the variables in the piece that are not frozen
        long nbNotFrozenVariables = p.getVariables()
                .filter(v -> !frozenVariables.contains(v))
                .count();

        // While there is a retraction to treat
        while (it.hasNext()) {
            Substitution sub = it.next(); // Get the retraction
            sub.removeIdentity();
            if (sub.isEmpty()) {
                continue;
            }

            Set<Variable> deletedVars = new HashSet<>(sub.keys());
            long nbVariablesRetracted = deletedVars.size();

            // If we can remove more variables than a precedent retraction
            if (nbVariablesRetracted > 0) {

                // Computes the variables that we have to check if there are not deleted
                Set<Term> variablesToCheck = sub.rangeTerms().stream()
                        .filter(v -> v instanceof Variable)
                        .filter(v -> !pVars.contains(v))
                        .filter(v -> !frozenVariables.contains(v))
                        .collect(Collectors.toSet());

                if (checkValidityRetraction(variablesToCheck, deletedVars, true)) {
                    // If all the variables are retracted, stop the search of retractions
                    if (nbVariablesRetracted == nbNotFrozenVariables) {
                        break;
                    }

                    // We count the variables in the piece that are not frozen
                    nbNotFrozenVariables = p.getVariables()
                            .filter(v -> !frozenVariables.contains(v))
                            .count();

                    // We remove the atoms that contain the deleted variables of the piece
                    Collection<Atom> toRemove = new ArrayList<>();
                    for (Variable v : deletedVars) {
                        Iterator<Atom> it_var = p.getAtomsByTerm(v);
                        while (it_var.hasNext()) {
                            toRemove.add(it_var.next());
                        }
                    }
                    p.removeAll(toRemove);

                    // Update the list of variables of p
                    pVars.removeAll(deletedVars);

                    // And we begin new research of retractions
                    it = computeHomomorphismsToFactBase(p, frozenVariablesSubstitution);
                }
            }
        }
    }

    private void retractPiecesBySpecialisation (SimpleInMemoryGraphStore p, Substitution frozenVariablesSubstitution)
            throws InterruptedException {
        // We get an iterator on the retractions of the piece into the atom set target
        Iterator<Substitution> it = computeHomomorphismsToFactBase(p, frozenVariablesSubstitution);

        // Variables of p
        Set<Variable> pVars = p.getVariables().collect(Collectors.toSet());

        // We count the variables in the piece that are not frozen
        long nbNotFrozenVariables = p.getVariables()
                .filter(v -> !frozenVariables.contains(v))
                .count();
        // At the beginning, no variable has been retracted
        long maxNbVariablesRetracted = 0;
        // We initialize the set of the variables that we have deleted
        Set<Variable> alreadyDeletedVars = new HashSet<>();

        // While there is a retraction to treat
        while (it.hasNext()) {
            Substitution sub = it.next(); // Get the retraction
            sub.removeIdentity();
            if (sub.isEmpty()) {
                continue;
            }

            Set<Variable> deletedVars = new HashSet<>(sub.keys());
            deletedVars.addAll(alreadyDeletedVars);
            long nbVariablesRetracted = deletedVars.size();

            // If we can remove more variables than a precedent retraction
            if (nbVariablesRetracted > maxNbVariablesRetracted) {

                // Computes the variables that we have to check if there are not deleted
                final var pVarsAux = pVars;
                final var alreadyDeletedVarsAux = alreadyDeletedVars;
                this.lockVariableDeleting.acquire();
                Set<Term> variablesToCheck = sub.rangeTerms().stream()
                        .filter(v -> v instanceof Variable)
                        .filter(v -> !pVarsAux.contains(v))
                        .filter(v -> !frozenVariables.contains(v))
                        .filter(v -> !alreadyDeletedVarsAux.contains(v))
                        .collect(Collectors.toSet());
                this.lockVariableDeleting.release();

                if (checkValidityRetraction(variablesToCheck, deletedVars, false)) {
                    // Update the set of variables to remove
                    alreadyDeletedVars = deletedVars;
                    // Update the maximum number of retracted variable
                    maxNbVariablesRetracted = nbVariablesRetracted;

                    // We fixed the deleted variables to their new value
                    for (Variable v : alreadyDeletedVars) {
                        Term img = sub.createImageOf(v);
                        if (img instanceof Variable && !pVars.contains(img)) {
                            frozenVariablesSubstitution.add(v, img);
                        }
                    }

                    this.lockVariableDeleting.release();

                    // Update the list of variables of p
                    pVars = p.getVariables().collect(Collectors.toSet());

                    // If all the variables are retracted, stop the search of retractions
                    if (maxNbVariablesRetracted == nbNotFrozenVariables) {
                        break;
                    }

                    // And we begin new research of retractions
                    it = computeHomomorphismsToFactBase(p, frozenVariablesSubstitution);
                }
            }
        }
    }


    /**
     * Notify the threads that we are waiting the end of their execution
     */
    private void waitThreads () throws InterruptedException {
        // We notify the threads that we want to stop
        this.stop.set(true);
        synchronized (this.piecesQueue) {
            this.piecesQueue.notifyAll();
        }

        // We wait on threads
        for (Thread t : threads) {
            t.join();
        }
    }
}
