package fr.boreal.test.query_evaluation;

import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.query_evaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.storage.builder.StorageBuilder;
import fr.boreal.views.FederatedFactBase;
import fr.boreal.views.builder.ViewBuilder;
import org.hsqldb.jdbc.JDBCDataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Iterator;

class ListOrSetSemanticTest {

    private static final Path viewDefinitionFile = Path.of("src", "test", "resources", "view.vd");
    private static FederatedFactBase federation;

    @BeforeAll
    public static void simulateView() throws ViewBuilder.ViewBuilderException, SQLException {
        JDBCDataSource sqlDataSource = new JDBCDataSource();
        sqlDataSource.setUrl("jdbc:hsqldb:mem:test");
        try (Connection c = sqlDataSource.getConnection()) {
            c.createStatement().execute("SET DATABASE SQL SYNTAX MYS TRUE;");
            c.createStatement().execute("CREATE TABLE IF NOT EXISTS view(label varchar(32));");
            c.createStatement().execute("INSERT INTO view VALUES('a');");
            c.createStatement().execute("INSERT INTO view VALUES('a');");
            c.createStatement().execute("INSERT INTO view VALUES('b');");
            c.createStatement().execute("INSERT INTO view VALUES('b');");
            c.createStatement().execute("INSERT INTO view VALUES('c');");
            c.createStatement().execute("INSERT INTO view VALUES('d');");
            c.createStatement().execute("INSERT INTO view VALUES('e');");
        }
        federation = new FederatedFactBase(StorageBuilder.defaultStorage(), ViewBuilder.createFactBases(viewDefinitionFile.toString()));
    }

    /**
     * Test that the match on a view uses the list semantic
     */
    @Test
    public void listSemanticOnMatch() {
        Iterator<?> result = federation.match(new AtomImpl(
                SameObjectPredicateFactory.instance().createOrGetPredicate("view", 1),
                SameObjectTermFactory.instance().createOrGetFreshVariable()
        ));
        assertCorrectSize(result, 7);
    }

    /**
     * Test that a query evaluation on a view uses the set semantic
     */
    @Test
    public void setSemanticOnQuery() {
        FOQuery<?> query = (FOQuery<?>) DlgpParser.parseDLGPString("?(X) :- view(X).").queries().stream().findFirst().orElseThrow();
        Iterator<?> result = GenericFOQueryEvaluator.defaultInstance().homomorphism(query, federation);
        assertCorrectSize(result, 5);
    }

    private void assertCorrectSize(Iterator<?> result, int expectedSize) {
        int resultCount = 0;
        while (result.hasNext()) {
            result.next();
            resultCount++;
        }
        Assertions.assertEquals(expectedSize, resultCount);
    }
}