package fr.boreal.test.query_evaluation;

import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.io.dlgp.DlgpUtil;
import fr.boreal.model.formula.FOFormulas;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.partition.TermPartition;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.query_evaluation.conjunction.backtrack.NaiveDynamicScheduler;
import fr.boreal.query_evaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class NaiveDynamicSchedulerTest {

    @Test
    void testSingleAtomQuery() throws Exception {
        String dlgpFacts = "p(a, b).";
        String dlgpQuery = "? :- p(X, Y).";

        FactBase factBase = new SimpleInMemoryGraphStore(DlgpUtil.parseFacts(dlgpFacts));
        FOQuery<FOFormulaConjunction> query = DlgpParser.parseDLGPString(dlgpQuery).queries().stream()
                .map(q -> FOQueryFactory.instance().createOrGetQuery(
                        FOFormulaFactory.instance().createOrGetConjunction(((FOQuery<?>)q).getFormula()), q.getAnswerVariables()))
                .findFirst().orElseThrow();

        NaiveDynamicScheduler scheduler = new NaiveDynamicScheduler(query, factBase);

        Assertions.assertTrue(scheduler.hasNext(0));
        Substitution substitution = new SubstitutionImpl();
        var firstFormula = scheduler.next(0, substitution);
        Assertions.assertNotNull(firstFormula);

        Assertions.assertFalse(scheduler.hasNext(1));
    }

    @Test
    void testFunctionalTerms() throws Exception {
        String dlgpFacts = " p(1, 2), s(3).";
        String dlgpQuery = "@computed ig:<stdfct> \n ?() :- s(ig:sum(X, Y)), p(X, Y).";

        FactBase factBase = new SimpleInMemoryGraphStore(DlgpUtil.parseFacts(dlgpFacts));
        FOQuery<FOFormulaConjunction> query = (FOQuery<FOFormulaConjunction>) DlgpParser.parseDLGPString(dlgpQuery).queries().stream().findFirst().orElseThrow();
        List<? extends FOFormula> subFormulas = query.getFormula().getSubElements().stream().toList();
        FOFormula pxy = subFormulas.get(1);
        FOFormula ssumxy = subFormulas.get(0);

        NaiveDynamicScheduler scheduler = new NaiveDynamicScheduler(query, factBase);

        Assertions.assertTrue(scheduler.hasNext(0));
        Substitution substitution = new SubstitutionImpl();

        var firstFormula = scheduler.next(0, substitution);
        Assertions.assertEquals(pxy, firstFormula);
        Assertions.assertTrue(scheduler.hasNext(1));

        substitution = GenericFOQueryEvaluator.defaultInstance().evaluate(
                FOQueryFactory.instance().createOrGetQuery(firstFormula, List.of()),
                factBase).next();
        var secondFormula = scheduler.next(1, substitution);
        Assertions.assertEquals(ssumxy, secondFormula);
        Assertions.assertFalse(scheduler.hasNext(2));

        dlgpFacts = "p(1, 2), p(2, 1), s(2).";
        dlgpQuery = "@computed std:<stdfct> \n ?() :- s(std:minus(std:sum(X, Y), 1)), p(X, Y).";

        factBase = new SimpleInMemoryGraphStore(DlgpUtil.parseFacts(dlgpFacts));
        query = (FOQuery<FOFormulaConjunction>) DlgpParser.parseDLGPString(dlgpQuery).queries().stream().findFirst().orElseThrow();
        subFormulas = query.getFormula().getSubElements().stream().toList();
        pxy = subFormulas.get(1);
        ssumxy = subFormulas.get(0);

        scheduler = new NaiveDynamicScheduler(query, factBase);

        Assertions.assertTrue(scheduler.hasNext(0));
        substitution = new SubstitutionImpl();

        firstFormula = scheduler.next(0, substitution);
        Assertions.assertEquals(pxy, firstFormula);
        Assertions.assertTrue(scheduler.hasNext(1));

        substitution = GenericFOQueryEvaluator.defaultInstance().evaluate(
                FOQueryFactory.instance().createOrGetQuery(firstFormula, List.of()),
                factBase).next();
        secondFormula = scheduler.next(1, substitution);
        Assertions.assertEquals(ssumxy, secondFormula);
        Assertions.assertFalse(scheduler.hasNext(2));

        dlgpFacts = "p(1, 2), p(2, 1), s(2).";
        dlgpQuery = "@computed std:<stdfct> \n ?() :- s(std:minus(std:sum(X, Y), 1)), p(X, Y), not s(std:minus(X, 1)).";

        factBase = new SimpleInMemoryGraphStore(DlgpUtil.parseFacts(dlgpFacts));
        query = (FOQuery<FOFormulaConjunction>) DlgpParser.parseDLGPString(dlgpQuery).queries().stream().findFirst().orElseThrow();
        subFormulas = query.getFormula().getSubElements().stream().toList();
        pxy = subFormulas.get(1);
        ssumxy = subFormulas.get(0);
        FOFormula ssumx = subFormulas.get(2);

        scheduler = new NaiveDynamicScheduler(query, factBase);

        Assertions.assertTrue(scheduler.hasNext(0));
        substitution = new SubstitutionImpl();

        firstFormula = scheduler.next(0, substitution);
        Assertions.assertEquals(pxy, firstFormula);
        Assertions.assertTrue(scheduler.hasNext(1));

        substitution = GenericFOQueryEvaluator.defaultInstance().evaluate(
                FOQueryFactory.instance().createOrGetQuery(firstFormula, List.of()),
                factBase).next();
        secondFormula = scheduler.next(1, substitution);
        Assertions.assertTrue(ssumxy.equals(secondFormula) || ssumx.equals(secondFormula));
        Assertions.assertTrue(scheduler.hasNext(2));

        substitution = GenericFOQueryEvaluator.defaultInstance().evaluate(
                FOQueryFactory.instance().createOrGetQuery(firstFormula, List.of(), new TermPartition(substitution)),
                factBase).next();
        FOFormula thirdFormula = scheduler.next(2, substitution);
        Assertions.assertTrue(((ssumxy.equals(thirdFormula) || ssumx.equals(thirdFormula)) && !thirdFormula.equals(secondFormula)));
        Assertions.assertFalse(scheduler.hasNext(3));
    }

    @Test
    void testComputedAtom() throws Exception {
        String dlgpFacts = "p(a, b). q(4).";
        String dlgpQuery = "@prefix ig: <stdfct> ? :- p(X, Y), ig:isEven(ig:sum(X, Y, 7)).";

        FactBase factBase = new SimpleInMemoryGraphStore(DlgpUtil.parseFacts(dlgpFacts));
        FOQuery<FOFormulaConjunction> query = (FOQuery<FOFormulaConjunction>) DlgpUtil.parseQueries(dlgpQuery).iterator().next();

        NaiveDynamicScheduler scheduler = new NaiveDynamicScheduler(query, factBase);

        Substitution substitution = new SubstitutionImpl();
        Assertions.assertTrue(scheduler.hasNext(0));
        var firstFormula = scheduler.next(0, substitution);
        Assertions.assertNotNull(firstFormula);
    }
}
