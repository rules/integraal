package fr.boreal.test.query_evaluation;

import java.util.Iterator;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import fr.boreal.io.dlgp.DlgpUtil;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.api.Query;
import fr.boreal.query_evaluation.conjunction.backtrack.isomorphism.SubGraphIsomorphismBacktrackEvaluator;
import fr.boreal.query_evaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.storage.natives.DefaultInMemoryAtomSet;

@RunWith(Parameterized.class)
class QueryNaming {

	static Stream<Arguments> data() {

		String dlgp0 = "p(U). q(U). 	?(X) :- p(X),q(X).";

		String dlgp1 = "p(U), q(U). 	?(X) :- p(X),q(X).";

		String dlgp2 = "p(U). q(U). 	?(U) :- p(U),q(U).";

		String dlgp3 = "p(U), q(U). 	?(U) :- p(U),q(U).";

		return Stream.of(Arguments.of(dlgp0, false), Arguments.of(dlgp1, true), Arguments.of(dlgp2, false),
				Arguments.of(dlgp3, true));

	}

	@DisplayName("Test Query Answering with DLGP input")
	@ParameterizedTest(name = "{index}: DLGP {0} should give {1}")
	@MethodSource("data")
	public void dlgpHomomorphismTest(String dlgp, boolean result) throws Exception {
		FactBase factBase = new DefaultInMemoryAtomSet(DlgpUtil.parseFacts(dlgp));
		Query query = DlgpUtil.parseQueries(dlgp).iterator().next();

		GenericFOQueryEvaluator evaluator = GenericFOQueryEvaluator.defaultInstance();
		evaluator.setConjunctionEvaluator(new SubGraphIsomorphismBacktrackEvaluator(evaluator));

		if (!(query instanceof FOQuery<?> typedQuery)) {
			throw new Exception("Unsupported query type. Expected FOQuery, got : " + query.getClass());
		}

		Iterator<Substitution> resultsub = evaluator.homomorphism(typedQuery, factBase);

		Assert.assertEquals(result, resultsub.hasNext());

		if (resultsub.hasNext()) {
			var sub = resultsub.next();
			System.out.println(sub);
		}

	}

}
