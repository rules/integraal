package fr.boreal.query_evaluation.generic;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.formula.api.FOFormulaDisjunction;
import fr.boreal.model.formula.api.FOFormulaNegation;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.model.ruleCompilation.api.RuleCompilation;
import fr.boreal.query_evaluation.atomic.AtomicFOQueryEvaluator;
import fr.boreal.query_evaluation.atomic.InfAtomicFOQueryEvaluator;
import fr.boreal.query_evaluation.atomic.UnfoldingAtomicFOQueryEvaluator;
import fr.boreal.query_evaluation.conjunction.backtrack.BacktrackEvaluator;
import fr.boreal.query_evaluation.negation.NegationFOQueryEvaluator;

import java.util.Collection;
import java.util.Iterator;

/**
 * Evaluates a {@link FOQuery} by dispatching it to the correct evaluator. This
 * is used to abstract the query evaluation when the exact query type is not
 * known such as during a compound query (conjunction or disjunction)
 * sub-queries evaluation.
 */
public class GenericFOQueryEvaluator implements FOQueryEvaluator<FOFormula> {

	private FOQueryEvaluator<Atom> atomicEvaluator;
	private FOQueryEvaluator<FOFormulaConjunction> conjunctionEvaluator;
	private FOQueryEvaluator<FOFormulaDisjunction> disjunctionEvaluator;
	private FOQueryEvaluator<FOFormulaNegation> negationEvaluator;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * The default instance is used to give a default behavior to the evaluator when
	 * one doesn't want to configure it.
	 * @return the default instance of the evaluator
	 */
	public static GenericFOQueryEvaluator defaultInstance() {
		GenericFOQueryEvaluator instance = new GenericFOQueryEvaluator();
		return instance.setAtomicEvaluator(new AtomicFOQueryEvaluator())
				.setConjunctionEvaluator(new BacktrackEvaluator(instance))
				.setNegationEvaluator(new NegationFOQueryEvaluator(instance));
	}

	/**
	 * @param compilation a compilation of rules
	 * @return an instance of the evaluator using compilation with unfold evaluation
	 *         strategy
	 */
	public static GenericFOQueryEvaluator defaultInstanceWithUnfoldCompilation(RuleCompilation compilation) {
		GenericFOQueryEvaluator instance = GenericFOQueryEvaluator.defaultInstance();
		return instance.setAtomicEvaluator(new UnfoldingAtomicFOQueryEvaluator(compilation))
				.setConjunctionEvaluator(new BacktrackEvaluator(instance))
				.setNegationEvaluator(new NegationFOQueryEvaluator(instance));
	}

	/**
	 * @param compilation a compilation of rules
	 * @return an instance of the evaluator using compilation with inf-homomorphism
	 *         evaluation strategy
	 */
	public static GenericFOQueryEvaluator defaultInstanceWithInfCompilation(RuleCompilation compilation) {
		GenericFOQueryEvaluator instance = GenericFOQueryEvaluator.defaultInstance();
		return instance.setAtomicEvaluator(new InfAtomicFOQueryEvaluator(compilation))
				.setConjunctionEvaluator(new BacktrackEvaluator(instance))
				.setNegationEvaluator(new NegationFOQueryEvaluator(instance));
	}

	/////////////////////////////////////////////////
	// Setters
	/////////////////////////////////////////////////

	/**
	 * Sets the evaluator for atomic queries
	 * 
	 * @param evaluator the atomic query evaluator to use for recursive calls
	 * @return this evaluator for successive calls
	 */
	public GenericFOQueryEvaluator setAtomicEvaluator(FOQueryEvaluator<Atom> evaluator) {
		this.atomicEvaluator = evaluator;
		return this;
	}

	/**
	 * Sets the evaluator for conjunction of queries
	 * 
	 * @param evaluator the conjunction evaluator to use for recursive calls
	 * @return this evaluator for successive calls
	 */
	public GenericFOQueryEvaluator setConjunctionEvaluator(FOQueryEvaluator<FOFormulaConjunction> evaluator) {
		this.conjunctionEvaluator = evaluator;
		return this;
	}

	/**
	 * Sets the evaluator for disjunction of queries
	 * 
	 * @param evaluator the disjunction evaluator to use for recursive calls
	 * @return this evaluator for successive calls
	 */
	public GenericFOQueryEvaluator setDisjunctionEvaluator(FOQueryEvaluator<FOFormulaDisjunction> evaluator) {
		this.disjunctionEvaluator = evaluator;
		return this;
	}

	/**
	 * Sets the evaluator for negation of queries
	 *
	 * @param evaluator the negation evaluator to use for recursive calls
	 * @return this evaluator for successive calls
	 */
	public GenericFOQueryEvaluator setNegationEvaluator(FOQueryEvaluator<FOFormulaNegation> evaluator) {
		this.negationEvaluator = evaluator;
		return this;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public Iterator<Substitution> evaluate(FOQuery<? extends FOFormula> query, FactBase factbase, Collection<Variable> vars, Substitution preHomomorphism) {
        return switch (query.getFormula()) {
            case Atom ignored -> this.atomicEvaluator.evaluate((FOQuery<Atom>) query, factbase, vars, preHomomorphism);
            case FOFormulaConjunction ignored -> this.conjunctionEvaluator.evaluate((FOQuery<FOFormulaConjunction>) query, factbase, vars, preHomomorphism);
            case FOFormulaDisjunction ignored -> this.disjunctionEvaluator.evaluate((FOQuery<FOFormulaDisjunction>) query, factbase, vars, preHomomorphism);
            case FOFormulaNegation ignored -> this.negationEvaluator.evaluate((FOQuery<FOFormulaNegation>) query, factbase, vars, preHomomorphism);
            default -> throw new IllegalStateException("Unexpected value: " + query.getFormula());
        };
	}

}
