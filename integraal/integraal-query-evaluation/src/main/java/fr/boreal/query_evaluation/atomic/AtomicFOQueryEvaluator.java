package fr.boreal.query_evaluation.atomic;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.*;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.query_evaluation.FOQueryEvaluators;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;

/**
 * Evaluates a FOQuery with an atomic formula by directly giving the query to the
 * {@link FactBase} and converting the resulting {@link Atom} to
 * {@link Substitution}.
 */
public class AtomicFOQueryEvaluator implements FOQueryEvaluator<Atom> {

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public Iterator<Substitution> evaluate(FOQuery<? extends Atom> query, FactBase factbase, Collection<Variable> vars, Substitution preHomomorphism) {
		FOQuery<? extends Atom> handledQuery = query;
		Substitution handledPreHomomorphism = preHomomorphism;

		boolean needsHandling = !query.getVariableEqualities().getElements().isEmpty();
		if(needsHandling) {
			Optional<? extends Pair<? extends FOQuery<? extends Atom>, Substitution>> equalityHandledParameters = FOQueryEvaluators.handleEqualities(query, preHomomorphism);
			if(equalityHandledParameters.isEmpty()) {
				return Collections.emptyIterator();
			}
			handledQuery = equalityHandledParameters.get().getLeft();
			handledPreHomomorphism = equalityHandledParameters.get().getRight();
		}

		Atom query_atom = handledQuery.getFormula();

		Iterator<Atom> result;
		if (query_atom instanceof ComputedAtom) {
			// In case of a computed atom, we create its image with the pre homomorphism to evaluated it
			Atom substitute = handledPreHomomorphism.createImageOf(query_atom);
			result = List.of(substitute).iterator();
		} else {
			result = factbase.match(query_atom, handledPreHomomorphism);
		}

		// TODO : modify from here to handle functional terms
		Map<Variable, Integer> variables_position = this.computeVariablesPosition(query);
		Iterator<Substitution> answers = new AtomicFOQueryResultIterator(result, variables_position);
		if(needsHandling) {
			answers = new FOQueryEvaluators.RebuildAnswerIterator(answers, query);
		}

		return postprocessResult(answers, vars);
	}

	/**
	 * Translates an Iterator<Atom> into a Iterator<Substitution> with respect to
	 * the given variables position This is used to give a lazy evaluation behavior
	 * and avoid iterating over all the answers before returning a result.
	 */
	private static class AtomicFOQueryResultIterator implements Iterator<Substitution> {

		private final Iterator<Atom> atoms;
		private final Map<Variable, Integer> variables_position;

		private Substitution next_result = null;

		private final Set<Substitution> seen = new HashSet<>();

		public AtomicFOQueryResultIterator(Iterator<Atom> atoms, Map<Variable, Integer> variables_position) {
			this.atoms = atoms;
			this.variables_position = variables_position;
		}

		@Override
		public boolean hasNext() {
			if (this.next_result == null) {
				this.computeNextResult();
			}
			return this.next_result != null;
		}

		@Override
		public Substitution next() {
			if (this.next_result == null) {
				computeNextResult();
			}
			if (this.next_result == null) {
				throw new NoSuchElementException();
			}
			Substitution tmp = this.next_result;
			this.next_result = null;
			return tmp;
		}

		private void computeNextResult() {
			// for each atom matched from the factbase
			boolean found = false;
			while (!found && this.atoms.hasNext()) {
				Atom current_match = this.atoms.next();
				if (current_match.getPredicate().equals(Predicate.TOP)) {
					found = true;
					this.next_result = new SubstitutionImpl();
				} else if(!current_match.getPredicate().equals(Predicate.BOTTOM)) {
					Substitution current_substitution = new SubstitutionImpl();
					// fill the substitution with the terms found in the factbase
					for (Variable v : variables_position.keySet()) {
						int v_pos = variables_position.get(v);
						current_substitution.add(v, current_match.getTerm(v_pos));
					}

					if (!seen.contains(current_substitution)) {
						found = true;
						this.next_result = current_substitution;
						seen.add(current_substitution);
					}
				}
			}
		}

	}

	/////////////////////////////////////////////////
	// Private methods
	/////////////////////////////////////////////////

	/**
	 * Compute and return the position of the variables in the query Variables must
	 * be present in the atom of the query and in its answer variables
	 *
	 * @param query the atomic query
	 */
	private Map<Variable, Integer> computeVariablesPosition(FOQuery<? extends Atom> query) {
		Map<Variable, Integer> variables_position = new HashMap<>();

		Atom query_atom = query.getFormula();
		for (int i = 0; i < query_atom.getPredicate().arity(); i++) {
			Term term_i = query_atom.getTerm(i);
			if (term_i.isVariable() && query.getAnswerVariables().contains(term_i)) {
				variables_position.put((Variable) term_i, i);
			}
		}
		return variables_position;
	}

}
