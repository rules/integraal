package fr.boreal.query_evaluation.conjunction.backtrack;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Substitution;

/**
 * A scheduler gives the next part of the conjunction to evaluate given the current level on the backtrack.
 * This order can either be static or dynamic and can change according to executions.
 */
public interface Scheduler {

	/**
	 * @param level           the current level
	 * @param currentSolution the current solution
	 * @return the next element to evaluate at the given level
	 */
    FOFormula next(int level, Substitution currentSolution);

	/**
	 * @param level the current level
	 * @return true iff there is a next element to evaluate at the given level
	 */
    boolean hasNext(int level);

}
