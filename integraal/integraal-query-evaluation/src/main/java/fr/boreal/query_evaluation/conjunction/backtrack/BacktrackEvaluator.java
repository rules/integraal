package fr.boreal.query_evaluation.conjunction.backtrack;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;

import fr.boreal.model.formula.FOFormulas;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;

/**
 * Evaluates a query with a conjunctive formula by using homomorphism computing using
 * backtrack algorithm to aggregate the result of each sub-query
 * <br/>
 * To evaluate a given query, a sub-query is chosen and evaluated. The first
 * result is assumed correct and the next sub-query is evaluated. If the results
 * of the second sub-query are consistent with the first (assumed correct)
 * result, the next sub-query is evaluated and so on. When a sub-query result is
 * not consistent with the previous results, a step of backtrack is used to go
 * back on the assumed result and take the next answer. If there is no more
 * answer to go back to, it is also considered inconsistent.
 * <br/>
 * This Backtrack algorithm is inspired by the Baget Jean-François Thesis
 * (Chapter 5) See <a href="http://www.lirmm.fr/~baget/publications/thesis.pdf">...</a> for more
 * information
 */
public class BacktrackEvaluator implements FOQueryEvaluator<FOFormulaConjunction> {

	/**
	 * Query evaluator used to evaluate the sub-queries
	 */
	protected final FOQueryEvaluator<FOFormula> evaluator;

	/**
	 * Function for creating Scheduler instances based on the query and factbase
	 */
	protected BiFunction<FOQuery<? extends FOFormulaConjunction>, FactBase, Scheduler> schedulerBuilder;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////


	/**
	 * Creates a new evaluator using the given evaluator to evaluate sub-queries and a function to create Schedulers
	 *
	 * @param evaluator the query evaluator to use for recursive calls
	 * @param schedulerBuilder the function to create Scheduler instances based on the query and factbase
	 */
	public BacktrackEvaluator(FOQueryEvaluator<FOFormula> evaluator, BiFunction<FOQuery<? extends FOFormulaConjunction>, FactBase, Scheduler> schedulerBuilder) {
		this.evaluator = evaluator;
		this.schedulerBuilder = schedulerBuilder;
	}

	/**
	 * Creates a new evaluator using the given evaluator to evaluate sub-queries
	 * 
	 * @param evaluator the query evaluator to use for recursive calls
	 */
	public BacktrackEvaluator(FOQueryEvaluator<FOFormula> evaluator) {
		this(evaluator, NaiveDynamicScheduler::new);
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public Iterator<Substitution> evaluate(FOQuery<? extends FOFormulaConjunction> query, FactBase factbase, Collection<Variable> vars, Substitution preHomomorphism) {
		return new BacktrackIterator(evaluator, query, factbase, vars, preHomomorphism);

	}

	/////////////////////////////////////////////////
	// Private methods
	/////////////////////////////////////////////////

	/**
	 * The BacktrackIterator is used to redefine the iterator return as an answer
	 * to the query. It computes results only when needed. Calling hasNext()
	 * effectively computes the next answer.
	 */
	protected class BacktrackIterator implements Iterator<Substitution> {

		/////////////////////////////
		// Evaluator configuration //
		/////////////////////////////

		/**
		 * Query evaluator used to evaluate the sub-queries
		 */
		protected final FOQueryEvaluator<FOFormula> evaluator;

		/**
		 * Query to evaluate
		 */
		protected final FOQuery<? extends FOFormulaConjunction> query;

		/**
		 * The factbase on which to evaluate the query
		 */
		protected final FactBase factbase;

		/////////////////////////////
		// Backtrack configuration //
		/////////////////////////////

		/**
		 * Object used to handle the results to the query.
		 * It is used as a sort of cache during the query evaluation
		 */
		protected SolutionManager solutionManager;

		/**
		 * Object used to choose the next sub-query to evaluate
		 */
		protected final Scheduler scheduler;

		////////////////////////
		// Internal variables //
		////////////////////////

		// high level variable
		protected boolean hasCurrentSolution = false; // true iff the current solution is already computed

		// low level variables
		protected int level = 0;
		protected boolean backtrack = false; // true iff going back up

		protected final Set<Variable> joinVariables;

		protected final Collection<Variable> vars;
		protected final Substitution preHomomorphism;

		
		//////////////////
		// Constructors //
		//////////////////

		public BacktrackIterator(FOQueryEvaluator<FOFormula> evaluator, FOQuery<? extends FOFormulaConjunction> query,
								 FactBase factbase , Collection<Variable> vars, Substitution preHomomorphism) {
			this.evaluator = evaluator;
			this.query = query;
			this.factbase = factbase;

			this.joinVariables = FOFormulas.getJoinVariables(query.getFormula());

			this.vars = vars;
			this.preHomomorphism = preHomomorphism;
			
			this.solutionManager = new SimpleSolutionManager(preHomomorphism);
			this.scheduler = schedulerBuilder.apply(query, factbase);
			//this.scheduler = new SimpleScheduler(query);
		}

		////////////////////
		// Public methods //
		////////////////////

		@Override
        public boolean hasNext() {
			if (this.hasCurrentSolution) {
				return true;
			} else {
				this.computeNextSolution();
				return this.hasCurrentSolution;
			}
		}

		@Override
        public Substitution next() {
			if (this.hasNext()) {
				this.hasCurrentSolution = false;
				return this.solutionManager.getCurrentSolution().limitedTo(query.getAnswerVariables());
			} else {
				throw new NoSuchElementException("[backtrack] There are no more answers to the query");
			}
		}

		/////////////////////
		// Private methods //
		/////////////////////

		/**
		 * Compute the next solution using backtrack algorithm.
		 */
		private void computeNextSolution() {
						
			while (!this.hasCurrentSolution && this.level >= 0) {
				if (this.solutionFound()) {
					this.hasCurrentSolution = true;
					this.fail();
				} else {
					if (this.backtrack) {
						// going back up
						if (this.solutionManager.next(this.level)) {
							this.success();
						} else {
							// backtrack
							this.fail();
						}
					} else {
						// going down
						if (this.scheduler.hasNext(this.level)) {
							FOFormula element = this.scheduler.next(this.level, this.solutionManager.getCurrentSolution());
							// answer variables = (general U join) \ already affected
							Collection<Variable> subquery_vars = new HashSet<>();
							subquery_vars.addAll(this.query.getAnswerVariables());
							subquery_vars.addAll(this.joinVariables);
							subquery_vars.removeAll(this.solutionManager.getCurrentSolution().keys());
							// keep variables from (1) the formula and (2) the equalities with an element of the formula
							subquery_vars.removeIf(v -> !element.getVariables().contains(v) &&
                                    this.query.getVariableEqualities().getClass(v).stream().noneMatch(v2 -> element.getVariables().contains(v2)));
							subquery_vars.retainAll(element.getVariables());
							Optional<Substitution> previous_step_subst_opt = this.preHomomorphism.merged(this.solutionManager.getCurrentSolution());
							if (previous_step_subst_opt.isPresent()) {
								Substitution subquery_subst = previous_step_subst_opt.get();
								FOQuery<? extends FOFormula> subquery = FOQueryFactory.instance().createOrGetQuery(element, subquery_vars, this.query.getVariableEqualities());
								Iterator<Substitution> sub_results = this.evaluator.evaluate(subquery, this.factbase, this.vars, subquery_subst);
								if (sub_results.hasNext()) {
									this.solutionManager.add(this.level, sub_results);
									if (this.solutionManager.next(this.level)) {
										this.success();
									} else {
										this.fail();
									}
								} else {
									this.fail();
								}
							} else {
								this.fail();
							}
						} else {
							throw new NoSuchElementException("[backtrack] There is no more subquery in this query to evaluate");
						}
					}
				}
			}
		}

		/**
		 * Called when there is a success during the backtrack algorithm. A success is
		 * when a sub-query is evaluated and correct in regard to the previously
		 * computed answer.
		 */
		protected void success() {
			this.backtrack = false;
			this.computeNextLevel();
		}

		/**
		 * Called when there is a failure during the backtrack algorithm. A failure is
		 * when a sub-query is evaluated and is incorrect in regard to the previously
		 * computed answer. A failure is also considered if the answer is found to be
		 * able to go to the next result if we want all the result of the query
		 */
		protected void fail() {
			this.backtrack = true;
			this.computePreviousLevel();
		}

		/**
		 * Compute and update the previous level
		 */
		protected void computePreviousLevel() {
			this.level--;
		}

		/**
		 * Compute and update the next level
		 */
		protected void computeNextLevel() {
			this.level++;
		}

		/**
		 * @return true iff a solution is found
		 */
		protected boolean solutionFound() {
			return this.level == this.query.getFormula().getSubElements().size();
		}
	}

}
