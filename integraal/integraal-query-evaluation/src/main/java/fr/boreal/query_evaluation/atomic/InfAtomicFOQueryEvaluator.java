package fr.boreal.query_evaluation.atomic;

import com.google.common.collect.Iterators;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.model.ruleCompilation.api.RuleCompilation;
import fr.boreal.query_evaluation.FOQueryEvaluators;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;

/**
 * Evaluates an Atomic FOQuery using the {@literal ≲} condition given from a rule compilation
 * <p>
 * Given a factbase F = {B, C, D}
 * an atomic query <code>A</code>
 * The ≲-homomorphism(A, F) ≡
 * B ≲ A
 * C ≲ A
 * D ≲ A
 * <p>
 * We could also reduce the size of F by only taking into account
 * atoms with a compatible predicate.
 *
 * @author Florent Tornil
 *
 */
public class InfAtomicFOQueryEvaluator implements FOQueryEvaluator<Atom> {

	private final RuleCompilation compilation;

	/**
	 * Constructor using a compilation
	 *
	 * @param compilation the rule compilation to compute homomorphisms according to
	 */
	public InfAtomicFOQueryEvaluator(RuleCompilation compilation) {
		this.compilation = compilation;
	}


	@Override
	public Iterator<Substitution> evaluate(FOQuery<? extends Atom> query, FactBase factbase, Collection<Variable> vars, Substitution preHomomorphism) {
		FOQuery<? extends Atom> handledQuery = query;
		Substitution handledPreHomomorphism = preHomomorphism;

		boolean needsHandling = !query.getVariableEqualities().getElements().isEmpty();
		if(needsHandling) {
			Optional<? extends Pair<? extends FOQuery<? extends Atom>, Substitution>> equalityHandledParameters = FOQueryEvaluators.handleEqualities(query, preHomomorphism);
			if(equalityHandledParameters.isEmpty()) {
				return Collections.emptyIterator();
			}
			handledQuery = equalityHandledParameters.get().getLeft();
			handledPreHomomorphism = equalityHandledParameters.get().getRight();
		}

		Atom queryAtom = handledQuery.getFormula();

		Iterator<Substitution> allResults = null;
		for(Predicate p : this.compilation.getCompatiblePredicates(queryAtom.getPredicate())) {
			Iterator<Atom> possibleMatches = factbase.getAtomsByPredicate(p);
			Iterator<Substitution> result = new InfAtomicFOQueryResultIterator(this.compilation, possibleMatches, query, handledPreHomomorphism);
			if(allResults == null) {
				allResults = result;
			} else {
				allResults = Iterators.concat(allResults, result);
			}
		}

		if(needsHandling) {
			allResults = new FOQueryEvaluators.RebuildAnswerIterator(allResults, query);
		}
		return postprocessResult(allResults, vars);
	}

	private static class InfAtomicFOQueryResultIterator implements Iterator<Substitution> {

		private final RuleCompilation compilation;
		private final Iterator<Atom> it;
		private final FOQuery<? extends Atom> query;
		private final Substitution preHomomorphism;

		private Set<Substitution> buffer = null;
		private Substitution next = null;

		public InfAtomicFOQueryResultIterator(RuleCompilation compilation, Iterator<Atom> matches, FOQuery<? extends Atom> query, Substitution preHomomorphism) {
			this.compilation = compilation;
			this.it = matches;
			this.query = query;
			this.preHomomorphism = preHomomorphism;
		}

		@Override
		public boolean hasNext() {
			if(this.next == null && this.it.hasNext()) {
				this.next = this.computeNext();
			}
			return this.next != null;
		}

		@Override
		public Substitution next() {
			if(this.next == null) {
				Substitution tmp = this.computeNext();
				if(tmp == null) {
					throw new NoSuchElementException();
				} else {
					return tmp;
				}
			} else {
				Substitution tmp = this.next;
				this.next = null;
				return tmp;
			}
		}

		private Substitution computeNext() {
			while(this.next == null) {
				if(this.buffer != null && !this.buffer.isEmpty()) {
					Substitution result = this.buffer.iterator().next();
					this.buffer.remove(result);
					return result;
				} else if(this.it.hasNext()) {
					Atom nextMatch = this.it.next();
					this.buffer = this.compilation.getHomomorphisms(this.query.getFormula(), nextMatch, preHomomorphism);
				} else {
					return null;
				}
			}
			return null;
		}
	}


}
