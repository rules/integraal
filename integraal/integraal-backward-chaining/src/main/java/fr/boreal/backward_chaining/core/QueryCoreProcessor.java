package fr.boreal.backward_chaining.core;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.query.api.FOQuery;

/**
 * Compute the core of a {@link FOQuery} seen as a set of atoms
 * The computed core is a {@link FOQuery} without redundant atoms;
 * 'core' corresponds to the notion of graph theory (the set of atoms being viewed as a bipartite (predicate/term) graph).
 *  
 * @author Florent Tornil
 * @author Guillaume Pérution-Kihli
 *
 */
public interface QueryCoreProcessor {

	/**
	 * Compute the core of a query by removing all the redundant atoms in it, in considering the answer variables as constants,
	 * these variables must belong to the core.
	 * 
	 * @param query a FOQuery we want to compute the core
	 * @return a query representing the core of the given query
	 */
    FOQuery<? extends FOFormula> computeCore(FOQuery<? extends FOFormula> query);
	
}
