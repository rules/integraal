package fr.boreal.backward_chaining.source_target.dlx.data;

/**
 * Object of a column
 */
public class ColumnObject extends DataObject {

	private static final String UNNAMED = "unnamed";

	/**
	 * size
	 */
	public int size = 0;

	/**
	 * name
	 */
	public Object name = UNNAMED;

	@Override
	public String toString() {
		if (name == UNNAMED)
			return UNNAMED + "_" + super.toString();
		return name.toString();
	}

}
