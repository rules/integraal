package fr.boreal.backward_chaining.source_target.dlx;

/**
 * DLX result processor that simply discards results without doing anything.
 */
public class NullResultProcessor implements DLXResultProcessor {

	boolean keepGoing = false;

	/**
	 * Empty constructor
	 */
	public NullResultProcessor() {
		// Do nothing
	}

	/**
	 * Constructor
	 * @param keepGoing continue
	 */
	public NullResultProcessor(boolean keepGoing) {
		this.keepGoing = keepGoing;
	}

	public boolean processResult(DLXResult result) {
		return keepGoing;
	}

}
