package fr.boreal.backward_chaining.cover;

import java.util.Set;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.query.api.FOQuery;

/**
 * Does nothing
 */
public class NoCover implements CoverFunction {

	@Override
	public Set<FOQuery<? extends FOFormula>> cover(Set<FOQuery<? extends FOFormula>> queries) {
		return queries;
	}

}
