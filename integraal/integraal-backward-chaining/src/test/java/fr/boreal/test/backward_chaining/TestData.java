package fr.boreal.test.backward_chaining;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Constant;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;

public class TestData {

	// Predicates
	public static final Predicate p1 = SameObjectPredicateFactory.instance().createOrGetPredicate("p1", 1);
	public static final Predicate pp1 = SameObjectPredicateFactory.instance().createOrGetPredicate("pp1", 1);
	public static final Predicate ppp1 = SameObjectPredicateFactory.instance().createOrGetPredicate("ppp1", 1);
	public static final Predicate q1 = SameObjectPredicateFactory.instance().createOrGetPredicate("q1", 1);
	public static final Predicate r1 = SameObjectPredicateFactory.instance().createOrGetPredicate("r1", 1);
	public static final Predicate s1 = SameObjectPredicateFactory.instance().createOrGetPredicate("s1", 1);
	public static final Predicate t1 = SameObjectPredicateFactory.instance().createOrGetPredicate("t1", 1);
	public static final Predicate p2 = SameObjectPredicateFactory.instance().createOrGetPredicate("p2", 2);
	public static final Predicate q2 = SameObjectPredicateFactory.instance().createOrGetPredicate("q2", 2);
	public static final Predicate r2 = SameObjectPredicateFactory.instance().createOrGetPredicate("r2", 2);
	public static final Predicate s2 = SameObjectPredicateFactory.instance().createOrGetPredicate("s2", 2);
	public static final Predicate t2 = SameObjectPredicateFactory.instance().createOrGetPredicate("t2", 2);
	
	public static final Predicate r3 = SameObjectPredicateFactory.instance().createOrGetPredicate("r3", 3);

	// Terms
	public static final Variable u = SameObjectTermFactory.instance().createOrGetVariable("u");
	public static final Variable v = SameObjectTermFactory.instance().createOrGetVariable("v");
	public static final Variable w = SameObjectTermFactory.instance().createOrGetVariable("w");
	public static final Variable x = SameObjectTermFactory.instance().createOrGetVariable("x");
	public static final Variable x1 = SameObjectTermFactory.instance().createOrGetVariable("x1");
	public static final Variable x2 = SameObjectTermFactory.instance().createOrGetVariable("x2");
	public static final Variable y = SameObjectTermFactory.instance().createOrGetVariable("y");
	public static final Variable z = SameObjectTermFactory.instance().createOrGetVariable("z");
	public static final Variable fa = SameObjectTermFactory.instance().createOrGetVariable("fa");
	public static final Variable fb = SameObjectTermFactory.instance().createOrGetVariable("fb");
	public static final Variable fc = SameObjectTermFactory.instance().createOrGetVariable("fc");

	public static final Constant a = SameObjectTermFactory.instance().createOrGetConstant("a");
	public static final Constant b = SameObjectTermFactory.instance().createOrGetConstant("b");
	public static final Constant c = SameObjectTermFactory.instance().createOrGetConstant("c");
	public static final Constant d = SameObjectTermFactory.instance().createOrGetConstant("d");

	// Atoms
	public static final Atom pu = new AtomImpl(p1, u);
	public static final Atom pv = new AtomImpl(p1, v);
	public static final Atom pw = new AtomImpl(p1, w);
	public static final Atom px = new AtomImpl(p1, x);
	public static final Atom py = new AtomImpl(p1, y);
	public static final Atom pz = new AtomImpl(p1, z);

	public static final Atom qu = new AtomImpl(q1, u);
	public static final Atom qv = new AtomImpl(q1, v);
	public static final Atom qw = new AtomImpl(q1, w);
	public static final Atom qx = new AtomImpl(q1, x);
	public static final Atom qy = new AtomImpl(q1, y);
	public static final Atom qz = new AtomImpl(q1, z);

	public static final Atom ru = new AtomImpl(r1, u);
	public static final Atom rv = new AtomImpl(r1, v);
	public static final Atom rw = new AtomImpl(r1, w);
	public static final Atom rx = new AtomImpl(r1, x);
	public static final Atom ry = new AtomImpl(r1, y);
	public static final Atom rz = new AtomImpl(r1, z);

	public static final Atom su = new AtomImpl(s1, u);
	public static final Atom sv = new AtomImpl(s1, v);
	public static final Atom sw = new AtomImpl(s1, w);
	public static final Atom sx = new AtomImpl(s1, x);
	public static final Atom sy = new AtomImpl(s1, y);
	public static final Atom sz = new AtomImpl(s1, z);

	public static final Atom tu = new AtomImpl(t1, u);
	public static final Atom tv = new AtomImpl(t1, v);
	public static final Atom tw = new AtomImpl(t1, w);
	public static final Atom tx = new AtomImpl(t1, x);
	public static final Atom ty = new AtomImpl(t1, y);
	public static final Atom tz = new AtomImpl(t1, z);

	public static final Atom pxy = new AtomImpl(p2, x, y);
	public static final Atom pyz = new AtomImpl(p2, y, z);
	public static final Atom pxz = new AtomImpl(p2, x, z);
	public static final Atom pyx = new AtomImpl(p2, y, x);
	public static final Atom pxx = new AtomImpl(p2, x, x);
	public static final Atom qxy = new AtomImpl(q2, x, y);
	public static final Atom qyz = new AtomImpl(q2, y, z);
	public static final Atom qxz = new AtomImpl(q2, x, z);
	public static final Atom qyx = new AtomImpl(q2, y, x);
	public static final Atom qxx = new AtomImpl(q2, x, x);
	public static final Atom rxy = new AtomImpl(r2, x, y);
	public static final Atom ryz = new AtomImpl(r2, y, z);
	public static final Atom rxz = new AtomImpl(r2, x, z);
	public static final Atom ryx = new AtomImpl(r2, y, x);
	public static final Atom rxx = new AtomImpl(r2, x, x);
	public static final Atom sxy = new AtomImpl(s2, x, y);
	public static final Atom syz = new AtomImpl(s2, y, z);
	public static final Atom sxz = new AtomImpl(s2, x, z);
	public static final Atom syx = new AtomImpl(s2, y, x);
	public static final Atom sxx = new AtomImpl(s2, x, x);
	public static final Atom txy = new AtomImpl(t2, x, y);
	public static final Atom tyz = new AtomImpl(t2, y, z);
	public static final Atom txz = new AtomImpl(t2, x, z);
	public static final Atom tyx = new AtomImpl(t2, y, x);
	public static final Atom txx = new AtomImpl(t2, x, x);

	public static final Atom ppx = new AtomImpl(pp1, x);
	public static final Atom ppy = new AtomImpl(pp1, y);
	public static final Atom pppx = new AtomImpl(ppp1, x);
	public static final Atom pppy = new AtomImpl(ppp1, y);
	
	public static final Atom sxu = new AtomImpl(s2, x, u);
	public static final Atom suy = new AtomImpl(s2, u, y);
	public static final Atom qxv = new AtomImpl(q2, x, v);
	public static final Atom qvu = new AtomImpl(q2, v, u);
	public static final Atom quv = new AtomImpl(q2, u, v);
	public static final Atom qvy = new AtomImpl(q2, v, y);
	public static final Atom quw = new AtomImpl(q2, u, w);
	public static final Atom qwy = new AtomImpl(q2, w, y);
	
	public static final Atom pxb = new AtomImpl(p2, x, b);
	public static final Atom pax = new AtomImpl(p2, a, x);
	public static final Atom qxb = new AtomImpl(q2, x, b);
	public static final Atom qax = new AtomImpl(q2, a, x);
	
	public static final Atom pbx = new AtomImpl(p2, b, x);
	public static final Atom qa = new AtomImpl(q1, a);
	public static final Atom qb = new AtomImpl(q1, b);
	
	public static final Atom puz = new AtomImpl(p2, u, z);
	public static final Atom ruvw = new AtomImpl(r3, u, v, w);
	public static final Atom rxyx = new AtomImpl(r3, x, y, x);
	public static final Atom rwzu = new AtomImpl(r3, w, z, u);
	
	public static final Atom pvz = new AtomImpl(p2, v, z);
	public static final Atom pvu = new AtomImpl(p2, v, u);
	public static final Atom pbu = new AtomImpl(p2, b, u);
	public static final Atom pax2 = new AtomImpl(p2, a, x2);
	public static final Atom px2c = new AtomImpl(p2, x2, c);
	public static final Atom pbx2 = new AtomImpl(p2, b, x2);
	public static final Atom pax1 = new AtomImpl(p2, a, x1);
	public static final Atom px1c = new AtomImpl(p2, x1, c);
	public static final Atom pbx1 = new AtomImpl(p2, b, x1);
	public static final Atom pxc = new AtomImpl(p2, x, c);

	public static final Atom pfax = new AtomImpl(p2, fa, x);
	public static final Atom pfbu = new AtomImpl(p2, fb, u);
	public static final Atom pfax2 = new AtomImpl(p2, fa, x2);
	public static final Atom px2fc = new AtomImpl(p2, x2, fc);
	public static final Atom pfbx2 = new AtomImpl(p2, fb, x2);
	public static final Atom pfax1 = new AtomImpl(p2, fa, x1);
	public static final Atom px1fc = new AtomImpl(p2, x1, fc);
	public static final Atom pfbx1 = new AtomImpl(p2, fb, x1);
	public static final Atom pfbx = new AtomImpl(p2, fb, x);
	public static final Atom pxfc = new AtomImpl(p2, x, fc);
}
