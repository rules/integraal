package fr.boreal.io.api;

/**
 * @author Florent Tornil
 * <br/>
 * Encodes the input data into output data
 * @param <I> Type of the input parameter
 * @param <O> type of the output parameter
 *
 */
public interface DataEncoder<I, O> {

	/**
	 * Encodes the given input and return the encoding
	 * <br/>
	 * The returned encoding can be the direct-encoded values or a path to the encoded files
	 * The encoding dictionary may also be provided as output.
	 *
	 * @param input the input data to encode
	 * @return the encoded input
	 */
	O encode(I input);

}
