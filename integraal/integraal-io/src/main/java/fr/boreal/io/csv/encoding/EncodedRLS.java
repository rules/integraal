package fr.boreal.io.csv.encoding;

/**
 * @author Florent Tornil
 * <p>
 * The encoded RLS is a triple with :
 * - The RLS file linking to the encoded CSVs
 * - The dictionary file
 * - The repare file (if using an optimistic encoding)
 */
public record EncodedRLS(String rlsFile, String dictionaryFile, String repareFile) {

	@Override
	public String toString() {
		return "RLS : " + this.rlsFile + "\n" +
				"Dictionary : " + this.dictionaryFile + "\n" +
				"Repare : " + this.repareFile + "\n";
	}

}
