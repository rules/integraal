/*
 * Copyright (C) Inria Sophia Antipolis - Méditerranée / LIRMM
 * (Université de Montpellier & CNRS) (2014 - 2017)
 *
 * Contributors :
 *
 * Clément SIPIETER <clement.sipieter@inria.fr>
 * Mélanie KÖNIG
 * Swan ROCHER
 * Jean-François BAGET
 * Michel LECLÈRE
 * Marie-Laure MUGNIER <mugnier@lirmm.fr>
 *
 *
 * This file is part of Graal <https://graphik-team.github.io/graal/>.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.boreal.io.dlgp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.boreal.views.builder.ViewBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.boreal.io.api.ParseException;
import fr.boreal.io.api.Parser;
import fr.lirmm.boreal.util.stream.AbstractCloseableIterator;
import fr.lirmm.boreal.util.stream.ArrayBlockingStream;

/**
 * @author Florent Tornil
 *
 *         Main access point of the DLGP parser
 */
public final class DlgpParser extends AbstractCloseableIterator<Object> implements Parser<Object> {

	private static final Logger LOG = LoggerFactory.getLogger(DlgpParser.class);

	private final ArrayBlockingStream<Object> buffer = new ArrayBlockingStream<>(512);
	private static final ExecutorService executor = Executors.newVirtualThreadPerTaskExecutor();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTOR
	// /////////////////////////////////////////////////////////////////////////

	private Reader reader;
	private boolean readerIsSTDIN = false;

	/**
	 * Constructor for parsing from the given reader.
	 * 
	 * @param reader the java reader used to retrieve data
	 * 
	 */
	public DlgpParser(Reader reader) {
		this.reader = reader;
		executor.submit(new Producer(reader, buffer));
	}

	/**
	 * Constructor for parsing from the standard input.
	 * 
	 */
	public DlgpParser() {
		this(new InputStreamReader(System.in));
		this.readerIsSTDIN = true;
	}

	/**
	 * Constructor for parsing from the given file.
	 * 
	 * @param file java file object to read from
	 * @throws FileNotFoundException if the given file does not exist
	 */
	public DlgpParser(File file) throws FileNotFoundException {
		this(new FileReader(file));
	}

	/**
	 * Constructor for parsing the content of the string s as DLGP content.
	 * 
	 * @param s                a DLGP input as String
	 */
	public DlgpParser(String s) {
		this(new StringReader(s));
	}

	/**
	 * Constructor for parsing the given InputStream.
	 * 
	 * @param in               java InputStream to read from
	 */
	public DlgpParser(InputStream in) {
		this(new InputStreamReader(in));
	}

	@Override
	protected void finalize() throws Throwable {
		this.close();
		super.finalize();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS
	// /////////////////////////////////////////////////////////////////////////

	@Override
	public boolean hasNext() {
		return buffer.hasNext();
	}

	@Override
	public Object next() throws ParseException {
		Object val = buffer.next();
		if (val instanceof Throwable) {
			if (val instanceof ParseException) {
				throw (ParseException) val;
			}
			throw new ParseException("An error occurred while parsing.", (Throwable) val);
		}
		return val;
	}

	/**
	 * Closes the stream and releases any system resources associated with it.
	 * Closing a previously closed parser has no effect.
	 * 
	 */
	@Override
	public void close() {
		if (this.reader != null && !this.readerIsSTDIN) {
			try {
				this.reader.close();
			} catch (IOException e) {
				LOG.error("Error during closing reader", e);
                throw new RuntimeException(
						String.format(
							"[%s::close] Error during the parsing of the dlgp: impossible to close the reader.",
							this.getClass()),
                        e);
			}
			this.reader = null;
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// STATIC METHODS
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Parses the file located at the given path
	 * <br/>
	 * Collects all the content of the file in a single object and return only after reading the
	 * whole file.
	 * 
	 * @param filepath path of the file to parse
	 * @return an object wrapping the dlgp
	 */
	public static ParserResult parseFile(String filepath) {
		try (Parser<?> parser = new DlgpParser(new File(filepath))) {
			return parser.parse();
		} catch (ParseException | FileNotFoundException | ViewBuilder.ViewBuilderException e) {
            LOG.error("Error parsing file at path: {}", filepath, e);
			throw new RuntimeException(
					String.format(
							"[DlgpParser::close] Error parsing file at path: %s",
							filepath),
					e);
		}
    }

	/**
	 * Parses the files located at a given collection of paths
	 * <br/>
	 * Collects all the file in a single object and return only after reading the
	 * whole file.
	 * 
	 * @param filepaths path of the file to parse
	 * @return an object wrapping the dlgp
	 */
	public static ParserResult parseFiles(Collection<String> filepaths) {
		ParserResult result = new ParserResult( List.of(), List.of(), List.of(), List.of());
		for (String singlepath : filepaths) {
			result = result.union(parseFile(singlepath));
		}
		return result;

	}

	/**
	 * Parses the DLGP passed as string.
	 * 
	 * @param dlgpString string corresponding to the DLGP to parse
	 * @return an object wrapping the dlgp
	 */
	public static ParserResult parseDLGPString(String dlgpString) {
		try (Parser<?> parser = new DlgpParser(dlgpString)) {
			return parser.parse();
		} catch (ParseException | ViewBuilder.ViewBuilderException e) {
			LOG.error("Error parsing DLGP string.", e);
			throw new RuntimeException("[DlgpParser::parseDLGPString] Error parsing DLGP string.", e);
		}
    }

}
