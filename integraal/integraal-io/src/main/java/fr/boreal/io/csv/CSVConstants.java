package fr.boreal.io.csv;

public class CSVConstants {

	/**
	 * Default CSV separator
	 */
	public static final char CSV_SEPARATOR = ',';

	/**
	 * Default CSV prefix
	 */
	public static final String CSV_PREFIX = "";

	/**
	 * If a csv is read we assume the first line defines the predicate arity
	/*
	 * If a csv is read, we assume the first line defines the predicate arity
	 */
	public static final int CSV_HEADER_SIZE = 0;

	/**
	 * If a csv is read via an RLS the arity is defined by the RLS
	 */
	public static final int CSV_HEADER_SIZE_WHEN_RLS = 0;
}
