package fr.boreal.io.rdf;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.rio.helpers.AbstractRDFHandler;

import fr.boreal.model.logicalElements.api.Atom;
import fr.lirmm.boreal.util.Prefix;
import fr.lirmm.boreal.util.stream.ArrayBlockingStream;

class RDFListener extends AbstractRDFHandler {

	private final ArrayBlockingStream<Object> set;
	
	private final RDFTranslator translator;
	
	public RDFListener(ArrayBlockingStream<Object> set, RDFTranslator translator) {
		this.set = set;
		this.translator = translator;
	}
	
	@Override
	public void handleStatement(Statement st) {
		Atom a = translator.statementToAtom(st);
		this.set.write(a);
	}

	@Override
	public void handleNamespace(String prefix, String uri) {
		this.set.write(new Prefix(prefix, uri));
 	}

}
