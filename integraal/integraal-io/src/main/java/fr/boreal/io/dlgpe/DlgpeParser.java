package fr.boreal.io.dlgpe;

import java.io.File;
import java.io.IOException;

import fr.boreal.io.api.ParseException;
import fr.boreal.io.api.Parser;
import fr.boreal.io.dlgp.DlgpParser;

/**
 * Translate a given file in Dlgpe format to a Dlgp file then parses the Dlgp file.
 * 
 * @author Florent Tornil
 *
 */
public class DlgpeParser implements Parser<Object> {
	
	private final DlgpParser parser;
	
	/**
	 * Constructor for parsing from the given file.
	 * 
	 * @param file java file object to read from
	 */
	public DlgpeParser(File file) {
		String outputFile = file.getName() + ".dlgp";
		
		ProcessBuilder build = new ProcessBuilder();
		
		//
		// TODO : Complete command line when dlgpetools is done for translation
		//
		build.command("dlgpetools", "translate", file.getAbsolutePath(), "-o", outputFile);
		
		Process proc;
		try {
			proc = build.start();
			this.parser = new DlgpParser(new File(outputFile));
		} catch (IOException e) {
			throw new RuntimeException("The translation of dlgpe to dlgp ended with an error\n" + e);
		}
		proc.destroy();
	}

	@Override
	public void close() {
		this.parser.close();
	}

	@Override
	public boolean hasNext() throws ParseException {
		return this.parser.hasNext();
	}

	@Override
	public Object next() throws ParseException {
		return this.parser.next();
	}

}
