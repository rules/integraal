package fr.boreal.io.csv;

/**
 * @author Florent Tornil
 * <p>
 * This class represents the result of the parsing of a line in a RLS file
 */
public record RLSCSVResult(String predicateName, int predicateArity, String csvFilepath) {

}
