package fr.boreal.io.dlgp;

/**
 * @author Clément Sipieter (INRIA) {@literal <clement@6pi.fr>}
 *
 */
final class DlgpGrammarUtils {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS
	// /////////////////////////////////////////////////////////////////////////

	private DlgpGrammarUtils() {

	}

	// /////////////////////////////////////////////////////////////////////////
	// PUBLIC METHODS
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Check if the string fulfill u-ident condition
	 * 
	 * @param s s
	 * @return true if the specified string fulfill u-ident condition
	 */
	public static boolean checkUIdent(String s) {
		return !s.isEmpty() && isUpperAlpha(s.charAt(0)) && containsOnlySimpleChar(s);
	}
	
	/**
	 * Check if the string fulfill l-ident condition
	 * 
	 * @param s s
	 * @return true if the specified string fulfill l-ident condition
	 */
	public static boolean checkLIdent(String s) {
		return !s.isEmpty() && isLowerAlpha(s.charAt(0)) && containsOnlySimpleChar(s);
	}
	
	public static final int FIRST_CHAR = 0;
	public static final int CHARS_BASE = 1;
	public static final int PERCENT_1 = 2;
	public static final int PERCENT_2 = 3;
	public static final int LOCAL_ESC = 4;
	/**
     * Check if the string fulfill LocalName condition (see
     *         turtle grammar <a href="https://www.w3.org/TR/turtle/">...</a>)
     * @param localname l
     * @return true iff the specified string fulfill localName condition
     */
	public static boolean checkLocalName(String localname) {
		int mode = FIRST_CHAR;
		int last_mode = mode;
		char last = '\0';
		for (char c : localname.toCharArray()) {
			last = c;
			last_mode = mode;
			switch (mode) {
			case PERCENT_1:
				if (!isHex(c)) {
					return false;
				}
				mode = PERCENT_2;
				break;
			case PERCENT_2:
				if (!isHex(c)) {
					return false;
				}
				mode = CHARS_BASE;
				break;
			case LOCAL_ESC:
				if (!isLocalEscape(c)) {
					return false;
				}
				mode = CHARS_BASE;
				break;
			case FIRST_CHAR:
				if (c == '%') {
					mode = PERCENT_1;
				} else if (c == '\\') {
					mode = LOCAL_ESC;
				} else if (c == ':' || isCharsU(c) || isDigit(c)) {
					mode = CHARS_BASE;
				} else {
					return false;
				}
				break;
			case CHARS_BASE:
				if (c == '%') {
					mode = PERCENT_1;
				} else if (c == '\\') {
					mode = LOCAL_ESC;
				} else if (c != ':' && c != '.' && !isChars(c)) {
					return false;
				}
				break;
			default:
				assert false;
				break;
			}
		}

		return (last_mode == FIRST_CHAR && (last == ':' || isCharsU(last) || isDigit(last)))
				|| (last_mode == CHARS_BASE && (last == ':' || isChars(last)))
				|| (last_mode == PERCENT_2 && isHex(last)) || (last_mode == LOCAL_ESC && isLocalEscape(last));

	}
	
	/**
	 * Check if the string contains only simple char (a-z A-Z 0-9 _)
	 * 
	 * @param s
	 * @return true if the string contains only simple characters, false otherwise.
	 */
	private static boolean containsOnlySimpleChar(String s) {
		for(char c : s.toCharArray()) {
			if(!isSimpleChar(c)) {
				return false;
			}
		}
		return true;
	}

	public static boolean isCharsBase(char c) {
		return !((c < 'A' || c > 'Z') && (c < 'a' || c > 'z') && (c < 'À' || c > 'Ö')
				&& (c < 'Ø' || c > 'ö') && (c < 'ø' || c > '˿') && (c < 'Ͱ' || c > 'ͽ')
				&& (c < 'Ϳ' || c > '\u1FFF') && (c < '\u200C' || c > '\u200D') && (c < '⁰' || c > '\u218F')
				&& (c < 'Ⰰ' || c > '\u2FEF') && (c < '、' || c > '\uD7FF') && (c < '豈' || c > '\uFDCF')
				&& (c < 'ﷰ' || c > 'ﷰ'));
	}

	public static boolean isCharsU(char c) {
		return c == '_' || isCharsBase(c);
	}

	public static boolean isChars(char c) {
		return c == '-' || isCharsU(c) || isDigit(c) || c == '·' || (c >= '̀' && c <= 'ͯ')
				|| (c >= '‿' && c <= '⁀');
	}

	public static boolean isHex(char c) {
		return (c >= '0' && c <= '9') || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f');
	}

	public static boolean isLocalEscape(char c) {
		return c == '_' || c == '~' || c == '.' || c == '-' || c == '!' || c == '$' || c == '&' || c == '\'' || c == '('
				|| c == ')' || c == '*' || c == '+' || c == ',' || c == ';' || c == '=' || c == '/' || c == '?'
				|| c == '#' || c == '@' || c == '%';
	}
	
	public static boolean isSimpleChar(char c) {
		return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')
		|| (c >= '0' && c <= '9') || (c == '_');
	}
	
	public static boolean isDigit(char c) {
		return c >= '0' && c <= '9';
	}
	
	public static boolean isUpperAlpha(char c) {
		return (c >= 'A' && c <= 'Z');
	}
	
	public static boolean isLowerAlpha(char c) {
		return (c >= 'a' && c <= 'z');
	}

	// /////////////////////////////////////////////////////////////////////////
	// OBJECT OVERRIDE METHODS
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// PRIVATE METHODS
	// /////////////////////////////////////////////////////////////////////////

}
