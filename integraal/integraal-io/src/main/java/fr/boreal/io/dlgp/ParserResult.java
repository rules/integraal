package fr.boreal.io.dlgp;

import java.util.Collection;
import java.util.LinkedHashSet;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.views.datasource.AbstractViewWrapper;

/**
 * Result of the DLGP parsing
 * @param atoms a
 * @param rules r
 * @param queries q
 * @param views v
 */
public record ParserResult(
		Collection<Atom> atoms,
		Collection<FORule> rules,
		Collection<Query> queries,
		Collection<AbstractViewWrapper<String, ?>> views) {

	/**
	 * Creates a union of two DlgpParserResult records.
	 *
	 * @param other The other DlgpParserResult to union with this one.
	 * @return A new DlgpParserResult representing the union of this and the other
	 *         record.
	 */
	public ParserResult union(ParserResult other) {
		// Create new collections for the union of atoms, rules, and queries
		Collection<Atom> unionAtoms = new LinkedHashSet<>(this.atoms);
		Collection<FORule> unionRules = new LinkedHashSet<>(this.rules);
		Collection<Query> unionQueries = new LinkedHashSet<>(this.queries);
		Collection<AbstractViewWrapper<String, ?>> unionViews = new LinkedHashSet<>(this.views);

		// Add all elements from the other DlgpParserResult to the collections
		unionAtoms.addAll(other.atoms);
		unionRules.addAll(other.rules);
		unionQueries.addAll(other.queries);
		unionViews.addAll(other.views);

		// Return a new DlgpParserResult with the union of the collections
		return new ParserResult(unionAtoms, unionRules, unionQueries, unionViews);
	}
}