/**
 * Module for input and output of data elements of InteGraal
 *
 * @author Florent Tornil
 *
 */
module fr.boreal.io {

	requires transitive fr.boreal.model;
	requires transitive fr.boreal.storage;
	requires transitive fr.boreal.views;

	requires transitive rdf4j.rio.api;
	requires org.apache.commons.collections4;

	requires fr.lirmm.boreal.util;
	requires fr.lirmm.graphik.dlgp3;


	requires radicchio;
	requires org.slf4j;
    requires rdf4j.model.api;
    requires commons.csv;

    exports fr.boreal.io.api;
	exports fr.boreal.io.dlgp;
	exports fr.boreal.io.dlgpe;
	exports fr.boreal.io.rdf;
	exports fr.boreal.io.csv;
}