package fr.boreal.test.io.dlgp;

import fr.boreal.io.api.ParseException;
import fr.boreal.io.api.Parser;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.io.dlgp.ParserResult;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.views.builder.ViewBuilder;
import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Tests that there is no conflicts between java objects corresponding to variables taking the same name
 *
 */
@RunWith(Parameterized.class)
class VariableNamingTest {

	public static final String dlgp1 = "p1(X). q2(X, Y).";
	public static final String dlgp2 = "p1(X), q2(X, Y).";

	static Stream<Arguments> data() {
		return Stream.of(
				Arguments.of(dlgp1, 3),
				Arguments.of(dlgp2, 2)
		);
	}

	@DisplayName("Tests the management of variables via factories during parsing")
	@ParameterizedTest(name = "{index}: DLGP {0} should give {1} distinct variables")
	@MethodSource("data")
	public void variableNamingTest(String dlgp, int numberOfDistinctVariables) throws ParseException, ViewBuilder.ViewBuilderException {

		Parser<?> parser = new DlgpParser(dlgp);
		ParserResult result = parser.parse();
		parser.close();

		Set<Variable> distinctVariables = result.atoms().stream().map(Atom::getVariables).flatMap(Collection::stream).collect(Collectors.toSet());
		Assert.assertEquals(numberOfDistinctVariables, distinctVariables.size());
	}

}
