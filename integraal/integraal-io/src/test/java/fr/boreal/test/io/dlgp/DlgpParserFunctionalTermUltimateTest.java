package fr.boreal.test.io.dlgp;

import fr.boreal.io.api.ParseException;
import fr.boreal.io.api.Parser;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.io.dlgp.ParserResult;
import fr.boreal.views.builder.ViewBuilder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.stream.Stream;

@RunWith(Parameterized.class)
class DlgpParserFunctionalTermUltimateTest {

    /* REQUISITES
    (inspired by https://rules.gitlabpages.inria.fr/integraal-website/features/dlgp)
    The directory "myproject" whose path is <pathtomyproject> contains the following path structure.
     /my_project
          ├── myfunctions.json
        ├── README.md
          └── util
              └── Utils.java
     I suppose that the json configuration file named myfunctions.json contains:
     {
             "default": {
                  "types": {},
                  "predicates": {},
                   "functions": {
                      "path": ".",
                      "package": "util",
                      "class": "Utils"
                 }
             }
         }
    I suppose that the java file Util.java contains:

    package util;

    public static Integer increment(Integer X) {
        return X+1;
    }
*/
    static String pathtomyproject = "./src/test/resources/";

    /* EXAMPLE 1 (NEG)
     */
    private static String dlgp1 = "p1(f(1)).";
    private static boolean expected_dlgp1 = false;
    // Here it is a syntax error, that should be found by the parser,
    // since a function name (here f) should always be expressed by a prefixed name

    /* EXAMPLE 2  (NEG)
     */
    private static String dlgp2 = "p1(ex:f(1)).";
    private static boolean expected_dlgp2 = false;
    // Here it is a semantic error, that should be found by analyzing the result of the parser,
    // since the prefix ex: is not defined.

    /* EXAMPLE 3  (NEG)
     */
    private static String dlgp3 = "@prefix ex:<http://www.lirmm.fr/~baget/examples/> p1(ex:f(1)).";
    private static boolean expected_dlgp3 = false;
    // Here it is a semantic error, that should be found by analyzing the result of the parser,
    // since the prefix ex: is defined by a @prefix instruction, however the prefix of a function name
    // must be defined by a @computed instruction.

    /* EXAMPLE 4  (NEG)
     */
    private static String dlgp4 = "@computed ex:<" + pathtomyproject + "nonexistingfile.json> p1(ex:f(1)).";
    private static boolean expected_dlgp4 = false;
    // Here it is a semantic error, that should be found by analyzing the result of the parser,
    // since the second argument of a @computed instruction must be a path to an existing file.

    /* EXAMPLE 5  (NEG)
     */
    private static String dlgp5 = "@computed ex:<" + pathtomyproject + "README.md> p1(ex:f(1)).";
    private static boolean expected_dlgp5 = false;
    // Here it is a semantic error, that should be found by analyzing the result of the parser,
    // since the second argument of a @computed instruction must be a path to an existing json file
    // (and it should also respect the syntax of such a configuration file: we should be able to both load
    // the json and match it against the specifications of the dlgp configuration file).

    /* EXAMPLE 6 (NEG)
     */
    private static String dlgp6 = "@computed ex:<" + pathtomyproject + "myfunctions.json> p1(ex:f(1)).";
    private static boolean expected_dlgp6 = false;
    // Here it is a semantic error, that should be found by analyzing the result of the parser,
    // since no function named f is found in the class Util.

    /* EXAMPLE 7 (POS)
     */
    private static String dlgp7 = "@computed ex:<" + pathtomyproject + "myfunctions.json> p1(ex:increment(1)).";
    private static boolean expected_dlgp7 = true;
    // This time it works, the function increment is found in the class Util. Note that the evaluation of the fact
    // is p1(2), which is the atom that should be added to the default fact base.

    /* EXAMPLE 8 (NEG)
     */
    private static String dlgp8 = "@computed ex:<" + pathtomyproject + "myfunctions.json> p1(ex:increment(a)).";
    private static boolean expected_dlgp8 = false;
    // Here it is a syntax error, that should be found by the parser,
    // since the arguments of a function are never logical constants.

    /* EXAMPLE 9 (DON'T KNOW)
     */
    private static String dlgp9 = "@computed ex:<" + pathtomyproject + "myfunctions.json> p1(ex:increment(\"foo\")).";
    // Here I don't know if there should be a semantic error, that should be found by analyzing the result of the parser,
    // because the type of the argument (a String) does not match with the expected type of the argument for this function
    // (an integer); or if there should be an error at run time when we try to evaluate the function.

    /* EXAMPLE 10 (NEG)
     */
    private static String dlgp10 = "@computed ex:<" + pathtomyproject + "myfunctions.json> ?() :- p1(ex:increment(X)).";
    private static boolean expected_dlgp10 = false;
    // Here it is a semantic error, that should be found by analyzing the result of the parser,
    // since there is a variable in a function of the body of the rule that does not appear (positively) outside
    // of a function elsewhere in the rule.

    /* EXAMPLE 11 (NEG)
     */
    private static String dlgp11 = "@computed ex:<" + pathtomyproject + "myfunctions.json> ?() :- p1(ex:increment(X)), not q(X).";
    private static boolean expected_dlgp11 = false;
    // Here it is a semantic error, that should be found by analyzing the result of the parser,
    // In insist on "positively" in the explanation of example 10.

    /* EXAMPLE 12 (POS)
     */
    private static String dlgp12 = "@computed ex:<" + pathtomyproject + "myfunctions.json> ?() :- q(X), p1(ex:increment(X)).";
    private static boolean expected_dlgp12 = true;
    // This time it works.

    /* EXAMPLE 13 (POS)
     */
    private static String dlgp13 = "@computed ex:<" + pathtomyproject + "myfunctions.json> ?() :- q(X), p1(ex:increment(ex:increment(X))).";
    private static boolean expected_dlgp13 = true;
    // Just to check that the parser inductively builds functional terms.

    /* EXAMPLE 14 (NEG)
     */
    private static String dlgp14 = "@computed ex:<" + pathtomyproject + "myfunctions.json> ?() :- q(ex:increment(X)), p1(ex:increment(ex:increment(X))).";
    private static boolean expected_dlgp14 = false;
    // And that it does not do something stupid with inductively defined functional terms.

    /* EXAMPLE 15 (NEG)
     */
    private static String dlgp15 = "@computed ex:<" + pathtomyproject + "myfunctions.json> p1(ex:increment(X)) :- .";
    private static boolean expected_dlgp15 = false;
    // Here it is a semantic error, that should be found by analyzing the result of the parser,
    // there is a variable appearing in a function of the head that is not a standard variable (i.e. not appearing in a function)
    // of the body.

    /* EXAMPLE 16 (POS)
     */
    private static String dlgp16 = "@computed ex:<" + pathtomyproject + "myfunctions.json> p1(ex:increment(X)) :- q(X).";
    private static boolean expected_dlgp16 = true;
    // Problem solved.

    /* EXAMPLE 17  (POS)
     */
    private static String dlgp17 = "@prefix ex:<http://www.lirmm.fr/~baget/examples/> p1(1).";
    private static boolean expected_dlgp17 = true;
    // Here it is a semantic error, that should be found by analyzing the result of the parser,
    // since the prefix ex: is defined by a @prefix instruction, however the prefix of a function name
    // must be defined by a @computed instruction.


    @Parameterized.Parameters
    static Stream<Arguments> data() {
        return Stream.of(
                Arguments.of(dlgp1, expected_dlgp1),
                Arguments.of(dlgp2, expected_dlgp2),
                Arguments.of(dlgp3, expected_dlgp3),
                Arguments.of(dlgp4, expected_dlgp4),
                Arguments.of(dlgp5, expected_dlgp5),
                Arguments.of(dlgp6, expected_dlgp6),
                Arguments.of(dlgp7, expected_dlgp7),
                Arguments.of(dlgp8, expected_dlgp8),
                Arguments.of(dlgp9, expected_dlgp10),
                Arguments.of(dlgp10, expected_dlgp11),
                Arguments.of(dlgp11, expected_dlgp11),
                Arguments.of(dlgp17, expected_dlgp17)
        );
    }
    @Parameterized.Parameters
    static Stream<Arguments> data2() {
        return Stream.of(
                Arguments.of(dlgp7, expected_dlgp7)
        );
    }

    @DisplayName("Test functional term creation")
    @ParameterizedTest(name = "{index} ...)")
    @MethodSource("data2")
    public void test1(String dlgp, boolean expected)
            throws ViewBuilder.ViewBuilderException, ParseException {
        System.out.println(dlgp8);
        Parser<?> parser = new DlgpParser(dlgp);
        ParserResult result = null;

        if (expected == false) {
            try {
                result = parser.parse();
                // if no error then fail
                //Assertions.fail();
            } catch (Exception e) {
                System.out.println(e.getMessage());
                return;
            }
        } else {
            result = parser.parse();
            System.out.println(result);
        }
    }

}
