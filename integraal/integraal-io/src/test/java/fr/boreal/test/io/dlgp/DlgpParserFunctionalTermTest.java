package fr.boreal.test.io.dlgp;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import fr.boreal.io.api.ParseException;
import fr.boreal.io.api.Parser;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.io.dlgp.ParserResult;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.EvaluableFunction;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.views.builder.ViewBuilder;

@RunWith(Parameterized.class)
class DlgpParserFunctionalTermTest {

	/*
	 * negative examples
	 */
	private static String dlgp1 = "@prefix ex:<pref:> ex:p1(ex:p2(a, X, <b>, \"c\", 1)).";
	private static String expected_dlgp1 = "pref:p1(pref:p2(a, X, <b>, \"c\", 1))";

	private static String dlgp2 = "@prefix ex:<pref:>  ex:p1(ex:p2(X)).";
	private static String expected_dlgp2 = "pref:p1(pref:p2(X))";

	private static String auxatom = "freshpredicate(2).";

	@Parameterized.Parameters
	static Stream<Arguments> data1() {
		return Stream.of(Arguments.of(dlgp1, expected_dlgp2));
	}

	@Parameterized.Parameters
	static Stream<Arguments> data2() {
		return Stream.of(Arguments.of(dlgp2, expected_dlgp1));
	}

	@DisplayName("Test functional term creation")
	@ParameterizedTest(name = "{index} ...)")
	@MethodSource("data1")
	public void testFunctionalTermCreationNonEvaluable(String dlgp, String expected)
			throws ViewBuilder.ViewBuilderException, ParseException {
		Parser<?> parser = new DlgpParser(dlgp);
		ParserResult result = parser.parse();
		//Assertions.assertEquals(result.atoms().stream().findFirst().get().toString(), expected_dlgp1);
		//Assertions.assertEquals(result.atoms().stream().findAny().get().getTerm(0).getClass(),StandardNonEvaluableFunctionalTermImpl.class);
		parser.close();
	}

	@DisplayName("Test functional term creation")
	@ParameterizedTest(name = "{index} ...)")
	@MethodSource("data2")
	public void testFunctionalTermCreationEvaluable(String dlgp, String expected)
			throws ViewBuilder.ViewBuilderException, ParseException {
		Parser<?> parser = new DlgpParser(dlgp);
		ParserResult result = parser.parse();
		Atom atom = result.atoms().stream().findAny().get();
		//Assertions.assertEquals(atom.toString(), expected_dlgp2);

		//Assertions.assertEquals(atom.getTerm(0).getClass(), EvaluableFunctionalTermImpl.class);
		parser.close();
		if (atom.getTerm(0) instanceof EvaluableFunction term) {
            // now trying to instanciate the atom
			Parser<?> parseraux = new DlgpParser(auxatom);
			var constant = parseraux.parse().atoms().stream().findFirst().get().getTerm(0);
			Map<Variable, Term> ass = new HashMap<>();
			ass.put((Variable) term.getVariables().stream().findAny().get(), constant);
			var s = new SubstitutionImpl(ass);
			var evaluated = term.eval(s);

			// Assertions.assertEquals(evaluated.getClass(),
			// NonEvaluableFunctionalTermImpl.class);

			//Assertions.assertEquals(evaluated.toString(), term.eval(s).toString().replace("X", constant.toString()));
			parseraux.close();
		}
	}

}
