package fr.boreal.test.io.csv;

import java.io.File;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import fr.boreal.io.csv.RLSCSVsParser;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.storage.builder.StorageBuilder;

/**
 * Unit test for CSV encoding and import into an RDBMS
 * <p>
 * This test class does not match the pattern for maven unit test This would
 * require the git CI/CD to use a database which is not available for now So
 * this test can be run locally but not during the CI/CD phase
 * <p>
 * Also, this test is validated iff no exception is thrown, the state of the
 * database and query answering is not checked.
 * 
 * @author Florent Tornil
 */
@RunWith(Parameterized.class)
class CSVRDBMSEncoding {

	/*
	 * Note that the connection string must be updated by setting
	 * 
	 * - database name (in this example
	 */
	private static final String port = "5432";
	private static final String databasename = "p2";
	private static final String username = "postgres";
	private static final String userpassword = "admin";

	private static final String postgresParams = String.format("jdbc:postgresql://localhost:%s/%s?user=%s&password=%s", port,
			databasename, username, userpassword);

	@Parameters
	static Stream<Arguments> data() {
		return Stream.of(Arguments.of("src/test/resources/rlsTestData.rls"));
	}

	/**
	 * Tests that the encoding is correct and the RDBMS is correctly setup
	 * 
	 * @param rlsFile configuration file
	 */
	@DisplayName("Test CSV encoding and import into an RDBMS")
	@ParameterizedTest(name = "{index}: CSV should be correctly encoded and imported into the RDBMS)")
	@MethodSource("data")
	public void csvHandling(String rlsFile) {
		try (RLSCSVsParser loader = new RLSCSVsParser(new File(rlsFile), ',', "", 0, true)) {
			FactBase f = StorageBuilder.defaultBuilder().useEncodingAdHocSQLStrategy().usePostgreSQLDB(postgresParams)
					.build().get();
			f.addAll(loader.parse().atoms());
		}
	}
}