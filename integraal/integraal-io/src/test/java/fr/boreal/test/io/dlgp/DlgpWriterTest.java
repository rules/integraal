package fr.boreal.test.io.dlgp;

import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.io.dlgp.DlgpWriter;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.impl.KnowledgeBaseImpl;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.logicalElements.impl.ConstantImpl;
import fr.boreal.model.logicalElements.impl.PredicateImpl;
import fr.boreal.model.logicalElements.impl.VariableImpl;
import fr.boreal.model.partition.TermPartition;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.rule.impl.FORuleImpl;
import fr.boreal.storage.natives.DefaultInMemoryAtomSet;
import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Unit test for DLGP export
 * @author Florent Tornil
 */
@RunWith(Parameterized.class)
class DlgpWriterTest {

	@Parameters
	static Stream<Arguments> data() {
		return Stream.of(
				// variables are written as upper case identifiers
				Arguments.of(new VariableImpl("A"), "A"),
				Arguments.of(new VariableImpl("a"), "VAR_a"),
				Arguments.of(new VariableImpl("aAa"), "VAR_aAa"),
				Arguments.of(new VariableImpl("AAa"), "AAa"),
				// constants are written as lower case identifiers
				Arguments.of(new ConstantImpl("A"), "<A>"),
				Arguments.of(new ConstantImpl("a"), "a"),
				Arguments.of(new ConstantImpl("aAa"), "aAa"),
				Arguments.of(new ConstantImpl("AAa"), "<AAa>"),
				// predicates are written as lower case identifiers
				Arguments.of(new PredicateImpl("A", 1), "<A>"),
				Arguments.of(new PredicateImpl("a", 2), "a"),
				Arguments.of(new PredicateImpl("aAa", 2), "aAa"),
				Arguments.of(new PredicateImpl("AAa", 1), "<AAa>"),
				// atom of arity 1
				Arguments.of(new AtomImpl(new PredicateImpl("AAa", 1), List.of(new VariableImpl("AAa"))), "<AAa>(AAa)"),
				Arguments.of(new AtomImpl(new PredicateImpl("a", 1), List.of(new ConstantImpl("AAa"))), "a(<AAa>)"),
				// atom with multiple terms
				Arguments.of(new AtomImpl(new PredicateImpl("AAa", 2), List.of(new VariableImpl("AAa"), new VariableImpl("AAa"))), "<AAa>(AAa, AAa)"),
				Arguments.of(new AtomImpl(new PredicateImpl("p", 4), List.of(new VariableImpl("X"), new VariableImpl("Y"), new VariableImpl("Y"), new VariableImpl("X"))), "p(X, Y, Y, X)"),
				// multiple atoms
				Arguments.of(List.of(
						new AtomImpl(new PredicateImpl("p", 2), List.of(new VariableImpl("AAa"), new VariableImpl("AAa"))),
						new AtomImpl(new PredicateImpl("q", 1), List.of(new VariableImpl("aAa"))),
						new AtomImpl(new PredicateImpl("p", 2), List.of(new ConstantImpl("aAa"), new ConstantImpl("AAa")))
				), "p(AAa, AAa), q(VAR_aAa), p(aAa, <AAa>)"),
				// rule without label
				Arguments.of(new FORuleImpl(
						FOFormulaFactory.instance().createOrGetConjunction(
								new AtomImpl(new PredicateImpl("p", 1), List.of(new VariableImpl("X")))),
						FOFormulaFactory.instance().createOrGetConjunction(new AtomImpl(new PredicateImpl("q", 1), List.of(new VariableImpl("X"))))
				), "q(X) :- p(X)."),
				// rule with label
				Arguments.of(new FORuleImpl("R1",
						FOFormulaFactory.instance().createOrGetConjunction(
								new AtomImpl(new PredicateImpl("p", 1), List.of(new VariableImpl("X")))),
						FOFormulaFactory.instance().createOrGetConjunction(new AtomImpl(new PredicateImpl("q", 1), List.of(new VariableImpl("X"))))
				), "[R1] q(X) :- p(X)."),
				// query
				Arguments.of(FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(
										new AtomImpl(new PredicateImpl("p", 1), List.of(new VariableImpl("X")))),
								List.of(new VariableImpl("X")),
								null),
						"?(X) :- p(X)."),
				Arguments.of(FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(
										new AtomImpl(new PredicateImpl("p", 2), List.of(new VariableImpl("X"), new VariableImpl("Y")))),
								List.of(new VariableImpl("X"), new VariableImpl("Y"), new VariableImpl("X")),
								null),
						"?(X, Y, X) :- p(X, Y)."),
				// empty ans vars query
				Arguments.of(FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(
										new AtomImpl(new PredicateImpl("p", 2), List.of(new VariableImpl("X"), new VariableImpl("Y")))),
								List.of(),
								null),
						"?() :- p(X, Y)."),
				// query with specialization
				Arguments.of(FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(
										new AtomImpl(new PredicateImpl("p", 2), List.of(new VariableImpl("X"), new VariableImpl("Y")))),
								List.of(new VariableImpl("X"), new VariableImpl("Z")),
								new TermPartition(Set.of(Set.of(new VariableImpl("Z"), new ConstantImpl("a"))))),
						"?(X, Z) :- p(X, Y), Z = a."),
				// factbase
				Arguments.of(new DefaultInMemoryAtomSet(new LinkedHashSet<>(List.of(
						new AtomImpl(new PredicateImpl("p", 2), List.of(new VariableImpl("AAa"), new VariableImpl("AAa"))),
						new AtomImpl(new PredicateImpl("q", 1), List.of(new VariableImpl("aAa"))),
						new AtomImpl(new PredicateImpl("p", 2), List.of(new ConstantImpl("aAa"), new ConstantImpl("AAa")))
				))), "@facts\np(AAa, AAa), q(VAR_aAa), p(aAa, <AAa>)."),
				// rulebase
				Arguments.of(new RuleBaseImpl(new LinkedHashSet<>(List.of(
						new FORuleImpl(FOFormulaFactory.instance().createOrGetConjunction(
								new AtomImpl(new PredicateImpl("p", 1),List.of(new VariableImpl("X")))),
								FOFormulaFactory.instance().createOrGetConjunction(new AtomImpl(new PredicateImpl("q", 1), List.of(new VariableImpl("X"))))
						),
						new FORuleImpl("R1",
								FOFormulaFactory.instance().createOrGetConjunction(
										new AtomImpl(new PredicateImpl("q", 1), List.of(new VariableImpl("X")))),
								FOFormulaFactory.instance().createOrGetConjunction(new AtomImpl(new PredicateImpl("p", 1), List.of(new VariableImpl("X"))))
						)
				))), "@rules\nq(X) :- p(X).\n[R1] p(X) :- q(X)."),
				// kb
				Arguments.of(new KnowledgeBaseImpl(new DefaultInMemoryAtomSet(new LinkedHashSet<>(List.of(
						new AtomImpl(new PredicateImpl("p", 2), List.of(new VariableImpl("AAa"), new VariableImpl("AAa"))),
						new AtomImpl(new PredicateImpl("q", 1), List.of(new VariableImpl("aAa"))),
						new AtomImpl(new PredicateImpl("p", 2), List.of(new ConstantImpl("aAa"), new ConstantImpl("AAa")))
				))),
						new RuleBaseImpl(new LinkedHashSet<>(List.of(
								new FORuleImpl(FOFormulaFactory.instance().createOrGetConjunction(
										new AtomImpl(new PredicateImpl("p", 1),List.of(new VariableImpl("X")))),
										FOFormulaFactory.instance().createOrGetConjunction(new AtomImpl(new PredicateImpl("q", 1), List.of(new VariableImpl("X"))))
								),
								new FORuleImpl("R1",
										FOFormulaFactory.instance().createOrGetConjunction(
												new AtomImpl(new PredicateImpl("q", 1), List.of(new VariableImpl("X")))),
										FOFormulaFactory.instance().createOrGetConjunction(new AtomImpl(new PredicateImpl("p", 1), List.of(new VariableImpl("X"))))
								)
						)))), "@facts\np(AAa, AAa), q(VAR_aAa), p(aAa, <AAa>).\n@rules\nq(X) :- p(X).\n[R1] p(X) :- q(X).")
		);
	}

	@Parameters
	static Stream<Arguments> exportImportData() {
		return Stream.of(Arguments.of(new KnowledgeBaseImpl(new DefaultInMemoryAtomSet(new LinkedHashSet<>(List.of(
				new AtomImpl(new PredicateImpl("p", 2), List.of(new VariableImpl("AAa"), new VariableImpl("AAa"))),
				new AtomImpl(new PredicateImpl("q", 1), List.of(new VariableImpl("aAa"))),
				new AtomImpl(new PredicateImpl("p", 2), List.of(new ConstantImpl("aAa"), new ConstantImpl("AAa")))
		))),
				new RuleBaseImpl(new LinkedHashSet<>(List.of(
						new FORuleImpl(FOFormulaFactory.instance().createOrGetConjunction(
								new AtomImpl(new PredicateImpl("r", 1),List.of(new VariableImpl("X")))),
								FOFormulaFactory.instance().createOrGetConjunction(new AtomImpl(new PredicateImpl("q", 1), List.of(new VariableImpl("X"))))
						),
						new FORuleImpl("R1",
								FOFormulaFactory.instance().createOrGetConjunction(
										new AtomImpl(new PredicateImpl("q", 1), List.of(new VariableImpl("X")))),
								FOFormulaFactory.instance().createOrGetConjunction(new AtomImpl(new PredicateImpl("r", 1), List.of(new VariableImpl("X"))))
						)
				)))), 3, 2, 0));
	}

	/**
	 * Tests that the given object exported in DLGP is equals to the expected result
	 * @param object to write in DLGP
	 * @param expected result dlgp string
	 * @throws IOException iff an error occur
	 */
	@DisplayName("Test DLGP export")
	@ParameterizedTest(name = "{index}: DLGP export of {0} should give {1})")
	@MethodSource("data")
	public void dlgpWriting(Object object, String expected) throws IOException {
		StringWriter output = new StringWriter();
		DlgpWriter writer = new DlgpWriter(output);

		writer.write(object);
		writer.close();

		Assert.assertEquals(expected, output.toString());
	}

	/**
	 * Tests that the given object exported in DLGP is equals to the expected result
	 * @param object to write in DLGP
	 * @param expectedAtoms atoms that should be parsed
	 * @param expectedRules rules that should be parsed
	 * @param expectedQueries queries that should be parsed
	 * @throws IOException iff an error occur
	 */
	@DisplayName("Test DLGP export can be imported")
	@ParameterizedTest(name = "{index}: DLGP export of {0} can be imported again)")
	@MethodSource("exportImportData")
	public void dlgpExportImport(Object object, int expectedAtoms, int expectedRules, int expectedQueries) throws IOException {
		File f = File.createTempFile("export", ".dlgp");
		DlgpWriter writer = new DlgpWriter(f);

		writer.write(object);
		writer.close();

		int atoms = 0 , rules = 0, queries = 0;
		DlgpParser parser = new DlgpParser(f);
		while (parser.hasNext()) {
			Object result = parser.next();
			if (result instanceof Atom) {
				atoms++;
			} else if (result instanceof FORule) {
				rules++;
			} else if (result instanceof FOQuery) {
				queries++;
			}
		}
		parser.close();

		Assert.assertEquals(expectedAtoms, atoms);
		Assert.assertEquals(expectedRules, rules);
		Assert.assertEquals(expectedQueries, queries);

	}
}