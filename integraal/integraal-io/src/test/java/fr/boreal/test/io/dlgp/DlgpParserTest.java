package fr.boreal.test.io.dlgp;

import fr.boreal.io.api.ParseException;
import fr.boreal.io.api.Parser;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.io.dlgp.ParserResult;
import fr.boreal.views.builder.ViewBuilder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.stream.Stream;

@RunWith(Parameterized.class)
class DlgpParserTest {

    private static final String dlgp1 = "p1(a).";
    private static final String dlgp2 = "p1(X).";
    private static final String dlgp3 = "p1(a), q1(b).";
    private static final String dlgp4 = "@facts p1(a).";
    private static final String dlgp5 = "q1(X) :- p1(a).";
    private static final String dlgp6 = "@rules q1(X) :- p1(a).";
    private static final String dlgp7 = "@facts p1(a). @rules q1(X) :- p1(a).";
    private static final String dlgp8 = "?(X) :- p1(X).";
    private static final String dlgp9 = "@queries ?(X) :- p1(X).";
    private static final String dlgp10 = "?(X) :- p1(a), X=b.";

    @Parameterized.Parameters
    static Stream<Arguments> data() {
        return Stream.of(
                Arguments.of(dlgp1),
                Arguments.of(dlgp2),
                Arguments.of(dlgp3),
                Arguments.of(dlgp4),
                Arguments.of(dlgp5),
                Arguments.of(dlgp6),
                Arguments.of(dlgp7),
                Arguments.of(dlgp8),
                Arguments.of(dlgp9),
                Arguments.of(dlgp10)
        );
    }

    @DisplayName("Test DLGP import using view declaration")
    @ParameterizedTest(name = "{index}: DLGP import should work ...)")
    @MethodSource("data")
    public void dlgpViewImport(String dlgp) throws ViewBuilder.ViewBuilderException, ParseException {
        Parser<?> parser = new DlgpParser(dlgp);
        ParserResult result = parser.parse();
        parser.close();
    }

}
