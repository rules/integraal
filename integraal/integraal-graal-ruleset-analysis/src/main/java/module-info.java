module fr.lirmm.integraal.rule_analysis {
	requires java.management;
	requires org.jgrapht.core;
	requires org.apache.commons.lang3;
	requires org.apache.commons.collections4;
	requires transitive graal.api;
	requires transitive graal.core;
	
	exports fr.lirmm.graphik.integraal.core;
	exports fr.lirmm.graphik.integraal.core.ruleset;
	exports fr.lirmm.graphik.integraal.core.unifier.checker;
	exports fr.lirmm.graphik.integraal.rulesetanalyser;
	exports fr.lirmm.graphik.integraal.rulesetanalyser.util;
	exports fr.lirmm.graphik.integraal.rulesetanalyser.property;
	exports fr.lirmm.graphik.integraal.api.core;
	exports fr.lirmm.graphik.integraal.core.factory;
	exports fr.lirmm.graphik.integraal.core.term;
	exports fr.lirmm.graphik.integraal.api.factory;
	exports fr.lirmm.graphik.util.stream;
	exports fr.lirmm.graphik.integraal.core.atomset;
	exports fr.lirmm.graphik.util.graph.scc;
}