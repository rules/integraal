/**
 * 
 */
package fr.lirmm.graphik.integraal.core.atomset.gaifman_graph;

import org.apache.commons.lang3.tuple.Pair;

import fr.lirmm.graphik.integraal.api.core.Term;

/**
 * An edge of a gaifman graph connects two terms
 * 
 * @author Guillaume Pérution-Kihli
 *
 */
public class Edge {
	Pair<Term, Term> terms;
	
	public Edge(Term t1, Term t2) {
		this.terms = Pair.of(t1, t2);
	}
	
	public Pair<Term,Term> getTerms() {
		return terms;
	}
	
	public boolean equals (Edge e) {
		return (terms.getKey().equals(e.terms.getKey()) && terms.getValue().equals(e.terms.getValue()))
				|| (terms.getKey().equals(e.terms.getValue()) && terms.getValue().equals(e.terms.getKey()));
	}
	
	@Override
	public String toString () {
		return terms.toString();
	}
}
