package fr.lirmm.graphik.integraal.api.core;

/**
 * This interface represents a query associated with a partial substitution.
 * 
 * @author Olivier Rodriguez
 */
public interface EffectiveConjunctiveQuery extends EffectiveQuery<ConjunctiveQuery, Substitution> {
};
