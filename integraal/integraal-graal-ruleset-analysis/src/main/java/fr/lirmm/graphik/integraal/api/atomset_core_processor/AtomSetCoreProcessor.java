package fr.lirmm.graphik.integraal.api.atomset_core_processor;

import java.io.IOException;
import java.util.Set;

import fr.lirmm.graphik.integraal.api.core.Atom;
import fr.lirmm.graphik.integraal.api.core.AtomSet;
import fr.lirmm.graphik.integraal.api.core.AtomSetException;
import fr.lirmm.graphik.integraal.api.core.Variable;
import fr.lirmm.graphik.integraal.api.homomorphism.HomomorphismException;
import fr.lirmm.graphik.util.stream.CloseableIterator;

/**
 * Compute the core of a set of atoms.
 * The computed core is a set of atoms without redundant atoms;
 * 'core' corresponds to the notion of graph theory (the set of atoms being viewed as a bipartite (predicate/term) graph).
 *  
 * @author Guillaume Pérution-Kihli
 *
 */
public interface AtomSetCoreProcessor 
{
	/**
	 * Compute the core of an atom set by removing all the redundant atoms in it
	 * 
	 * @param a a set atoms on which we want to compute the core
	 * @throws AtomSetException
	 * @throws HomomorphismException
	 * @throws IOException
	 */
	public void computeCore(AtomSet a) throws AtomSetException, HomomorphismException, IOException;

	/**
	 * Compute the core of an atom set by removing all the redundant atoms in it, in considering the frozen variables as constants,
	 * these variables must belong to the core.
	 * 
	 * @param a a set atoms on which we want to compute the core
	 * @param frozenVariables Variables that will not be removed even if they are redundant ???will be treated as constants
	 * @throws AtomSetException
	 * @throws HomomorphismException
	 * @throws IOException
	 */
	public void computeCore(AtomSet a, Set<Variable> frozenVariables) throws AtomSetException, HomomorphismException, IOException;

	/**
	 * Copy and compute the core of an atom set by removing all the redundant atoms in the copy
	 * 
	 * @param a closeable iterator of atoms on which we want to compute the core
	 * @throws AtomSetException
	 * @throws HomomorphismException
	 * @throws IOException
	 */
	public AtomSet copyAndComputeCore(CloseableIterator<Atom> a) throws AtomSetException, HomomorphismException, IOException;

	/**
	 * Copy and compute the core of an atom set by removing all the redundant atoms in the copy, except the ones containing some frozen variables
	 * 
	 * @param a closeable iterator of atoms on which we want to compute the core
	 * @param frozenVariables Variables that will not be removed even if they are redundant
	 * @throws AtomSetException
	 * @throws HomomorphismException
	 * @throws IOException
	 */
	public AtomSet copyAndComputeCore(CloseableIterator<Atom> a, Set<Variable> frozenVariables) throws AtomSetException, HomomorphismException, IOException;
	
}
