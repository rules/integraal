/*
 * Copyright (C) Inria Sophia Antipolis - Méditerranée / LIRMM
 * (Université de Montpellier & CNRS) (2014 - 2017)
 *
 * Contributors :
 *
 * Clément SIPIETER <clement.sipieter@inria.fr>
 * Mélanie KÖNIG
 * Swan ROCHER
 * Jean-François BAGET
 * Michel LECLÈRE
 * Marie-Laure MUGNIER <mugnier@lirmm.fr>
 *
 *
 * This file is part of Graal <https://graphik-team.github.io/graal/>.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 package fr.lirmm.graphik.integraal.api.core;

import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import fr.lirmm.graphik.util.stream.CloseableIterableWithoutException;
import fr.lirmm.graphik.util.stream.CloseableIteratorWithoutException;

/**
 * This interface represents an InMemory AtomSet. So, AtomSet methods are
 * redefined without the ability to throw exception.
 * 
 * @author Clément Sipieter (INRIA) {@literal <clement@6pi.fr>}
 *
 */
public interface InMemoryAtomSet extends AtomSet, CloseableIterableWithoutException<Atom> {
	
	@Override
	boolean contains(Atom atom);

	@Override
	CloseableIteratorWithoutException<Atom> match(Atom atom, Substitution s);

	@Override
	CloseableIteratorWithoutException<Atom> atomsByPredicate(Predicate p);

	@Override
	CloseableIteratorWithoutException<Term> termsByPredicatePosition(Predicate p, int position);

	@Override
	Set<Predicate> getPredicates();

	@Override
	CloseableIteratorWithoutException<Predicate> predicatesIterator();

	@Override
	Set<Term> getTerms();
	
	@Override
	Set<Variable> getVariables();
	
	@Override
	Set<Constant> getConstants();
	
	@Override
	Set<Literal> getLiterals();

	@Override
	CloseableIteratorWithoutException<Term> termsIterator();
	
	@Override
	CloseableIteratorWithoutException<Variable> variablesIterator();
	
	@Override
	CloseableIteratorWithoutException<Constant> constantsIterator();
	
	@Override
	CloseableIteratorWithoutException<Literal> literalsIterator();
	
	@Override
	@Deprecated
	Set<Term> getTerms(Term.Type type);

	@Override
	@Deprecated
	CloseableIteratorWithoutException<Term> termsIterator(Term.Type type);

	@Override
	CloseableIteratorWithoutException<Atom> iterator();

	@Override
	@Deprecated
	boolean isSubSetOf(AtomSet atomset);

	@Override
	boolean isEmpty();

	@Override
	boolean add(Atom atom);

	@Override
	boolean remove(Atom atom);

	@Override
	void removeWithoutCheck(Atom atom);

	@Override
	void clear();

	Atom[] toArray();

	boolean removeAll(CloseableIteratorWithoutException<? extends Atom> atoms);
	
	boolean removeAll(InMemoryAtomSet atoms);
	
	boolean addAll(InMemoryAtomSet atoms);
	
	boolean addAll(CloseableIteratorWithoutException<? extends Atom> atoms);

	@Override
	int size();

    /**
     * Creates a {@code Spliterator} over the elements in this InMemoryAtomSet.
     *
     * <p>The {@code Spliterator} reports {@link Spliterator#DISTINCT}.
     * Implementations should document the reporting of additional
     * characteristic values.
     *
     * The default implementation creates a
     * <em><a href="Spliterator.html#binding">late-binding</a></em> spliterator
     * from the InMemoryAtomSet's {@code Iterator}.  The spliterator inherits the
     * <em>fail-fast</em> properties of the InMemoryAtomSet's iterator.
     * <p>
     * The created {@code Spliterator} additionally reports
     * {@link Spliterator#SIZED}.
     *
     * The created {@code Spliterator} additionally reports
     * {@link Spliterator#SUBSIZED}.
     *
     * @return a {@code Spliterator} over the elements in this InMemoryAtomSet
     */
    @Override
    default Spliterator<Atom> spliterator() {
        return Spliterators.spliterator(this.iterator(), size(), Spliterator.DISTINCT);
    }
    
    /**
     * Returns a sequential {@code Stream} with this InMemoryAtomSet as its source.
     *
     * <p>This method should be overridden when the {@link #spliterator()}
     * method cannot return a spliterator that is {@code IMMUTABLE},
     * {@code CONCURRENT}, or <em>late-binding</em>. (See {@link #spliterator()}
     * for details.)
     *
     * The default implementation creates a sequential {@code Stream} from the
     * InMemoryAtomSet {@code Spliterator}.
     *
     * @return a sequential {@code Stream} over the elements in this InMemoryAtomSet
     */

    default Stream<Atom> stream() {
        return StreamSupport.stream(spliterator(), false);
    }

    /**
     * Returns a possibly parallel {@code Stream} with this InMemoryAtomSet as its
     * source.  It is allowable for this method to return a sequential stream.
     *
     * <p>This method should be overridden when the {@link #spliterator()}
     * method cannot return a spliterator that is {@code IMMUTABLE},
     * {@code CONCURRENT}, or <em>late-binding</em>. (See {@link #spliterator()}
     * for details.)
     *
     * The default implementation creates a parallel {@code Stream} from the
     * InMemoryAtomSet {@code Spliterator}.
     *
     * @return a possibly parallel {@code Stream} over the elements in this
     * InMemoryAtomSet
     */
    default Stream<Atom> parallelStream() {
        return StreamSupport.stream(spliterator(), true);
    }
}
