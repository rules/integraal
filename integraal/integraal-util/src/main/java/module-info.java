/**
 * Module for util elements of InteGraal
 * 
 * @author Florent Tornil
 * 
 */
module fr.lirmm.boreal.util {

	requires transitive fr.boreal.model;
	
	requires com.google.common;
	requires transitive fr.lirmm.integraal.rule_analysis;
	requires org.slf4j;

	exports fr.lirmm.boreal.util;
	exports fr.lirmm.boreal.util.time;
	exports fr.lirmm.boreal.util.converter;
	exports fr.lirmm.boreal.util.stream;
	exports fr.lirmm.boreal.util.stream.filter;
	exports fr.lirmm.boreal.util.validator;
	exports fr.lirmm.boreal.util.validator.data;
	exports fr.lirmm.boreal.util.validator.query;
	exports fr.lirmm.boreal.util.validator.rule;
	exports fr.lirmm.boreal.util.object_analyzer;
	exports fr.lirmm.boreal.util.enumerations;
	exports fr.lirmm.boreal.util.timeout;

}