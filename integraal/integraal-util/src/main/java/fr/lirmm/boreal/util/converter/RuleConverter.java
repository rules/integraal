package fr.lirmm.boreal.util.converter;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.rule.impl.FORuleImpl;

/**
 * Converts FORule to and from Graal's Rule
 * <br/>
 * This is used for compatibility with Graal 1.3 version.
 * 
 * @author Florent Tornil
 *
 */
public class RuleConverter {

	private static final BiMap<FORule, fr.lirmm.graphik.integraal.api.core.Rule> cache = HashBiMap.create();

	/**
	 * Converts the given FORule into a Graal Rule
	 * 
	 * @param rule the rule to convert
	 * @return the converted rule
	 */
	public static fr.lirmm.graphik.integraal.api.core.Rule convert(FORule rule) {
		fr.lirmm.graphik.integraal.api.core.Rule r = cache.get(rule);
		if (r == null) {
			r = fr.lirmm.graphik.integraal.core.factory.DefaultRuleFactory.instance().create(AtomSetConverter.convert(rule.getBody()),
					AtomSetConverter.convert(rule.getHead()));
			cache.put(rule, r);
		}
		return r;

	}


	/**
	 * Converts the given FORule into a Graal Rule
	 * 
	 * @param rule the rule to convert
	 * @return the converted rule
	 */
	public static fr.lirmm.graphik.graal.api.core.Rule convert2(FORule rule) {
		return fr.lirmm.graphik.graal.core.factory.DefaultRuleFactory.instance().create(AtomSetConverter.convert2(rule.getBody()),
					AtomSetConverter.convert2(rule.getHead()));
	}

	/**
	 * Converts the given Graal Rule into a FORule
	 * 
	 * @param rule the rule to convert
	 * @return the converted rule
	 */
	public static FORule reverse(fr.lirmm.graphik.integraal.api.core.Rule rule) {
		FORule r = cache.inverse().get(rule);
		if (r == null) {
			r = new FORuleImpl(AtomSetConverter.reverse(rule.getBody()), AtomSetConverter.reverse(rule.getHead()));
			cache.inverse().put(rule, r);
		}
		return r;
	}

}
