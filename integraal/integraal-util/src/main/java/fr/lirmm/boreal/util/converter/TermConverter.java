package fr.lirmm.boreal.util.converter;

import fr.boreal.model.logicalElements.api.*;

import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;

/**
 * Converts Term to and from Graal's Term
 * <br/>
 * This is used for compatibility with Graal 1.3 version.
 * 
 * @author Florent Tornil
 *
 */
public class TermConverter {

	private static boolean WARNING_PRINTED = false;

	/**
	 * Converts the given Term into a Graal Term
	 * 
	 * @param t the term to convert
	 * @return the converted term
	 */
	public static fr.lirmm.graphik.integraal.api.core.Term convert(Term t) {
        switch (t) {
            case Constant c -> {
                return fr.lirmm.graphik.integraal.core.term.DefaultTermFactory.instance().createConstant(c.label());
            }
            case Variable v -> {
                return fr.lirmm.graphik.integraal.core.term.DefaultTermFactory.instance().createVariable(v.label());
            }
            case Literal<?> l -> {
                return fr.lirmm.graphik.integraal.core.term.DefaultTermFactory.instance().createLiteral(l.value());
            }
            case LogicalFunctionalTerm ignored -> {
                if (!WARNING_PRINTED) {
                    System.err.println(
                            "[WARNING] : Converted a functional term to a Graal constant : result may not be exact.");
                    WARNING_PRINTED = true;
                }
                return fr.lirmm.graphik.integraal.core.term.DefaultTermFactory.instance()
                        .createConstant("integraal_function");
            }
            case null, default -> {
                return null;
            }
        }
	}

	/**
	 * Converts the given Term into a Graal Term
	 * 
	 * @param t the term to convert
	 * @return the converted term
	 */
	public static fr.lirmm.graphik.graal.api.core.Term convert2(Term t) {
        switch (t) {
            case Constant c -> {
                return fr.lirmm.graphik.graal.core.term.DefaultTermFactory.instance().createConstant(c.label());
            }
            case Variable v -> {
                return fr.lirmm.graphik.graal.core.term.DefaultTermFactory.instance().createVariable(v.label());
            }
            case Literal<?> l -> {
                return fr.lirmm.graphik.graal.core.term.DefaultTermFactory.instance().createLiteral(l.value());
            }
            case LogicalFunctionalTerm ignored -> {
                if (!WARNING_PRINTED) {
                    System.err.println(
                            "[WARNING] : Converted a functional term to a Graal constant : result may not be exact.");
                    WARNING_PRINTED = true;
                }
                return fr.lirmm.graphik.graal.core.term.DefaultTermFactory.instance().createConstant("integraal_function");
            }
            case null, default -> {
                return null;
            }
        }
	}

	/**
	 * Converts the given Graal Term into a Term
	 * 
	 * @param t the term to convert
	 * @return the converted term
	 */
	public static Term reverse(fr.lirmm.graphik.integraal.api.core.Term t) {
        return switch (t) {
            case fr.lirmm.graphik.integraal.api.core.Literal l ->
                    SameObjectTermFactory.instance().createOrGetLiteral(l.getValue());
            case fr.lirmm.graphik.integraal.api.core.Variable v ->
                    SameObjectTermFactory.instance().createOrGetVariable(v.getLabel());
            case fr.lirmm.graphik.integraal.api.core.Constant c ->
                    SameObjectTermFactory.instance().createOrGetConstant(c.getLabel());
            case null, default -> null;
        };
	}

}
