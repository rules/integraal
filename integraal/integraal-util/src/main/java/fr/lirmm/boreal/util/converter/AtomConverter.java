package fr.lirmm.boreal.util.converter;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.impl.AtomImpl;

/**
 * Converts Atom to and from Graal's Atom
 * <br/>
 * This is used for compatibility with Graal 1.3 version.
 * 
 * @author Florent Tornil
 *
 */
public class AtomConverter {
	
	/**
	 * Converts the given Atom into a Graal Atom
	 * @param a the atom to convert
	 * @return the converted atom
	 */
	public static fr.lirmm.graphik.integraal.api.core.Atom convert(Atom a) {
		fr.lirmm.graphik.integraal.api.core.Predicate p = PredicateConverter.convert(a.getPredicate());
		List<fr.lirmm.graphik.integraal.api.core.Term> terms = Stream.of(a.getTerms()).map(TermConverter::convert).collect(Collectors.toList());
		return fr.lirmm.graphik.integraal.core.factory.DefaultAtomFactory.instance().create(p, terms);
	}
	

	/**
	 * Converts the given Atom into a Graal Atom
	 * @param a the atom to convert
	 * @return the converted atom
	 */
	public static fr.lirmm.graphik.graal.api.core.Atom convert2(Atom a) {
		fr.lirmm.graphik.graal.api.core.Predicate p = PredicateConverter.convert2(a.getPredicate());
		List<fr.lirmm.graphik.graal.api.core.Term> terms = Stream.of(a.getTerms()).map(TermConverter::convert2).collect(Collectors.toList());
		return fr.lirmm.graphik.graal.core.factory.DefaultAtomFactory.instance().create(p, terms);
	}
	
	
	/**
	 * Converts the given Graal Atom into an Atom
	 * @param a the atom to convert
	 * @return the converted atom
	 */
	public static Atom reverse(fr.lirmm.graphik.integraal.api.core.Atom a) {
		Predicate p = PredicateConverter.reverse(a.getPredicate());
		List<Term> terms = a.getTerms().stream().map(TermConverter::reverse).collect(Collectors.<Term>toList());
		return new AtomImpl(p, terms);
	}

}
