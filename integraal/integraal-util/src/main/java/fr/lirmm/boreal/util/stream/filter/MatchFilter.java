/*
 * Copyright (C) Inria Sophia Antipolis - Méditerranée / LIRMM
 * (Université de Montpellier & CNRS) (2014 - 2017)
 *
 * Contributors :
 *
 * Clément SIPIETER <clement.sipieter@inria.fr>
 * Mélanie KONIG
 * Swan ROCHER
 * Jean-François BAGET
 * Michel LECLERE
 * Marie-Laure MUGNIER <mugnier@lirmm.fr>
 *
 *
 * This file is part of Graal <https://graphik-team.github.io/graal/>.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lirmm.boreal.util.stream.filter;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;

/**
 * Filter used for the match method as a post filter
 * 
 * @author Florent Tornil
 *
 */
public class MatchFilter implements Filter<Atom> {

	private final Atom atom;
	private final Substitution substitution;

	/**
	 * Filters the given atom iterator, removing atoms that do not match the given atom
	 * This can be used by storages that do not include filtering or generation of queries with the correct filters.
	 * An atom match another one if constants are the same and the same variables are instantiated by the same constant.
	 * @param atom the filter atom
	 * @param s the filter substitution
	 */
	public MatchFilter(Atom atom, Substitution s) {
		this.substitution = s;
		this.atom = atom;
	}

	@Override
	public boolean filter(Atom e) {
		boolean valid = true;
		for(int i = 0; valid && i< this.atom.getPredicate().arity(); i++) {
			Term t_a = this.atom.getTerm(i);
			if(t_a.isFrozen(this.substitution)) {
				t_a = this.substitution.createImageOf(t_a);
				// check constants
				if(! t_a.equals(e.getTerm(i))) {
					valid = false;
				}
			} else if(t_a.isVariable()) {
				// check variables equality
				int[] indexes = this.atom.indexesOf(t_a);
				if(indexes.length > 1) {
					for(int j = 1; valid && j<indexes.length; j++) {
						int index_j = indexes[j];
						int index_j_1 = indexes[j-1];
						if(! e.getTerm(index_j).equals(e.getTerm(index_j_1))) {
							valid = false;
						}
					}
				}
			}
		}
		return valid;
	}

}
