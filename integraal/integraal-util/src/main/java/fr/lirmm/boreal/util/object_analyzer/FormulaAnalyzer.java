package fr.lirmm.boreal.util.object_analyzer;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.formula.api.FOFormulaDisjunction;
import fr.boreal.model.formula.api.FOFormulaNegation;
import fr.boreal.model.formula.api.CompoundFOFormula;
import fr.boreal.model.logicalElements.api.Atom;

public class FormulaAnalyzer {

	public static String info(FOFormula f) {

		StringBuilder result = new StringBuilder();

		switch (f) {

		case Atom ignored -> { }
		case FOFormulaConjunction c -> result.append("\nnumber of atoms ").append(c.getSubElements().size());
		case FOFormulaDisjunction d -> result.append("\nnumber of disjuncts ").append(d.getSubElements().size());
		case FOFormulaNegation ignored -> {	}
		case CompoundFOFormula ignored -> { }
		}

		return result.toString();
	}

}
