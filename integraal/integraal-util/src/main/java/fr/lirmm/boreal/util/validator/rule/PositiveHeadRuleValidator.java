package fr.lirmm.boreal.util.validator.rule;

import fr.boreal.model.rule.api.FORule;
import fr.lirmm.boreal.util.validator.Validator;

/**
 * @author Florent Tornil
 * Checks that the head of the rule if positive
 */
public class PositiveHeadRuleValidator implements Validator<FORule> {

	@Override
	public boolean check(FORule rule) {
		return PositiveFormulaValidator.instance().check(rule.getHead());
	}

}
