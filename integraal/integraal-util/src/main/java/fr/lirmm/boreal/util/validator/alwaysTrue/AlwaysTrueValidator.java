package fr.lirmm.boreal.util.validator.alwaysTrue;

import fr.lirmm.boreal.util.validator.Validator;

public class AlwaysTrueValidator implements Validator<Object> {
    @Override
    public boolean check(Object element) {
        return true;
    }
}
