package fr.lirmm.boreal.util.object_analyzer;

import fr.boreal.model.query.api.FOQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

public class ObjectAnalizer {

	static final Logger LOG = LoggerFactory.getLogger(ObjectAnalizer.class);

	/**
	 *
	 * @return information on the query structure
	 */
	public static String info(Object o) {
        if (Objects.requireNonNull(o) instanceof FOQuery<?> q) {
            return FormulaAnalyzer.info(q.getFormula());
        }
        return o.toString();
    }

	/**
	 * 
	 * @return a short representation of the query
	 */
	public static String digest(Object o) {

        if (Objects.requireNonNull(o) instanceof FOQuery<?> f) {
            String s = f.toString();
            int size = 50;
            return s.length() <= size ? s : s.substring(0, size) + " [... rest of the expression cut]";
        }
        LOG.error("unsupported type");
        throw new RuntimeException(
                String.format(
                        "[ObjectAnalyzer::digest] Unsupported type: %s(%s).",
                        o.getClass(), o));

    }

}
