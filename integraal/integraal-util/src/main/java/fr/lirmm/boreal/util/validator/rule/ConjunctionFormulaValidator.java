package fr.lirmm.boreal.util.validator.rule;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.lirmm.boreal.util.validator.Validator;

import java.util.Collection;

/**
 * @author Florent Tornil
 * Checks that the formula is only a (possibly multi-level) conjunction
 */
public class ConjunctionFormulaValidator implements Validator<FOFormula> {
	private static final ConjunctionFormulaValidator INSTANCE = new ConjunctionFormulaValidator();

	/**
	 * Returns the default instance of ConjunctionFormulaValidator.
	 * @return the instance of ConjunctionFormulaValidator
	 */
	public static ConjunctionFormulaValidator instance() {
		return INSTANCE;
	}

	@Override
	public boolean check(FOFormula formula) {
		if(formula.isConjunction()) {
			Collection<? extends FOFormula> subqueries = ((FOFormulaConjunction) formula).getSubElements();
			return this.check(subqueries);
		} else {
			return formula.isAtomic();
		}
	}

}
