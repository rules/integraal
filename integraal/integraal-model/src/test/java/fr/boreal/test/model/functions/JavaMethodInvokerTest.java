package fr.boreal.test.model.functions;

import fr.boreal.model.functions.JavaMethodInvoker;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

class JavaMethodInvokerTest {
    private TermFactory termFactory;
    private static Path tempDir;
    private static final String CLASS_NAME = "test.resources.JavaMethodInvokerExample";

    @BeforeAll
    static void compileJavaMethodInvokerExample() throws IOException {
        tempDir = Files.createTempDirectory("compiledClasses");
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();

        int result = compiler.run(null, null, null,
                "src/test/resources/JavaMethodInvokerExample.java",
                "-d", tempDir.toAbsolutePath().toString());

        assertEquals(0, result, "Compilation failed. Ensure JavaMethodInvokerExample.java compiles correctly.");
    }

    @BeforeEach
    void setup() {
        termFactory = SameObjectTermFactory.instance();
    }

    @Test
    void testConstructor_ValidCase() {
        assertDoesNotThrow(() -> new JavaMethodInvoker(termFactory, tempDir.toString(), CLASS_NAME, "getGreeting"));
    }

    @Test
    void testConstructor_ClassNotFound() {
        assertThrows(ClassNotFoundException.class, () ->
                new JavaMethodInvoker(termFactory, tempDir.toString(), "NonExistentClass", "getGreeting")
        );
    }

    @Test
    void testConstructor_NoSuchMethod() {
        assertThrows(NoSuchMethodException.class, () ->
                new JavaMethodInvoker(termFactory, tempDir.toString(), CLASS_NAME, "nonExistentMethod")
        );
    }

    @Test
    void testInvoke_ValidMethod() throws Exception {
        JavaMethodInvoker invoker = new JavaMethodInvoker(termFactory, tempDir.toString(), CLASS_NAME, "getGreeting");
        Term result = invoker.invoke();
        assertNotNull(result);
    }

    @Test
    void testInvoke_ArgumentMismatch() {
        JavaMethodInvoker invoker = assertDoesNotThrow(() -> new JavaMethodInvoker(termFactory, tempDir.toString(), CLASS_NAME, "echo"));
        Term term1 = termFactory.createOrGetLiteral("Hello");
        Term term2 = termFactory.createOrGetLiteral("World");
        assertThrows(IllegalArgumentException.class, () -> invoker.invoke(term1, term2));
    }

    @Test
    void testInvoke_MethodThrowsException() throws Exception {
        JavaMethodInvoker invoker = new JavaMethodInvoker(termFactory, tempDir.toString(), CLASS_NAME, "methodThatThrows");
        Term term = termFactory.createOrGetLiteral("test");
        assertThrows(RuntimeException.class, () -> invoker.invoke(term));
    }

    @Test
    void testInvoke_VariadicParameters() throws Exception {
        JavaMethodInvoker invoker = new JavaMethodInvoker(termFactory, tempDir.toString(), CLASS_NAME, "concatenateStrings");
        Term term1 = termFactory.createOrGetLiteral("Hello ");
        Term term2 = termFactory.createOrGetLiteral("World!");
        Term result = invoker.invoke(term1, term2);
        assertNotNull(result);
    }

    // Additional test to cover `terms[i] instanceof Literal<?>` false case
    @Test
    void testInvoke_NonLiteralTerm() throws Exception {
        JavaMethodInvoker invoker = new JavaMethodInvoker(termFactory, tempDir.toString(), CLASS_NAME, "echo");
        Term nonLiteralTerm = termFactory.createOrGetConstant("NonLiteral");
        Term result = invoker.invoke(nonLiteralTerm);
        assertNotNull(result);
    }

    // Additional test to cover IllegalStateException
    @Test
    void testInvoke_PrivateMethod_ThrowsIllegalStateException() {
        JavaMethodInvoker invoker = assertDoesNotThrow(() ->
                new JavaMethodInvoker(termFactory, tempDir.toString(), CLASS_NAME, "privateMethod")
        );
        assertThrows(IllegalStateException.class, invoker::invoke);
    }

    // Additional test to cover InvocationTargetException
    @Test
    void testInvoke_MethodWithException_ThrowsInvocationTargetException() throws Exception {
        JavaMethodInvoker invoker = new JavaMethodInvoker(termFactory, tempDir.toString(), CLASS_NAME, "methodThatThrows");
        assertThrows(RuntimeException.class, invoker::invoke);
    }

    @Test
    void testInvoke_MethodThatThrowsCheckedException_ThrowsInvocationTargetException() {
        JavaMethodInvoker invoker = assertDoesNotThrow(() ->
                new JavaMethodInvoker(termFactory, tempDir.toString(), CLASS_NAME, "methodThatThrowsCheckedException")
        );
        assertThrows(RuntimeException.class, invoker::invoke);
    }
}
