package fr.boreal.test.model.partition;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runners.Parameterized.Parameters;

import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.partition.TermPartition;
import fr.boreal.model.partition.TermPartitionFactory;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.rule.api.Rule;
import fr.boreal.model.rule.impl.FORuleImpl;

class TermPartitionFactoryTest {

	@Parameters
	static Stream<Arguments> getByPositionPartitionTestData() {

		Predicate p = SameObjectPredicateFactory.instance().createOrGetPredicate("p", 1);
		Predicate r = SameObjectPredicateFactory.instance().createOrGetPredicate("r", 2);
		Predicate q = SameObjectPredicateFactory.instance().createOrGetPredicate("q", 1);

		Term x = SameObjectTermFactory.instance().createOrGetVariable("x");
		Term y = SameObjectTermFactory.instance().createOrGetVariable("y");
		Term u = SameObjectTermFactory.instance().createOrGetVariable("u");
		Term v = SameObjectTermFactory.instance().createOrGetVariable("v");

		Atom rxy = new AtomImpl(r, x, y);
		Atom ruv = new AtomImpl(r, u, v);
		Atom px = new AtomImpl(p, x);
		Atom qy = new AtomImpl(q, y);
		Atom ruu = new AtomImpl(r, u, u);

		FORule R = new FORuleImpl(px,
				FOFormulaFactory.instance().createOrGetConjunction(rxy, qy));

		return Stream.of(
				Arguments.of(
						Set.of(rxy, ruv),
						List.of(Set.of(u,x), Set.of(v,y)), 
						R,
						true),
				Arguments.of(
						Set.of(rxy, ruu),
						List.of(Set.of(u,x, y)),
						R,
						false)
				);
	}

	@DisplayName("Test getByPositionPartition method")
	@ParameterizedTest(name = "{index}: ({0}, {1}, {2}, {3})")
	@MethodSource("getByPositionPartitionTestData")
	void getByPositionPartitionTest(Collection<Atom> atoms, List<Set<Term>> partition, Rule rule, boolean isValid) {
		TermPartition tp1 = TermPartitionFactory.instance().getByPositionPartition(atoms);
		TermPartition tp2 = new TermPartition(partition);
		Assertions.assertEquals(tp2, tp1);
		Assertions.assertEquals(isValid, tp1.isValid(rule));
	}

}
