package fr.boreal.test.model.partition;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import fr.boreal.model.partition.Partition;

@RunWith(Parameterized.class)
class PartitionTest {

	@Parameters
	static Stream<Arguments> data() {
		return Stream.of(
				Arguments.of(Set.of(1,2), List.of(Set.of(1,2)), List.of(List.of(1,2))),
				Arguments.of(Set.of(1,2,3,4,5,6,7,8), 
						List.of(Set.of(1,2,5,6,8), Set.of(3,4), Set.of(7)), 
						List.of(List.of(1,2), List.of(3,4), 
								List.of(2,5), List.of(1,6),
								List.of(2,8))),
				Arguments.of(Set.of(1,2,3,4,5,6), 
						List.of(Set.of(1,2,3,4,5), Set.of(6)), 
						List.of(List.of(1,2), List.of(3,4), List.of(1,3), List.of(2,5)))
				);
	}

	@Parameters
	static Stream<Arguments> notEqualsData() {
		return Stream.of(
				Arguments.of(List.of(Set.of(1,2)), List.of(Set.of(1,2,5,6,8), Set.of(3,4), Set.of(7))),
				Arguments.of(List.of(Set.of(1,2,5,6,8), Set.of(3,4), Set.of(7)), List.of(Set.of(1,2,3,4,5), Set.of(6))),
				Arguments.of(List.of(Set.of(1,2,3,4,5), Set.of(6)), List.of()),
				Arguments.of(List.of(Set.of(1,2,3,4,5), Set.of(6)), List.of(Set.of(1,2,3,4,6), Set.of(5))),
				Arguments.of(List.of(Set.of(1,2,3,4,5), Set.of(6)), List.of(Set.of(1,2,3,4,6), Set.of(8)))
				);
	}

	@DisplayName("Test addClass method")
	@ParameterizedTest(name = "{index}: ({1})")
	@MethodSource("data")
	public void addClassTest(Set<Integer> set, List<Set<Integer>> partition) {

		Partition<Integer> part = new Partition<>();
		for (var cl : partition) {
			part.addClass(cl);
		}

		var classes = part.getClasses();
		Assert.assertTrue(partition.containsAll(classes));
		Assert.assertTrue(classes.containsAll(partition));
		Assert.assertEquals(set, part.getElements());
	}

	@DisplayName("Test getRepresentative method")
	@ParameterizedTest(name = "{index}: ({1})")
	@MethodSource("data")
	public void getRepresentativeTest(Set<Integer> set, List<Set<Integer>> partition) {
		Partition<Integer> part = new Partition<>(partition);

		for (var cl : partition) {
			var it = cl.iterator();
			Integer repr = part.getRepresentative(it.next());
			Assert.assertTrue(cl.contains(repr));

			while(it.hasNext()) {
				Assert.assertEquals(repr, part.getRepresentative(it.next()));
			}
		}
	}

	@DisplayName("Test getClass method")
	@ParameterizedTest(name = "{index}: ({1})")
	@MethodSource("data")
	public void getClassTest(Set<Integer> set, List<Set<Integer>> partition) {
		Partition<Integer> part = new Partition<>(partition);

		for (var cl : partition) {
			for (Integer i : cl) {
                Set<Integer> s = new HashSet<>(part.getClass(i));
				Assert.assertEquals(cl, s);
			}
		}
	}

	@DisplayName("Test union method")
	@ParameterizedTest(name = "{index}: ({1})")
	@MethodSource("data")
	public void unionTest(Set<Integer> set, List<Set<Integer>> partition, List<List<Integer>> unions) {
		Partition<Integer> part = new Partition<>(set);

		for (var u : unions) {
			part.union(u.get(0), u.get(1));
		}

		Assert.assertTrue(partition.containsAll(part.getClasses()));
	}

	@DisplayName("Test getClasses method")
	@ParameterizedTest(name = "{index}: ({1})")
	@MethodSource("data")
	public void getClassesTest(Set<Integer> set, List<Set<Integer>> partition) {
		Partition<Integer> part = new Partition<>(partition);
		Assert.assertTrue(partition.containsAll(part.getClasses()));
	}

	@DisplayName("Test getElements method")
	@ParameterizedTest(name = "{index}: ({1})")
	@MethodSource("data")
	public void getElementsTest(Set<Integer> set, List<Set<Integer>> partition) {
		Partition<Integer> part = new Partition<>(partition);
		Assert.assertEquals(set, part.getElements());
	}


	@DisplayName("Test hashCode method")
	@ParameterizedTest(name = "{index}: ({1})")
	@MethodSource("data")
	public void hashCodeTest(Set<Integer> set, List<Set<Integer>> partition, List<List<Integer>> unions) {
		// The building method must not bias the hash code
		Partition<Integer> part1 = new Partition<>(set);
		for (var u : unions) {
			part1.union(u.get(0), u.get(1));
		}
		Partition<Integer> part2 = new Partition<>(partition);

		Assert.assertEquals(part1.hashCode(), part2.hashCode());
	}


	@DisplayName("Test equals method")
	@ParameterizedTest(name = "{index}: ({1})")
	@MethodSource("data")
	public void equalsTest(Set<Integer> set, List<Set<Integer>> partition, List<List<Integer>> unions) {
		// The building method must not bias the equality
		Partition<Integer> part1 = new Partition<>(set);
		for (var u : unions) {
			part1.union(u.get(0), u.get(1));
		}
		Partition<Integer> part2 = new Partition<>(partition);
		
		// We test equality twice because the inner structure can be changed by the equality method
		Assert.assertEquals(part1, part2);
		Assert.assertEquals(part1, part2);
	}


	@DisplayName("Test equals method when not equals")
	@ParameterizedTest(name = "{index}: ({1})")
	@MethodSource("notEqualsData")
	public void notEqualsTest(List<Set<Integer>> partition1, List<Set<Integer>> partition2) {
		Partition<Integer> part1 = new Partition<>(partition1);
		Partition<Integer> part2 = new Partition<>(partition2);

		Assert.assertNotEquals(part1, part2);
	}
}
