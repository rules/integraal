package test.resources;

public class JavaMethodInvokerExample {

    public static String getGreeting() {
        return "Hello, World!";
    }

    public static String echo(String message) {
        return "Echo: " + message;
    }

    public static int add(int a, int b) {
        return a + b;
    }

    public static String concatenateStrings(String... strings) {
        StringBuilder result = new StringBuilder();
        for (String str : strings) {
            result.append(str);
        }
        return result.toString();
    }

    public static void methodThatThrows() {
        throw new UnsupportedOperationException("This method always throws an exception");
    }

    // Private method for IllegalAccessException testing
    private static void privateMethod() {
        System.out.println("This is a private method");
    }

    // Method to throw a checked exception for testing InvocationTargetException
    public static void methodThatThrowsCheckedException() throws Exception {
        throw new Exception("Checked exception thrown for testing");
    }
}
