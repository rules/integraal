/**
 * Module for model elements of InteGraal
 * 
 * @author Florent Tornil
 *
 */
module fr.boreal.model {

	requires com.google.common;
	requires org.apache.commons.lang3;
	requires org.apache.commons.collections4;
	requires org.slf4j;
	requires java.compiler; // Java compiler for the unary tests

	exports fr.boreal.model.logicalElements.api;
	exports fr.boreal.model.logicalElements.factory.api;
	exports fr.boreal.model.logicalElements.factory.impl;
	exports fr.boreal.model.logicalElements.impl;
	exports fr.boreal.model.logicalElements.functional;

	exports fr.boreal.model.formula;
	exports fr.boreal.model.formula.api;
	exports fr.boreal.model.formula.factory;

	exports fr.boreal.model.rule.api;
	exports fr.boreal.model.rule.impl;

	exports fr.boreal.model.kb.api;
	exports fr.boreal.model.kb.impl;

	exports fr.boreal.model.query.api;
	exports fr.boreal.model.query.factory;
	exports fr.boreal.model.query.impl;

	exports fr.boreal.model.partition;
	exports fr.boreal.model.queryEvaluation.api;
	exports fr.boreal.model.functions;

	exports fr.boreal.model.ruleCompilation;
	exports fr.boreal.model.ruleCompilation.api;
	exports fr.boreal.model.ruleCompilation.id;
    exports fr.boreal.model.logicalElements.impl.functionalTerms;

}