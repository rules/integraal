package fr.boreal.model.logicalElements.api;

import fr.boreal.model.logicalElements.impl.functionalTerms.SpecializableLogicalFunctionalTermImpl;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * An evaluable function is a Term that can be evaluated into another (non-functional) term
 *
 * @author Florent Tornil
 */
public interface EvaluableFunction extends Term {
    /**
     *
     * @return a string representing the function name
     */
    String getFunctionName();

    /**
     * (1) applies the substitution to the term
     * (2) evaluate the function associated with the term
     * <p>
     * Precondition : a functional term is evaluable IFF all of its arguments -- after applying the substitution -- are literals
     *
     * @param s a substitution
     * @return the resulting term of the evaluation
     */
    Term eval(Substitution s);

    /**
     * @return the first level terms used to parametrize the evaluable function
     */
    List<Term> getTerms();

    /**
     * Recursively enumerate all variable terms
     *
     * @return the set of variables included in this term
     */
    default Set<Variable> getVariables() {
        return getTerms(Variable.class);
    }

    /**
     * Recursively enumerate all variable terms
     *
     * @return the set of variables included in this term
     */
    default Set<Literal> getLiterals() {
        return getTerms(Literal.class);
    }

    /**
     * Recursively enumerate all terms
     *
     * @return the set of variables included in this term
     * classtype filters the terms by type
     */
    <T extends Term> Set<T> getTerms(Class<T> classtype);

    /**
     * @param s substitution
     * @return true IFF the term after applying the substitution contains only literals
     */
    default boolean isEvaluableWith(Substitution s) {
        return this.setFunctionParameters(s).getTerms().stream().allMatch(term -> term.isLiteral());
    }

    /**
     * @param s substitution
     * @return a new evaluable function where some variables have been replaced by other terms, as defined by the substitution,
     * and where all evaluable nested functions that are already evaluable have been evaluated
     */
    EvaluableFunction evaluateNestedFunctions(Substitution s);

    /**
     * @param s substitution
     * @return a new function where some variables have been replaced by other terms, as defined by the substitution s
     */
    EvaluableFunction setFunctionParameters(Substitution s);

    /**
     * @return the evaluable function represented as a logical functional term
     */
    default LogicalFunctionalTerm asLogicalFunctionalTerm() {
        return new SpecializableLogicalFunctionalTermImpl(getFunctionName(), this.getTerms().stream().map(term -> {
            if (term.isEvaluableFunction()) {
                return ((EvaluableFunction) term).asLogicalFunctionalTerm();
            }
            return term;
        }).collect(Collectors.toList()));
    }
}
