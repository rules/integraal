package fr.boreal.model.logicalElements.factory.api;

import fr.boreal.model.logicalElements.api.Constant;
import fr.boreal.model.logicalElements.api.Literal;
import fr.boreal.model.logicalElements.api.Variable;

/**
 * Factory to create terms
 * 
 * @author Florent Tornil
 *
 */

public interface TermFactory {

	/**
	 * Two object created by a call to this function with the same label argument must be equals according to the definition given by the {@link Constant} implementation used
	 * @param label the label
	 * @return a constant with the given label
	 */
	Constant createOrGetConstant(String label);

	/**
	 * Two object created by a call to this function with the same typed value argument must be equals according to the definition given by the {@link Literal} implementation used
	 * @param value the value
	 * @param <T> the native type of the literal
	 * @return a literal with the given value
	 */
	<T> Literal<T> createOrGetLiteral(T value);

	/**
	 * Two object created by a call to this function with the same label argument must be equals according to the definition given by the {@link Variable} implementation used
	 * 
	 * @param label the label
	 * @return a variable with the given label
	 */
	Variable createOrGetVariable(String label);

	/**
	 * Two object created by a call to this function must never be equals according to the definition given by the {@link Variable} implementation used
	 * A Fresh Variable must not be equals to an existing Variable created by {@link TermFactory#createOrGetVariable}.
	 * 
	 * @return a variable with a new label
	 */
	Variable createOrGetFreshVariable();
	
	/**
	 * Forgets a previously created constant.
	 * <br/>
	 * This constant will no longer be considered as already existing in the factory.
	 * 
	 * @param label the label of the previously created constant
	 * @return true iff such a constant already exists in the factory, and thus forgets it.
	 */
	boolean forgetConstant(String label);

}
