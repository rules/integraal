package fr.boreal.model.logicalElements.impl.identityObjects;

import fr.boreal.model.logicalElements.api.Constant;

/**
 * Implementation of a Constant where the equality test is redefined to only test java object equality on reference.
 * <br/>
 * It is recommended to use a factory to create these terms.
 *
 * @author Florent Tornil
 */
public record IdentityConstantImpl(String label) implements Constant {

	@Override
	public String toString() {
		return this.label;
	}
}
