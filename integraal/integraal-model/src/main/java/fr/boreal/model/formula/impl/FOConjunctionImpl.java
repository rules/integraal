package fr.boreal.model.formula.impl;

import java.util.Collection;

import fr.boreal.model.formula.api.CompoundFOFormula;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;

/**
 * Default implementation of FOFormulaConjunction
 * 
 * @author Florent Tornil
 *
 */
public class FOConjunctionImpl extends CompoundFOFormula implements FOFormulaConjunction {

	private static final String connector_representation = "^";

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Constructor using sub formulas
	 * @param subformulas the sub formulas
	 */
	public FOConjunctionImpl(Collection<? extends FOFormula> subformulas) {
		super(subformulas);
	}

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	@Override
	public String getConnectorRepresentation() {
		return connector_representation;
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof FOFormulaConjunction other) {
            return this.getSubElements().containsAll(other.getSubElements())
					&& other.getSubElements().containsAll(this.getSubElements());
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return this.getSubElements().stream().map(f -> f.hashCode() * this.getSubElements().size() * 17).reduce(0,
				Integer::sum);
	}
}
