package fr.boreal.model.kb.api;

/**
 * Type of column (term position) in an atom
 * 
 * @author Florent Tornil
 *
 */
public enum ColumnType {
    /**
     * String column type
     */
    STRING,
    /**
     * Integer column type
     */
    INTEGER
}
