package fr.boreal.model.logicalElements.api;

/**
 * A computed atom is an Atom with special meaning
 * <br/>
 * This kind of atom must use a full substitution of its variables to be evaluated
 *
 * @author Florent Tornil
 */
public interface ComputedAtom extends Atom {

    /**
     * Evaluate this atom
     *
     * @param s a full substitution of this atom variables
     * @param forceFunctionEvaluation true iff the evaluation of the functions in the atom must be forced
     * @return an atom
     */
    Atom eval(Substitution s, boolean forceFunctionEvaluation);

    /**
     * Evaluate this atom
     *
     * @param s a full substitution of this atom variables
     *          by default it forces the evaluation of functions used in the atom
     * @return an atom
     */
    default Atom eval(Substitution s){
        return eval(s,true);
    }

    default boolean isComputedAtom() {
        return true;
    }
}
