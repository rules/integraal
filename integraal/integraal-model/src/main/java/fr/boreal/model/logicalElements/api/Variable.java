package fr.boreal.model.logicalElements.api;

/**
 * A variable is a placeholder for another {@link Term}.
 * 
 * @author Florent Tornil
 *
 */
public interface Variable extends Term {
	
	@Override
	default boolean isVariable() {
		return true;
	}

}
