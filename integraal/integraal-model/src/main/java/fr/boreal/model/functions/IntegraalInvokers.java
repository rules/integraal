package fr.boreal.model.functions;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import fr.boreal.model.logicalElements.api.Literal;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import org.apache.commons.lang3.tuple.Pair;

/**
 * Default invokers for InteGraal.
 * This class provides a collection of invoker functions used for various operations
 * on terms such as mathematical calculations, string manipulation, set operations, etc.
 * Each invoker throws specific exceptions when encountering invalid arguments or errors.
 * @author Florent Tornil
 * @author Guillaume Pérution-Kihli
 */
public class IntegraalInvokers {

	private final TermFactory termFactory;
	private final Map<String, StdInvoker> strToStdInvoker;
	private final Map<StdInvoker, Invoker> invokerMap;

	/**
	 * Enum representing standard invokers.
	 * Each invoker corresponds to a specific function (e.g., SUM, MIN, MAX).
	 */
	public enum StdInvoker {
		SUM,
		MIN,
		MAX,
		MINUS,
		PRODUCT,
		DIVIDE,
		AVERAGE,
		MEDIAN,
		IS_EVEN,
		IS_ODD,
		IS_GREATER_THAN,
		IS_GREATER_OR_EQUALS_TO,
		IS_SMALLER_THAN,
		IS_SMALLER_OR_EQUALS_TO,
		IS_LEXICOGRAPHICALLY_GREATER_THAN,
		IS_LEXICOGRAPHICALLY_GREATER_OR_EQUALS_TO,
		IS_LEXICOGRAPHICALLY_SMALLER_THAN,
		IS_LEXICOGRAPHICALLY_SMALLER_OR_EQUALS_TO,
		IS_PRIME,
		EQUALS,
		CONCAT,
		TO_LOWER_CASE,
		TO_UPPER_CASE,
		REPLACE,
		LENGTH,
		WEIGHTED_AVERAGE,
		WEIGHTED_MEDIAN,
		SET,
		IS_SUBSET,
		IS_STRICT_SUBSET,
		UNION,
		SIZE,
		INTERSECTION,
		CONTAINS,
		IS_EMPTY,
		IS_BLANK,
		IS_NUMERIC,
		TO_STRING,
		TO_DOUBLE,
		TO_INT,
		TO_LONG,
		TO_FLOAT,
		TO_BOOLEAN,
		TO_SET,
		TO_TUPLE,
		DICT,
		MERGE_DICTS,
		DICT_KEYS,
		DICT_VALUES,
		GET,
		TUPLE,
		CONTAINS_KEY,
		CONTAINS_VALUE
	}

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Default constructor.
	 * Initializes the invokers and maps their corresponding string representations.
	 *
	 * @param tf A term factory used to create terms.
	 */
	public IntegraalInvokers(TermFactory tf) {
		this.termFactory = tf;

		strToStdInvoker = new HashMap<>();
		for (StdInvoker invoker : StdInvoker.values()) {
			strToStdInvoker.put(toCamelCase(invoker.name()), invoker);
		}

		invokerMap = new HashMap<>();
		for (Field field : getClass().getDeclaredFields()) {
			if (Invoker.class.isAssignableFrom(field.getType())) {
				StdInvoker enumConstant = strToStdInvoker.get(field.getName());
				if (enumConstant != null) {
					try {
						Invoker invoker = (Invoker) field.get(this);
						invokerMap.put(enumConstant, invoker);
					} catch (IllegalAccessException e) {
						throw new RuntimeException(e);
					}
				}
			}
		}
	}

	/**
	 * Retrieves the invoker corresponding to a given function.
	 *
	 * @param fct The name of the function.
	 * @return The invoker corresponding to the given name.
	 */
	public Invoker getInvoker(StdInvoker fct) {
		return invokerMap.get(fct);
	}

	/**
	 * Retrieves the invoker by function name as a string.
	 *
	 * @param fct The function name.
	 * @return The corresponding invoker, or null if none exists.
	 */
	public Invoker getInvoker(String fct) {
		StdInvoker stdInvoker = strToStdInvoker.get(fct);
		return stdInvoker != null ? this.getInvoker(stdInvoker) : null;
	}

	/**
	 * Retrieves all invoker names as a List of Strings.
	 *
	 * @return A list of all invoker names (in camelCase).
	 */
	public List<String> getAllInvokerNames() {
		return new ArrayList<>(strToStdInvoker.keySet());
	}
	
	/////////////////////////////////////////////////
	// Predefined Invokers
	/////////////////////////////////////////////////

	// Invoker Definitions //

	/**
	 * Sums a list of numbers.
	 * If no valid numbers are provided, it throws an InvokerException.
	 * <p>
	 * Arguments: Literals or a collection of literals representing numbers.
	 * Returns: a Literal&lt;? extends Number&gt; representing the sum of the numbers.
	 * Throws: InvokerException if no arguments are provided or if an error occurs.
	 */
	private final Invoker sum = new Invoker() {
		@Override
		public Literal<? extends Number> invoke(Term... args) {
			try {
				args = flattenIfCollection(args);
				List<Number> numbers = parseNumbers(args);

				if (numbers.isEmpty()) {
					throw new InvokerException("sum", args, "No arguments provided.");
				}

				// If all numbers are integers (including both Integer and Long)
				if (allIntegers(numbers)) {
					long sum = numbers.stream()
							.mapToLong(Number::longValue)  // Simply call longValue() on each Number
							.sum();
					return termFactory.createOrGetLiteral(sum);
				} else {
					// Otherwise, sum them as doubles
					double sum = numbers.stream()
							.mapToDouble(Number::doubleValue)
							.sum();
					return termFactory.createOrGetLiteral(sum);
				}
			} catch (Exception e) {
				throw new InvokerException("sum", args, e);
			}
		}
	};

	/**
	 * Subtracts a list of numbers.
	 * If no valid numbers are provided, it throws an InvokerException.
	 * <p>
	 * Arguments: Literals or a collection of literals representing numbers.
	 * Returns: a Literal&lt;? extends Number&gt; representing the result of the subtraction.
	 * Throws: InvokerException if no valid arguments are provided or if an error occurs.
	 */
	private final Invoker minus = new Invoker() {
		@Override
		public Literal<? extends Number> invoke(Term... args) {
			try {
				args = flattenIfCollection(args);
				List<Number> numbers = parseNumbers(args);

				if (numbers.isEmpty()) {
					throw new InvokerException("minus", args, "No arguments provided.");
				}

				// If all numbers are integers (including both Integer and Long)
				if (allIntegers(numbers)) {
					long result = numbers.get(0).longValue();
					for (int i = 1; i < numbers.size(); i++) {
						result -= numbers.get(i).longValue();
					}
					return termFactory.createOrGetLiteral(result);
				} else {
					double result = numbers.get(0).doubleValue();
					for (int i = 1; i < numbers.size(); i++) {
						result -= numbers.get(i).doubleValue();
					}
					return termFactory.createOrGetLiteral(result);
				}
			} catch (Exception e) {
				throw new InvokerException("minus", args, e);
			}
		}
	};

	/**
	 * Multiplies a list of numbers.
	 * If no valid numbers are provided, it throws an InvokerException.
	 * <p>
	 * Arguments: Literals or a collection of literals representing numbers.
	 * Returns: a Literal&lt;? extends Number&gt; representing the product of the numbers.
	 * Throws: InvokerException if no valid arguments are provided or if an error occurs.
	 */
	private final Invoker product = new Invoker() {
		@Override
		public Literal<? extends Number> invoke(Term... args) {
			try {
				args = flattenIfCollection(args);
				List<Number> numbers = parseNumbers(args);

				if (numbers.isEmpty()) {
					throw new InvokerException("product", args, "No arguments provided.");
				}

				// If all numbers are integers (including both Integer and Long)
				if (allIntegers(numbers)) {
					long result = numbers.get(0).longValue();
					for (int i = 1; i < numbers.size(); i++) {
						result *= numbers.get(i).longValue();
					}
					return termFactory.createOrGetLiteral(result);
				} else {
					double result = numbers.get(0).doubleValue();
					for (int i = 1; i < numbers.size(); i++) {
						result *= numbers.get(i).doubleValue();
					}
					return termFactory.createOrGetLiteral(result);
				}
			} catch (Exception e) {
				throw new InvokerException("product", args, e);
			}
		}
	};

	/**
	 * Divides a list of numbers.
	 * Throws an exception in case of division by zero or invalid input.
	 * <p>
	 * Arguments: Literals or a collection of literals representing numbers.
	 * Returns: a Literal&lt;Double&gt; representing the result of the division.
	 * Throws: InvokerException if division by zero occurs or if invalid input is provided.
	 */
	private final Invoker divide = new Invoker() {
		@Override
		public Literal<Double> invoke(Term... args) {
			try {
				args = flattenIfCollection(args);
				List<Number> numbers = parseNumbers(args);

				if (numbers.size() < 2) {
					throw new InvokerException("divide", args, "At least two arguments required.");
				}

				double result = numbers.getFirst().doubleValue();
				for (int i = 1; i < numbers.size(); i++) {
					double divisor = numbers.get(i).doubleValue();
					if (divisor == 0) {
						throw new InvokerException("divide", args, "Division by zero.");
					}
					result /= divisor;
				}
				return termFactory.createOrGetLiteral(result);
			} catch (Exception e) {
				throw new InvokerException("divide", args, e);
			}
		}
	};

	/**
	 * Finds the maximum value in a list of numbers.
	 * If no valid numbers are provided, it throws an InvokerException.
	 * <p>
	 * Arguments: Literals or a collection of literals representing numbers.
	 * Returns: a Literal&lt;? extends Number&gt; representing the maximum value,
	 * keeping the original type (Integer, Long, Double, etc.).
	 * Throws: InvokerException if no arguments are provided or if an error occurs.
	 */
	private final Invoker max = new Invoker() {
		@Override
		public Literal<? extends Number> invoke(Term... args) {
			try {
				args = flattenIfCollection(args);
				List<Number> numbers = parseNumbers(args);

				if (numbers.isEmpty()) {
					throw new InvokerException("max", args, "No arguments provided.");
				}

				// Find the maximum while preserving the type of the number
				Number max = numbers.getFirst();
				for (Number number : numbers) {
					if (Double.compare(number.doubleValue(), max.doubleValue()) > 0) {
						max = number;
					}
				}
				return termFactory.createOrGetLiteral(max);
			} catch (Exception e) {
				throw new InvokerException("max", args, e);
			}
		}
	};

	/**
	 * Finds the minimum value in a list of numbers.
	 * If no valid numbers are provided, it throws an InvokerException.
	 * <p>
	 * Arguments: Literals or a collection of literals representing numbers.
	 * Returns: a Literal&lt;? extends Number&gt; representing the minimum value,
	 * keeping the original type (Integer, Long, Double, etc.).
	 * Throws: InvokerException if no arguments are provided or if an error occurs.
	 */
	private final Invoker min = new Invoker() {
		@Override
		public Literal<? extends Number> invoke(Term... args) {
			try {
				args = flattenIfCollection(args);
				List<Number> numbers = parseNumbers(args);

				if (numbers.isEmpty()) {
					throw new InvokerException("min", args, "No arguments provided.");
				}

				// Find the minimum while preserving the type of the number
				Number min = numbers.get(0);
				for (Number number : numbers) {
					if (Double.compare(number.doubleValue(), min.doubleValue()) < 0) {
						min = number;
					}
				}
				return termFactory.createOrGetLiteral(min);
			} catch (Exception e) {
				throw new InvokerException("min", args, e);
			}
		}
	};

	/**
	 * Calculates the average of a list of numbers.
	 * If no valid numbers are provided, it throws an InvokerException.
	 * <p>
	 * Arguments: Literals or a collection of literals representing numbers.
	 * Returns: a Literal&lt;Double&gt; representing the average of the numbers.
	 * Throws: InvokerException if no valid arguments are provided or if an error occurs.
	 */
	private final Invoker average = new Invoker() {
		@Override
		public Literal<Double> invoke(Term... args) {
			try {
				args = flattenIfCollection(args);
				List<Number> numbers = parseNumbers(args);

				if (numbers.isEmpty()) {
					throw new InvokerException("average", args, "No arguments provided.");
				}

				double sum = numbers.stream().mapToDouble(Number::doubleValue).sum();
				double avg = sum / numbers.size();

				return termFactory.createOrGetLiteral(avg);
			} catch (Exception e) {
				throw new InvokerException("average", args, e);
			}
		}
	};

	/**
	 * Calculates the median of a list of numbers.
	 * If no valid numbers are provided, it throws an InvokerException.
	 * <p>
	 * Arguments: Literals or a collection of literals representing numbers.
	 * Returns: a Literal&lt;Double&gt; representing the median of the numbers.
	 * Throws: InvokerException if the list is empty or invalid, input is provided.
	 */
	private final Invoker median = new Invoker() {
		@Override
		public Literal<Double> invoke(Term... args) {
			try {
				args = flattenIfCollection(args);
				List<Number> numbers = parseNumbers(args);

				if (numbers.isEmpty()) {
					throw new InvokerException("median", args, "Cannot compute median for an empty set.");
				}

				List<Double> sortedNumbers = numbers.stream()
						.map(Number::doubleValue)
						.sorted()
						.toList();

				int size = sortedNumbers.size();
				double median;
				if (size % 2 == 0) {
					median = (sortedNumbers.get(size / 2 - 1) + sortedNumbers.get(size / 2)) / 2.0;
				} else {
					median = sortedNumbers.get(size / 2);
				}

				return termFactory.createOrGetLiteral(median);
			} catch (Exception e) {
				throw new InvokerException("median", args, e);
			}
		}
	};

	/**
	 * Checks if a number is even.
	 * <p>
	 * Arguments: a single literal representing a number.
	 * Returns: a Literal&lt;Boolean&gt; representing whether the number is even.
	 * Throws: InvokerException if invalid arguments are provided.
	 */
	private final Invoker isEven = new Invoker() {
		@Override
		public Literal<Boolean> invoke(Term... args) {
			if (args.length != 1 || !(args[0] instanceof Literal<?> lit)) {
				throw new InvokerException("isEven", args, "Exactly one literal argument is required.");
			}
			try {
				long value = extractLongValue(lit, args, "isEven");
				return termFactory.createOrGetLiteral(value % 2 == 0);
			} catch (Exception e) {
				throw new InvokerException("isEven", args, e);
			}
		}
	};

	/**
	 * Checks if a number is odd.
	 * <p>
	 * Arguments: a single literal representing a number.
	 * Returns: a Literal&lt;Boolean&gt; representing whether the number is odd.
	 * Throws: InvokerException if invalid arguments are provided.
	 */
	private final Invoker isOdd = new Invoker() {
		@Override
		public Literal<Boolean> invoke(Term... args) {
			if (args.length != 1 || !(args[0] instanceof Literal<?> lit)) {
				throw new InvokerException("isOdd", args, "Exactly one literal argument is required.");
			}
			try {
				long value = extractLongValue(lit, args, "isOdd");
				return termFactory.createOrGetLiteral(value % 2 != 0);
			} catch (Exception e) {
				throw new InvokerException("isOdd", args, e);
			}
		}
	};

	/**
	 * Checks if the first number is greater than the second.
	 * <p>
	 * Arguments: two literals representing numbers.
	 * Returns: a Literal&lt;Boolean&gt; representing whether the first number is greater than the second.
	 * Throws: InvokerException if invalid arguments are provided.
	 */
	private final Invoker isGreaterThan = new Invoker() {
		@Override
		public Literal<Boolean> invoke(Term... args) {
			if (args.length != 2 || !(args[0] instanceof Literal<?>) || !(args[1] instanceof Literal<?>)) {
				throw new InvokerException("isGreaterThan", args, "Exactly two literal arguments are required.");
			}
			try {
				Number num1 = extractNumber((Literal<?>) args[0], args, "isGreaterThan");
				Number num2 = extractNumber((Literal<?>) args[1], args, "isGreaterThan");

				return termFactory.createOrGetLiteral(Double.compare(num1.doubleValue(), num2.doubleValue()) > 0);
			} catch (Exception e) {
				throw new InvokerException("isGreaterThan", args, e);
			}
		}
	};

	/**
	 * Checks if the first number is greater than or equal to the second.
	 * <p>
	 * Arguments: two literals representing numbers.
	 * Returns: a Literal&lt;Boolean&gt; representing whether the first number is greater than or equal to the second.
	 * Throws: InvokerException if invalid arguments are provided.
	 */
	private final Invoker isGreaterOrEqualsTo = new Invoker() {
		@Override
		public Literal<Boolean> invoke(Term... args) {
			if (args.length != 2 || !(args[0] instanceof Literal<?>) || !(args[1] instanceof Literal<?>)) {
				throw new InvokerException("isGreaterOrEqualsTo", args, "Exactly two literal arguments are required.");
			}
			try {
				Number num1 = extractNumber((Literal<?>) args[0], args, "isGreaterOrEqualsTo");
				Number num2 = extractNumber((Literal<?>) args[1], args, "isGreaterOrEqualsTo");

				return termFactory.createOrGetLiteral(Double.compare(num1.doubleValue(), num2.doubleValue()) >= 0);
			} catch (Exception e) {
				throw new InvokerException("isGreaterOrEqualsTo", args, e);
			}
		}
	};

	/**
	 * Checks if the first number is smaller than the second.
	 * <p>
	 * Arguments: two literals representing numbers.
	 * Returns: a Literal&lt;Boolean&gt; representing whether the first number is smaller than the second.
	 * Throws: InvokerException if invalid arguments are provided.
	 */
	private final Invoker isSmallerThan = new Invoker() {
		@Override
		public Literal<Boolean> invoke(Term... args) {
			if (args.length != 2 || !(args[0] instanceof Literal<?>) || !(args[1] instanceof Literal<?>)) {
				throw new InvokerException("isSmallerThan", args, "Exactly two literal arguments are required.");
			}
			try {
				Number num1 = extractNumber((Literal<?>) args[0], args, "isSmallerThan");
				Number num2 = extractNumber((Literal<?>) args[1], args, "isSmallerThan");

				return termFactory.createOrGetLiteral(Double.compare(num1.doubleValue(), num2.doubleValue()) < 0);
			} catch (Exception e) {
				throw new InvokerException("isSmallerThan", args, e);
			}
		}
	};

	/**
	 * Checks if the first number is smaller than or equal to the second.
	 * <p>
	 * Arguments: two literals representing numbers.
	 * Returns: a Literal&lt;Boolean&gt; representing whether the first number is smaller than or equal to the second.
	 * Throws: InvokerException if invalid arguments are provided.
	 */
	private final Invoker isSmallerOrEqualsTo = new Invoker() {
		@Override
		public Literal<Boolean> invoke(Term... args) {
			if (args.length != 2 || !(args[0] instanceof Literal<?>) || !(args[1] instanceof Literal<?>)) {
				throw new InvokerException("isSmallerOrEqualsTo", args, "Exactly two literal arguments are required.");
			}
			try {
				Number num1 = extractNumber((Literal<?>) args[0], args, "isSmallerOrEqualsTo");
				Number num2 = extractNumber((Literal<?>) args[1], args, "isSmallerOrEqualsTo");

				return termFactory.createOrGetLiteral(Double.compare(num1.doubleValue(), num2.doubleValue()) <= 0);
			} catch (Exception e) {
				throw new InvokerException("isSmallerOrEqualsTo", args, e);
			}
		}
	};

	/**
	 * Checks if the first string is lexicographically greater than the second.
	 * <p>
	 * Arguments: two literals representing strings.
	 * Returns: a Literal&lt;Boolean&gt; representing whether the first string is lexicographically greater.
	 * Throws: InvokerException if invalid arguments are provided.
	 */
	private final Invoker isLexicographicallyGreaterThan = new Invoker() {
		@Override
		public Literal<Boolean> invoke(Term... args) {
			if (args.length != 2 || !(args[0] instanceof Literal<?>) || !(args[1] instanceof Literal<?>)) {
				throw new InvokerException("isLexicographicallyGreaterThan", args, "Exactly two literal arguments are required.");
			}
			try {
				String str1 = extractString((Literal<?>) args[0], args, "isLexicographicallyGreaterThan");
				String str2 = extractString((Literal<?>) args[1], args, "isLexicographicallyGreaterThan");

				return termFactory.createOrGetLiteral(str1.compareTo(str2) > 0);
			} catch (Exception e) {
				throw new InvokerException("isLexicographicallyGreaterThan", args, e);
			}
		}
	};

	/**
	 * Checks if the first string is lexicographically greater than or equal to the second.
	 * <p>
	 * Arguments: two literals representing strings.
	 * Returns: a Literal&lt;Boolean&gt; representing whether the first string is lexicographically greater or equal.
	 * Throws: InvokerException if invalid arguments are provided.
	 */
	private final Invoker isLexicographicallyGreaterOrEqualsTo = new Invoker() {
		@Override
		public Literal<Boolean> invoke(Term... args) {
			if (args.length != 2 || !(args[0] instanceof Literal<?>) || !(args[1] instanceof Literal<?>)) {
				throw new InvokerException("isLexicographicallyGreaterOrEqualsTo", args, "Exactly two literal arguments are required.");
			}
			try {
				String str1 = extractString((Literal<?>) args[0], args, "isLexicographicallyGreaterOrEqualsTo");
				String str2 = extractString((Literal<?>) args[1], args, "isLexicographicallyGreaterOrEqualsTo");

				return termFactory.createOrGetLiteral(str1.compareTo(str2) >= 0);
			} catch (Exception e) {
				throw new InvokerException("isLexicographicallyGreaterOrEqualsTo", args, e);
			}
		}
	};

	/**
	 * Checks if the first string is lexicographically smaller than the second.
	 * <p>
	 * Arguments: two literals representing strings.
	 * Returns: a Literal&lt;Boolean&gt; representing whether the first string is lexicographically smaller.
	 * Throws: InvokerException if invalid arguments are provided.
	 */
	private final Invoker isLexicographicallySmallerThan = new Invoker() {
		@Override
		public Literal<Boolean> invoke(Term... args) {
			if (args.length != 2 || !(args[0] instanceof Literal<?>) || !(args[1] instanceof Literal<?>)) {
				throw new InvokerException("isLexicographicallySmallerThan", args, "Exactly two literal arguments are required.");
			}
			try {
				String str1 = extractString((Literal<?>) args[0], args, "isLexicographicallySmallerThan");
				String str2 = extractString((Literal<?>) args[1], args, "isLexicographicallySmallerThan");

				return termFactory.createOrGetLiteral(str1.compareTo(str2) < 0);
			} catch (Exception e) {
				throw new InvokerException("isLexicographicallySmallerThan", args, e);
			}
		}
	};

	/**
	 * Checks if the first string is lexicographically smaller than or equal to the second.
	 * <p>
	 * Arguments: two literals representing strings.
	 * Returns: a Literal&lt;Boolean&gt; representing whether the first string is lexicographically smaller or equal.
	 * Throws: InvokerException if invalid arguments are provided.
	 */
	private final Invoker isLexicographicallySmallerOrEqualsTo = new Invoker() {
		@Override
		public Literal<Boolean> invoke(Term... args) {
			if (args.length != 2 || !(args[0] instanceof Literal<?>) || !(args[1] instanceof Literal<?>)) {
				throw new InvokerException("isLexicographicallySmallerOrEqualsTo", args, "Exactly two literal arguments are required.");
			}
			try {
				String str1 = extractString((Literal<?>) args[0], args, "isLexicographicallySmallerOrEqualsTo");
				String str2 = extractString((Literal<?>) args[1], args, "isLexicographicallySmallerOrEqualsTo");

				return termFactory.createOrGetLiteral(str1.compareTo(str2) <= 0);
			} catch (Exception e) {
				throw new InvokerException("isLexicographicallySmallerOrEqualsTo", args, e);
			}
		}
	};

	/**
	 * Checks if a number is prime.
	 * <p>
	 * Arguments: a single term representing an integer.
	 * Returns: a Literal&lt;Boolean&gt; representing whether the number is prime.
	 * Throws: InvokerException if invalid arguments are provided.
	 */
	private final Invoker isPrime = new Invoker() {
		private boolean isPrime(int n) {
			if (n <= 1) return false;
			if (n <= 3) return true;
			if (n % 2 == 0 || n % 3 == 0) return false;
			for (int i = 5; i * i <= n; i += 6) {
				if (n % i == 0 || n % (i + 2) == 0) return false;
			}
			return true;
		}

		@Override
		public Literal<Boolean> invoke(Term... args) {
			if (args.length != 1 || !(args[0] instanceof Literal<?> lit)) {
				throw new InvokerException("isPrime", args, "Exactly one literal argument required.");
			}

			try {
				Number num = extractLongValue(lit, args, "isPrime");

				return termFactory.createOrGetLiteral(isPrime(num.intValue()));
			} catch (Exception e) {
				throw new InvokerException("isPrime", args, e);
			}
		}
	};

	/**
	 * Checks if all terms are equal.
	 * <p>
	 * Arguments: two or more terms.
	 * Returns: a Literal&lt;Boolean&gt; representing whether all terms are equal.
	 * Throws: InvokerException if invalid arguments are provided.
	 */
	private final Invoker equals = new Invoker() {
		@Override
		public Literal<Boolean> invoke(Term... args) {
			if (args.length < 2) {
				throw new InvokerException("equals", args, "At least two arguments required.");
			}

			Term first = args[0];
			boolean allEqual = Arrays.stream(args).allMatch(t -> t.equals(first));
			return termFactory.createOrGetLiteral(allEqual);
		}
	};

	/**
	 * Concatenates two strings or two lists.
	 * <p>
	 * Arguments: two literals representing strings or lists/tuples.
	 * Returns: a Literal representing the concatenated result.
	 * Throws: InvokerException if invalid arguments are provided.
	 */
	private final Invoker concat = new Invoker() {
		@Override
		public Literal<?> invoke(Term... args) {
			if (args.length != 2 || !(args[0] instanceof Literal<?>) || !(args[1] instanceof Literal<?>)) {
				throw new InvokerException("concat", args, "Exactly two literal arguments required.");
			}

			try {
				Object value1 = ((Literal<?>) args[0]).value();
				Object value2 = ((Literal<?>) args[1]).value();

				if (value1 instanceof List<?> list1 && value2 instanceof List<?> list2) {
					List<Object> concatenatedList = new ArrayList<>(list1);
					concatenatedList.addAll(list2);
					return termFactory.createOrGetLiteral(concatenatedList);
				} else if (value1 instanceof String && value2 instanceof String) {
					String concatenatedString = value1 + (String) value2;
					return termFactory.createOrGetLiteral(concatenatedString);
				} else {
					throw new InvokerException("concat", args, "Both arguments must be strings or lists/tuples.");
				}
			} catch (Exception e) {
				throw new InvokerException("concat", args, e);
			}
		}
	};

	/**
	 * Converts a string to a lower case.
	 * <p>
	 * Arguments: one literal representing a string.
	 * Returns: a Literal&lt;String&gt; in lower case.
	 * Throws: InvokerException if invalid arguments are provided.
	 */
	private final Invoker toLowerCase = new Invoker() {
		@Override
		public Literal<String> invoke(Term... args) {
			if (args.length != 1 || !(args[0] instanceof Literal<?> lit)) {
				throw new InvokerException("toLowerCase", args, "Exactly one literal argument required.");
			}
			try {
				String value = extractString(lit, args, "toLowerCase");
				return termFactory.createOrGetLiteral(value.toLowerCase());
			} catch (Exception e) {
				throw new InvokerException("toLowerCase", args, e);
			}
		}
	};

	/**
	 * Converts a string to an upper case.
	 * <p>
	 * Arguments: one literal representing a string.
	 * Returns: a Literal&lt;String&gt; in upper case.
	 * Throws: InvokerException if invalid arguments are provided.
	 */
	private final Invoker toUpperCase = new Invoker() {
		@Override
		public Literal<String> invoke(Term... args) {
			if (args.length != 1 || !(args[0] instanceof Literal<?> lit)) {
				throw new InvokerException("toUpperCase", args, "Exactly one literal argument required.");
			}
			try {
				String value = extractString(lit, args, "toUpperCase");
				return termFactory.createOrGetLiteral(value.toUpperCase());
			} catch (Exception e) {
				throw new InvokerException("toUpperCase", args, e);
			}
		}
	};

	/**
	 * Replaces occurrences of a substring within a string.
	 * <p>
	 * Arguments: three literals representing the target string, the string to be replaced, and the replacement.
	 * Returns: a Literal&lt;String&gt; representing the modified string.
	 * Throws: InvokerException if invalid arguments are provided.
	 */
	private final Invoker replace = new Invoker() {
		@Override
		public Literal<String> invoke(Term... args) {
			if (args.length != 3
					|| !(args[0] instanceof Literal<?> litTarget)
					|| !(args[1] instanceof Literal<?> litOld)
					|| !(args[2] instanceof Literal<?> litNew)) {
				throw new InvokerException("replace", args, "Exactly three literal arguments required.");
			}
			try {
				String target = extractString(litTarget, args, "replace");
				String oldStr = extractString(litOld, args, "replace");
				String newStr = extractString(litNew, args, "replace");
				return termFactory.createOrGetLiteral(target.replace(oldStr, newStr));
			} catch (Exception e) {
				throw new InvokerException("replace", args, e);
			}
		}
	};

	/**
	 * Returns the length of a string.
	 * <p>
	 * Arguments: one literal representing a string.
	 * Returns: a Literal&lt;Integer&gt; representing the length of the string.
	 * Throws: InvokerException if invalid arguments are provided.
	 */
	private final Invoker length = new Invoker() {
		@Override
		public Literal<Integer> invoke(Term... args) {
			if (args.length != 1 || !(args[0] instanceof Literal<?> lit)) {
				throw new InvokerException("length", args, "Exactly one literal argument required.");
			}
			try {
				String value = extractString(lit, args, "length");
				return termFactory.createOrGetLiteral(value.length());
			} catch (Exception e) {
				throw new InvokerException("length", args, e);
			}
		}
	};

	/**
	 * Calculates the weighted average of a list of value-weight pairs.
	 * <p>
	 * Arguments:
	 * - Either a collection of literals, or a list of literals.
	 * - These literals can be:
	 *   - Tuples of size 2 (value, weight),
	 *   - Or alternating value and weight literals (value1, weight1, value2, weight2, ...).
	 * <p>
	 * Returns: A Literal&lt;Double&gt; representing the weighted average.
	 * Throws: InvokerException if invalid arguments are provided.
	 *
	 * <p>Examples:</p>
	 * <ul>
	 *   <li><code>weightedAverage(tuple(3, 1), tuple(6, 3))</code> will return 5.25 (weighted average).</li>
	 *   <li><code>weightedAverage(3, 1, 6, 3)</code> will return the same result: 5.25.</li>
	 *   <li><code>weightedAverage(tuple(tuple(3, 1), tuple(6, 3)))</code> will return 5.25 when passed as
	 *   	a collection.</li>
	 *   <li><code>weightedAverage(tuple(3, 1, 6, 3))</code> will also return 5.25 when passed as a collection of
	 *   	alternating values and weights.</li>
	 * </ul>
	 */
	private final Invoker weightedAverage = new Invoker() {
		@Override
		public Literal<Double> invoke(Term... args) {
			args = flattenIfCollection(args);  // Handle the case where arguments are provided as a collection.
			double sum = 0;
			double totalWeight = 0;

			// Handle cases with tuples or alternating value and weight arguments
			if (areAllTuples(args)) {
				for (Term tuple : args) {
					List<?> pair = (List<?>) ((Literal<?>) tuple).value();
					double value = extractNumber((Literal<?>) pair.get(0), args, "weightedAverage").doubleValue();
					double weight = extractNumber((Literal<?>) pair.get(1), args, "weightedAverage").doubleValue();
					sum += value * weight;
					totalWeight += Math.abs(weight);
				}
			} else {
				if (args.length % 2 != 0) {
					throw new InvokerException("weightedAverage", args, "Arguments must be in value-weight pairs.");
				}
				for (int i = 0; i < args.length; i += 2) {
					double value = extractNumber((Literal<?>) args[i], args, "weightedAverage").doubleValue();
					double weight = extractNumber((Literal<?>) args[i + 1], args, "weightedAverage").doubleValue();
					sum += value * weight;
					totalWeight += Math.abs(weight);
				}
			}

			if (totalWeight == 0) {
				throw new InvokerException("weightedAverage", args, "Total weight cannot be zero.");
			}

			return termFactory.createOrGetLiteral(sum / totalWeight);
		}
	};

	/**
	 * Calculates the weighted median of a list of value-weight pairs.
	 * <p>
	 * Arguments:
	 * - Either a collection of literals, or a list of literals.
	 * - These literals can be:
	 *   - Tuples of size 2 (value, weight),
	 *   - Or alternating value and weight literals (value1, weight1, value2, weight2, ...).
	 * <p>
	 * Returns: A Literal&lt;Double&gt; representing the weighted median.
	 * Throws: InvokerException if invalid arguments are provided.
	 *
	 * <p>Examples:</p>
	 * <ul>
	 *   <li><code>weightedMedian(tuple(3, 1), tuple(6, 3))</code> will return 6 (weighted median).</li>
	 *   <li><code>weightedMedian(3, 1, 6, 3)</code> will return the same result: 6.</li>
	 *   <li><code>weightedMedian(tuple(tuple(3, 1), tuple(6, 3)))</code> will return 6 when passed as a collection of
	 *   	tuples.</li>
	 *   <li><code>weightedMedian(tuple(3, 1, 6, 3))</code> will return 6 when passed as a collection of alternating
	 *   	values and weights.</li>
	 * </ul>
	 */
	private final Invoker weightedMedian = new Invoker() {
		@Override
		public Literal<Double> invoke(Term... args) {
			args = flattenIfCollection(args);  // Handle case where arguments are provided as a collection.

			List<Pair<Double, Double>> valuesAndWeights = new ArrayList<>();

			// Handle cases with tuples or alternating value and weight arguments
			if (areAllTuples(args)) {
				for (Term tuple : args) {
					List<?> pair = (List<?>) ((Literal<?>) tuple).value();
					double value = extractNumber((Literal<?>) pair.get(0), args, "weightedMedian").doubleValue();
					double weight = extractNumber((Literal<?>) pair.get(1), args, "weightedMedian").doubleValue();
					if (weight < 0) {
						throw new InvokerException("weightedMedian", args, "Weights cannot be negative.");
					}
					valuesAndWeights.add(Pair.of(value, weight));
				}
			} else {
				if (args.length % 2 != 0) {
					throw new InvokerException("weightedMedian", args, "Arguments must be in value-weight pairs.");
				}
				for (int i = 0; i < args.length; i += 2) {
					double value = extractNumber((Literal<?>) args[i], args, "weightedMedian").doubleValue();
					double weight = extractNumber((Literal<?>) args[i + 1], args, "weightedMedian").doubleValue();
					if (weight < 0) {
						throw new InvokerException("weightedMedian", args, "Weights cannot be negative.");
					}
					valuesAndWeights.add(Pair.of(value, weight));
				}
			}

			valuesAndWeights.sort(Map.Entry.comparingByKey());

			double totalWeight = valuesAndWeights.stream().mapToDouble(Pair::getValue).sum();
			if (totalWeight == 0) {
				throw new InvokerException("weightedMedian", args, "Total weight cannot be zero.");
			}

			double halfWeight = totalWeight / 2;
			double cumulativeWeight = 0;
			for (int i = 0; i < valuesAndWeights.size(); i++) {
				Pair<Double, Double> pair = valuesAndWeights.get(i);
				cumulativeWeight += pair.getValue();

				if (cumulativeWeight > halfWeight) {
					return termFactory.createOrGetLiteral(pair.getKey());
				}

				// If cumulative weight equals exactly half, we need to average the current and next value
				if (cumulativeWeight == halfWeight && i + 1 < valuesAndWeights.size()) {
					double nextValue = valuesAndWeights.get(i + 1).getKey();
					double average = (pair.getKey() + nextValue) / 2.0;
					return termFactory.createOrGetLiteral(average);
				}
			}

			throw new InvokerException("weightedMedian", args, "Could not compute weighted median.");
		}
	};

	/**
	 * Creates a set from a list of terms.
	 * <p>
	 * Arguments: some terms
	 * Returns: a Literal&lt;Set&gt; representing the set of terms.
	 * Throws: InvokerException if invalid arguments are provided.
	 */
	private final Invoker set = new Invoker() {
		@Override
		public Literal<Set<Term>> invoke(Term... args) {
			Set<Term> termSet = new HashSet<>(Arrays.asList(args));
			return termFactory.createOrGetLiteral(termSet);
		}
	};

	/**
	 * Returns the size of a collection or map.
	 * <p>
	 * Arguments: one literal representing a collection or a map.
	 * Returns: a Literal&lt;Integer&gt; representing the size.
	 * Throws: InvokerException if invalid arguments are provided.
	 */
	private final Invoker size = new Invoker() {
		@Override
		public Literal<Integer> invoke(Term... args) {
			if (args.length != 1 || !(args[0] instanceof Literal<?> lit)) {
				throw new InvokerException("size", args, "Exactly one literal argument required.");
			}

			Object value = lit.value();
			if (value instanceof Collection<?> collection) {
				return termFactory.createOrGetLiteral(collection.size());
			} else if (value instanceof Map<?, ?> map) {
				return termFactory.createOrGetLiteral(map.size());
			} else {
				throw new InvokerException("size", args, "Argument must be a collection or a map.");
			}
		}
	};

	/**
	 * Computes the intersection of multiple sets.
	 * <p>
	 * Arguments: One or more literals representing sets.
	 * Returns: a Literal&lt;Set&gt; representing the intersection of all sets provided.
	 * Throws: InvokerException if any argument is not a set.
	 *
	 * <p>Examples:</p>
	 * <ul>
	 *   <li><code>intersection(set(1, 2), set(2, 3))</code> will return <code>{2}</code>.</li>
	 *   <li><code>intersection(set(1, 2), set(2, 3), set(2, 4))</code> will return <code>{2}</code>.</li>
	 *   <li><code>intersection(tuple(set(1, 2), set(2, 3), set(2, 4)))</code> will return <code>{2}</code> when passed
	 *   	as a collection.</li>
	 * </ul>
	 */
	private final Invoker intersection = new Invoker() {
		@Override
		public Literal<Set<Object>> invoke(Term... args) {
			args = flattenIfCollection(args);  // Handle the case where arguments are provided as a collection.

			if (args.length == 0) {
				throw new InvokerException("intersection", args, "At least one set is required.");
			}

			Set<Object> intersectionSet = null;

			for (Term arg : args) {
				if (!(arg instanceof Literal<?> lit) || !(lit.value() instanceof Collection<?> currentSet)) {
					throw new InvokerException("intersection", args, "All arguments must be sets.");
				}

                if (intersectionSet == null) {
					intersectionSet = new HashSet<>(currentSet);  // Initialize with the first set
				} else {
					intersectionSet.retainAll(currentSet);  // Retain only common elements
				}
			}

			return termFactory.createOrGetLiteral(intersectionSet);
		}
	};

	/**
	 * Computes the union of multiple sets.
	 * <p>
	 * Arguments: One or more literals representing sets.
	 * Returns: a Literal&lt;Set&gt; representing the union of all sets provided.
	 * Throws: InvokerException if any argument is not a set.
	 *
	 * <p>Examples:</p>
	 * <ul>
	 *   <li><code>union(set(1, 2), set(3, 4))</code> will return <code>{1, 2, 3, 4}</code>.</li>
	 *   <li><code>union(set(1, 2), set(2, 3), set(3, 4))</code> will return <code>{1, 2, 3, 4}</code>.</li>
	 *   <li><code>union(tuple(set(1, 2), set(3, 4)))</code> will return <code>{1, 2, 3, 4}</code> when passed as a
	 *   	collection.</li>
	 * </ul>
	 */
	private final Invoker union = new Invoker() {
		@Override
		public Literal<Set<Object>> invoke(Term... args) {
			args = flattenIfCollection(args);  // Handle the case where arguments are provided as a collection.

			Set<Object> unionSet = new HashSet<>();

			for (Term arg : args) {
				if (!(arg instanceof Literal<?> lit) || !(lit.value() instanceof Collection<?>)) {
					throw new InvokerException("union", args, "All arguments must be sets.");
				}

				unionSet.addAll((Collection<?>) lit.value());
			}

			return termFactory.createOrGetLiteral(unionSet);
		}
	};

	/**
	 * Checks if the first set is a subset of the second set.
	 * <p>
	 * Arguments: Two literals representing sets.
	 * Returns: A Literal&lt;Boolean&gt; indicating whether the first set is a subset of the second.
	 * Throws: InvokerException if the arguments are not valid sets.
	 */
	private final Invoker isSubset = new Invoker() {
		@Override
		public Literal<Boolean> invoke(Term... args) {
			if (args.length != 2
					|| !(args[0] instanceof Literal<?> lit1)
					|| !(lit1.value() instanceof Set<?> set1)
					|| !(args[1] instanceof Literal<?> lit2)
					|| !(lit2.value() instanceof Set<?> set2)) {
				throw new InvokerException("isSubset", args, "Exactly two set arguments are required.");
			}

			boolean isStrictSubset = set1.size() <= set2.size() && set2.containsAll(set1);
			return termFactory.createOrGetLiteral(isStrictSubset);
		}
	};

	/**
	 * Checks if the first set is a strict subset of the second set.
	 * <p>
	 * Arguments: Two literals representing sets.
	 * Returns: A Literal&lt;Boolean&gt; indicating whether the first set is a strict subset of the second.
	 * Throws: InvokerException if the arguments are not valid sets.
	 */
	private final Invoker isStrictSubset = new Invoker() {
		@Override
		public Literal<Boolean> invoke(Term... args) {
			if (args.length != 2
					|| !(args[0] instanceof Literal<?> lit1)
					|| !(lit1.value() instanceof Set<?> set1)
					|| !(args[1] instanceof Literal<?> lit2)
					|| !(lit2.value() instanceof Set<?> set2)) {
				throw new InvokerException("isStrictSubset", args, "Exactly two set arguments are required.");
			}

			boolean isStrictSubset = set1.size() < set2.size() && set2.containsAll(set1);
			return termFactory.createOrGetLiteral(isStrictSubset);
		}
	};

	/**
	 * Checks if a collection (e.g., a set or tuple) or a string contains a specific element or substring.
	 * <p>
	 * Arguments:
	 * - If the first argument is a literal representing a collection, the second argument should be a term
	 * 	representing the element to check within the collection.
	 * - If the first argument is a literal representing a string, the second argument should be a literal
	 * 	representing a substring to check within the string.
	 * </p>
	 * <p>
	 * Returns: a Literal&lt;Boolean&gt; indicating if the element or substring is present in the collection or string.
	 * </p>
	 * <p>
	 * Throws: InvokerException if the first argument is not a collection or string, or if the second argument is invalid for the given first argument type.
	 * </p>
	 */
	private final Invoker contains = new Invoker() {
		@Override
		public Literal<Boolean> invoke(Term... args) {
			if (args.length != 2 || !(args[0] instanceof Literal<?> lit)) {
				throw new InvokerException("contains", args, "The first argument must be a collection or a string.");
			}

			Object value = lit.value();
			if (value instanceof Collection<?> collection) {
				// Handle collection
				return termFactory.createOrGetLiteral(collection.contains(args[1]));
			} else if (value instanceof String string) {
				// Handle string case, expecting args[1] to be a string or a char
				if (!(args[1] instanceof Literal<?> element) || !(element.value() instanceof CharSequence)) {
					throw new InvokerException("contains", args, "The second argument must be a string for string containment.");
				}
				return termFactory.createOrGetLiteral(string.contains((String) element.value()));
			} else {
				throw new InvokerException("contains", args, "The first argument must be a collection or a string.");
			}
		}
	};


	/**
	 * Creates a dictionary from a list of key-value pairs (tuples).
	 * <p>
	 * Arguments: Some literals, each being a tuple (list) of two elements: a key and a value.
	 * Returns: A Literal&lt;Map&gt; representing the dictionary (mapping of keys to values).
	 * Throws: InvokerException if the arguments are not valid tuples of size 2.
	 * <p>
	 * Examples:
	 * <ul>
	 *     <li><code>dict(("key1", "value1"), ("key2", "value2"))</code> returns a dictionary with two key-value
	 *     	pairs: {key1=value1, key2=value2}</li>
	 * </ul>
	 */
	private final Invoker dict = new Invoker() {
		@Override
		public Literal<Map<Term, Term>> invoke(Term... args) {
			Map<Term, Term> dictionary = new HashMap<>();

			for (Term arg : args) {
				if (!(arg instanceof Literal<?> lit) || !(lit.value() instanceof List<?> pair) || pair.size() != 2) {
					throw new InvokerException("dict", args, "Each argument must be a tuple (list) of size 2.");
				}

				Term key = (Term) pair.get(0);
				Term value = (Term) pair.get(1);
				dictionary.put(key, value);
			}

			return termFactory.createOrGetLiteral(dictionary);
		}
	};

	/**
	 * Merges two dictionaries into one.
	 * <p>
	 * Arguments: Two literals representing dictionaries.
	 * Returns: A Literal&lt;Map&gt; representing the merged dictionary.
	 * Throws: InvokerException if the arguments are not valid dictionaries.
	 */
	private final Invoker mergeDicts = new Invoker() {
		@Override
		public Literal<Map<Term, Term>> invoke(Term... args) {
			if (args.length != 2
					|| !(args[0] instanceof Literal<?> lit1)
					|| !(lit1.value() instanceof Map<?, ?> dict1)
					|| !(args[1] instanceof Literal<?> lit2)
					|| !(lit2.value() instanceof Map<?, ?> dict2)) {
				throw new InvokerException("mergeDicts", args, "Arguments must be dictionaries.");
			}

			Map<Term, Term> mergedDict = new HashMap<>((Map<Term, Term>) dict1);
			mergedDict.putAll((Map<Term, Term>) dict2);
			return termFactory.createOrGetLiteral(mergedDict);
		}
	};

	/**
	 * Retrieves the keys from a dictionary (map).
	 * <p>
	 * Arguments: A single literal representing a dictionary (map).
	 * Returns: A Literal&lt;Set&gt; representing the set of keys from the dictionary.
	 * Throws: InvokerException if the argument is not a valid dictionary.
	 * <p>
	 * Examples:
	 * <ul>
	 *     <li><code>dictKeys(dict(("key1", "value1"), ("key2", "value2")))</code> returns the set of keys:
	 *     	{"key1", "key2"}</li>
	 *     <li><code>dictKeys(dict())</code> returns an empty set: {}</li>
	 * </ul>
	 */
	private final Invoker dictKeys = new Invoker() {
		@Override
		public Literal<Set<?>> invoke(Term... args) {
			if (args.length != 1 || !(args[0] instanceof Literal<?> lit) || !(lit.value() instanceof Map<?, ?> dict)) {
				throw new InvokerException("dictKeys", args,
						"Argument must be a literal containing a dictionary (map).");
			}

			Set<?> keys = dict.keySet();
			return termFactory.createOrGetLiteral(keys);
		}
	};

	/**
	 * Retrieves the values from a dictionary (map).
	 * <p>
	 * Arguments: A single literal representing a dictionary (map).
	 * Returns: A Literal&lt;Collection&gt; representing the collection of values from the dictionary.
	 * Throws: InvokerException if the argument is not a valid dictionary.
	 * <p>
	 * Examples:
	 * <ul>
	 *     <li><code>dictValues(dict(("key1", "value1"), ("key2", "value2")))</code> returns the collection of values:
	 *     	{"value1", "value2"}</li>
	 *     <li><code>dictValues(dict())</code> returns an empty collection: {}</li>
	 * </ul>
	 */
	private final Invoker dictValues = new Invoker() {
		@Override
		public Literal<Collection<?>> invoke(Term... args) {
			if (args.length != 1 || !(args[0] instanceof Literal<?> lit) || !(lit.value() instanceof Map<?, ?> dict)) {
				throw new InvokerException("dictValues", args, "Argument must be a literal containing a dictionary (map).");
			}

			Collection<?> values = dict.values();
			return termFactory.createOrGetLiteral(values);
		}
	};

	/**
	 * Retrieves a value from a list or map by index or key.
	 * <p>
	 * Arguments: a literal representing a list/tuple or map, and an index or key.
	 * Returns: A Literal representing the value at the index or key.
	 * Throws: InvokerException if the arguments are invalid.
	 * <p>
	 * Examples:
	 * <ul>
	 *     <li><code>get(tuple(1, 2, 3), 1)</code> returns 2</li>
	 *     <li><code>get(dict(("key1", "value1"), ("key2", "value2")), "key1")</code> returns "value1"</li>
	 * </ul>
	 */
	private final Invoker get = new Invoker() {
		@Override
		public Term invoke(Term... args) {
			if (args.length != 2 || !(args[0] instanceof Literal<?> container)) {
				throw new InvokerException("get", args, "Must provide a literal container and an index or key.");
			}

			Object containerValue = getLiteralValue(container);
			if (containerValue instanceof List<?> list) {
				try {
					int index = extractLongValue((Literal<?>) args[1], args, "get").intValue();
					if (index < 0 || index >= list.size()) {
						throw new InvokerException("get", args, "Index out of bounds.");
					}
					return (Term) list.get(index);
				} catch (Exception e) {
					throw new InvokerException("get", args, "Invalid index.", e);
				}
			}

			if (containerValue instanceof Map<?, ?> dict) {
				return (Term) dict.get(args[1]);
			}

			throw new InvokerException("get", args, "First argument must be a list or a map.");
		}
	};

	/**
	 * Creates a tuple from the given arguments.
	 * <p>
	 * Arguments: any number of terms.
	 * Returns: A Literal representing the tuple (a list).
	 * <p>
	 * Example:
	 * <ul>
	 *     <li><code>tuple("a", 1, "b", 2)</code> returns the tuple: ["a", 1, "b", 2]</li>
	 * </ul>
	 */
	private final Invoker tuple = new Invoker() {
		@Override
		public Literal<List<Term>> invoke(Term... args) {
			List<Term> tuple = Arrays.asList(args);
			return termFactory.createOrGetLiteral(tuple);
		}
	};

	/**
	 * Checks if a map contains a specific key.
	 * <p>
	 * Arguments: a literal representing a map and a key.
	 * Returns: A Literal&lt;Boolean&gt; indicating whether the key exists in the map.
	 * <p>
	 * Example:
	 * <ul>
	 *     <li><code>containsKey(dict(("key1", "value1"), ("key2", "value2")), "key1")</code> returns true</li>
	 *     <li><code>containsKey(dict(("key1", "value1"), ("key2", "value2")), "key3")</code> returns false</li>
	 * </ul>
	 */
	private final Invoker containsKey = new Invoker() {
		@Override
		public Literal<Boolean> invoke(Term... args) {
			if (args.length != 2 || !(args[0] instanceof Literal<?> lit) || !(lit.value() instanceof Map<?, ?> dict)) {
				throw new InvokerException("containsKey", args, "First argument must be a map.");
			}

			return termFactory.createOrGetLiteral(dict.containsKey(args[1]));
		}
	};

	/**
	 * Checks if a map contains a specific value.
	 * <p>
	 * Arguments: a literal representing a map and a value.
	 * Returns: A Literal&lt;Boolean&gt; indicating whether the value exists in the map.
	 * <p>
	 * Example:
	 * <ul>
	 *     <li><code>containsValue(dict(("key1", "value1"), ("key2", "value2")), "value1")</code> returns true</li>
	 *     <li><code>containsValue(dict(("key1", "value1"), ("key2", "value2")), "value3")</code> returns false</li>
	 * </ul>
	 */
	private final Invoker containsValue = new Invoker() {
		@Override
		public Literal<Boolean> invoke(Term... args) {
			if (args.length != 2 || !(args[0] instanceof Literal<?> lit) || !(lit.value() instanceof Map<?, ?> dict)) {
				throw new InvokerException("containsValue", args, "First argument must be a map.");
			}

			return termFactory.createOrGetLiteral(dict.containsValue(args[1]));
		}
	};

	/**
	 * Checks if a collection or string is empty.
	 * <p>
	 * Arguments: a literal representing a collection or string.
	 * Returns: A Literal&lt;Boolean&gt; indicating whether the collection or string is empty.
	 * <p>
	 * Examples:
	 * <ul>
	 *     <li><code>isEmpty(list())</code> returns true</li>
	 *     <li><code>isEmpty("hello")</code> returns false</li>
	 *     <li><code>isEmpty(set("hello"))</code> returns false</li>
	 * </ul>
	 */
	private final Invoker isEmpty = new Invoker() {
		@Override
		public Literal<Boolean> invoke(Term... args) {
			if (args.length != 1 || !(args[0] instanceof Literal<?> lit)) {
				throw new InvokerException("isEmpty", args, "Must provide a single literal argument.");
			}

			Object value = lit.value();
			if (value instanceof Collection<?> collection) {
				return termFactory.createOrGetLiteral(collection.isEmpty());
			} else if (value instanceof String string) {
				return termFactory.createOrGetLiteral(string.isEmpty());
			}

			throw new InvokerException("isEmpty", args, "Argument must be a collection or a string.");
		}
	};

	/**
	 * Checks if a string is blank.
	 * <p>
	 * Arguments: a literal representing a string.
	 * Returns: A Literal&lt;Boolean&gt; indicating whether the string is blank.
	 * <p>
	 * Example:
	 * <ul>
	 *     <li><code>isBlank(" ")</code> returns true</li>
	 *     <li><code>isBlank("\n")</code> returns true</li>
	 *     <li><code>isBlank("")</code> returns true</li>
	 *     <li><code>isBlank("hello")</code> returns false</li>
	 * </ul>
	 */
	private final Invoker isBlank = new Invoker() {
		@Override
		public Literal<Boolean> invoke(Term... args) {
			if (args.length != 1 || !(args[0] instanceof Literal<?> lit)) {
				throw new InvokerException("isBlank", args, "Must provide a single literal argument.");
			}

			Object value = lit.value();
			if (value instanceof String string) {
				return termFactory.createOrGetLiteral(string.isBlank());
			}

			throw new InvokerException("isBlank", args, "Argument must be a string.");
		}
	};

	/**
	 * Checks if a value is numeric (either a number or a numeric string).
	 * <p>
	 * Arguments: a literal.
	 * Returns: A Literal&lt;Boolean&gt; indicating whether the value is numeric.
	 * <p>
	 * Example:
	 * <ul>
	 *     <li><code>isNumeric("123")</code> returns true</li>
	 * </ul>
	 */
	private final Invoker isNumeric = new Invoker() {
		@Override
		public Literal<Boolean> invoke(Term... args) {
			if (args.length != 1 || !(args[0] instanceof Literal<?> lit)) {
				throw new InvokerException("isNumeric", args, "Must provide a single literal argument.");
			}

			Object value = lit.value();
			if (value instanceof Number) {
				return termFactory.createOrGetLiteral(true);
			} else if (value instanceof String string) {
				try {
					Double.parseDouble(string);
					return termFactory.createOrGetLiteral(true);
				} catch (NumberFormatException e) {
					return termFactory.createOrGetLiteral(false);
				}
			}

			return termFactory.createOrGetLiteral(false);
		}
	};

	/**
	 * Converts a literal into a double.
	 * <p>
	 * Arguments: a literal representing a string, integer, long, float, or double.
	 * Returns: A Literal&lt;Double&gt; representing the double value.
	 * Throws: InvokerException if the argument cannot be converted to a double.
	 * <p>
	 * Example usage:
	 * <pre>
	 * toDouble("12.34")  => Literal(12.34)
	 * toDouble(12)  => Literal(12.0)
	 * toDouble(12L)  => Literal(12.0)
	 * toDouble("invalid")  => InvokerException
	 * </pre>
	 */
	private final Invoker toDouble = new Invoker() {
		@Override
		public Literal<Double> invoke(Term... args) {
			if (args.length != 1 || !(args[0] instanceof Literal<?> lit)) {
				throw new InvokerException("toDouble", args, "Must provide a single literal argument.");
			}

			Object value = getLiteralValue(lit);
			try {
				if (value instanceof Number) {
					return termFactory.createOrGetLiteral(((Number) value).doubleValue());
				} else if (value instanceof String s) {
					return termFactory.createOrGetLiteral(Double.parseDouble(s));
				}
			} catch (NumberFormatException e) {
				throw new InvokerException("toDouble", args, "Invalid double format.");
			}

			throw new InvokerException("toDouble", args, "Argument must be a number or string.");
		}
	};

	/**
	 * Converts a literal into an integer.
	 * <p>
	 * Arguments: a literal representing a string, integer, long, float, or double.
	 * Returns: A Literal&lt;Integer&gt; representing the integer value.
	 * Throws: InvokerException if the argument cannot be converted to an integer.
	 * <p>
	 * Example usage:
	 * <pre>
	 * toInt("123")  => Literal(123)
	 * toInt(123.45)  => Literal(123)
	 * toInt(123L)  => Literal(123)
	 * toInt("invalid")  => InvokerException
	 * </pre>
	 */
	private final Invoker toInt = new Invoker() {
		@Override
		public Literal<Integer> invoke(Term... args) {
			if (args.length != 1 || !(args[0] instanceof Literal<?> lit)) {
				throw new InvokerException("toInt", args, "Must provide a single literal argument.");
			}

			Object value = getLiteralValue(lit);
			try {
				if (value instanceof Number) {
					return termFactory.createOrGetLiteral(((Number) value).intValue());
				} else if (value instanceof String s) {
					return termFactory.createOrGetLiteral(Integer.parseInt(s));
				}
			} catch (NumberFormatException e) {
				throw new InvokerException("toInt", args, "Invalid integer format.");
			}

			throw new InvokerException("toInt", args, "Argument must be a number or string.");
		}
	};

	/**
	 * Converts a literal into a float.
	 * <p>
	 * Arguments: a literal representing a string, integer, long, float, or double.
	 * Returns: A Literal&lt;Float&gt; representing the float value.
	 * Throws: InvokerException if the argument cannot be converted to a float.
	 * <p>
	 * Example usage:
	 * <pre>
	 * toFloat("12.34")  => Literal(12.34f)
	 * toFloat(12)  => Literal(12.0f)
	 * toFloat(12L)  => Literal(12.0f)
	 * toFloat("invalid")  => InvokerException
	 * </pre>
	 */
	private final Invoker toFloat = new Invoker() {
		@Override
		public Literal<Float> invoke(Term... args) {
			if (args.length != 1 || !(args[0] instanceof Literal<?> lit)) {
				throw new InvokerException("toFloat", args, "Must provide a single literal argument.");
			}

			Object value = getLiteralValue(lit);
			try {
				if (value instanceof Number) {
					return termFactory.createOrGetLiteral(((Number) value).floatValue());
				} else if (value instanceof String s) {
					return termFactory.createOrGetLiteral(Float.parseFloat(s));
				}
			} catch (NumberFormatException e) {
				throw new InvokerException("toFloat", args, "Invalid float format.");
			}

			throw new InvokerException("toFloat", args, "Argument must be a number or string.");
		}
	};

	/**
	 * Converts a literal into a long.
	 * <p>
	 * Arguments: a literal representing a string, integer, long, float, or double.
	 * Returns: A Literal&lt;Long&gt; representing the long value.
	 * Throws: InvokerException if the argument cannot be converted to a long.
	 * <p>
	 * Example usage:
	 * <pre>
	 * toLong("123456789")  => Literal(123456789L)
	 * toLong(123)  => Literal(123L)
	 * toLong(123.45)  => Literal(123L)
	 * toLong("invalid")  => InvokerException
	 * </pre>
	 */
	private final Invoker toLong = new Invoker() {
		@Override
		public Literal<Long> invoke(Term... args) {
			if (args.length != 1 || !(args[0] instanceof Literal<?> lit)) {
				throw new InvokerException("toLong", args, "Must provide a single literal argument.");
			}

			Object value = getLiteralValue(lit);
			try {
				if (value instanceof Number) {
					return termFactory.createOrGetLiteral(((Number) value).longValue());
				} else if (value instanceof String s) {
					return termFactory.createOrGetLiteral(Long.parseLong(s));
				}
			} catch (NumberFormatException e) {
				throw new InvokerException("toLong", args, "Invalid long format.");
			}

			throw new InvokerException("toLong", args, "Argument must be a number or string.");
		}
	};

	/**
	 * Converts any term into a string.
	 * <p>
	 * Arguments: a single term representing any value (literal, variable, etc.).
	 * Returns: A Literal&lt;String&gt; representing the string representation of the value.
	 * Throws: InvokerException if no argument is provided.
	 * <p>
	 * Example usage:
	 * <pre>
	 * toString(123)  => Literal("123")
	 * toString(12.34)  => Literal("12.34")
	 * toString(tuple(1, 2, 3))  => Literal("[1, 2, 3]")
	 * toString("a", "b")  => InvokerException
	 * </pre>
	 */
	private final Invoker toString = new Invoker() {
		@Override
		public Literal<String> invoke(Term... args) {
			if (args.length != 1) {
				throw new InvokerException("toString", args, "Exactly one argument is required.");
			}
			return termFactory.createOrGetLiteral(args[0].toString());
		}
	};

	/**
	 * Converts a literal into a boolean, similarly to the conversion to bool in Python
	 * <p>
	 * Arguments: a literal representing a string, integer, float, list, set, or map.
	 * Returns: A Literal&lt;Boolean&gt; representing the boolean value.
	 * Throws: InvokerException if the argument cannot be converted to a boolean.
	 * <p>
	 * Conversion rules:
	 * <ul>
	 *     <li><code>toBoolean(0)</code> returns <code>false</code></li>
	 *     <li><code>toBoolean(1)</code> returns <code>true</code></li>
	 *     <li><code>toBoolean(0.0)</code> returns <code>false</code></li>
	 *     <li><code>toBoolean(1.0)</code> returns <code>true</code></li>
	 *     <li><code>toBoolean("False")</code> returns <code>false</code></li>
	 *     <li><code>toBoolean("True")</code> returns <code>true</code></li>
	 *     <li><code>toBoolean("")</code> returns <code>false</code></li>
	 *     <li><code>toBoolean("NonEmptyString")</code> returns <code>true</code></li>
	 *     <li><code>toBoolean(null)</code> returns <code>false</code></li>
	 *     <li><code>toBoolean(tuple())</code> returns <code>false</code></li>
	 *     <li><code>toBoolean(tuple(1, 2))</code> returns <code>true</code></li>
	 *     <li><code>toBoolean(set())</code> returns <code>false</code></li>
	 *     <li><code>toBoolean(dict("key", "value"))</code> returns <code>true</code></li>
	 *     <li><code>toBoolean(set())</code> returns <code>false</code></li>
	 *     <li><code>toBoolean(set(1))</code> returns <code>true</code></li>
	 *     <li>Any other value returns <code>false</code></li>
	 * </ul>
	 */
	private final Invoker toBoolean = new Invoker() {
		@Override
		public Literal<Boolean> invoke(Term... args) {
			if (args.length != 1 || !(args[0] instanceof Literal<?> lit)) {
				throw new InvokerException("toBoolean", args, "Must provide a single literal argument.");
			}

			Object value = getLiteralValue(lit);
			if (value instanceof Number) {
				return termFactory.createOrGetLiteral(((Number) value).intValue() != 0);
			} else if (value instanceof String str) {
				String trimmedStr = str.trim();
				if (trimmedStr.isEmpty()) {
					return termFactory.createOrGetLiteral(false);
				}
				if (trimmedStr.equalsIgnoreCase("false") || trimmedStr.equals("0")) {
					return termFactory.createOrGetLiteral(false);
				}
				return termFactory.createOrGetLiteral(true);
			} else if (value instanceof Collection<?> collection) {
				return termFactory.createOrGetLiteral(!collection.isEmpty());
			} else if (value instanceof Map<?, ?> dict) {
				return termFactory.createOrGetLiteral(!dict.isEmpty());
			}

			return termFactory.createOrGetLiteral(false); // Treat all other types as falsy
		}
	};

	/**
	 * Converts a collection to a set, removing any duplicate elements.
	 * <p>
	 * Arguments: a single collection literal (e.g., set, tuple, or list).
	 * Returns: a Literal&lt;Set&gt; containing unique elements from the collection.
	 * Throws: InvokerException if the argument is not a collection.
	 * <p>
	 * Example:
	 * <ul>
	 *     <li><code>toSet(tuple(1, 2, 2, 3))</code> returns the set: {1, 2, 3}</li>
	 * </ul>
	 */
	private final Invoker toSet = new Invoker() {
		@Override
		public Literal<Set<?>> invoke(Term... args) {
			if (args.length != 1
					|| !(args[0] instanceof Literal<?> lit)
					|| !(lit.value() instanceof Collection<?> collection)) {
				throw new InvokerException("toSet", args, "Expected a single collection as an argument.");
			}
			Set<?> termSet = new HashSet<>(collection);
			return termFactory.createOrGetLiteral(termSet);
		}
	};

	/**
	 * Converts a collection to a tuple.
	 * <p>
	 * Arguments: a single collection literal.
	 * Returns: a Literal&lt;List&gt; representing the tuple.
	 * Throws: InvokerException if the argument is not a collection.
	 * <p>
	 * Example:
	 * <ul>
	 *     <li><code>toTuple(set(1, 3, 2))</code> can return the tuple: <code>(1, 3, 2)</code> (or in any other order
	 *     since a set is not ordered).</li>
	 * </ul>
	 */
	private final Invoker toTuple = new Invoker() {
		@Override
		public Literal<List<?>> invoke(Term... args) {
			if (args.length != 1
					|| !(args[0] instanceof Literal<?> lit)
					|| !(lit.value() instanceof Collection<?> collection)) {
				throw new InvokerException("toTuple", args, "Expected a single collection as an argument.");
			}
			List<?> tuple = new ArrayList<>(collection);
			return termFactory.createOrGetLiteral(tuple);
		}
	};


	// Utility Methods and Helpers //

	private String toCamelCase(String s) {
		String[] parts = s.toLowerCase().split("_");
		StringBuilder camelCaseString = new StringBuilder(parts[0]);
		for (int i = 1; i < parts.length; i++) {
			camelCaseString.append(parts[i].substring(0, 1).toUpperCase()).append(parts[i].substring(1));
		}
		return camelCaseString.toString();
	}

	private Term[] flattenIfCollection(Term... args) {
		if (args.length == 1 && args[0].isLiteral() && ((Literal<?>) args[0]).value() instanceof Collection<?> collection) {
            return collection.toArray(new Term[0]);
		}
		return args;
	}

	private boolean isTuple(Term term) {
		return term.isLiteral()
				&& ((Literal<?>) term).value() instanceof List<?> list
				&& list.size() == 2;
	}

	private boolean areAllTuples(Term... args) {
		return Stream.of(args).allMatch(this::isTuple);
	}

	/**
	 * Helper method to ensure that a term is a literal and return its value.
	 * Throws an exception if the term is not a literal.
	 *
	 * @param t The term to check.
	 * @return The value of the literal.
	 * @throws RuntimeException if the term is not a literal.
	 */
	private Object getLiteralValue(Term t) {
		if (!t.isLiteral()) {
			throw new RuntimeException(String.format("%s is not a literal.", t));
		}
		return ((Literal<?>) t).value();
	}

	/**
	 * Helper method to parse numbers from terms.
	 * Supports Integer, Double, Long, and String representations of numbers.
	 *
	 * @param args The terms to parse.
	 * @return A list of numbers.
	 * @throws RuntimeException if any term is not a number.
	 */
	private List<Number> parseNumbers(Term... args) {
		List<Number> numbers = new ArrayList<>();
		for (Term t : args) {
			Object value = getLiteralValue(t);
			if (value instanceof Number) {
				numbers.add((Number) value);
			} else if (value instanceof String s) {
				try {
					numbers.add(Integer.parseInt(s));
				} catch (NumberFormatException e) {
					numbers.add(Double.parseDouble(s));
				}
			} else {
				throw new RuntimeException(String.format("%s is not a number.", value));
			}
		}
		return numbers;
	}

	/**
	 * Checks if all numbers in the list are integers.
	 * Supports both Integer and Long types.
	 *
	 * @param numbers The list of numbers to check.
	 * @return true if all numbers are instances of Integer or Long.
	 */
	private boolean allIntegers(List<Number> numbers) {
		return numbers.stream().allMatch(n -> n instanceof Integer || n instanceof Long);
	}

	/**
	 * Utility function to extract a long value from a literal or a string.
	 * This method will throw an InvokerException if the value cannot be converted.
	 *
	 * @param lit The literal to extract the value from.
	 * @param args The original arguments for error reporting.
	 * @param functionName The name of the function for error reporting.
	 * @return The long value extracted from the literal.
	 * @throws InvokerException if the value cannot be converted to a long.
	 */
	private Long extractLongValue(Literal<?> lit, Term[] args, String functionName) {
		Object value = lit.value();
        switch (value) {
            case Integer i -> {
                return i.longValue();
            }
            case Long l -> {
                return l;
            }
            case String s -> {
                try {
                    return Long.parseLong(s);
                } catch (NumberFormatException e) {
                    throw new InvokerException(functionName, args, String.format("Cannot convert %s to a long.", s));
                }
            }
            case null, default ->
                    throw new InvokerException(functionName, args, String.format("%s is not a valid number.", value));
        }
	}

	/**
	 * Utility function to extract a comparable number from a literal.
	 * Supports Integer, Long, and Double types.
	 * Throw an InvokerException if the value is not a valid number.
	 */
	private Number extractNumber(Literal<?> lit, Term[] args, String functionName) {
		Object value = lit.value();
		if (value instanceof Number) {
			return (Number) value;
		} else if (value instanceof String s) {
			try {
				return Double.parseDouble(s); // Parsing as double in case it's a string number
			} catch (NumberFormatException e) {
				throw new InvokerException(functionName, args, String.format("Cannot convert %s to a number.", s));
			}
		} else {
			throw new InvokerException(functionName, args, String.format("%s is not a valid number.", value));
		}
	}

	/**
	 * Utility function to extract a string value from a literal.
	 * Throws an InvokerException if the value is not a valid string.
	 */
	private String extractString(Literal<?> lit, Term[] args, String functionName) {
		Object value = lit.value();
		if (value instanceof String) {
			return (String) value;
		} else {
			throw new InvokerException(functionName, args, String.format("%s is not a valid string.", value));
		}
	}

	/**
	 * Custom exception for invoker errors.
	 * This exception provides detailed information about the function name,
	 * the arguments provided, and the cause of the error.
	 */
	public static class InvokerException extends RuntimeException {
		public InvokerException(String functionName, Term[] args, Throwable cause) {
			super(String.format("[%s] Error when evaluating the computed function/predicate %s with arguments (%s): %s",
					IntegraalInvokers.class, functionName, formatArgs(args), cause.getMessage()), cause);
		}

		public InvokerException(String functionName, Term[] args, String message) {
			super(String.format("[%s] Error when evaluating the computed function/predicate %s with arguments (%s): %s",
					IntegraalInvokers.class, functionName, formatArgs(args), message));
		}

		public InvokerException(String functionName, Term[] args, String message, Throwable cause) {
			super(
					String.format("[%s] Error when evaluating the computed function/predicate %s with arguments (%s): %s",
							IntegraalInvokers.class, functionName, formatArgs(args),
							String.format("Message: %s \n Cause: %s", message, cause.getMessage())),
					cause);
		}

		private static String formatArgs(Term[] args) {
			if (args == null) {
				return "null";
			}
			return Arrays.stream(args)
					.map(term -> term == null ? "null" : term.toString())
					.collect(Collectors.joining(", "));
		}
	}
}
