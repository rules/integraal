package fr.boreal.model.kb.api;

import java.util.Collection;
import java.util.Iterator;

import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.rule.api.FORule;

/**
 * This interface represents storage systems that can directly apply datalog
 * rules as part of their implementation. Datalog rules are rules without
 * existential variables.
 * <br/>
 * This is extended to the direct evaluation of conjunctive queries.
 */
public interface DatalogDelegable extends Readable, Writeable {

    /**
     * Apply all the given (datalog) rules.
     *
     * @param rules the rules to apply
     * @return true iff at least one atom have been produced by the application of
     * the rules
     * @throws Exception if a given rule is not datalog. The exact type is depending
     *                   on the implementation
     */
    boolean delegate(Collection<FORule> rules) throws Exception;

    /**
     * Evaluate the given (conjunctive) query
     *
     * @param query            the query to evaluate
     * @param countAnswersOnly true iff the query has to return only the number of
     *                         answers
     * @return an {@link Iterator} of {@link Substitution} over all the answers of
     * the given query with respect to the query answer variables.
     * @throws Exception if the given query is not conjunctive. The exact type is
     *                   depending on the implementation
     */

    Iterator<Substitution> delegate(FOQuery<?> query, boolean countAnswersOnly) throws Exception;

    /**
     * Evaluate the given (conjunctive) query
     *
     * @param query the query to evaluate
     * @return an {@link Iterator} of {@link Substitution} over all the answers of
     * the given query with respect to the query answer variables.
     * @throws Exception if the given query is not conjunctive. The exact type is
     **/
    default Iterator<Substitution> delegate(FOQuery<?> query) throws Exception {
        return delegate(query, false);
    }

}
