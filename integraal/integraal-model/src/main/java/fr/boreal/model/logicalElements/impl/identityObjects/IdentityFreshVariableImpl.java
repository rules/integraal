package fr.boreal.model.logicalElements.impl.identityObjects;

/**
 * Implementation of a FreshVariable where the equality test is redefined to only test java object equality on reference.
 * <br/>
 * It is recommended to use a factory to create these terms.
 * 
 * @author Florent Tornil
 *
 */
public class IdentityFreshVariableImpl extends IdentityVariableImpl {

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Constructor using a label
	 * @param label string representation 
	 */
	public IdentityFreshVariableImpl(String label) {
		super(label);
	}

}
