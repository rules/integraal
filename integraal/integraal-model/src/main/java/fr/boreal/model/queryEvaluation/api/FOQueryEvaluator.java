package fr.boreal.model.queryEvaluation.api;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.FOQuery;

import java.util.Collection;
import java.util.Iterator;

/**
 * An FOQuery evaluator is one possible algorithm to evaluate a {@link FOQuery}.
 * <br/>
 * Evaluating a FOQuery means retrieving all the {@link Substitution} of the
 * answer variables from the query with respect to the atoms from the
 * {@link FactBase}.
 *
 * @param <Formula> The type of {@link FOFormula} that the evaluator can evaluate
 *
 */
public interface FOQueryEvaluator<Formula extends FOFormula> extends QueryEvaluator<FOQuery<? extends Formula>> {

    @Override
    Iterator<Substitution> evaluate(FOQuery<? extends Formula> query, FactBase factbase, Collection<Variable> variablesThatMustBeMappedToConstants, Substitution preHomomorphism);

}
