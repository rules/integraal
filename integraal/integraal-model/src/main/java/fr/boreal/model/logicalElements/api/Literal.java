package fr.boreal.model.logicalElements.api;

/**
 * A Literal is a typed constant representing strings, numbers, dates…
 * 
 * @author Florent Tornil
 *
 * @param <T> the native type of the Literal
 */
public interface Literal<T> extends Term {

	@Override
	default boolean isLiteral() {
		return true;
	}

	default boolean isGround() {
		return true;
	}

	/**
	 * @return the value of this Literal as a native Java object
	 */
	T value();

}
