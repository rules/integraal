package fr.boreal.model.logicalElements.factory.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.impl.identityObjects.IdentityPredicateImpl;

/**
 * This factory creates Predicates.
 * <br/>
 * For each call at a same method with the same
 * parameters, the same java object will be returned. Therefore, the equality
 * test between those objects can be restricted to java object reference
 * comparison (==). Please make sure the terms implementations used are
 * immutable otherwise there could be unwanted side effects.
 *
 * @author Florent Tornil
 *
 */
public class SameObjectPredicateFactory implements PredicateFactory {

	private static final PredicateFactory INSTANCE = new SameObjectPredicateFactory();
	private static final String FRESH_PREFIX = "InteGraal:freshPredicate";
	private int fresh_counter = 0;

	private final Map<String, Predicate> predicates = new HashMap<>();

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * @return the default instance of this factory
	 */
	public static PredicateFactory instance() {
		return INSTANCE;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public synchronized Predicate createOrGetPredicate(String label, int arity) {
		Predicate result = this.predicates.get(label);
		if (result == null) {
			result = new IdentityPredicateImpl(label, arity);
			this.predicates.put(label, result);
		} else if (arity != result.arity()) {
			throw new IllegalArgumentException("Cannot create a predicate " + label + " of arity " + arity
					+ " because a predicate with the same label of arity " + result.arity() + " already exists.");
		}
		return result;
	}

	@Override
	public Predicate createOrGetFreshPredicate(int arity) {
		return new IdentityPredicateImpl(FRESH_PREFIX + ++fresh_counter, arity);
	}

	@Override
	public boolean forgetPredicate(String label) {
		if (this.predicates.get(label) != null) {
			this.predicates.remove(label);
			return true;
		}
		else return false; 
	}

	@Override
	public Optional<Predicate> getPredicate(String label) {
		return Optional.ofNullable(this.predicates.get(label));
	}

}
