package fr.boreal.model.functions;

import java.util.Optional;

import fr.boreal.model.logicalElements.api.Term;

/**
 * An Invoker is an object that invoke (execute) a function
 * @author Florent Tornil
 *
 */
public interface Invoker {

	/**
	 * Execute the function associated to this Invoker object with the given arguments
	 * @param args the array of term arguments
	 * @return the result term
	 */
    Term invoke(Term... args);
}
