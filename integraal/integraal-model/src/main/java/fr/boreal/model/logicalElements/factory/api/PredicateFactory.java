package fr.boreal.model.logicalElements.factory.api;

import java.util.Optional;

import fr.boreal.model.logicalElements.api.Predicate;

/**
 * Factory to create predicates
 * @author Florent Tornil
 *
 */
public interface PredicateFactory {

	/**
	 * Two object created by a call to this function with the same label and arity argument must be equals according to the definition given by the {@link Predicate} implementation used
	 * 
	 * @param label the label
	 * @param arity the arity
	 * @return a predicate with the given label and arity
	 */
	Predicate createOrGetPredicate(String label, int arity);
	
	/**
	 * Creates a new predicate which won't be equal to another previously created predicate.
	 * <br/>
	 * The label of the predicate may already exist in some rare cases
	 * 
	 * @param arity the arity
	 * @return a fresh predicate with the given label and arity
	 */
	Predicate createOrGetFreshPredicate(int arity);
	
	/**
	 * Forgets a previously created (non fresh) predicate.
	 * <br/>
	 * This predicate will no longer be considered as already existing in the factory.
	 * 
	 * @param label the label of the previously created predicate
	 * @return true iff such a predicate already exists in the factory, and thus forgets it.
	 */
	boolean forgetPredicate(String label);

	/**
	 * Gets a previously created predicate.
	 * <br/>
	 * This predicate will no longer be considered as already existing in the factory.
	 * 
	 * @param label the label of the previously created predicate
	 * @return an optional that contains the predicate if there is one 
	 */
	Optional<Predicate> getPredicate(String label);
}
