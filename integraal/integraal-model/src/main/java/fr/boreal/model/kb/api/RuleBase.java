package fr.boreal.model.kb.api;

import java.util.Collection;
import java.util.stream.Stream;

import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.rule.api.FORule;

/**
 * A RuleBase is a set of Rule
 * 
 * @author Florent Tornil
 *
 */
public interface RuleBase {

	/**
	 * @return the rules of this base
	 */
	Collection<FORule> getRules();

	/**
	 * @param p a predicate
	 * @return the rules of this base that contains at least an atom with the given predicate in the head
	 */
	Collection<FORule> getRulesByHeadPredicate(Predicate p);

	/**
	 * @param p a predicate
	 * @return the rules of this base that contains at least an atom with the given predicate in the body
	 */
	Collection<FORule> getRulesByBodyPredicate(Predicate p);
	
	/**
	 * Add the given rule to the rule base
	 * @param r a rule
	 */
	void add(FORule r);

	/**
	 * Add the given rules to the rule base
	 * @param rules rules to add
	 */
	default void addAll(Collection<FORule> rules) {
		rules.forEach(this::add);
	}

	/**
	 * Add the given rules to the rule base
	 * @param rules rules to add
	 */
	default void addAll(Stream<FORule> rules) {
		rules.forEach(this::add);
	}

	/**
	 * Removes the given rule from the rule base
	 * @param r a rule
	 */
	void remove(FORule r);

}
