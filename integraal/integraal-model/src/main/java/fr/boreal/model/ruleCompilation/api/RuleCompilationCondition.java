package fr.boreal.model.ruleCompilation.api;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.partition.TermPartition;

/**
 * A RuleCompilationCondition is a structure that represents a condition as defined in Melanie König's thesis.
 * <br/>
 * A condition is defined as c = (q, p, Eq, Lp)
 * 
 * @author Florent Tornil
 *
 */
public interface RuleCompilationCondition {

	/**
	 * @param a an atom
	 * @param b an atom
	 * @return true iff this condition applies for a {@literal <=} b
	 */
	boolean check(Atom a, Atom b);

	/**
	 * The terms are returned with a substitution representing the specialization from the unification with the compiled rule.
	 * 
	 * @param head a list of term
	 * @return a list of terms corresponding to the body of this condition
	 */
	Pair<List<Term>, Substitution> instantiate(List<Term> head);

	/**
	 * Compose the current Condition with another Condition. (x,y,x) ->
	 * (x,y) with (x,x) -> (x) produce (x,x,x) -> (x) (x,y,x) -> (y,x) with
	 * (x,y) -> (y) produce (x,y,x) -> (y)
	 * 
	 * @param condition another condition
	 * @return a new Condition representing the composition.
	 */
	RuleCompilationCondition composeWith(RuleCompilationCondition condition);

	/**
	 * @return true if the current condition represents an identity condition, false otherwise.
	 */
	boolean isIdentity();

	/**
	 * This method assumes that a {@literal <=} b according to this condition.
	 * <br/>
	 * Compute the homomorphism from head to to with respect to initialSub
	 * @param head a list of terms
	 * @param to a list of terms
	 * @param initialSub a substitution
	 * @return the substitution from head to to with respect to initialSub
	 */
	Substitution homomorphism(List<Term> head, List<Term> to, Substitution initialSub);

	/**
	 * This method assumes that a {@literal <=} b according to this condition.
	 * <br/>
	 * Compute the unifier between a and b
	 * @param a an atom
	 * @param b an atom
	 * @return the unification of the terms of a and b
	 */
	TermPartition unifier(Atom a, Atom b);

}
