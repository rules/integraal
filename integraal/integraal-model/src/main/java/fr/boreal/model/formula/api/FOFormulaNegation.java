package fr.boreal.model.formula.api;

import java.util.Set;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Constant;
import fr.boreal.model.logicalElements.api.Literal;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Variable;

/**
 * Representation of the negation of a sub formula
 * 
 * @author Florent Tornil
 *
 */
public non-sealed interface FOFormulaNegation extends FOFormula {

	@Override
	default boolean isNegation() {
		return true;
	}

	/**
	 * @return the sub formula
	 */
	FOFormula element();
	
	@Override
	default Set<Atom> asAtomSet() {
		return this.element().asAtomSet();
	}

	@Override
	default Set<Predicate> getPredicates() {
		return this.element().getPredicates();
	}

	@Override
	default Set<Variable> getVariables() {
		return this.element().getVariables();
	}

	@Override
	default Set<Constant> getConstants() {
		return this.element().getConstants();
	}

	@Override
	default Set<Literal<?>> getLiterals() {
		return this.element().getLiterals();
	}

}
