package fr.boreal.model.formula.api;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Constant;
import fr.boreal.model.logicalElements.api.Literal;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Variable;

/**
 * Abstract implementation for compound formula
 * 
 * @author Florent Tornil
 * 
 */
public abstract non-sealed class CompoundFOFormula implements FOFormula {

	private final Collection<? extends FOFormula> subformulas;

	private Set<Atom> flat;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Constructor using sub formulas
	 * @param subformulas the sub formulas
	 */
	public CompoundFOFormula(Collection<? extends FOFormula> subformulas) {
		this.subformulas = subformulas;
	}

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	/**
	 * @return the string representation of the connector
	 */
	public abstract String getConnectorRepresentation();

	/**
	 * @return the sub formulas
	 */
	public Collection<? extends FOFormula> getSubElements() {
		return this.subformulas;
	}

	@Override
	public Set<Atom> asAtomSet() {
		if (this.flat == null) {
			this.flat = this.getSubElements().stream().flatMap(f -> f.asAtomSet().stream())
					.collect(Collectors.<Atom>toSet());
		}
		return this.flat;
	}
	
	private Set<Predicate> predicates = null;
	@Override
	public Set<Predicate> getPredicates() {
		if (this.predicates == null) {
			this.predicates = this.getSubElements().stream().flatMap(f -> f.getPredicates().stream())
					.collect(Collectors.<Predicate>toSet());
		}
		return this.predicates;
	}
	
	private Set<Variable> variables = null;
	@Override
	public Set<Variable> getVariables() {
		if (this.variables == null) {
			this.variables = this.getSubElements().stream().flatMap(f -> f.getVariables().stream())
					.collect(Collectors.<Variable>toUnmodifiableSet());
		}
		return this.variables;
	}
	
	private Set<Constant> constants = null;
	@Override
	public Set<Constant> getConstants() {
		if (this.constants == null) {
			this.constants = this.getSubElements().stream().flatMap(f -> f.getConstants().stream())
					.collect(Collectors.<Constant>toUnmodifiableSet());
		}
		return this.constants;
	}
	
	private Set<Literal<?>> literals = null;
	@Override
	public Set<Literal<?>> getLiterals() {
		if (this.literals == null) {
			this.literals = this.getSubElements().stream().flatMap(f -> f.getLiterals().stream())
					.collect(Collectors.<Literal<?>>toUnmodifiableSet());
		}
		return this.literals;
	}
	
	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for (FOFormula q : this.getSubElements()) {
			if (!first) {
				sb.append(" ");
				sb.append(this.getConnectorRepresentation());
				sb.append(" ");
			}
			sb.append(q.toString());
			first = false;
		}
		return sb.toString();
	}
}