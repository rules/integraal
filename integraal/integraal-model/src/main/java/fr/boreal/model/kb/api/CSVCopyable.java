package fr.boreal.model.kb.api;

import fr.boreal.model.logicalElements.api.Atom;

/**
 * This interface represents storage systems that can directly import CSV files as part of their implementation. 
 */
public interface CSVCopyable extends Readable, Writeable {

	/**
	 * Copy the content of the given CSV file into the storage system
	 * The exact layout of the resulting storage is dependent on the implementation
	 * @param csvFilePath the csv file to copy
	 * @param headerSize size of the CSV header
	 * @param separator separator char of the CSV
	 * @param witness atom used to pre-create the storing schema
	 * @return true iff at least one atom have been added by the copy
	 * @throws Exception if an error occur
	 */
	boolean copy(String csvFilePath, char separator, int headerSize, Atom witness) throws Exception;

}

