package fr.boreal.model.query.impl;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.partition.TermPartition;
import fr.boreal.model.query.api.FOQuery;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;

/**
 * Abstract class for FOQueries.
 * This is used to factor common code to queries such as label or getters. 
 *
 * @author Florent Tornil
 *
 */
public class FOQueryImpl<Formula extends FOFormula> implements FOQuery<Formula> {

	private final String label;

	private final Formula formula;
	private final Collection<Variable> answer_variables;

	private final TermPartition equalities;

	/**
	 * Constructor using a label
	 * @param label the label of this query
	 * @param formula the formula of this query
	 * @param answer_variables the list of answer variables
	 * @param equalities the equalities of the query variables
	 */
	public FOQueryImpl(String label, Formula formula, Collection<Variable> answer_variables, TermPartition equalities) {
		this.label = label;
		this.formula = formula;

		if (answer_variables == null) {
			answer_variables = formula.getVariables();
		}
		this.answer_variables = answer_variables;

		if (equalities == null) {
			equalities = new TermPartition();
		}
		this.equalities = equalities;

		for(Variable v : answer_variables) {
			if(!formula.getVariables().contains(v) && !equalities.getElements().contains(v)) {
				throw new IllegalArgumentException("[WrongQueryAnswerVariables] Tried to create query " + formula +
						" with answer variable " + v +
						" and equalities " + equalities +
						" that is not part of the query nor the equalities.");
			}
		}


	}

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	@Override
	public Collection<Variable> getAnswerVariables() {
		return this.answer_variables;
	}

	@Override
	public TermPartition getVariableEqualities() {
		return this.equalities;
	}

	@Override
	public Formula getFormula() {
		return this.formula;
	}

	@Override
	public String getLabel() {
		return this.label;
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof FOQuery<?> other) {
			return this.hashCode() == other.hashCode()
					&& Arrays.equals(this.getAnswerVariables().toArray(), other.getAnswerVariables().toArray())
					&& this.getVariableEqualities().equals(other.getVariableEqualities())
					&& this.getFormula().equals(other.getFormula());
		} else {
			return false;
		}
	}

	private int hash = -1;
	@Override
	public int hashCode() {
		if(this.hash == -1) {
			this.hash = Objects.hash(this.getAnswerVariables(), this.getVariableEqualities(), this.getFormula());
		}
		return this.hash;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("?(");
		boolean first = true;
		for (Variable v : this.answer_variables) {
			if (!first) {
				sb.append(", ");
			}
			sb.append(v.toString());
			first = false;
		}
		sb.append(") :- ");

		sb.append(this.formula.toString());

		if(!this.equalities.getElements().isEmpty()) {
			for(Set<Term> equals : this.equalities.getClasses()) {
				if(equals.size() > 1) {
					sb.append(" (");
					first = true;
					for(Term t : equals) {
						if(!first) {
							sb.append(" = ");
						}
						first = false;
						sb.append(t);
					}
					sb.append(")");
				}
			}
		}
		return sb.toString();
	}

}
