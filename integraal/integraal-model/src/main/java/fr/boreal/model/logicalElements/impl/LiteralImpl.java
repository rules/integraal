package fr.boreal.model.logicalElements.impl;

import fr.boreal.model.logicalElements.api.Literal;

/**
 * Default implementation of Literal
 *
 * @param <T> the native type of the Literal
 * @author Florent Tornil
 */
public record LiteralImpl<T>(T value) implements Literal<T> {

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Constructor from a single value
	 *
	 * @param value the value
	 */
	public LiteralImpl {
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////


	@Override
	public String label() {
		return this.value.toString();
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof Literal<?> other) {
            return this.value().equals(other.value());
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return this.value.toString();
	}
}
