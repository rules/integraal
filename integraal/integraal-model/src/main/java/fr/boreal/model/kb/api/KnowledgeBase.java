package fr.boreal.model.kb.api;

/**
 * A knowledge base is composed of facts, existential rules.
 * <br/>
 * It provides methods to facilitate their management.
 * 
 * @author Florent Tornil
 *
 */
public interface KnowledgeBase {
	
	/**
	 * @return the FactBase attached to this KnowledgeBase
	 */
	FactBase getFactBase();
	
	/**
	 * @return the RuleBase attached to this KnowledgeBase
	 */
	RuleBase getRuleBase();

}
