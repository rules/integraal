package fr.boreal.model.logicalElements.impl.functionalTerms;

import fr.boreal.model.logicalElements.api.*;

import java.util.*;

/**
 * Default implementation of a ground FunctionalTerm (not specializable, nor evaluable)
 *
 * @author Federico Ulliana
 */
public class GroundFunctionalTermImpl extends AbstractLogicalFunctionalTermImpl {

    public GroundFunctionalTermImpl(String function_name, List<Term> sub_terms) {
        super(function_name, sub_terms);
    }

    @Override
    protected void sanityCheck(String functionName, List<Term> subTerms) {
        checkThereAreNoVariables(subTerms);
    }

    private void checkThereAreNoVariables(List<Term> subTerms) {
        for (Term t : subTerms) {
            // reject variables
            if (t.isVariable()) {
                throw new IllegalArgumentException(
                        "Ground functional terms cannot have variables");
            }
            // reject functional terms which contain variables
            if (t.isFunctionalTerm() && t instanceof LogicalFunctionalTerm tf) {
                if (!tf.isGround())
                    throw new IllegalArgumentException(
                            "Ground functional terms cannot have variables");
            }
        }
    }


}
