package fr.boreal.model.logicalElements.impl;

import fr.boreal.model.logicalElements.api.Predicate;

/**
 * Default implementation of Predicate
 *
 * @author Florent Tornil
 */
public record PredicateImpl(String label, int arity) implements Predicate {

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Constructor with a label and arity
	 *
	 * @param label the string representation
	 * @param arity the number of terms
	 */
	public PredicateImpl {
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////


	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.arity();
		result = prime * result * this.label.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof Predicate other) {
            return this.arity() == other.arity() && this.label().equals(other.label());
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return this.label;
	}

}
