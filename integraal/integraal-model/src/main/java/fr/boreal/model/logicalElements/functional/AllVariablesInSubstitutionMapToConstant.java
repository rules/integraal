package fr.boreal.model.logicalElements.functional;

import org.apache.commons.collections4.Predicate;

import fr.boreal.model.logicalElements.api.Substitution;

/**
 * Predicate used to filter substitutions that do not map only to constants.
 */

public class AllVariablesInSubstitutionMapToConstant implements Predicate<Substitution> {

	public boolean evaluate(Substitution s) {
		return s.mapsToConstantsOnly();
	}

}
