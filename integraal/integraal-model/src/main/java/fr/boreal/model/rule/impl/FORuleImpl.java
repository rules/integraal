package fr.boreal.model.rule.impl;

import java.util.Collection;
import java.util.Objects;

import com.google.common.collect.Sets;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Constant;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.rule.api.FORule;

/**
 * Default implementation of FORule
 * @author Florent Tornil
 *
 */
public class FORuleImpl implements FORule {

	private final String label;

	private final FOFormula body;
	private final FOFormula head;


	private Collection<Variable> frontier = null;
	private Collection<Variable> existentials = null;
	private Collection<Constant> constants = null;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Constructor without label
	 * @param body the body of the rule
	 * @param head the head of the rule
	 */
	public FORuleImpl(FOFormula body, FOFormula head) {
		this("", body, head);
	}

	/**
	 * Constructor with label
	 * @param label the label of the rule
	 * @param body the body of the rule
	 * @param head the head of the rule
	 */
	public FORuleImpl(String label, FOFormula body, FOFormula head) {
		this.label = label;
		this.head = head;
		this.body = body;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	@Override
	public FOFormula getBody() {
		return this.body;
	}

	@Override
	public FOFormula getHead() {
		return this.head;
	}

	@Override
	public Collection<Variable> getFrontier() {
		if (this.frontier == null) {
			this.frontier = Sets.intersection(
					this.getHead().getVariables(),
					this.getBody().getVariables());
		}
		return this.frontier;
	}

	@Override
	public Collection<Variable> getExistentials() {
		if (this.existentials == null) {
			this.existentials = Sets.difference(
					this.getHead().getVariables(),
					this.getBody().getVariables());
		}
		return this.existentials;
	}

	@Override
	public Collection<Constant> getConstants() {
		if (this.constants == null) {
			this.constants = Sets.union(
					this.getHead().getConstants(),
					this.getBody().getConstants());
		}
		return this.constants;
	}

	@Override
	public String getLabel() {
		return this.label;
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	private int hash = -1;
	@Override
	public int hashCode() {
		if(this.hash == -1) {
			this.hash = Objects.hash(this.getBody(), this.getHead());
		}
		return this.hash;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof FORule other) {
            return this.hashCode() == other.hashCode()
					&& this.getBody().equals(other.getBody())
					&& this.getHead().equals(other.getHead());
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
        return this.getBody().toString() +
                " -> " +
                this.getHead().toString();
	}

}
