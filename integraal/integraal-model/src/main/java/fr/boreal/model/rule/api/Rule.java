package fr.boreal.model.rule.api;

import java.util.Collection;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Constant;
import fr.boreal.model.logicalElements.api.Variable;

/**
 * This interface represents an existential rule.
 * <br/>
 * An existential rule is a positive rule of the form B -> H,
 * where B and H are conjunctions of atoms.
 * It is interpreted as the formula ∀X(∃Y B[X,Y] -> ∃Z H[X,Z]), or equivalently
 * ∀X∀Y(B[X,Y] -> ∃Z H[X,Z]), where X are the variables shared by B and H,
 * Y are the variables that occur only in B
 * and Z the ones that occur only in H.
 * Note that Z are existentially quantified.
 *  
 */
public interface Rule {

	/**
	 * @return the label of the rule
	 */
	String getLabel();

	/**
	 * @return the body of the rule
	 */
	FOFormula getBody();

	/**
	 * @return the head of the rule
	 */
	FOFormula getHead();

	/**
	 * @return the frontier of the rule
	 */
	Collection<Variable> getFrontier();

	/**
	 * @return the existential variables of the rule
	 */
	Collection<Variable> getExistentials();

	/**
	 * @return the constants of the rule
	 */
	Collection<Constant> getConstants();

}
