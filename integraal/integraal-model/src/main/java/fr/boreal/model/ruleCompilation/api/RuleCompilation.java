package fr.boreal.model.ruleCompilation.api;

import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;

import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.partition.TermPartition;

/**
 * A RuleCompilation is a structure that represents a compilation as defined in Melanie König's thesis.
 * 
 * @author ftornil
 *
 */
public interface RuleCompilation {

	/**
	 * This method also removes the compiled rules from the given rulebase.
	 * 
	 * @param rb rulebase to compile
	 */
	void compile(RuleBase rb);
	
	/**
	 * This method returns the original rulebase, the compilable rules, and the non-compilable rules.
	 * 
	 * @param rb rulebase to compile
	 */
	RuleCompilationResult compileAndGet(RuleBase rb);
	
	/**
	 * This method is not used for the moment.
	 * @param a an atom
	 * @param b an atom
	 * @return true iff a {@literal <=} b
	 */
	boolean isMoreSpecificThan(Atom a, Atom b);
	
	/**
	 * The unfolding of an atom <code>a</code> is a set A' such that for all a' in A', a' {@literal <=} a.
	 * The atom is returned with a substitution representing the specialization from the unification with the compiled rule.
	 * 
	 * @param a an atom
	 * @return the unfolding of a with this compilation
	 */
	Set<Pair<Atom, Substitution>> unfold(Atom a);
	
	/**
	 * Two predicates are compatible iff there exists a condition c = (p, q, Ep, Lq)
	 * 
	 * @param p a predicate
	 * @param q a predicate
	 * @return true iff p and q are compatible
	 */
	boolean isCompatible(Predicate p, Predicate q);
	
	/**
	 * @param p a predicate
	 * @return the set of all the predicates that are compatible with p
	 */
	Set<Predicate> getCompatiblePredicates(Predicate p);
	
	/**
	 * Compute all the possible homomorphisms from a to b
	 * @param a an atom
	 * @param b an atom
	 * @return the set of substitutions from the terms of <code>a</code> to the terms of b
	 */
	default Set<Substitution> getHomomorphisms(Atom a, Atom b) {
		return this.getHomomorphisms(a, b, new SubstitutionImpl());
	}
	
	/**
	 * Compute all the possible homomorphisms from a to b with respect to s
	 * @param a an atom
	 * @param b an atom
	 * @param s a substitution
	 * @return the set of substitutions from the terms of <code>a</code> to the terms of b with respect to s
	 */
	Set<Substitution> getHomomorphisms(Atom a, Atom b, Substitution s);
	
	/**
	 * Compute all the possible unifiers between a and b
	 * @param a an atom
	 * @param b an atom
	 * @return the set of all partitions from the terms of b and a
	 */
	Set<TermPartition> getUnifications(Atom a, Atom b);

}
