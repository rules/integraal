package fr.boreal.test.grd.viewer;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;

import fr.boreal.grd.api.GraphOfFORuleDependencies;
import fr.boreal.model.rule.api.FORule;

public class GRDViewer {

	private static GRDViewer instance;
	public static synchronized GRDViewer instance() {
		if (instance == null)
			instance = new GRDViewer();

		return instance;
	}

	public void display(GraphOfFORuleDependencies g) {
		Graph graphDisp = new SingleGraph("Graph Of Rules Dependencies");
		computeVertices(g , graphDisp);
		addDependencies(g , graphDisp);

		System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");

		//graphDisp.setAttribute("ui.stylesheet", "url(file:styles.css)");
		graphDisp.setAttribute("ui.quality");
		graphDisp.setAttribute("ui.antialias");
		graphDisp.display();			
	}


	private void computeVertices(GraphOfFORuleDependencies g , Graph graphDisp) {
		for(FORule r : g.getRules()) {
			Node n = graphDisp.addNode(r.toString());
			n.setAttribute("label", r.toString());
			n.setAttribute("rule", r.toString());
		}
	}


	private void addDependencies(GraphOfFORuleDependencies g , Graph graphDisp) {
		for(FORule src : g.getRules()) {
			for(FORule dest : g.getTriggeredRules(src)) {
                String edgeID = src.toString() +
                        "|" +
                        dest.toString();

				Edge e = graphDisp.addEdge(edgeID,
						src.toString(),
						dest.toString(),
						true);
				e.setAttribute("label", '+');
				e.setAttribute("ui.class", "plus");
			}
		}
	}
}