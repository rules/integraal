package fr.boreal.test.grd;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Constant;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.rule.impl.FORuleImpl;

public class TestData {

	// Predicates
	public static final Predicate p1 = SameObjectPredicateFactory.instance().createOrGetPredicate("p1", 1);
	public static final Predicate q1 = SameObjectPredicateFactory.instance().createOrGetPredicate("q1", 1);
	public static final Predicate r1 = SameObjectPredicateFactory.instance().createOrGetPredicate("r1", 1);
	public static final Predicate s1 = SameObjectPredicateFactory.instance().createOrGetPredicate("s1", 1);
	public static final Predicate t1 = SameObjectPredicateFactory.instance().createOrGetPredicate("t1", 1);
	public static final Predicate p2 = SameObjectPredicateFactory.instance().createOrGetPredicate("p2", 2);
	public static final Predicate q2 = SameObjectPredicateFactory.instance().createOrGetPredicate("q2", 2);
	public static final Predicate r2 = SameObjectPredicateFactory.instance().createOrGetPredicate("r2", 2);

	// Terms
	public static final Variable x = SameObjectTermFactory.instance().createOrGetVariable("x");
	public static final Variable y = SameObjectTermFactory.instance().createOrGetVariable("y");
	public static final Variable z = SameObjectTermFactory.instance().createOrGetVariable("z");

	public static final Constant a = SameObjectTermFactory.instance().createOrGetConstant("a");

	// Atoms
	public static final Atom px = new AtomImpl(p1, x);
	public static final Atom py = new AtomImpl(p1, y);
	public static final Atom qx = new AtomImpl(q1, x);
	public static final Atom rx = new AtomImpl(r1, x);
	public static final Atom sx = new AtomImpl(s1, x);
	public static final Atom tx = new AtomImpl(t1, x);

	public static final Atom pxy = new AtomImpl(p2, x, y);
	public static final Atom pyz = new AtomImpl(p2, y, z);
	public static final Atom pxx = new AtomImpl(p2, x, x);
	public static final Atom qxy = new AtomImpl(q2, x, y);
	public static final Atom qxx = new AtomImpl(q2, x, x);
	public static final Atom rxy = new AtomImpl(r2, x, y);
	public static final Atom rxx = new AtomImpl(r2, x, x);

	public static final Atom pzy = new AtomImpl(p2, z, y);
	public static final Atom pyy = new AtomImpl(p2, y, y);
	public static final Atom qzy = new AtomImpl(q2, z, y);

	public static FOFormula and(FOFormula f1, FOFormula f2) {
		return FOFormulaFactory.instance().createOrGetConjunction(f1, f2);
	}

	public static FOFormula not(FOFormula f) {
		return FOFormulaFactory.instance().createOrGetNegation(f);
	}

	// p(x) -> q(x)
	public static final FORule Rpx_qx = new FORuleImpl(px, qx);

	// p(x) -> p(y)
	public static final FORule Rpx_py = new FORuleImpl(px, py);

	// q(x) -> r(x, y)
	public static final FORule Rqx_rxy = new FORuleImpl(qx, rxy);

	// p(x, y) -> q(z, y)
	public static final FORule Rpxy_qzy = new FORuleImpl(pxy, qzy);

	// q(x, y) -> p(z, y)
	public static final FORule Rquv_pwv = new FORuleImpl(qxy, pzy);

	// p(x, y) -> p(y, z)
	public static final FORule Rpxy_pyz = new FORuleImpl(pxy, pyz);

	// p(x, y) -> p(y, y)
	public static final FORule Rpxy_pyy = new FORuleImpl(pxy, pyy);

	// p(x), not q(x) -> r(x)
	public static final FORule Rpxnotqx_rx = new FORuleImpl(and(px, not(qx)), rx);

	// s(x) -> q(x)
	public static final FORule Rsx_qx = new FORuleImpl(sx, qx);

	// p(x), not q(x) -> q(x)
	public static final FORule Rpxnotqx_qx = new FORuleImpl(and(px, not(qx)), qx);

	// p(x), not r(x) -> q(x)
	public static final FORule Rpxnotrx_qx = new FORuleImpl(and(px, not(rx)), qx);

	// p(x, y), not q(x, y) -> r(x, y)
	public static final FORule Rpxynotqxy_rxy = new FORuleImpl(and(pxy, not(qxy)), rxy);

	// p(x, x), not r(x, x) -> q(x, x)
	public static final FORule Rpxxnotrxx_qxx = new FORuleImpl(and(pxx, not(rxx)), qxx);

	// p(x, y) -> q(x, y)
	public static final FORule Rpxy_qxy = new FORuleImpl(pxy, qxy);

	// q(x, x), not p(x, x) -> r(x)
	public static final FORule Rqxxnotpxx_rx = new FORuleImpl(and(qxx, not(pxx)), rx);

	// q(x, x), not r(x, x) -> s(x)
	public static final FORule Rqxxnotrxx_sx = new FORuleImpl(and(qxx, not(rxx)), sx);

	// p(x), not r(x) -> s(x)
	public static final FORule Rpxnotrx_sx = new FORuleImpl(and(px, not(rx)), sx);

	// p(x), not s(x) -> t(x)
	public static final FORule Rpxnotsx_tx = new FORuleImpl(and(px, not(sx)), tx);
	
	public static final FORule Rqx_rx = new FORuleImpl(qx, rx);
	
	public static final FORule Rrx_sx = new FORuleImpl(rx, sx);
	
	public static final FORule Rqxx_sx = new FORuleImpl(qxx, sx);
	
	public static final FORule Rtxnotsx_pxx = new FORuleImpl(and(tx, not(sx)), pxx);
}
