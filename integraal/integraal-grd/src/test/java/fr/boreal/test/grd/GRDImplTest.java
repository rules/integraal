package fr.boreal.test.grd;

import fr.boreal.grd.impl.GRDImpl;
import fr.boreal.grd.impl.dependency_checker.RestrictedProductivityChecker;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.rule.api.FORule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runners.Parameterized.Parameters;

import java.util.*;
import java.util.stream.Stream;

class GRDImplTest extends TestData {

	@Parameters
	static Stream<Arguments> productivityGRDData() {
		return Stream.of(
				Arguments.of(Set.of(), Map.of()), // empty Ruleset -> empty GRD
				Arguments.of(Set.of(Rpx_qx), Map.of(Rpx_qx, Set.of())), // single rule Ruleset -> one node with no dependency
				Arguments.of(Set.of(Rpx_py), Map.of(Rpx_py, Set.of(Rpx_py))), // single recursive rule Ruleset -> GRD with circular dependency
				Arguments.of(Set.of(Rpx_qx, Rqx_rxy), Map.of(Rpx_qx, Set.of(Rqx_rxy), Rqx_rxy, Set.of())), // 2 rules with a dependency (path shaped) -> GRD with 1 dependency
				Arguments.of(Set.of(Rpx_py, Rpx_qx, Rqx_rxy), Map.of(Rpx_qx, Set.of(Rqx_rxy), Rpx_py, Set.of(Rpx_py, Rpx_qx), Rqx_rxy, Set.of())), // 2 previous cases together + 1 linear dependency
				Arguments.of(Set.of(Rpxy_qzy, Rquv_pwv), Map.of(Rpxy_qzy, Set.of(Rquv_pwv), Rquv_pwv, Set.of(Rpxy_qzy))), // Cycle GRD (with productivity checker)
				Arguments.of(Set.of(Rpxy_pyz, Rpxy_pyy), Map.of(Rpxy_pyz, Set.of(Rpxy_pyz, Rpxy_pyy), Rpxy_pyy, Set.of(Rpxy_pyz)))
				);
	}
	
	@Parameters
	static Stream<Arguments> restrictedGRDData() {
		return Stream.of(
				Arguments.of(Set.of(), Map.of()),
				Arguments.of(Set.of(Rpx_qx), Map.of(Rpx_qx, Set.of())),
				Arguments.of(Set.of(Rpx_py), Map.of(Rpx_py, Set.of())),
				Arguments.of(Set.of(Rpx_qx, Rqx_rxy), Map.of(Rpx_qx, Set.of(Rqx_rxy), Rqx_rxy, Set.of())),
				Arguments.of(Set.of(Rpx_py, Rpx_qx, Rqx_rxy), Map.of(Rpx_qx, Set.of(Rqx_rxy), Rpx_py, Set.of(Rpx_qx), Rqx_rxy, Set.of())),
				Arguments.of(Set.of(Rpxy_qzy, Rquv_pwv), Map.of(Rpxy_qzy, Set.of(), Rquv_pwv, Set.of())),
				Arguments.of(Set.of(Rpxy_pyz, Rpxy_pyy), Map.of(Rpxy_pyz, Set.of(Rpxy_pyz, Rpxy_pyy), Rpxy_pyy, Set.of()))
				);
	}
	
	@Parameters
	static Stream<Arguments> negationGRDData() {
		return Stream.of(
			Arguments.of(Set.of(), Map.of(), Map.of()),
			Arguments.of(Set.of(Rsx_qx, Rpxnotqx_rx),
					Map.of(Rsx_qx, Set.of(), Rpxnotqx_rx, Set.of()),
					Map.of(Rsx_qx, Set.of(Rpxnotqx_rx), Rpxnotqx_rx, Set.of())),
			Arguments.of(Set.of(Rpx_qx, Rpxnotqx_rx),
					Map.of(Rpx_qx, Set.of(), Rpxnotqx_rx, Set.of()),
					Map.of(Rpx_qx, Set.of(Rpxnotqx_rx), Rpxnotqx_rx, Set.of())),
			Arguments.of(Set.of(Rpxnotqx_qx),
					Map.of(Rpxnotqx_qx, Set.of()),
					Map.of(Rpxnotqx_qx, Set.of(Rpxnotqx_qx))),
			Arguments.of(Set.of(Rpxnotrx_qx, Rpxnotqx_rx),
					Map.of(Rpxnotrx_qx, Set.of(), Rpxnotqx_rx, Set.of()),
					Map.of(Rpxnotrx_qx, Set.of(Rpxnotqx_rx), Rpxnotqx_rx, Set.of(Rpxnotrx_qx))),
			Arguments.of(Set.of(Rpxynotqxy_rxy, Rpxxnotrxx_qxx),
					Map.of(Rpxynotqxy_rxy, Set.of(), Rpxxnotrxx_qxx, Set.of()),
					Map.of(Rpxynotqxy_rxy, Set.of(Rpxxnotrxx_qxx), Rpxxnotrxx_qxx, Set.of(Rpxynotqxy_rxy))),
			Arguments.of(Set.of(Rpxy_qxy, Rqxxnotpxx_rx),
					Map.of(Rpxy_qxy, Set.of(), Rqxxnotpxx_rx, Set.of()),
					Map.of(Rpxy_qxy, Set.of(), Rqxxnotpxx_rx, Set.of())),
			Arguments.of(Set.of(Rpxynotqxy_rxy, Rqxxnotrxx_sx),
					Map.of(Rpxynotqxy_rxy, Set.of(), Rqxxnotrxx_sx, Set.of()),
					Map.of(Rpxynotqxy_rxy, Set.of(), Rqxxnotrxx_sx, Set.of()))
		);
	}
	
	@Parameters
	static Stream<Arguments> stratificationData() {
		return Stream.of(
			Arguments.of(Set.of(), true, List.of()),
			Arguments.of(Set.of(Rpx_qx), true, List.of(Set.of(Rpx_qx))),
			Arguments.of(Set.of(Rsx_qx, Rpxnotqx_rx), true, List.of(Set.of(Rsx_qx), Set.of(Rpxnotqx_rx))),
			Arguments.of(Set.of(Rpxnotqx_rx, Rpxnotrx_sx, Rpxnotsx_tx), true,
					List.of(Set.of(Rpxnotqx_rx), Set.of(Rpxnotrx_sx), Set.of(Rpxnotsx_tx))),
			Arguments.of(Set.of(Rpxnotsx_tx, Rpxnotqx_rx, Rpxnotrx_sx), true,
					List.of(Set.of(Rpxnotqx_rx), Set.of(Rpxnotrx_sx), Set.of(Rpxnotsx_tx))),
			Arguments.of(Set.of(Rpxnotqx_qx), false, null),
			Arguments.of(Set.of(Rpxnotqx_rx, Rpxnotrx_qx), false, null),
			Arguments.of(Set.of(Rpx_qx, Rqx_rx, Rrx_sx, Rtxnotsx_pxx), true, List.of(Set.of(Rpx_qx), Set.of(Rqx_rx), Set.of(Rrx_sx), Set.of(Rtxnotsx_pxx)))
		);
	}
	
	@Parameters
	static Stream<Arguments> pseudoMinimalStratificationData() {
		return Stream.of(
			Arguments.of(Set.of(), true, List.of()),
			Arguments.of(Set.of(Rpx_qx), true, List.of(Set.of(Rpx_qx))),
			Arguments.of(Set.of(Rsx_qx, Rpxnotqx_rx), true, List.of(Set.of(Rsx_qx), Set.of(Rpxnotqx_rx))),
			Arguments.of(Set.of(Rpxnotqx_rx, Rpxnotrx_sx, Rpxnotsx_tx), true,
					List.of(Set.of(Rpxnotqx_rx), Set.of(Rpxnotrx_sx), Set.of(Rpxnotsx_tx))),
			Arguments.of(Set.of(Rpxnotsx_tx, Rpxnotqx_rx, Rpxnotrx_sx), true,
					List.of(Set.of(Rpxnotqx_rx), Set.of(Rpxnotrx_sx), Set.of(Rpxnotsx_tx))),
			Arguments.of(Set.of(Rpxnotqx_qx), false, null),
			Arguments.of(Set.of(Rpxnotqx_rx, Rpxnotrx_qx), false, null),
			Arguments.of(Set.of(Rpx_qx, Rqx_rx, Rrx_sx, Rtxnotsx_pxx), true, List.of(Set.of(Rpx_qx, Rqx_rx, Rrx_sx), Set.of(Rtxnotsx_pxx))),
			Arguments.of(Set.of(Rpx_qx, Rqx_rx, Rrx_sx, Rtxnotsx_pxx, Rqxx_sx), true, List.of(Set.of(Rpx_qx, Rqx_rx, Rrx_sx, Rqxx_sx), Set.of(Rtxnotsx_pxx)))
		);
	}


	@DisplayName("Test GRD construction")
	@ParameterizedTest(name = "{index}: ({0})")
	@MethodSource("productivityGRDData")
	void productivityGRDTest(Collection<FORule> rules, Map<FORule, Set<FORule>> expected) {
		GRDImpl grd = new GRDImpl(new RuleBaseImpl(rules));
		for(FORule r : expected.keySet()) {
			Set<FORule> computed_dependencies = grd.getTriggeredRules(r);
			Set<FORule> expected_dependencies = expected.get(r);

			Assertions.assertTrue(expected_dependencies.containsAll(computed_dependencies), "All computed dependencies should be expected");
			Assertions.assertTrue(computed_dependencies.containsAll(expected_dependencies), "All expected dependencies should be computed");
		}
	}
	
	@DisplayName("Test GRD construction with restricted productivity")
	@ParameterizedTest(name = "{index}: ({0})")
	@MethodSource("restrictedGRDData")
	void restrictedGRDTest(Collection<FORule> rules, Map<FORule, Set<FORule>> expected) {
		GRDImpl grd = new GRDImpl(new RuleBaseImpl(rules), List.of(RestrictedProductivityChecker.instance()));
		for(FORule r : expected.keySet()) {
			Set<FORule> computed_dependencies = grd.getTriggeredRules(r);
			Set<FORule> expected_dependencies = expected.get(r);

			Assertions.assertTrue(expected_dependencies.containsAll(computed_dependencies), "All computed dependencies should be expected");
			Assertions.assertTrue(computed_dependencies.containsAll(expected_dependencies), "All expected dependencies should be computed");
		}
	}
	
	@DisplayName("Test GRD construction with safe negation rules")
	@ParameterizedTest(name = "{index}: ({0})")
	@MethodSource("negationGRDData")
	void negationGRDTest(Collection<FORule> rules, Map<FORule, Set<FORule>> expectedPositiveDependencies, Map<FORule, Set<FORule>> expectedNegativeDependencies) {
		GRDImpl grd = new GRDImpl(new RuleBaseImpl(rules), List.of(RestrictedProductivityChecker.instance()));
		for(FORule r : expectedPositiveDependencies.keySet()) {
			Set<FORule> computed_dependencies = grd.getTriggeredRules(r);
			Set<FORule> expected_dependencies = expectedPositiveDependencies.get(r);

			Assertions.assertTrue(expected_dependencies.containsAll(computed_dependencies), "All computed positive dependencies should be expected");
			Assertions.assertTrue(computed_dependencies.containsAll(expected_dependencies), "All expected positive dependencies should be computed");
		}
		for(FORule r : expectedNegativeDependencies.keySet()) {
			Set<FORule> computed_dependencies = grd.getPreventedRules(r);
			Set<FORule> expected_dependencies = expectedNegativeDependencies.get(r);

			Assertions.assertTrue(expected_dependencies.containsAll(computed_dependencies), "All computed negative dependencies should be expected");
			Assertions.assertTrue(computed_dependencies.containsAll(expected_dependencies), "All expected negative dependencies should be computed");
		}
	}
	
	@DisplayName("Test stratification")
	@ParameterizedTest(name = "{index}: ({0})")
	@MethodSource("stratificationData")
	void stratificationTest(Collection<FORule> rules, boolean isStratifiable, List<Set<FORule>> expectedStrates) {
		GRDImpl grd = new GRDImpl(new RuleBaseImpl(rules), List.of(RestrictedProductivityChecker.instance()));
		
		Assertions.assertEquals(isStratifiable, grd.isStratifiable(), "Stratificability is incorrect");
		
		if(isStratifiable) {
			List<RuleBase> computedStrates = grd.getStratification();
			for(int i = 0; i< expectedStrates.size(); i++) {
				Collection<FORule> computedStrate = computedStrates.get(i).getRules();
				Set<FORule> expectedStrate = expectedStrates.get(i);
				
				Assertions.assertTrue(computedStrate.containsAll(expectedStrate), "All expected rules of the strate should be present");
				Assertions.assertTrue(expectedStrate.containsAll(computedStrate), "All rules present in the strate should be expected");
			}
		}
	}
	
	@DisplayName("Test pseudo minimal stratification")
	@ParameterizedTest(name = "{index}: ({0})")
	@MethodSource("pseudoMinimalStratificationData")
	void pseudoMinimalStratificationTest(Collection<FORule> rules, boolean isStratifiable, List<Set<FORule>> expectedStrates) {
		GRDImpl grd = new GRDImpl(new RuleBaseImpl(rules), List.of(RestrictedProductivityChecker.instance()));
		
		Optional<List<RuleBase>> computedStratesOpt = grd.getPseudoMinimalStratification();
		
		Assertions.assertEquals(isStratifiable, computedStratesOpt.isPresent(), "Stratificability should be correct");
		
		if(isStratifiable) {
			List<RuleBase> computedStrates = computedStratesOpt.orElseThrow();
		
			for(int i = 0; i< expectedStrates.size(); i++) {
				Collection<FORule> computedStrate = computedStrates.get(i).getRules();
				Set<FORule> expectedStrate = expectedStrates.get(i);
				
				Assertions.assertTrue(computedStrate.containsAll(expectedStrate), "All expected rules of the strate should be present");
				Assertions.assertTrue(expectedStrate.containsAll(computedStrate), "All rules present in the strate should be expected");
			}
		}
	}

}
