/**
 * Module for unifier elements of InteGraal
 *  
 * @author Florent Tornil
 *
 */
module fr.boreal.unifiers {
	
	requires transitive fr.boreal.model;
	requires fr.lirmm.boreal.util;
    requires com.google.common;

    exports fr.boreal.unifier;
}