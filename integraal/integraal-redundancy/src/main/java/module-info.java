/**
 * Module for util elements of InteGraal
 *
 * @author Paul Fontaine
 *
 */
module fr.boreal.redundancy{

    requires transitive fr.boreal.model;
    
    requires fr.boreal.storage;
    requires fr.boreal.query_evaluation;
    requires fr.boreal.backward_chaining;
    requires fr.boreal.io;

    exports fr.boreal.redundancy;

}