package fr.boreal.test.redundancy;

import fr.boreal.io.api.ParseException;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.rule.impl.FORuleImpl;
import fr.boreal.redundancy.Redundancy;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class RedundancyTest extends TestData {

	static final FOFormulaFactory formulaFactory = FOFormulaFactory.instance();

	static FORule atomicHeadRule (Atom head, Atom...body){
		FOFormula h = formulaFactory.createOrGetConjunction(head);
		FOFormula b = formulaFactory.createOrGetConjunction(body);
		return new FORuleImpl(b,h);
	}

	@Test
	void equivalenceTest1 () {
		FORule r1 = atomicHeadRule(qx, pxy, pxz);
		FORule r2 = atomicHeadRule(qy, pyx);
		Assertions.assertTrue(Redundancy.equivalentRules(r1, r2));
		Assertions.assertTrue(Redundancy.equivalentRules(r2, r1));
	}

	@Test
	void equivalenceTest2 (){
		FORule r1 = atomicHeadRule(qax,pxy);
		FORule r2 = atomicHeadRule(qbx,pxy);
		Assertions.assertFalse(Redundancy.equivalentRules(r1, r2));
		Assertions.assertFalse(Redundancy.equivalentRules(r2, r1));
	}

	@Test
	void consequenceTest1() throws ParseException {
		DlgpParser parser = new DlgpParser("p(X,Y,Z),q(X,Y,Z) :- r(X,Z). " +
				"p(X,Y1,Z),q(X,Y2,Z) :- r(X,Z). ");
		FORule r1 = (FORule) parser.next();
		FORule r2 = (FORule) parser.next();
		parser.close();
		Assertions.assertTrue(Redundancy.ruleConsequence(new RuleBaseImpl(List.of(r1)), r2));
	}

	@Test
	void alphaEquivalenceTest1(){
		FORule r01 = atomicHeadRule(qx, pxy,new AtomImpl(r2,y,a));
		FORule r02 = atomicHeadRule(qy, pyx,new AtomImpl(r2,x,a));
		Assertions.assertTrue(Redundancy.isRuleIsomorphism(r01,r02));
		Assertions.assertTrue(Redundancy.isRuleIsomorphism(r02,r01));
	}
	@Test
	void alphaEquivalenceTest2 () {
		FORule r1 = atomicHeadRule(qx, pxy, pxz);
		FORule r2 = atomicHeadRule(qy, pyx);
		Assertions.assertFalse(Redundancy.isRuleIsomorphism(r1,r2));
		Assertions.assertFalse(Redundancy.isRuleIsomorphism(r2,r1));
	}

	@Test
	void alphaEquivalenceTest3(){
		FORule r1 = atomicHeadRule(qxz, pxy);
		FORule r2 = atomicHeadRule(new AtomImpl(q2,y,fa), pyx);
		Assertions.assertTrue(Redundancy.isRuleIsomorphism(r2,r1));
		Assertions.assertTrue(Redundancy.isRuleIsomorphism(r1,r2));
	}

	@Test
	void alphaEquivalenceTest4(){
		FORule r1 = atomicHeadRule(qxz, pxy);
		FORule r2 = atomicHeadRule(rxz,pxy);
		Assertions.assertFalse(Redundancy.isRuleIsomorphism(r1,r2));
		Assertions.assertFalse(Redundancy.isRuleIsomorphism(r2,r1));
	}

	@Test
	void alphaEquivalenceTest5(){
		FORule r1 = atomicHeadRule(qxz, pxy);
		FORule r2 = atomicHeadRule(new AtomImpl(q2,x,a), pxy);
		Assertions.assertFalse(Redundancy.isRuleIsomorphism(r2,r1));
		Assertions.assertFalse(Redundancy.isRuleIsomorphism(r1,r2));
	}
}
