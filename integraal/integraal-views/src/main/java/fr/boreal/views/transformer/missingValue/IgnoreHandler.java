package fr.boreal.views.transformer.missingValue;

import java.util.Optional;

import fr.boreal.model.logicalElements.api.Term;

/**
 * handles a missing value by ignoring it, replacing it by an empty optional
 * 
 * @author Florent Tornil
 *
 */
public class IgnoreHandler implements MissingValueHandler {

	@Override
	public Optional<Term> handle() {
		return Optional.empty();
	}

}
