package fr.boreal.views.specializer;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.views.datasource.ViewParameters;

/**
 * This specializer replaces a given placeholder from the initial String query by the frozen positions of the given atom.
 * <p>
 * The replacement is done in order of the atom, until the replacement string is not found or all the frozen positions of the atom are taken into account
 * <p>
 * There is no guaranty that the mandatory positions of the parameters is taken into account.
 */
public class OrderedStringReplacementSpecializer implements Specializer<String> {

	private final String placeholder;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new OrderedStringReplacementSpecializer with the given placeholder
	 * @param placeholder to replace
	 */
	public OrderedStringReplacementSpecializer(String placeholder) {
		this.placeholder = placeholder;
	}

	/////////////////////////////////////////////////
	// Public method
	/////////////////////////////////////////////////

	@Override
	public ViewParameters<String> specialize(ViewParameters<String> parameters, Atom a, Substitution s) {
		String query = parameters.nativeQuery();
		int i = 0;
		while(i < a.getPredicate().arity() && query.contains(this.placeholder)) {
			Term t_a = a.getTerm(i);
			if(t_a.isFrozen(s)) {
				String replacement = s.createImageOf(t_a).label();
				query = query.replaceFirst(this.placeholder, replacement);
				++i;
			}
			++i;
		}
		return new ViewParameters<>(query, parameters.viewElements(), parameters.position());
	}

}
