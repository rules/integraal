package fr.boreal.views.transformer.missingValue;

import java.util.Optional;

import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;

/**
 * handles a missing value by replacing it by a fresh variable
 * 
 * @author Florent Tornil
 *
 */
public class OptionalHandler implements MissingValueHandler {

	@Override
	public Optional<Term> handle() {
		return Optional.of(SameObjectTermFactory.instance().createOrGetFreshVariable());
	}

}
