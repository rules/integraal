package fr.boreal.views.transformer;

import java.util.Iterator;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.views.datasource.ViewParameters;

/**
 * Transform a native result into Atoms
 * The transformation and correctness regarding data-types is implementation-specific
 * @param <NativeResultType> the type of the native result t transform into atoms
 */
public interface Transformer<NativeResultType> {

	/**
	 * Computes and returns all the atoms represented by the native results
	 * Some implementations may compute a filter using the given atom when converting the results
	 * @param nativeResults the native results representing atoms
	 * @param parameters the parameters of the view
	 * @param a the atom used for filtering / instantiation
	 * @param s the pre-affectation from variables of <code>a</code> to terms
	 * @return all the atoms represented by the native results
	 */
	Iterator<Atom> transform(NativeResultType nativeResults, ViewParameters<?> parameters, Atom a, Substitution s);

}
