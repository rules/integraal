package fr.boreal.views.datasource;

import java.util.*;

import fr.boreal.model.kb.api.Readable;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.storage.external.evaluator.NativeQueryEvaluator;
import fr.boreal.views.specializer.Specializer;
import fr.boreal.views.transformer.Transformer;
import fr.lirmm.boreal.util.stream.filter.FilterIteratorWithoutException;
import fr.lirmm.boreal.util.stream.filter.MatchFilter;

/**
 * This wrapper represents data-sources accessed using mappings.
 * Theses are sources queried by user-given queries with read-only rights
 * <br/>
 * A data-source can define several relational views, which associate a Predicate (of the relational view) to a specification of this view 
 *
 * @param <NativeQueryType> the type used to represent the native query
 * @param <NativeResultType> the type used to represent the results as the native format
 */
public abstract class AbstractViewWrapper<NativeQueryType, NativeResultType> implements Readable {

	private String name; 
	private final Specializer<NativeQueryType> specializer;
	private final NativeQueryEvaluator<NativeQueryType, NativeResultType> evaluator;
	private final Transformer<NativeResultType> transformer;

	private final Map<Predicate, ViewParameters<NativeQueryType>> viewByPredicate;

	/**
	 * Create a new wrapper over a data-source with a relational view
	 * @param specializer to specialize the native query
	 * @param evaluator to evaluate the native query
	 * @param transformer to transform the native result into atoms
	 */
	public AbstractViewWrapper(Specializer<NativeQueryType> specializer, NativeQueryEvaluator<NativeQueryType, NativeResultType> evaluator, Transformer<NativeResultType> transformer) {
		this(null, specializer,evaluator,transformer);
	}
	
	/**
	 * Create a new wrapper over a data-source with a relational view
	 * @param name to identify the datasource
	 * @param specializer to specialize the native query
	 * @param evaluator to evaluate the native query
	 * @param transformer to transform the native result into atoms
	 */
	public AbstractViewWrapper(String name, Specializer<NativeQueryType> specializer, NativeQueryEvaluator<NativeQueryType, NativeResultType> evaluator, Transformer<NativeResultType> transformer) {
		this.name = name;
		this.specializer = specializer;
		this.evaluator = evaluator;
		this.transformer = transformer;
		this.viewByPredicate = new HashMap<>();
	}

	/**
	 * Register the given view and configuration
	 * Replaces existing mapping associated to the given predicate if any exists
	 * @param p view predicate associated to this mapping
	 * @param viewParameters associated to this view predicate
	 */
	public void registerView(Predicate p, ViewParameters<NativeQueryType> viewParameters) {
		this.viewByPredicate.put(p, viewParameters);
	}

	/**
	 * @param p the predicate representing the relational view
	 * @return the native query associated to the view predicate p
	 */
	public ViewParameters<NativeQueryType> getViewParameters(Predicate p) {
		return this.viewByPredicate.get(p);
	}

	/**
	 * @param p the predicate representing the relational view
	 * @return the native query specializer associated to the view predicate p
	 */
	public Specializer<NativeQueryType> getSpecializer(Predicate p) {
		return this.specializer;
	}

	/**
	 * @param p the predicate representing the relational view
	 * @return the native query evaluator associated to the view predicate p
	 */
	public NativeQueryEvaluator<NativeQueryType, NativeResultType> getEvaluator(Predicate p) {
		return this.evaluator;
	}

	/**
	 * @param p the predicate representing the relational view
	 * @return the native result transformer associated to the view predicate p
	 */
	public Transformer<NativeResultType> getTransformer(Predicate p) {
		return this.transformer;
	}

	/**
	 * @return the predicates corresponding to the relational views of the datasource
	 */
	public Collection<Predicate> getPredicates() {
		return this.viewByPredicate.keySet();
	}
	
	/**
	 * @return an optional that contains the datasource name if there is one
	 */
	public Optional<String> getName() {
		return Optional.ofNullable(this.name);
	}
	
	/**
	 * @param name the new datasource name
	 */
	public void setName(String name) {
		this.name=name;
	}
	


	/////////////////////////////////////////////////
	// FactStorage methods
	/////////////////////////////////////////////////

	@Override
	public Iterator<Atom> match(Atom a, Substitution s) {
		Predicate p = a.getPredicate();
		ViewParameters<NativeQueryType> viewParameters = this.getViewParameters(p);

		ViewParameters<NativeQueryType> specializedViewParameters = this.getSpecializer(p).specialize(viewParameters, a, s);

		Optional<NativeResultType> nativeResult = this.getEvaluator(p).evaluate(specializedViewParameters.nativeQuery());
		if(nativeResult.isEmpty()) {
			return Collections.emptyIterator();
		} else {
			Iterator<Atom> matched_atoms = this.getTransformer(p).transform(nativeResult.get(), specializedViewParameters, a, s);
			return new FilterIteratorWithoutException<>(matched_atoms, new MatchFilter(a, s));
		}

	}

	@Override
	public Iterator<Atom> match(Atom a) {
		return this.match(a, new SubstitutionImpl());
	}

}
