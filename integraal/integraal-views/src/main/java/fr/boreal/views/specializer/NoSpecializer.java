package fr.boreal.views.specializer;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.views.datasource.ViewParameters;

/**
 * This specializer do nothing.
 * <p>
 * The query must not have any mandatory parameters
 * 
 * @param <NativeQueryType> the type of the query to specialize
 */
public class NoSpecializer<NativeQueryType> implements Specializer<NativeQueryType> {

	@Override
	public ViewParameters<NativeQueryType> specialize(ViewParameters<NativeQueryType> parameters, Atom filter, Substitution s) {
		return parameters;
	}

}
