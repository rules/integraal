package fr.boreal.views.datasource;

import java.util.Optional;

/**
 * @author Florent Tornil
 *
 * @param missingValueHandling the strategy to handle missing values
 * @param mandatoryAs iff the parameter is mandatory, the value of the replacement string, empty otherwise
 * @param selection iff this is the signature of a json object, represents the selection JSONPath query
 */
public record ViewParameterSignature(String missingValueHandling,
		Optional<String> mandatoryAs, Optional<String> selection) {

	/**
	 * @return true iff this parameter is mandatory
	 */
	public boolean isMandatory() {
		return this.mandatoryAs().isPresent();
	}
	
}
