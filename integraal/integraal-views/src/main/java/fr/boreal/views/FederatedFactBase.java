package fr.boreal.views;

import com.google.common.collect.Iterators;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.FactBaseType;
import fr.boreal.model.kb.api.Readable;
import fr.boreal.model.kb.impl.FactBaseDescription;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.views.datasource.AbstractViewWrapper;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Stream;

/**
 * A factBase made of several FactBases. This storage system assumes that the
 * Predicate of the Atom is enough to determine to which sub-storage-system we
 * need to delegate the queries
 * 
 * @author Florent Tornil
 *
 */
public class FederatedFactBase implements FactBase {

	private final Map<Predicate, Readable> facts_by_storage;
	private FactBase default_storage;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Constructor with only a local storage
	 * 
	 * @param default_base the default storage in writing mode
	 */
	public FederatedFactBase(FactBase default_base) {
		this(default_base, new HashMap<>());
	}

	/**
	 * Constructor with a local storage and another storage
	 * 
	 * @param default_base the default storage in writing mode
	 * @param p            a predicate
	 * @param storage      a storage associated with p
	 */
	public FederatedFactBase(FactBase default_base, Predicate p, Readable storage) {
		this(default_base);
		this.addStorage(p, storage);
	}

	/**
	 * Constructor with a local storage and a colelction of view wrappers
	 *
	 * @param default_base the default storage in writing mode
	 * @param views        the views
	 */
	public FederatedFactBase(FactBase default_base, Collection<AbstractViewWrapper<String, ?>> views) {
		this(default_base);
		for (AbstractViewWrapper<String, ?> view : views) {
			for (Predicate p : view.getPredicates()) {
				this.addStorage(p, view);
			}
		}
	}

	/**
	 * Constructor with multiple storages associated to predicates
	 * 
	 * @param default_base     the default storage in writing mode
	 * @param facts_by_storage a map of storage by predicate
	 */
	public FederatedFactBase(FactBase default_base, Map<Predicate, Readable> facts_by_storage) {
		this.default_storage = default_base;
		this.facts_by_storage = facts_by_storage;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public Iterator<Atom> match(Atom a) {
		return this.chooseStorage(a.getPredicate()).match(a);
	}

	@Override
	public Iterator<Atom> match(Atom a, Substitution s) {
		return this.chooseStorage(a.getPredicate()).match(a, s);
	}

	/**
	 * Add the given association
	 * 
	 * @param p the predicate
	 * @param s the storage
	 */
	public void addStorage(Predicate p, Readable s) {
		this.facts_by_storage.put(p, s);
	}

	/**
	 * Add the given storage as default storage. Replace the existing one is any
	 * exists
	 * 
	 * @param s the storage
	 */
	public void setDefaultStorage(FactBase s) {
		this.default_storage = s;
	}

	/**
	 * @return the default storage
	 */
	public FactBase getDefaultStorage() {
		return this.default_storage;
	}

	/**
	 * @return the mappings
	 */

	public Map<Predicate, Readable> getViewDefinitions() {

		return facts_by_storage;

	}

	/////////////////////////////////////////////////
	// Writable methods
	/////////////////////////////////////////////////

	@Override
	public boolean add(Atom atom) {
		return this.default_storage.add(atom);
	}

	@Override
	public boolean add(FOFormula atoms) {
		return this.default_storage.add(atoms);
	}

	@Override
	public boolean addAll(Collection<Atom> atoms) {
		return this.default_storage.addAll(atoms);
	}

	@Override
	public boolean remove(Atom atom) {
		return this.default_storage.remove(atom);
	}

	@Override
	public boolean remove(FOFormula atoms) {
		return this.default_storage.remove(atoms);
	}

	@Override
	public boolean removeAll(Collection<Atom> atoms) {
		return this.default_storage.removeAll(atoms);
	}

	@Override
	public long size() {
		return this.default_storage.size();
	}

	/////////////////////////////////////////////////
	// FactBase Methods
	/////////////////////////////////////////////////

	@Override
	public Stream<Atom> getAtoms() {
		// System.err.println("WARNING : Used atomset access on a federated datasource
		// with views ; returned only the local facts");
		return this.default_storage.getAtoms();
	}

	@Override
	public Iterator<Atom> getAtomsByPredicate(Predicate predicate) {
		if (this.facts_by_storage.containsKey(predicate)) {
			System.err.println("WARNING : Used index AtomsByPredicate on a view datasource ; returned null");
			return null;
		} else {
			return this.default_storage.getAtomsByPredicate(predicate);
		}
	}

	@Override
	public Iterator<Predicate> getPredicates() {
		return Iterators.concat(this.facts_by_storage.keySet().iterator(), this.default_storage.getPredicates());
	}

	@Override
	public Iterator<Term> getTermsByPredicatePosition(Predicate p, int position) {
		if (this.facts_by_storage.containsKey(p)) {
			System.err.println("WARNING : Used index TermsByPredicatePosition on a view datasource ; returned null");
			return null;
		} else {
			return this.default_storage.getTermsByPredicatePosition(p, position);
		}
	}

	@Override
	public FactBaseDescription getDescription(Predicate viewPredicate) {
		return null;
	}

	@Override
	public FactBaseType getType(Predicate viewPredicate) {
		return FactBaseType.GRAAL;
	}

	/////////////////////////////////////////////////
	// Private methods
	/////////////////////////////////////////////////

	private Readable chooseStorage(Predicate p) {
		return this.facts_by_storage.getOrDefault(p, this.default_storage);
	}
}