package fr.boreal.views.transformer;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.views.datasource.ViewParameterSignature;
import fr.boreal.views.datasource.ViewParameters;
import fr.boreal.views.transformer.missingValue.FreezeHandler;
import fr.boreal.views.transformer.missingValue.IgnoreHandler;
import fr.boreal.views.transformer.missingValue.MissingValueHandler;
import fr.boreal.views.transformer.missingValue.OptionalHandler;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 * @author Florent Tornil
 *
 * @param <NativeResultType> the type of the native result
 */
public abstract class AbstractTransformer<NativeResultType> implements Transformer<NativeResultType> {

	@Override
	public Iterator<Atom> transform(NativeResultType nativeResults,
			ViewParameters<?> parameters,
			Atom a, Substitution s) {
		return this.transform(nativeResults, parameters.viewElements(), a, s);
	}

	/**
	 * Computes and returns all the atoms represented by the native results
	 * Some implementations may compute a filter using the given atom when converting the results
	 * @param nativeResults the native results representing atoms
	 * @param viewElements the signature of the view's tuples
	 * @param a the atom used for filtering / instantiation
	 * @param s the pre-affectation from variables of <code>a</code> to terms
	 * @return all the atoms represented by the native results
	 */
	public abstract Iterator<Atom> transform(NativeResultType nativeResults, List<ViewParameterSignature> viewElements, Atom a,
			Substitution s);

	/**
	 * Convert a native object into the corresponding atom
	 * @param nativeResult the native object
	 * @param signatures the signature of the view's tuples
	 * @param a atom used for filtering
	 * @param s pre affectation
	 * @return the Term corresponding to the given object with respect to the signature
	 */
	public Optional<Atom> transformAtom(Object nativeResult, List<ViewParameterSignature> signatures, Atom a, Substitution s) {
		int arity = a.getPredicate().arity();

		List<Term> terms = new ArrayList<>(arity);
		int nativeResultIndex = 0;
		for(int signatureIndex = 0; signatureIndex < signatures.size(); ++signatureIndex) {
			ViewParameterSignature signature = signatures.get(signatureIndex);
			if(signature.isMandatory()) {
				Term givenValue = s.createImageOf(a.getTerm(signatureIndex));
				terms.add(givenValue);
			} else {
				Object o = this.getObjectAtIndex(nativeResult, nativeResultIndex, signature);
				Optional<Term> opt_t = this.transformTerm(o, signature, a, s);
				if(opt_t.isEmpty()) {
					return Optional.empty();
				} else {
					terms.add(opt_t.get());
					++nativeResultIndex;
				}
			}
		}
		return Optional.of(new AtomImpl(a.getPredicate(), terms));
	}

	private Optional<Term> transformTerm(Object o, ViewParameterSignature signature, Atom a, Substitution s) {
		if(this.isMissingValue(o)) {
			MissingValueHandler missingHandler = switch (signature.missingValueHandling()) {
			case "IGNORE" -> new IgnoreHandler();
			case "FREEZE" -> new FreezeHandler();
			case "OPTIONAL" -> new OptionalHandler();
			default -> throw new IllegalArgumentException("Unknown handling type: " + signature.missingValueHandling());
			};
			return missingHandler.handle();
		} else {
			return this.convertType(o, signature, a, s);
		}
	}

	/**
	 * Get the native object at the given position of the native result
	 * @param nativeResult the native result
	 * @param nativeResultIndex the index
	 * @param signature signature of the element of the tuple to get
	 * @return the native object at the given position of the native result
	 */
	public abstract Object getObjectAtIndex(Object nativeResult, int nativeResultIndex, ViewParameterSignature signature);

	/**
	 * Does the given object represents the native datasource missing (NULL) value
	 * @param o a native element
	 * @return true iff o represents the native type missing value
	 */
	public abstract boolean isMissingValue(Object o);

	/**
	 * Convert a native object into the corresponding term
	 * @param o the native object
	 * @param signature the signature of the view's tuple for the position of o
	 * @param a atom used for filtering
	 * @param s pre affectation
	 * @return the Term corresponding to the given object with respect to the signature
	 */
	public abstract Optional<Term> convertType(Object o, ViewParameterSignature signature, Atom a, Substitution s);

}
