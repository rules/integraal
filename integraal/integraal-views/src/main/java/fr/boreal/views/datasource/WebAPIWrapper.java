package fr.boreal.views.datasource;

import fr.boreal.storage.external.evaluator.HttpQueryEvaluator;
import fr.boreal.views.specializer.MandatoryParameterStringReplacement;
import fr.boreal.views.transformer.JSONStringTransformer;

/**
 * Access to a datasource using web api queries
 * 
 * @author Florent Tornil
 *
 */
public class WebAPIWrapper extends AbstractViewWrapper<String, String> {

	/**
	 * Create a new datasource on a web api
	 * @param username the user login name
	 * @param password the user login password
	 */
	public WebAPIWrapper(String username, String password) {
		super(	new MandatoryParameterStringReplacement(),
				new HttpQueryEvaluator(username, password),
				new JSONStringTransformer());
	}

}
