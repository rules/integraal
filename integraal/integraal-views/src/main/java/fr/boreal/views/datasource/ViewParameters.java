package fr.boreal.views.datasource;

import java.util.List;
import java.util.Optional;

/**
 * @author Florent Tornil
 *
 * @param <NativeQueryType> the type used to represent the native query
 * @param nativeQuery the native query
 * @param viewElements the elements of the view
 * @param position iff this is the parameters of a json view, represents the position JSONPath query
 */
public record ViewParameters<NativeQueryType>(
		NativeQueryType nativeQuery,
		List<ViewParameterSignature> viewElements,
		Optional<String> position) { }
