package fr.boreal.views.transformer.missingValue;

import java.util.Optional;

import fr.boreal.model.logicalElements.api.Constant;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;

/**
 * handles a missing value by replacing it by a constant
 * 
 * @author Florent Tornil
 *
 */
public class FreezeHandler implements MissingValueHandler {
	
	/**
	 * Value of the missing constant
	 */
	public static final Constant missing = SameObjectTermFactory.instance().createOrGetConstant("__missing_value__");

	@Override
	public Optional<Term> handle() {
		return Optional.of(missing);
	}
}
