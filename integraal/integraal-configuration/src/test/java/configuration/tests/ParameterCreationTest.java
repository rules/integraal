package configuration.tests;
import fr.boreal.configuration.keywords.InteGraalKeywords;
import fr.boreal.configuration.parameters.IGParameter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runners.Parameterized.Parameters;

import java.time.Duration;
import java.util.stream.Stream;

/*
 * Checks that parameters are correctly converted
 */
public class ParameterCreationTest {

    // Test Integer parameters
    private static final String max = "MAX";
    private static final String rank = "Rank";
    private static final String val0 = "0";
    private static final String valNeg = "-3";
    private static final String val5 = "5";

    //Test Duration parameters
    private static final String timeout = "TimeOut";
    private static final String val5s = "PT5s";
    private static final String val0s = "pt0s";

    // Test Enum parameters
    private static final String checker = "CHECKER";
    private static final String images = "IMAGES";
    private static final String valChecker = "oblivious";
    private static final String valImages = "ALL";

    // Test Enum parameters open values
    private static final String url = InteGraalKeywords.URL.toString();
    private static final String urlVal = "/u/r/l";

    // Test unknown, empty or null parameters
    private static final String unknown = "Unknown";
    private static final String empty = "";
    private static final String nullPara = null;


    @Parameters
    static Stream<Arguments> data() {
        return Stream.of(
                Arguments.of(InteGraalKeywords.MAX, 5),
                Arguments.of(InteGraalKeywords.TIMEOUT, Duration.parse("PT5s")),
                Arguments.of(InteGraalKeywords.CHECKER, InteGraalKeywords.Algorithms.Parameters.Chase.Checker.OBLIVIOUS),
                Arguments.of(InteGraalKeywords.URL, urlVal)
        );
    }

    @Parameters
    static Stream<Arguments> negativeExamples() {
        return Stream.of(
                Arguments.of(InteGraalKeywords.MAX, "PT5s"),
                Arguments.of(InteGraalKeywords.CHECKER, InteGraalKeywords.Algorithms.Parameters.Chase.Applier.PARALLEL)
        );
    }

    @Parameters
    static Stream<Arguments> openParameters() {
        return Stream.of(
                Arguments.of(InteGraalKeywords.URL, urlVal)
                );
    }


    @DisplayName("Test converter ")
    @ParameterizedTest(name = "{index}: test creation <{0},{1}> should always succeed")
    @MethodSource({"data", "openParameters"})
    public void testConverterTest(Object name, Object value) {
        new IGParameter(name, value);
    }

    @DisplayName("Test converter Negative Examples ")
    @ParameterizedTest(name = "{index}: test creation <{0},{1}> should raise IGParameterException")
    @MethodSource("negativeExamples")
    public void testConverterTestNegativeExamples(Object name, Object value) {
        try{
            new IGParameter(name, value);
            Assertions.fail();
        } catch (Exception e) {
            //test ok
        }
    }


}
