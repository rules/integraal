package fr.boreal.configuration.parameters;

import fr.boreal.configuration.keywords.InteGraalKeywords;

/**
 * Exception for errors caused by inconsistent use of the {@link IGParameter}
 * record with respect to {@link InteGraalKeywords}.
 *
 * @author Pierre Bisquert, Michel Leclère
 */
public class IGParameterException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new instance of IGParameterException with the specified errorMessage.
	 * 
	 * @param errorMessage The detailed message of the exception.
	 * 
	 */
	public IGParameterException(String errorMessage) {
		super(errorMessage);
	}
}

