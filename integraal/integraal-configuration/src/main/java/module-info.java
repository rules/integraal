/**
 * High level components for the main functionalities of InteGraal
 *
 * @author Federico
 *
 */
open module fr.boreal.configuration {
    requires fr.lirmm.boreal.util;
    exports fr.boreal.configuration.keywords;
    exports fr.boreal.configuration.parameters;

}