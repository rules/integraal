package fr.boreal.storage.external.rdbms.driver;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.postgresql.ds.PGSimpleDataSource;
import org.postgresql.util.PSQLException;
import org.postgresql.util.PSQLState;

import fr.boreal.storage.external.rdbms.layout.RDBMSStorageLayout;

/**
 * Driver for PostgreSQL databases
 */
public class PostgreSQLDriver implements RDBMSDriver {

	private final String JDBCString;
	private final PGSimpleDataSource ds;
	private final Connection test_connection;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new driver using the given parameters
	 * 
	 * @param host     the url of the database
	 * @param dbName   the name of the database
	 * @param user     the username used to connect to the database
	 * @param password the password used to connect to the database
	 * @throws PSQLException iff some error occur
	 */
	public PostgreSQLDriver(String host, String dbName, String user, String password) throws PSQLException {
		this("jdbc:postgresql://" + host + "/" + dbName + "?user=" + user + "&password=" + password);
	}

	/**
	 * Creates a new driver over the given jdbc string
	 * 
	 * @param uri jdbc string
	 * @throws PSQLException iff some error occur
	 */
	public PostgreSQLDriver(String uri) throws PSQLException {
		this.ds = new PGSimpleDataSource();
		this.ds.setUrl(uri);
		this.JDBCString = uri;
		try {
			this.test_connection = this.ds.getConnection();
		} catch (SQLException e) {
			PSQLState state = null;
			for (int i = 0; state == null && i < PSQLState.values().length; ++i) {
				PSQLState state_i = PSQLState.values()[i];
				if (state_i.getState().equals(e.getErrorCode() + "")) {
					state = state_i;
				}
			}
			throw new PSQLException(
					"[PostgreSQLDriver] An error occurred while connecting to the PostgreSQL database at " + uri + "\n"
							+ "Please make sure this connection path is correct and the database is accessible.",
					state, e);
		}
	}

	@Override
	public String getJDBCString() {
		return this.JDBCString;
	}

	@Override
	public DataSource getDatasource() {
		return this.ds;
	}

	@Override
	public Connection getConnection() {
		return this.test_connection;
	}

	@Override
	public String getBaseSafeInsertQuery() {
		return "INSERT INTO %t VALUES(%f) ON CONFLICT DO NOTHING;";
	}

	@Override
	public String getBaseSafeInsertSelectQuery() {
		return "INSERT INTO %t %s ON CONFLICT DO NOTHING;";
	}

	@Override
	public String getTextFieldName() {
		return "TEXT";
	}

	@Override
	public String getNumberFieldName() {
		return "INT";
	}

	@Override
	public String getCSVCopyQuery(String tableName, String csvFilePath, char delimiter, int headerSize) {
		return "COPY " + tableName + " FROM stdin WITH (FORMAT CSV); ";
	}

	/**
	 * Clears all tables in the database by truncating them.
	 * 
	 * @throws SQLException If an error occurs during database access.
	 */
	@Override
	public void dropAllTables() throws SQLException {
		Connection conn = null;
		Statement stmt = null;
		try {
			conn = this.getConnection(); // Use existing method to get connection
			DatabaseMetaData metaData = conn.getMetaData();
			String[] types = { "TABLE" };
			ResultSet rs = metaData.getTables(null, null, "%", types);

			// PostgreSQL does not allow TRUNCATE on a table that has foreign key references
			// from other tables,
			// so we disable foreign key checks temporarily.
			stmt = conn.createStatement();
			stmt.execute("SET session_replication_role = replica;");

			while (rs.next()) {
				String tableName = rs.getString("TABLE_NAME");

				if (tableName.startsWith(RDBMSStorageLayout.ATOM_TABLE_PREDICATE_PREFIX)
						|| tableName.startsWith(RDBMSStorageLayout.TERM_TABLE_NAME)
						|| tableName.startsWith(RDBMSStorageLayout.PREDICATE_TABLE_NAME)) {

					stmt.executeUpdate("DROP TABLE " + tableName + " CASCADE;");

				}
			}

			stmt.execute("SET session_replication_role = DEFAULT;");
		} catch (SQLException e) {
			throw new SQLException("Failed to clear all tables: " + e.getMessage(), e);
		} finally {
			if (stmt != null) {
				stmt.close();
			}
			if (conn != null && conn != this.test_connection) {
				conn.close(); // Close the connection if it's not the class's persistent connection
			}
		}
	}

}
