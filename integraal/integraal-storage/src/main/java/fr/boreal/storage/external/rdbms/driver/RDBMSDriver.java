package fr.boreal.storage.external.rdbms.driver;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * RDBMSDrivers represents a way to connect and physically execute queries over
 * a database. This is a wrapper over java.sql which also give access to some
 * useful and driver-specific SQL queries.
 * <br/>
 * A driver may keep a connection open in order to execute queries faster
 */
public interface RDBMSDriver {

	/**
	 * @return the {@link DataSource} represented by this driver
	 */
	DataSource getDatasource();

	/**
	 * A driver may keep a connection open in order to execute queries faster or
	 * create a new one each time
	 * 
	 * @return a {@link Connection} for the database represented by this driver
	 */
	Connection getConnection();

	/**
	 * @return the jdbc connection string
	 */
	String getJDBCString();

	/**
	 * @param table_name the name of the table
	 * @return true iff the database represented by this driver have a table with
	 *         the given name
	 * @throws SQLException iff something bad happen
	 */
	default boolean hasTable(String table_name) throws SQLException {
		DatabaseMetaData dbMetaData = this.getConnection().getMetaData();
		ResultSet results = dbMetaData.getTables(null, null, table_name, new String[] { "TABLE" });
		if (results.next()) {
			return results.getString(3).equals(table_name);
		}
		return false;
	}

	/**
	 * @return the base SELECT query corresponding to this driver
	 */
	default String getBaseSelectQuery() {
		return "SELECT %s FROM %t";
	}

	/**
	 * @return the base SELECT query with a WHERE clause corresponding to this
	 *         driver
	 */
	default String getBaseSelectFilteredQuery() {
		return "SELECT %s FROM %t WHERE %c";
	}

	/**
	 * @return the base alias used to name tables for this driver
	 */
	default String getBaseTableAlias() {
		return "atom";
	}

	/**
	 * @return the base INSERT query corresponding to this driver
	 */
	default String getBaseInsertQuery() {
		return "INSERT INTO %t VALUES(%d);";
	}

	/**
	 * @return the base INSERT query which does not create exceptions in case of
	 *         duplicates corresponding to this driver
	 */
	String getBaseSafeInsertQuery();

	/**
	 * @return the base INSERT query using a SELECT clause which does not create
	 *         exceptions in case of duplicates corresponding to this driver
	 */
	String getBaseSafeInsertSelectQuery();

	/**
	 * @return the name of the Text field corresponding to this driver
	 */
	String getTextFieldName();

	/**
	 * @return the name of the Number field corresponding to this driver
	 */
	String getNumberFieldName();

	/**
	 * @param tableName   table name to store data
	 * @param csvFilePath path to the csv file
	 * @param delimiter   delimiter of the csv file
	 * @param headerSize  size of the csv header
	 * @return the query used to copy a CSV file into the database system
	 */
	String getCSVCopyQuery(String tableName, String csvFilePath, char delimiter, int headerSize);

	/**
	 * Clears all tables in the database by truncating them.
	 * 
	 * @throws SQLException If an error occurs during database access.
	 */
	default void dropAllTables() throws SQLException {
		Logger LOG = LoggerFactory.getLogger(getClass());
        LOG.error("Unsupported operation for {}", getClass().getSimpleName());
		throw new UnsupportedOperationException(
				String.format("[%s::dropAllTables] Unsupported operation for this class.",
						getClass()));
	}

	/**
	 * Drops all indexes in the database except for primary keys.
	 */
	default void dropAllIndexes() {
		Logger LOG = LoggerFactory.getLogger(getClass());
        LOG.error("Unsupported operation for {}", getClass().getSimpleName());
        throw new UnsupportedOperationException(
                String.format("[%s::dropAllIndexes] Unsupported operation for this class.",
                        getClass()));
	}

}
