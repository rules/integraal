package fr.boreal.storage.external.rdbms.layout;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.storage.external.rdbms.SQLParameterizedQuery;
import fr.boreal.storage.external.rdbms.driver.RDBMSDriver;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ArrayListHandler;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.*;

/**
 * The AdHoc strategy stores atoms as follows
 * <br/>
 * A predicate table stores all the predicates associated with their arity and
 * the corresponding table Each predicate is associated to an SQL table A term
 * table stores all the terms and their corresponding type (variable, literal or
 * constant)
 * <br/>
 * For each predicate, the associated table contains its arity number of
 * columns Each column represents a position in the atom Each row stores a tuple
 * representing a list of terms corresponding to an atom.
 * 
 */
public class AdHocSQLLayout implements RDBMSStorageLayout {

	/**
	 * RDBMS Driver
	 */
	protected final RDBMSDriver driver;

	/**
	 * Apache runner to execute SQL queries
	 */
	protected final QueryRunner runner;

	private final Map<Predicate, String> table_name_by_predicate = new HashMap<>();
	private long predicate_count = -1;

	/**
	 * Determine with which letter to store the type of the given term in the
	 * database
	 * 
	 * @param t a term
	 * @return the encoding letter of the type of t
	 */
	protected static String getType(Term t) {
		if (t.isVariable()) {
			return "V";
		} else if (t.isLiteral()) {
			return "L";
		} else {
			return "C";
		}
	}

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new Strategy over the given driver
	 * 
	 * @param driver the database driver
	 */
	public AdHocSQLLayout(RDBMSDriver driver) {
		this.driver = driver;
		DataSource ds = driver.getDatasource();
		this.runner = new QueryRunner(ds);
	}

	/////////////////////////////////////////////////
	// SQLStorageStrategy methods
	/////////////////////////////////////////////////

	@Override
	public boolean canHandleFiltering() {
		return true;
	}

	@Override
	public String getTableName(Atom atom) throws SQLException {
		Predicate p = atom.getPredicate();
		if (table_name_by_predicate.containsKey(p)) {
			return table_name_by_predicate.get(p);
		}
		try {
			List<Object[]> result = this.runner.query(this.driver.getConnection(), this.get_predicate_table_query(),
					new ArrayListHandler(), p.label(), p.arity());
			if (result.isEmpty()) {
				String fresh_table_name = this.getFreshPredicateTableName() + "_"
						+ p.toString().replaceAll("[^a-zA-Z]", "");
				this.runner.insert(this.driver.getConnection(), this.get_insert_predicate_query(),
						new ArrayListHandler(), p.label(), p.arity(), fresh_table_name);
				String query = this.get_create_atom_table().replace("%s", fresh_table_name);
				for (int i = 0; i < p.arity(); ++i) {
					if (i == p.arity() - 1) {
						query = query.replace("%f",
								this.getColumnName(fresh_table_name, i) + " " + this.driver.getTextFieldName());
					} else {
						query = query.replace("%f", this.getColumnName(fresh_table_name, i) + " "
								+ this.driver.getTextFieldName() + ", %f");
					}
				}
				this.runner.update(this.driver.getConnection(), query);
				table_name_by_predicate.put(p, fresh_table_name);
				return fresh_table_name;
			} else {
				String table_name = (String) result.getFirst()[0];
				table_name_by_predicate.put(p, table_name);
				return table_name;
			}
		} catch (SQLException e) {
			throw new SQLException(
					"[AdHocSQLStrategy] An SQL error occurred while trying to get the name of the table for the atom "
							+ atom,
					e);
		}
	}

	@Override
	public Collection<String> getAllTableNames() throws SQLException {
		Collection<String> table_names = new ArrayList<>();
		try {
			List<Object[]> results = this.runner.query(this.driver.getConnection(),
					"SELECT predicate_table FROM " + this.get_predicate_table_name() + ";", new ArrayListHandler());
			for (Object[] res : results) {
				table_names.add(res[0].toString());
			}
		} catch (SQLException e) {
			throw new SQLException(
					"[AdHocSQLStrategy] An SQL error occurred while trying to get the name of all the tables in the database",
					e);
		}
		return table_names;
	}

	@Override
	public Collection<Predicate> getAllPredicates(PredicateFactory factory) throws SQLException {
		Collection<Predicate> predicates_names = new ArrayList<>();
		try {
			List<Object[]> results = this.runner.query(this.driver.getConnection(), this.get_all_predicates_query(),
					new ArrayListHandler());
			for (Object[] res : results) {
				predicates_names
						.add(this.createPredicate(res[0].toString(), Integer.parseInt(res[1].toString()), factory));
			}
		} catch (SQLException e) {
			throw new SQLException(
					"[AdHocSQLStrategy] An SQL error occurred while trying to get the name of all the predicates in the database",
					e);
		}
		return predicates_names;
	}

	@Override
	public void handleTerms(Set<Term> terms) throws SQLException {
		String query = this.driver.getBaseSafeInsertQuery();
		query = query.replace("%t", this.getTermsTableName() + "(term, term_type)");
		query = query.replace("%f", "? ,?");
		List<String[]> arguments = terms.parallelStream().map(term -> new String[] { term.label(), getType(term) }).toList();
		try {
			this.runner.insertBatch(this.driver.getConnection(), query, new ArrayListHandler(),
					arguments.toArray(new String[arguments.size()][]));
		} catch (SQLException e) {
			throw new SQLException("[AdHocSQLStrategy] An SQL error occurred while trying to insert terms", e);
		}
	}

	@Override
	public Term createTerm(String term, TermFactory factory) throws SQLException {
		List<Object[]> result = this.runner.query(this.driver.getConnection(), this.get_term_table_query(),
				new ArrayListHandler(), term);
		if (!result.isEmpty()) {
			String type = result.getFirst()[1].toString();
            return switch (type) {
                case "V" -> factory.createOrGetVariable(term);
                case "C" -> factory.createOrGetConstant(term);
                case "L" -> factory.createOrGetLiteral(term);
                default -> throw new IllegalArgumentException("Unexpected value: " + type);
            };
		} else {
			// Ugly solution : CSV specific missing values from index
			return factory.createOrGetLiteral(term);
		}
	}

	@Override
	public Predicate createPredicate(String predicate, int arity, PredicateFactory factory) {
		return factory.createOrGetPredicate(predicate, arity);
	}

	@Override
	public String getColumnName(String table, int term_index) {
		return "TERM" + term_index;
	}

	@Override
	public SQLParameterizedQuery addSpecificConditions(String sql_query, List<Object> arguments, FOQuery<?> q) {
		return new SQLParameterizedQuery(sql_query, arguments);
	}

	@Override
	public boolean hasCorrectDatabaseSchema(RDBMSDriver driver) throws SQLException {
		return driver.hasTable(this.get_predicate_table_name()) && driver.hasTable(this.getTermsTableName());
	}

	@Override
	public void createDatabaseSchema(RDBMSDriver driver) throws SQLException {
		String predicate_query = this.get_create_predicate_table_query().replaceAll("%s",
				this.driver.getTextFieldName());
		predicate_query = predicate_query.replaceAll("%i", this.driver.getNumberFieldName());
		this.runner.update(this.driver.getConnection(), predicate_query);
		String term_query = this.get_create_terms_table_query().replaceAll("%s", this.driver.getTextFieldName());
		term_query = term_query.replaceAll("%i", this.driver.getTextFieldName());
		this.runner.update(this.driver.getConnection(), term_query);
	}

	@Override
	public String getTermsTableName() {
		return RDBMSStorageLayout.TERM_TABLE_NAME;
	}

	/////////////////////////////////////////////////
	// Private methods
	/////////////////////////////////////////////////

	private String getFreshPredicateTableName() throws SQLException {
		if (this.predicate_count == -1) {
			List<Object[]> result = this.runner.query(this.driver.getConnection(), this.get_count_predicate_query(),
					new ArrayListHandler());
			if (result.isEmpty()) {
				this.predicate_count = 0;
			} else {
				this.predicate_count = Long.parseLong(result.getFirst()[0].toString());
			}
		}
		return this.get_atom_table_prefix() + this.predicate_count++;
	}

	/////////////////////////////////////////////////
	// Constant getters
	/////////////////////////////////////////////////

	// methods are protected because they can be redefined by subclasses

	/**
	 * @return the name of the predicate table
	 */
	protected String get_predicate_table_name() {
		return RDBMSStorageLayout.PREDICATE_TABLE_NAME;
	}

	/**
	 * @return the SQL query to create the predicate table
	 */
	protected String get_create_predicate_table_query() {
		return "CREATE TABLE " + this.get_predicate_table_name()
				+ "(predicate_label %s PRIMARY KEY, predicate_arity %i, predicate_table %s);";
	}

	/**
	 * @return the SQL query to get the table storing the parameterized predicate
	 */
	protected String get_predicate_table_query() {
		return "SELECT predicate_table" + " FROM " + this.get_predicate_table_name()
				+ " WHERE predicate_label = ? AND predicate_arity = ?;";
	}

	/**
	 * @return the SQL query to get all the predicates (label and arity) in the
	 *         database
	 */
	protected String get_all_predicates_query() {
		return "SELECT predicate_label, predicate_arity" + " FROM " + this.get_predicate_table_name() + ";";
	}

	/**
	 * @return the SQL query to insert a new predicate
	 */
	protected String get_insert_predicate_query() {
		return "INSERT INTO " + this.get_predicate_table_name() + " VALUES (?, ?, ?);";
	}

	/**
	 * @return the SQL query to count the number of predicates
	 */
	protected String get_count_predicate_query() {
		return "SELECT count(*) FROM " + this.get_predicate_table_name() + ";";
	}

	/**
	 * @return the SQL query to create the term table
	 */
	protected String get_create_terms_table_query() {
		return "CREATE TABLE " + this.getTermsTableName() + "(term %s PRIMARY KEY, term_type %s);";
	}

	/**
	 * @return the SQL query to get a term by its label
	 */
	protected String get_term_table_query() {
		return "SELECT term, term_type" + " FROM " + this.getTermsTableName() + " WHERE term = ?;";
	}

	/**
	 * @return the SQL query to create the term table
	 */
	protected String get_create_atom_table() {
		return "CREATE TABLE %s(%f);";
	}

	/**
	 * @return the prefix for the name of predicate tables
	 */
	protected String get_atom_table_prefix() {
		return RDBMSStorageLayout.ATOM_TABLE_PREDICATE_PREFIX;
	}

}