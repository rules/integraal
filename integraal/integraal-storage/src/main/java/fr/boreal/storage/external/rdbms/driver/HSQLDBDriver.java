package fr.boreal.storage.external.rdbms.driver;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.hsqldb.jdbc.JDBCDataSource;

/**
 * Driver for HSQLDB databases
 */
public class HSQLDBDriver implements RDBMSDriver {

	private final JDBCDataSource ds;
	private final Connection test_connection;
	private final String JDBCString;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new driver using the given alias
	 * This represents a new memory database
	 * @param alias of the database
	 * @throws SQLException iff some error occur
	 */
	public HSQLDBDriver(String alias) throws SQLException {
		this.ds = new JDBCDataSource();
		this.ds.setUrl("jdbc:hsqldb:mem:" + alias);
		this.JDBCString = "jdbc:hsqldb:mem:" + alias;
		try {
			Connection c = this.ds.getConnection();
			c.createStatement().execute("SET DATABASE SQL SYNTAX MYS TRUE;");
			this.test_connection = c;
		} catch (SQLException e) {
			throw new SQLException("[HSQLDBDriver] An error occurred while connecting to the HSQL database at " + this.ds.getUrl() + "\n"
					+ "Please make sure this connection path is correct and the database is accessible.", e.getSQLState(), e.getErrorCode(), e);
		}
	}

	/**
	 * Creates a new driver using the given alias
	 * This represents a new memory database
	 * @param url of the database
	 * @param alias of the database
	 * @throws SQLException iff some error occur
	 */
	public HSQLDBDriver(String url, String alias) throws SQLException {
		this.ds = new JDBCDataSource();
		this.ds.setUrl("jdbc:hsqldb:" + url + ":" + alias);
		this.JDBCString = "jdbc:hsqldb:" + url + ":" + alias;
		try {
			Connection c = this.ds.getConnection();
			c.createStatement().execute("SET DATABASE SQL SYNTAX MYS TRUE;");
			this.test_connection = c;
		} catch (SQLException e) {
			throw new SQLException("[HSQLDBDriver] An error occurred while connecting to the HSQL database at " + this.ds.getUrl() + "\n"
					+ "Please make sure this connection path is correct and the database is accessible.", e.getSQLState(), e.getErrorCode(), e);
		}
	}

	@Override
	public DataSource getDatasource() {
		return this.ds;
	}

	@Override
	public String getBaseSafeInsertQuery() {
		return "INSERT IGNORE INTO %t VALUES(%f);";
	}

	@Override
	public String getBaseSafeInsertSelectQuery() {
		return "INSERT IGNORE INTO %t %s;";
	}

	@Override
	public String getTextFieldName() {
		return "TEXT";
	}

	@Override
	public String getNumberFieldName() {
		return "INT";
	}

	@Override
	public Connection getConnection() {
		return this.test_connection;
	}

	@Override
	public String getJDBCString() {
		return this.JDBCString;
	}

	@Override
	public String getCSVCopyQuery(String tableName, String csvFilePath, char delimiter, int headerSize) {
		return "CALL SQLCSV('SELECT * FROM "+ tableName + "', '" + csvFilePath + "', 'charset=UTF-8');";
	}
}
