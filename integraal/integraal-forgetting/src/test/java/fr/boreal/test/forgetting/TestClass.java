package fr.boreal.test.forgetting;

import fr.boreal.io.api.ParseException;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Constant;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.rule.impl.FORuleImpl;
import fr.boreal.redundancy.Redundancy;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;

public class TestClass {
	static final PredicateFactory predicateFactory = SameObjectPredicateFactory.instance();
	static final TermFactory termFactory = SameObjectTermFactory.instance();

	static final FOFormulaFactory formulaFactory = FOFormulaFactory.instance();

	static FORule atomicHeadRule (Atom head, Atom...body){
		FOFormula h = formulaFactory.createOrGetConjunction(head);
		FOFormula b = formulaFactory.createOrGetConjunction(body);
		return new FORuleImpl(b,h);

	}
	static final Variable X = termFactory.createOrGetVariable("X");
	static final Variable Y = termFactory.createOrGetVariable("Y");
	static final Variable Z = termFactory.createOrGetVariable("Z");


	static final Constant a = termFactory.createOrGetConstant("a");
	static final Constant b = termFactory.createOrGetConstant("b");

	static final Predicate p = predicateFactory.createOrGetPredicate("p",3);
	static final Predicate q = predicateFactory.createOrGetPredicate("q",3);
	static final Predicate r = predicateFactory.createOrGetPredicate("r",3);
	static final Predicate s = predicateFactory.createOrGetPredicate("s",3);


	static final Atom pXYZ = new AtomImpl(p,X,Y,Z);
	static final Atom qXaY = new AtomImpl(q,X,a,Y);
	static final Atom qYXb = new AtomImpl(q,Y,X,b);
	static final Atom rXYZ = new AtomImpl(r,X,Y,Z);
	static final Atom raYZ = new AtomImpl(r,a,Y,Z);
	static final Atom sXYZ = new AtomImpl(s,X,Y,Z);


	//p(X,Y,Z) :- q(Y,X,b),r(X,Y,Z).
	static final FORule r1 = atomicHeadRule(pXYZ,qYXb,rXYZ);
	//q(X,a,Y):- s(X,Y,Z).
	static final FORule r2 = atomicHeadRule(qXaY,sXYZ);

	private static final Atom paYZ = new AtomImpl(p,a,Y,Z);
	private static final Atom sYbX = new AtomImpl(s,Y,b,X);
	static final FORule r1r2 = atomicHeadRule(paYZ,sYbX,raYZ);


	static Pair<RuleBase,RuleBase> ruleBaseDiff (RuleBase ruleBase1, RuleBase ruleBase2){
		RuleBase rb1 = new RuleBaseImpl(new HashSet<>(ruleBase1.getRules()));
		RuleBase rb2 = new RuleBaseImpl(new HashSet<>(ruleBase2.getRules()));
		while (! rb1.getRules().isEmpty() && ! rb2.getRules().isEmpty() ){
			FORule r1 = rb1.getRules().stream().findAny().orElseThrow();
			Set<Predicate> p1 = r1.getHead().getPredicates();
			Optional<FORule> r2 = rb2.getRulesByHeadPredicate(p1.stream().findAny().orElseThrow()).stream().filter(r -> Redundancy.isRuleIsomorphism(r,r1)).findAny();
			rb1.remove(r1);
            r2.ifPresent(rb2::remove);
		}
		return new ImmutablePair<>(rb1,rb2);
	}


	static void testRbEq (RuleBase expected, RuleBase actual){
		Pair<RuleBase,RuleBase> diff = ruleBaseDiff(expected,actual);
		Assertions.assertEquals(new HashSet<>(diff.getLeft().getRules()),new HashSet<>());
		Assertions.assertEquals(new HashSet<>(diff.getRight().getRules()),new HashSet<>());
	}

	static void testRbEq(String pathExpected, Function<RuleBase,RuleBase> fn, String pathActual) throws IOException {
		Collection<FORule> rules = new ArrayList<>();


		for(String filepath : new String[] {
				pathExpected
		}) {

			File input = new File(filepath);
			DlgpParser dlgp_input = new DlgpParser(input);
			while (dlgp_input.hasNext()) {
				try {
					Object result = dlgp_input.next();
					if (result instanceof FORule) {
						rules.add((FORule)result);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			dlgp_input.close();
		}
		RuleBase actual = fn.apply(new RuleBaseImpl(rules));
		Collection<FORule> expected = new ArrayList<>();


		for(String filepath : new String[] {
				pathActual

		}) {

			File input = new File(filepath);
			DlgpParser dlgp_expected = new DlgpParser(input);
			while (dlgp_expected.hasNext()) {
				try {
					Object result = dlgp_expected.next();
					if (result instanceof FORule) {
						expected.add((FORule)result);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			dlgp_expected.close();
		}
		testRbEq(new RuleBaseImpl(expected),actual);
	}
}
