package fr.boreal.explanation.api;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runners.Parameterized.Parameters;

import fr.boreal.api.high_level_api.EndUserAPI;
import fr.boreal.configuration.keywords.InteGraalKeywords;
import fr.boreal.configuration.keywords.InteGraalKeywords.Algorithms;
import fr.boreal.configuration.parameters.IGParameter;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.io.dlgp.ParserResult;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;

/*
 * Checks that rewritings are correctly computed when using the EndUserAPI
 */

public class SaturationViaEndUserAPITest {

	// Test 1: Test simple case
	private static final String rule_and_data1 = """
			t(X, Z) :- s(X, Z).
			s(a,a).
			""";
	private static final String expected_saturation1 = """
			s(a,a).
			t(a,a).
			""";

	// Test 2: Test empty file
	private static final String rule_and_data2 = "";
	private static final String expected_saturation2 = "";

	// Test 3: Test with no rule
	private static final String rule_and_data3 = """
			s(a,a).
			""";
	private static final String expected_saturation3 = """
			s(a,a).
			""";

	// Test 4: Test with no rule_and_data
	private static final String rule_and_data4 = """
			t(X, Z) :- s(X, Y), s(Y, Z).
			s(X, Z) :- t(X, Y), t(Y, Z).
			s(a,a).
			""";
	private static final String expected_saturation4 = """
			s(a,a).
			t(a,a).
			""";

	// Test rank 2
	private static final String rule_and_data_2_ranks = """
			w(X, Z) :- t(X, Z).
			t(X, Z) :- s(X, Z).
			s(a,a).
			""";
	private static final String expected_saturation_2_ranks = """
			s(a,a).
			t(a,a).
			w(a,a).
			""";

	@Parameters
	static Stream<Arguments> data() {
		return Stream.of(Arguments.of(rule_and_data1, expected_saturation1),
				Arguments.of(rule_and_data2, expected_saturation2), Arguments.of(rule_and_data3, expected_saturation3),
				Arguments.of(rule_and_data4, expected_saturation4));
	}

	@DisplayName("Test saturation ")
	@ParameterizedTest(name = "{index}: KB saturation {0} should give {1})")
	@MethodSource("data")
	public void testSaturationNewTest(String dlgp, String dlgpExpected) {

		ParserResult parser = DlgpParser.parseDLGPString(dlgp);

		ParserResult dlgpExpected_parser = DlgpParser.parseDLGPString(dlgpExpected);

		RuleBase rb = new RuleBaseImpl(parser.rules());

		FactBase fb = new SimpleInMemoryGraphStore(parser.atoms());

		var paramTimeout = new IGParameter(InteGraalKeywords.TIMEOUT, Duration.parse("PT100s"));
		var paramRank = new IGParameter(InteGraalKeywords.RANK, 1);
		var paramChecker = new IGParameter(InteGraalKeywords.CHECKER, Algorithms.Parameters.Chase.Checker.RESTRICTED);

		var saturatedFactbase = EndUserAPI.saturate(fb, rb, paramTimeout, paramChecker, paramRank);

		Collection<Atom> expectedSaturation = new ArrayList<>(dlgpExpected_parser.atoms());

		Assertions.assertEquals(expectedSaturation.size(), saturatedFactbase.size());

		// test that rb has not changed
		// We should test isomorphism instead of double homomorphism
		Assertions.assertEquals(parser.rules(), rb.getRules());

	}

	@DisplayName("Test saturation ")
	@ParameterizedTest(name = "{index}: KB saturation {0} should give {1})")
	@MethodSource("data")
	public void testSaturationTest(String dlgp, String dlgpExpected) {

		ParserResult parser = DlgpParser.parseDLGPString(dlgp);

		ParserResult dlgpExpected_parser = DlgpParser.parseDLGPString(dlgpExpected);

		RuleBase rb = new RuleBaseImpl(parser.rules());

		FactBase fb = new SimpleInMemoryGraphStore(parser.atoms());

		var saturatedFactbase = EndUserAPI.saturateOld(fb, rb, null);

		Collection<Atom> expectedSaturation = new ArrayList<>(dlgpExpected_parser.atoms());

		Assertions.assertEquals(expectedSaturation.size(), saturatedFactbase.size());

		// test that rb has not changed
		// We should test isomorphism instead of double homomorphism
		Assertions.assertEquals(parser.rules(), rb.getRules());

	}

	@DisplayName("Test saturation should not give the expected result because rank is not enough)")
	public void testSaturationNewNegativeExampleTest() {

		ParserResult parser = DlgpParser.parseDLGPString(rule_and_data_2_ranks);

		ParserResult dlgpExpected_parser = DlgpParser.parseDLGPString(expected_saturation_2_ranks);

		RuleBase rb = new RuleBaseImpl(parser.rules());

		FactBase fb = new SimpleInMemoryGraphStore(parser.atoms());

		var paramTimeout = new IGParameter(InteGraalKeywords.TIMEOUT, Duration.parse("PT100s"));
		var paramRank = new IGParameter(InteGraalKeywords.RANK, 1);
		var paramChecker = new IGParameter(InteGraalKeywords.CHECKER, Algorithms.Parameters.Chase.Checker.RESTRICTED);

		var saturatedFactbase = EndUserAPI.saturate(fb, rb, paramTimeout, paramChecker, paramRank);

		Collection<Atom> expectedSaturation = new ArrayList<>(dlgpExpected_parser.atoms());

		Assertions.assertFalse(expectedSaturation.size() == (saturatedFactbase.size()));

	}
}
