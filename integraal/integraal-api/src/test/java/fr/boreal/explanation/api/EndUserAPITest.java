package fr.boreal.explanation.api;

import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.stream.Stream;

import org.hsqldb.jdbc.JDBCDataSource;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import fr.boreal.api.high_level_api.EndUserAPI;
import fr.boreal.configuration.keywords.InteGraalKeywords;
import fr.boreal.configuration.parameters.IGParameter;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.io.dlgp.ParserResult;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.query.impl.UnionFOQuery;
import fr.boreal.storage.builder.StorageBuilder;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;
import fr.boreal.views.FederatedFactBase;
import fr.boreal.views.builder.ViewBuilder;

@RunWith(Parameterized.class)
class EndUserAPITest {

	private static final Path viewDefinitionFile = Path.of("src", "test", "resources", "mapping.vd");
	private static final Path dlgpFile = Path.of("src", "test", "resources", "data.dlgp");

	private static FederatedFactBase federation;
	private static FactBase memory;
	private static Collection<Atom> memory_save;

	private static RuleBase rulebase;
	private static Collection<Query> queries;

	@BeforeAll
	public static void loadData() throws ViewBuilder.ViewBuilderException, SQLException {
		JDBCDataSource sqlDataSource = new JDBCDataSource();
		sqlDataSource.setUrl("jdbc:hsqldb:mem:test");
		Connection c = sqlDataSource.getConnection();
		c.createStatement().execute("SET DATABASE SQL SYNTAX MYS TRUE;");
		c.createStatement().execute("CREATE TABLE IF NOT EXISTS p(label varchar(32), name varchar(32));");
		c.createStatement().execute("INSERT INTO p VALUES('a', 'a');");
		c.createStatement().execute("INSERT INTO p VALUES('a', 'b');");
		c.createStatement().execute("INSERT INTO p VALUES('b', 'a');");
		c.createStatement().execute("INSERT INTO p VALUES('b', 'c');");
		c.createStatement().execute("INSERT INTO p VALUES('c', 'd');");
		c.createStatement().execute("INSERT INTO p VALUES('d', 'c');");
		c.createStatement().execute("INSERT INTO p VALUES('e', 'e');");

		federation = new FederatedFactBase(StorageBuilder.defaultStorage(),
				ViewBuilder.createFactBases(viewDefinitionFile.toString()));

		ParserResult parserResult = DlgpParser.parseFile(dlgpFile.toString());
		memory_save = parserResult.atoms();
		memory = new SimpleInMemoryGraphStore(memory_save);
		rulebase = new RuleBaseImpl(parserResult.rules());
		queries = parserResult.queries();
	}

	@AfterEach
	public void backupFactBase() {
		memory = new SimpleInMemoryGraphStore(memory_save);
		federation.setDefaultStorage(new SimpleInMemoryGraphStore());
	}

	@Parameterized.Parameters
	static Stream<Arguments> saturationData() {
		return Stream.of(Arguments.of("federation", federation, rulebase, 27),
				Arguments.of("memory", memory, rulebase, 34));
	}

	@Parameterized.Parameters
	static Stream<Arguments> rewritingData() {
		return Stream.of(Arguments.of(rulebase, queries, 5));
	}

	@Parameterized.Parameters
	static Stream<Arguments> evaluationData() {
		return Stream.of(Arguments.of("memory", memory, queries, 7),
				Arguments.of("federation", federation, queries, 7));
	}

	@Parameterized.Parameters
	static Stream<Arguments> evaluationWithReasoningData() {
		return Stream.of(Arguments.of("memory", memory, rulebase, queries, 9),
				Arguments.of("federation", federation, rulebase, queries, 9));
	}

	@DisplayName("Test EndUSerAPI#saturate")
	@ParameterizedTest(name = "{index}: Saturation on {0} with EndUserAPI should leave {3} atoms in the local storage")
	@MethodSource("saturationData")
	public void apiSaturationTest(String desc, FactBase fb, RuleBase rb, long expected_fb_size) {
		EndUserAPI.saturateOld(fb, rb, InteGraalKeywords.Algorithms.Parameters.Chase.Checker.SEMI_OBLIVIOUS);
		Assert.assertEquals(expected_fb_size, fb.size());
	}

	@DisplayName("Test EndUSerAPI#saturate")
	@ParameterizedTest(name = "{index}: Saturation on {0} with EndUserAPI should leave {3} atoms in the local storage")
	@MethodSource("saturationData")
	public void apiSaturationNewTest(String desc, FactBase fb, RuleBase rb, long expected_fb_size) {
		EndUserAPI.saturate(fb, rb, new IGParameter(InteGraalKeywords.CHECKER,
				InteGraalKeywords.Algorithms.Parameters.Chase.Checker.SEMI_OBLIVIOUS));
		Assert.assertEquals(expected_fb_size, fb.size());
	}

	@DisplayName("Test EndUSerAPI#rewrite")
	@ParameterizedTest(name = "{index}: Rewriting with EndUserAPI should give {2} rewritings")
	@MethodSource("rewritingData")
	public void apiRewritingTest(RuleBase rb, Collection<Query> qs, long expected_rewritings_size) {
		Collection<Query> rewritings = EndUserAPI.rewriteOld(rb, qs,
				InteGraalKeywords.Algorithms.Parameters.Compilation.NO_COMPILATION);
		rewritings.forEach(System.out::println);
		Assert.assertEquals(expected_rewritings_size, rewritings.size());
	}

	@DisplayName("Test EndUSerAPI#rewrite")
	@ParameterizedTest(name = "{index}: Rewriting with EndUserAPI should give {2} rewritings")
	@MethodSource("rewritingData")
	public void apiRewritingNewTest(RuleBase rb, Collection<Query> qs, long expected_ucq_in_the_rewritings) {

		Collection<UnionFOQuery> rewritings = EndUserAPI.rewrite(rb, qs, new IGParameter(InteGraalKeywords.COMPILATION,
				InteGraalKeywords.Algorithms.Parameters.Compilation.NO_COMPILATION));
		rewritings.forEach(System.out::println);
		int count = 0;
		for(var r:rewritings) {
			count+=r.getQueries().size();
		}

		Assert.assertEquals(expected_ucq_in_the_rewritings, count);
	}

	@DisplayName("Test EndUSerAPI#evaluate")
	@ParameterizedTest(name = "{index}: Query evaluation on {0} with EndUserAPI should give {3} answers")
	@MethodSource("evaluationData")
	public void apiEvaluationTest(String desc, FactBase fb, Collection<Query> qs, long expected_answer_size) {
		Iterator<Substitution> query_answers = EndUserAPI.evaluateOld(fb, qs);
		long count = 0;
		while (query_answers.hasNext()) {
			query_answers.next();
			count++;
		}
		Assert.assertEquals(expected_answer_size, count);
	}

	@DisplayName("Test EndUSerAPI#evaluate")
	@ParameterizedTest(name = "{index}: Query evaluation on {0} with EndUserAPI should give {3} answers")
	@MethodSource("evaluationData")
	public void apiEvaluationNewTest(String desc, FactBase fb, Collection<Query> qs, long expected_answer_size) {
		Iterator<Substitution> query_answers = EndUserAPI.evaluate(fb, qs);
		long count = 0;
		while (query_answers.hasNext()) {
			query_answers.next();
			count++;
		}
		Assert.assertEquals(expected_answer_size, count);
	}

	
	@DisplayName("Test EndUSerAPI#evaluate with reasoning")
	@ParameterizedTest(name = "{index}: Query evaluation with reasoning on {0} with EndUserAPI should give {4} answers")
	@MethodSource("evaluationWithReasoningData")
	public void apiEvaluationWithReasoningTest(String desc, FactBase fb, RuleBase rb, Collection<Query> qs,
			long expected_answer_size) {
		Iterator<Substitution> rewriting_query_answers = EndUserAPI.evaluateRewOld(fb, qs, rb);
		long rewriting_count = 0;
		while (rewriting_query_answers.hasNext()) {
			rewriting_query_answers.next();
			rewriting_count++;
		}
		Assert.assertEquals(expected_answer_size, rewriting_count);

		Iterator<Substitution> saturation_query_answers = EndUserAPI.evaluateSatOld(fb, qs, rb);
		long saturation_count = 0;
		while (saturation_query_answers.hasNext()) {
			saturation_query_answers.next();
			saturation_count++;
		}
		Assert.assertEquals(expected_answer_size, saturation_count);
	}
	
	@DisplayName("Test EndUSerAPI#evaluate with reasoning")
	@ParameterizedTest(name = "{index}: Query evaluation with reasoning on {0} with EndUserAPI should give {4} answers")
	@MethodSource("evaluationWithReasoningData")
	public void apiEvaluationWithReasoningNewTest(String desc, FactBase fb, RuleBase rb, Collection<Query> qs,
			long expected_answer_size) {
		Iterator<Substitution> rewriting_query_answers = EndUserAPI.evaluateRew(fb, qs, rb);
		long rewriting_count = 0;
		while (rewriting_query_answers.hasNext()) {
			rewriting_query_answers.next();
			rewriting_count++;
		}
		Assert.assertEquals(expected_answer_size, rewriting_count);

		Iterator<Substitution> saturation_query_answers = EndUserAPI.evaluateSatOld(fb, qs, rb);
		long saturation_count = 0;
		while (saturation_query_answers.hasNext()) {
			saturation_query_answers.next();
			saturation_count++;
		}
		Assert.assertEquals(expected_answer_size, saturation_count);
	}

}
