package fr.boreal.explanation.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runners.Parameterized.Parameters;

import fr.boreal.api.high_level_api.EndUserAPI;
import fr.boreal.configuration.keywords.InteGraalKeywords;
import fr.boreal.configuration.parameters.IGParameter;
import fr.boreal.io.api.ParseException;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.query.impl.UnionFOQuery;
import fr.boreal.model.rule.api.FORule;

/**
 * Checks that rewritings are correctly computed when using the EndUserAPI
 */

public class RewritingViaEndUserAPITest {

	// Test 1: Test simple case
	private static final String rule_and_query1 = """
			t(X, Z) :- s(X, Z).
			?(X, Y) :- t(X, Y).
			""";
	private static final String expected_rewritings1 = """
			t(X, Z) :- s(X, Z).
			?(X, Y) :- s(X, Y).
			?(X, Y) :- t(X, Y).
			""";

	// Test 2: Test empty file
	private static final String rule_and_query2 = "";
	private static final String expected_rewritings2 = "";

	// Test 3: Test with no rule
	private static final String rule_and_query3 = """
			?(X,Y) :- t(X,Y).
			""";
	private static final String expected_rewritings3 = """
			?(X,Y) :- t(X,Y).
			""";

	// Test 4: Test with no rule_and_query
	private static final String rule_and_query4 = """
			t(X, Z) :- s(X, Y), s(Y, Z).
			s(X, Z) :- q(X, Y), q(Y, Z).
			""";
	private static final String expected_rewritings4 = """
			t(X, Z) :- s(X, Y), s(Y, Z).
			s(X, Z) :- q(Y, Z), q(X, Y).
			""";

	@Parameters
	static Stream<Arguments> data() {
		return Stream.of(Arguments.of(rule_and_query1, expected_rewritings1),
				Arguments.of(rule_and_query2, expected_rewritings2),
				Arguments.of(rule_and_query3, expected_rewritings3),
				Arguments.of(rule_and_query4, expected_rewritings4));
	}

	@Parameters
	static Stream<Arguments> empty() {
		return Stream.of();
	}

	@DisplayName("Test rewriting ")
	@ParameterizedTest(name = "{index}: rewrite {0} should give {1})")
	@MethodSource("data")
	public void testRewritingTest(String dlgp, String dlgpExpected) {

		DlgpParser dlgp_parser = new DlgpParser(dlgp);

		Collection<FORule> rules = new ArrayList<>();
		Collection<Query> queries = new ArrayList<>();

		Collection<FORule> rulesExpected = new ArrayList<>();
		Collection<Query> queriesExpected = new ArrayList<>();

		while (dlgp_parser.hasNext()) {
			try {
				Object result = dlgp_parser.next();
				if (result instanceof FORule) {
					rules.add((FORule) result);
				} else if (result instanceof Query q) {
					queries.add(q);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		dlgp_parser.close();

		DlgpParser dlgpExpected_parser = new DlgpParser(dlgpExpected);
		while (dlgpExpected_parser.hasNext()) {
			try {
				Object result = dlgpExpected_parser.next();

				if (result instanceof FORule) {
					rulesExpected.add((FORule) result);
				} else if (result instanceof FOQuery) {
					queriesExpected.add((FOQuery<? extends FOFormula>) result);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		dlgpExpected_parser.close();

		RuleBase rb = new RuleBaseImpl(rules);

		Collection<Query> result = EndUserAPI.rewriteOld(rb, queries,
				InteGraalKeywords.Algorithms.Parameters.Compilation.NO_COMPILATION);

	}

	@DisplayName("Test rewriting ")
	@ParameterizedTest(name = "{index}: rewrite {0} should give {1})")
	@MethodSource("data")
	public void testRewritingNewTest(String dlgp, String dlgpExpected) {

		DlgpParser dlgp_parser = new DlgpParser(dlgp);

		Collection<FORule> rules = new ArrayList<>();
		Collection<Query> queries = new ArrayList<>();

		Collection<FORule> rulesExpected = new ArrayList<>();
		Collection<Query> queriesExpected = new ArrayList<>();

		while (dlgp_parser.hasNext()) {
			try {
				Object result = dlgp_parser.next();
				if (result instanceof FORule) {
					rules.add((FORule) result);
				} else if (result instanceof Query q) {
					queries.add(q);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		dlgp_parser.close();

		DlgpParser dlgpExpected_parser = new DlgpParser(dlgpExpected);
		while (dlgpExpected_parser.hasNext()) {
			try {
				Object result = dlgpExpected_parser.next();

				if (result instanceof FORule) {
					rulesExpected.add((FORule) result);
				} else if (result instanceof FOQuery) {
					queriesExpected.add((FOQuery<? extends FOFormula>) result);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		dlgpExpected_parser.close();

		RuleBase rb = new RuleBaseImpl(rules);

		var nocompilParam = new IGParameter(InteGraalKeywords.COMPILATION,
				InteGraalKeywords.Algorithms.Parameters.Compilation.NO_COMPILATION);

		Collection<UnionFOQuery> result = EndUserAPI.rewrite(rb, queries, nocompilParam);

		int ucq_size;
		if (result.stream().findAny().isEmpty()) {
			ucq_size = 0;
		} else {
			ucq_size = result.stream().findAny().get().getQueries().size();
		}

		Assertions.assertTrue(ucq_size == queriesExpected.size());

	}

}
