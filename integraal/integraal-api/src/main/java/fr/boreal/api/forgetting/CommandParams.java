package fr.boreal.api.forgetting;

import java.util.Collection;
import java.util.List;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;

class RedundancyValidator implements IParameterValidator{
	public void validate(String name, String value){
		if (! List.of(CommandParams.REDUN_NONE,CommandParams.REDUN_ALL,CommandParams.REDUN_ADDED).contains(value.toLowerCase()))
			throw new ParameterException("Parameter of " + name + " should be '"+CommandParams.REDUN_NONE
					+ "' '"+ CommandParams.REDUN_ADDED
					+"' or '" + CommandParams.REDUN_ALL
					+ ", found: " + value);
	}
}

public class CommandParams {
	public final static String REDUN_NONE = "none";
	public final static String REDUN_ALL = "all";
	public final static String REDUN_ADDED = "added";

	@Parameter(names={"-rules","-rulepath"},description = "one or more predicate labels to datalogForget")
	public String rulesPath;
	@Parameter(names={"-f","-datalogForget"},description = "predicates to datalogForget")
	public List<String> toForget = List.of();
	@Parameter(names={"-k,-keep","-remember"},description = "one or more predicate labels to keep")
	public List<String> toKeep = List.of();


	@Parameter (names={"-red"},validateWith = RedundancyValidator.class,
			description = " set of rule that have not to be redundant : '" + REDUN_NONE + "' '" + REDUN_ADDED + "' or '" + REDUN_ALL )
	public String redundancy = REDUN_NONE;
	@Parameter (names={"-slow","-noopt"})
	public boolean slowMethod;

	@Parameter (names={"-close","-c"},description = "close rule base instead of datalogForget")
	public boolean close;
	@Parameter(names={"-help","-h"},help = true)
	public boolean help;

	@Parameter
	public String rulesPathMain;

	public boolean keepInsteadOfForget;

	public Collection<String> predicates;


}
