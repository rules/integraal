package fr.boreal.api.integraal_repl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jline.builtins.Less;
import org.jline.builtins.Source;
import org.jline.builtins.Source.InputStreamSource;
import org.jline.reader.LineReader;

import fr.boreal.api.integraal_repl.ComplexEnvironment.WriteMode;
import fr.boreal.api.integraal_repl.IGCommands.InteGraalCommands;
import fr.boreal.api.integraal_repl.IGRepl.PrintLevel;
import fr.boreal.io.api.ParseException;
import fr.boreal.io.dlgp.DlgpWriter;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.storage.builder.StorageBuilder;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;
import fr.boreal.views.FederatedFactBase;
import fr.boreal.views.builder.ViewBuilder;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

/**
 * An object that gathers REPL commands relating to the {@link ComplexEnvironment}.
 *
 * @author Pierre Bisquert
 */
public class CECommands {

	/**
	 * Top-level command that just prints help.
	 */
	@Command(name = "",
			usageHelpAutoWidth = true,
			footer = {"", "Press Ctrl-D to exit."},
			subcommands = {LoadDLGPCommand.class, PeekBaseCommand.class, InspectBaseCommand.class,
					ListBaseCommand.class, SetBaseCommand.class, CreateBaseCommand.class,
					RemoveBaseCommand.class, AssertCommand.class, RetractCommand.class, MappingCommand.class})
	static class ComplexEnvironmentCommands implements Runnable {
		static LineReader lineReader;
		static ComplexEnvironment env;

		ComplexEnvironmentCommands(ComplexEnvironment env) {
			ComplexEnvironmentCommands.env = env;
		}

		public void setLineReader(LineReader reader){
			lineReader = reader;
		}

		public void run() {
			lineReader.getTerminal().writer().println(new CommandLine(this).getUsageMessage());
		}
	}

	/**
	 * Command class for the "loadDLGP" command.
	 */
	@Command(mixinStandardHelpOptions = true, usageHelpAutoWidth = true, name = "load",
			description = "Loads a DLGP file into up to three bases (facts, rules and queries).")
	static class LoadDLGPCommand implements Runnable {

		@Parameters(paramLabel = "FILES", arity="1..*", description = "The DLGP file(s) to load.")
		private File[] files;
		@Option(names = {"-f", "--fact"}, arity="0..1", description = "Only loads the facts of the given DLGP file")
		private String fact = null;
		@Option(names = {"-r", "--rule"}, arity="0..1", description = "Only loads the rules of the given DLGP file")
		private String rule = null;
		@Option(names = {"-q", "--query"}, arity="0..1", description = "Only loads the queries of the given DLGP file")
		private String query = null;
		@Option(names = {"-m", "--mode"}, description = "The writing mode to use if the base receiving the result is not empty (${COMPLETION-CANDIDATES}).")
		private WriteMode mode = WriteMode.PROTECTED;

		@Override
		public void run() {
			boolean set = false;
			if (this.fact == null && this.rule == null && this.query == null) {
				this.fact = "";
				this.rule = "";
				this.query = "";
				if (files.length == 1) set = true;
			} else if (files.length > 1) set = true;

			boolean abortWriting = false;

			if (this.fact != null) {
				ArrayList<String> namesToCheck = new ArrayList<>();
				if (!this.fact.isEmpty()) namesToCheck.add(this.fact);
				else {
					for (File file : this.files) namesToCheck.add(file.getName());
				}

				for (String nameToCheck : namesToCheck) {
					if (Checks.factbaseExists(nameToCheck) && !Checks.factbaseIsEmpty(nameToCheck)) {
						IGRepl.writeIfVerbose("Warning: fact base \"" + nameToCheck + "\" is not empty. ", PrintLevel.WARNING);
						abortWriting = !Checks.checkWriteModeAndWarn(this.mode);
					}
				}
			}

			if (this.rule != null) {
				ArrayList<String> namesToCheck = new ArrayList<>();
				if (!this.rule.isEmpty()) namesToCheck.add(this.rule);
				else {
					for (File file : this.files) namesToCheck.add(file.getName());
				}

				for (String nameToCheck : namesToCheck) {
					if (Checks.rulebaseExists(nameToCheck) && !Checks.rulebaseIsEmpty(nameToCheck)) {
						IGRepl.writeIfVerbose("Warning: target rule base \"" + nameToCheck + "\" is not empty. ", PrintLevel.WARNING);
						abortWriting = !Checks.checkWriteModeAndWarn(this.mode);
					}
				}
			}

			if (this.query != null) {
				ArrayList<String> namesToCheck = new ArrayList<>();
				if (!this.query.isEmpty()) namesToCheck.add(this.query);
				else {
					for (File file : this.files) namesToCheck.add(file.getName());
				}

				for (String nameToCheck : namesToCheck) {
					if (Checks.querybaseExists(nameToCheck) && !Checks.querybaseIsEmpty(nameToCheck)) {
						IGRepl.writeIfVerbose("Warning: target query base \"" + nameToCheck + "\" is not empty. ", PrintLevel.WARNING);
						abortWriting = !Checks.checkWriteModeAndWarn(this.mode);
					}
				}
			}

			if (!abortWriting) {

				// One file
				if (this.files.length == 1) {
					Path filePath = Paths.get(this.files[0].getAbsolutePath());
					IGRepl.writeIfVerbose("Loading file \"" + filePath + "\"...\n", PrintLevel.MAXIMAL);

					if ((this.fact != null && this.fact.isEmpty())) this.fact = this.files[0].getName();
					if ((this.rule != null && this.rule.isEmpty())) this.rule = this.files[0].getName();
					if ((this.query != null && this.query.isEmpty())) this.query = this.files[0].getName();

					try {
						ComplexEnvironmentCommands.env.loadDLGPFile(this.fact, this.rule, this.query, new FileInputStream(this.files[0]), filePath, this.mode);
						if (this.fact != null && set) ComplexEnvironmentCommands.env.setCurrentFactBase(this.fact);
						if (this.rule != null && set) ComplexEnvironmentCommands.env.setCurrentRuleBase(this.rule);
						if (this.query != null && set) ComplexEnvironmentCommands.env.setCurrentQueryBase(this.query);
					} catch (ParseException e) {
						IGRepl.writeIfVerbose("Warning: Parse Exception on \"" + this.files[0].getAbsolutePath() + "\".", PrintLevel.MINIMAL);
						IGRepl.writeIfVerbose(e.getMessage() + "\n", PrintLevel.MINIMAL);
						IGRepl.writeIfVerbose("Aborting...\n", PrintLevel.MINIMAL);
						if (this.fact != null && !Checks.factbaseIsEmpty(this.fact))
							ComplexEnvironmentCommands.env.removeFactBase(this.fact);
						if (this.rule != null && !Checks.rulebaseIsEmpty(this.rule))
							ComplexEnvironmentCommands.env.removeRuleBase(this.rule);
						if (this.query != null && !Checks.querybaseIsEmpty(this.query))
							ComplexEnvironmentCommands.env.removeQueryBase(this.query);
					} catch (FileNotFoundException e) {
						IGRepl.writeIfVerbose("Warning: file \"" + this.files[0].getAbsolutePath() + "\" does not exist. Aborting...\n", PrintLevel.MINIMAL);
					}

					// Several files
				} else {

					// If at least a target base has been specified, then we load every file in the same specified base(s)
					// TODO: warning : this part has undesirable behavior when dlgp files have some VD loading (only the VD of last file will be kept)
					if ((this.fact != null && !this.fact.isEmpty()) || (this.rule != null && !this.rule.isEmpty()) || (this.query != null && !this.query.isEmpty())) {

						if (this.fact != null && !this.fact.isEmpty()) ComplexEnvironmentCommands.env.createFactBase(this.fact, this.mode);
						if (this.rule != null && !this.rule.isEmpty()) ComplexEnvironmentCommands.env.createRuleBase(this.rule, this.mode);
						if (this.query != null && !this.query.isEmpty()) ComplexEnvironmentCommands.env.createQueryBase(this.query, this.mode);
						int counter = 0;
						FederatedFactBase finalFFB = new FederatedFactBase(new SimpleInMemoryGraphStore());
						for (File file : this.files) {
							Path filePath = Paths.get(file.getAbsolutePath());
							IGRepl.writeIfVerbose("Loading file \"" + filePath + "\"...\n", PrintLevel.MAXIMAL);
							try {
								// TODO: hideous much?
								// When we want to aggregate the different files' content (facts) into one base, we first load a file
								// then put its content in a FFB iteratively (we might have views)
								ComplexEnvironmentCommands.env.loadDLGPFile(this.fact, this.rule, this.query, new FileInputStream(file), filePath, WriteMode.APPEND);
								if (this.fact != null) {
									FactBase temp = ComplexEnvironmentCommands.env.getFactBaseByName(this.fact);
									if (temp instanceof FederatedFactBase) {
										((FederatedFactBase) temp).getViewDefinitions().forEach((pred, fb) -> finalFFB.addStorage(pred, fb));
									}
									finalFFB.addAll(temp.getAtoms());
									
								}
							} catch (ParseException e) {
								IGRepl.writeIfVerbose("Warning: Parse Exception on \"" + file.getAbsolutePath() + "\".\n", PrintLevel.MINIMAL);
								IGRepl.writeIfVerbose(e.getMessage() + "\n", PrintLevel.MINIMAL);
								IGRepl.writeIfVerbose("Skipping this file...\n", PrintLevel.MINIMAL);
								counter++;
							} catch (FileNotFoundException e) {
								IGRepl.writeIfVerbose("Warning: \"" + file.getAbsolutePath() + "\" does not exist. Skipping this file...\n", PrintLevel.MINIMAL);
							}
						}
						if (finalFFB.getViewDefinitions().size() == 0) {
							ComplexEnvironmentCommands.env.putFactBase(this.fact, new SimpleInMemoryGraphStore(finalFFB.getDefaultStorage().getAtoms().toList()), this.mode);
						} else {
							ComplexEnvironmentCommands.env.putFactBase(this.fact, finalFFB, this.mode);
						}
						
						if (counter == this.files.length) {
							if (this.fact != null && !Checks.factbaseIsEmpty(this.fact)) {
								ComplexEnvironmentCommands.env.removeFactBase(this.fact);
							}
							if (this.rule != null && !Checks.rulebaseIsEmpty(this.rule))
								ComplexEnvironmentCommands.env.removeRuleBase(this.rule);
							if (this.query != null && !Checks.querybaseIsEmpty(this.query))
								ComplexEnvironmentCommands.env.removeQueryBase(this.query);
						}

					} else {  // Else, we load every file in its own base
						for (File file : this.files) {
							String fileName = file.getName();
							Path filePath = Paths.get(file.getAbsolutePath());
							IGRepl.writeIfVerbose("Loading file \"" + file.getAbsolutePath() + "\"...\n", PrintLevel.MAXIMAL);

							try {
								ComplexEnvironmentCommands.env.loadDLGPFile(fileName, fileName, fileName, new FileInputStream(file), filePath, this.mode);
							} catch (ParseException e) {
								IGRepl.writeIfVerbose("Warning: Parse Exception on \"" + file.getAbsolutePath() + "\".\n", PrintLevel.MINIMAL);
								IGRepl.writeIfVerbose(e.getMessage() + "\n", PrintLevel.MINIMAL);
								IGRepl.writeIfVerbose("Skipping this file...\n", PrintLevel.MINIMAL);
								ComplexEnvironmentCommands.env.removeFactBase(fileName);
								ComplexEnvironmentCommands.env.removeRuleBase(fileName);
								ComplexEnvironmentCommands.env.removeQueryBase(fileName);

								if (this.fact != null && !Checks.factbaseIsEmpty(fileName))
									ComplexEnvironmentCommands.env.removeFactBase(fileName);
								if (this.rule != null && !Checks.rulebaseIsEmpty(fileName))
									ComplexEnvironmentCommands.env.removeRuleBase(fileName);
								if (this.query != null && !Checks.querybaseIsEmpty(fileName))
									ComplexEnvironmentCommands.env.removeQueryBase(fileName);
							} catch (FileNotFoundException e) {
								IGRepl.writeIfVerbose("Warning: file \"" + file.getAbsolutePath() + "\" does not exist. Skipping this file...\n", PrintLevel.MINIMAL);
							}
						}
					}
				}
				IGRepl.writeIfVerbose("Done loading!\n\n", PrintLevel.MINIMAL);
			} else {
				IGRepl.writeIfVerbose("Warning: One or several of the target base(s) is/are not empty. Aborting...\n\n", PrintLevel.MINIMAL);
			}
		}
	}

	/**
	 * Command class for the "peek" command.
	 */
	@Command(mixinStandardHelpOptions = true, usageHelpAutoWidth = true, name = "peek",
			description = "Prints (a limited amount of) the content of the (current or specified) base(s).")
	static class PeekBaseCommand implements Runnable {

		@Parameters(paramLabel = "BASENAME", arity = "0..1")
		private String baseName = null;
		@Option(names = {"-f", "--fact"}, description = "Specifically print the facts of the base")
		private boolean fact;
		@Option(names = {"-r", "--rule"}, description = "Specifically print the rules of the base")
		private boolean rule;
		@Option(names = {"-q", "--query"}, description = "Specifically print the queries of the base")
		private boolean query;

		@Override
		public void run() {
			if (!(this.fact || this.rule || this.query)) {
				this.fact = true;
				this.rule = true;
				this.query = true;
			}

			String factBaseName = ComplexEnvironmentCommands.env.getCurrentFactBase();
			String ruleBaseName = ComplexEnvironmentCommands.env.getCurrentRuleBase();
			String queryBaseName = ComplexEnvironmentCommands.env.getCurrentQueryBase();
			if (baseName != null) {
				if (this.fact) {
					if (!Checks.factbaseExists(baseName)) {
						IGRepl.writeIfVerbose("Warning: fact base \"" + baseName + "\" does not exist.\n", PrintLevel.WARNING);
						this.fact = false;
					}
					else factBaseName = baseName;
				}
				if (this.rule) {
					if (!Checks.rulebaseExists(baseName)) {
						IGRepl.writeIfVerbose("Warning: rule base \"" + baseName + "\" does not exist.\n", PrintLevel.WARNING);
						this.rule = false;
					}
					else ruleBaseName = baseName;
				}
				if (this.query) {
					if (!Checks.querybaseExists(baseName)) {
						IGRepl.writeIfVerbose("Warning: query base \"" + baseName + "\" does not exist.\n", PrintLevel.WARNING);
						this.query = false;
					}
					else queryBaseName = baseName;
				}
			}

			// TODO: the following is quite ugly, but simple to avoid printing some lone "+"
			if (!(this.fact || this.rule || this.query)) {
				IGRepl.writeIfVerbose("\n", PrintLevel.MINIMAL);
				return;
			}

			// TODO: I'm about to do some nasty stuff in order to circumvent the dlgpWriter linebreak problem
			// i.e.: using a different DlgpWriter to write into a string that I can modify... Yikes...
			StringWriter stringDlgpWriter = new StringWriter();
			DlgpWriter dlgpW = new DlgpWriter(stringDlgpWriter);

			int printSizeLimit = 10;
			int nbCol = (int)Stream.of(this.fact, this.rule, this.query).filter(Boolean.TRUE::equals).count();
			int widthSizeLimit = (Math.round(ComplexEnvironmentCommands.lineReader.getTerminal().getWidth() - (nbCol * 4)) / nbCol);
			widthSizeLimit = Math.max(widthSizeLimit, 20);
			List<List<String>> matrix = new ArrayList<>();

			String factBaseTitle = "Fact base: " + factBaseName;
			String ruleBaseTitle = "Rule base: " + ruleBaseName;
			String queryBaseTitle = "Query base: " + queryBaseName;
			int longestFact = factBaseTitle.length();
			int longestRule = ruleBaseTitle.length();
			int longestQuery = queryBaseTitle.length();
			if (this.fact) {
				// TODO: the following really needs a refactoring...
				FactBase fb = ComplexEnvironmentCommands.env.getFactBaseByName(factBaseName);
				
				List<String> arrFB = new ArrayList<>();
				int adjust = printSizeLimit;

				//Listing the views
				List<String> arrView = new ArrayList<>();
				Iterator<Predicate> iterView = null;
				
				if(fb instanceof FederatedFactBase ffb) {
					int idx = 0;
					iterView = ffb.getViewDefinitions().keySet().iterator();
					while (iterView.hasNext() && idx < printSizeLimit / 2) {
						Predicate p = iterView.next();
						String fString =
                                p.label() +
                                "/" +
                                p.arity();
						if (fString.length() > widthSizeLimit) fString = fString.substring(0, widthSizeLimit-3) + "...";
						if (fString.length() > longestFact) longestFact = fString.length();
						arrView.add(fString);
						idx++;
					}
					adjust = printSizeLimit - (idx + 1);
				}

				// Listing the facts
				List<String> arrAtom = new ArrayList<>();
				Iterator<Atom> iterAtom = fb.getAtoms().iterator();
				int idx = 0;
				while (iterAtom.hasNext() && idx < Math.min(printSizeLimit,adjust)) {
					try {
						dlgpW.write(iterAtom.next());
					} catch (IOException e) {
						IGRepl.systemRegistry.trace(e);
					}
					String fString = stringDlgpWriter.toString().replace("\n", "") + ".";
					if (fString.length() > widthSizeLimit) fString = fString.substring(0, widthSizeLimit-3) + "...";
					if (fString.length() > longestFact) longestFact = fString.length();
					arrAtom.add(fString);
					stringDlgpWriter.getBuffer().setLength(0);
					idx++;
				}

				String sep = "-";
				int dash_size = 0;
				if(fb instanceof FederatedFactBase ffb) {
					factBaseTitle = "Federated fact base: "+ factBaseName;
					longestFact = Math.max(longestFact, factBaseTitle.length());
					sep = " Views ";
					dash_size = (longestFact - sep.length()) / 2;
					arrFB.add("-".repeat(dash_size) + sep + "-".repeat(longestFact - (dash_size + sep.length())));
					arrView.sort(null);
					if (arrView.size() == 0) arrFB.add("(Empty)");
					else arrFB.addAll(arrView);
					if (iterView.hasNext()) arrFB.add("...");
					sep = " Facts ";
				}
				arrFB.add(0,factBaseTitle);
				dash_size = (longestFact - sep.length()) / 2;
				arrFB.add("-".repeat(dash_size) + sep + "-".repeat(longestFact - (dash_size + sep.length())));
				arrAtom.sort(null);
				if (arrAtom.size() == 0) arrFB.add("(Empty)");
				else arrFB.addAll(arrAtom);
				if (iterAtom.hasNext()) arrFB.add("...");
				matrix.add(arrFB);
			}
			if (this.rule) {
				List<String> arrRule = new ArrayList<>();
				Iterator<FORule> iter = ComplexEnvironmentCommands.env.getRuleBaseByName(ruleBaseName).getRules().iterator();
				int idx = 0;
				while (iter.hasNext() && idx < printSizeLimit) {
					try {
						dlgpW.write(iter.next());
					} catch (IOException e) {
						IGRepl.systemRegistry.trace(e);
					}
					String rString = stringDlgpWriter.toString().replace("\n", "");
					if (rString.length() > widthSizeLimit) rString = rString.substring(0, widthSizeLimit-3) + "...";
					if (rString.length() > longestRule) longestRule = rString.length();
					arrRule.add(rString);
					stringDlgpWriter.getBuffer().setLength(0);
					idx++;
				}
				arrRule.sort(null);
				if (iter.hasNext()) arrRule.add("...");
				if (arrRule.size() == 0) arrRule.add("(Empty)");
				arrRule.add(0,ruleBaseTitle);
				arrRule.add(1, "-".repeat(longestRule));
				matrix.add(arrRule);
			}
			if (this.query) {
				List<String> arrQuery = new ArrayList<>();
				Iterator<Query> iter = ComplexEnvironmentCommands.env.getQueryBaseByName(queryBaseName).iterator();
				int idx = 0;
				while (iter.hasNext() && idx < printSizeLimit) {
					try {
						dlgpW.write(iter.next());
					} catch (IOException e) {
						IGRepl.systemRegistry.trace(e);
					}
					String qString = stringDlgpWriter.toString().replace("\n", "");
					if (qString.length() > widthSizeLimit) qString = qString.substring(0, widthSizeLimit-3) + "...";
					if (qString.length() > longestQuery) longestQuery = qString.length();
					arrQuery.add(qString);
					stringDlgpWriter.getBuffer().setLength(0);
					idx++;
				}
				arrQuery.sort(null);
				if (iter.hasNext()) arrQuery.add("...");
				if (arrQuery.size() == 0) arrQuery.add("(Empty)");
				arrQuery.add(0,queryBaseTitle);
				arrQuery.add(1, "-".repeat(longestQuery));
				matrix.add(arrQuery);
			}

			try {
				dlgpW.close();
			} catch (IOException e) {
				IGRepl.systemRegistry.trace(e);
			}

			HashMap<String, Integer> mapSize = new HashMap<>();
			mapSize.put(factBaseTitle, longestFact);
			mapSize.put(ruleBaseTitle, longestRule);
			mapSize.put(queryBaseTitle, longestQuery);
			int baseMaxSize = 0;
			for (List<String> a : matrix) {
				baseMaxSize = Math.max(baseMaxSize, a.size());
			}

			StringBuilder sepLine = new StringBuilder("+");
			for (List<String> col : matrix) {
				if (col.getFirst().equals(factBaseTitle)) {
					sepLine.append("-".repeat(Math.max(0, longestFact + 2)));
				} else if (col.getFirst().equals(ruleBaseTitle)) {
					sepLine.append("-".repeat(Math.max(0, longestRule + 2)));
				} else {
					sepLine.append("-".repeat(Math.max(0, longestQuery + 2)));
				}
				sepLine.append("+");
			}

			StringBuilder finalString = new StringBuilder(sepLine.toString());
			finalString.append("\n");
			for (int i = 0; i < baseMaxSize; i++) {
				finalString.append("|");
				for (List<String> col : matrix) {
					if (i < col.size()) {
						finalString.append(" ");
						finalString.append(col.get(i));
						finalString.append(" ".repeat(Math.max(0, mapSize.get(col.getFirst()) - col.get(i).length())));
					} else {
						finalString.append(" ".repeat(Math.max(0, mapSize.get(col.getFirst()) + 1)));
					}
					finalString.append(" |");
				}
				finalString.append("\n");
			}
			finalString.append(sepLine);
			finalString.append("\n\n");

			IGRepl.writeIfVerbose(finalString.toString(), PrintLevel.MINIMAL);
		}
	}


	/**
	 * Command class for the "inspect" command.
	 */
	@Command(mixinStandardHelpOptions = true, usageHelpAutoWidth = true, name = "inspect",
			description = "Uses a file pager to inspect the content of the (current or specified) base(s).")
	static class InspectBaseCommand implements Runnable {

		@Parameters(paramLabel = "BASENAME", arity = "0..1")
		String baseName;
		@Option(names = {"-f", "--fact"}, arity = "1..*",  description = "desc")
		private String[] factBases = null;
		@Option(names = {"-r", "--rule"}, arity = "1..*",  description = "desc")
		private String[] ruleBases = null;
		@Option(names = {"-q", "--query"}, arity = "1..*",  description = "desc")
		private String[] queryBases = null;


		public void run() {
			if (baseName == null && this.factBases == null && this.ruleBases == null && this.queryBases == null) {
				this.factBases = new String[]{InteGraalCommands.env.getCurrentFactBase()};
				this.ruleBases = new String[]{InteGraalCommands.env.getCurrentRuleBase()};
				this.queryBases = new String[]{InteGraalCommands.env.getCurrentQueryBase()};
			}

			if (baseName != null) {
				this.factBases = new String[]{baseName};
				this.ruleBases = new String[]{baseName};
				this.queryBases = new String[]{baseName};
			}

			if (this.factBases == null) this.factBases = new String[]{};
			if (this.ruleBases == null) this.ruleBases = new String[]{};
			if (this.queryBases == null) this.queryBases = new String[]{};

			// We don't really need dir, but it is mandatory for instantiating Less
			Supplier<Path> dir = () -> Paths.get(System.getProperty("user.dir"));
			Less less = new Less(ComplexEnvironmentCommands.lineReader.getTerminal(), dir.get());
			ArrayList<Source> sources = new ArrayList<>();

			try {
				// TODO: same trick as in peek command...
				StringWriter stringDlgpWriter = new StringWriter();
				DlgpWriter dlgpW = new DlgpWriter(stringDlgpWriter);


				
				for (String factbase : this.factBases) {
//					List<String> arrFact = new ArrayList<>();
					
					FactBase fb = ComplexEnvironmentCommands.env.getFactBaseByName(factbase);
					
					if (Checks.factbaseExists(factbase)) {
						List<String> arrView = new ArrayList<>();
						if (fb instanceof FederatedFactBase ffb) {
							Iterator<Predicate> iterView = ffb.getViewDefinitions().keySet().iterator();
							while (iterView.hasNext()) {
								Predicate p = iterView.next();
								String fString =
		                                p.label() + "/" + p.arity() + "\n";
								arrView.add(fString);
							}
							arrView.sort(null);
						}
						
						ArrayList<String> arrAtom = new ArrayList<>();
						Iterable<Atom> iter = fb.getAtoms()::iterator;
						for (Atom a : iter) {
							dlgpW.write(a);
							dlgpW.flush();
							arrAtom.add(stringDlgpWriter.toString().replace("\n", "") + ".\n");
							stringDlgpWriter.getBuffer().setLength(0);
						}
						arrAtom.sort(null);
						if (arrAtom.size() == 0) arrAtom.add("(Empty)");
						if (arrView.size() == 0) arrView.add("(Empty)");
						
//						if (arrView.size() != 0) {
							arrView.add(0, "*********\n");
							arrView.add(1, "* Views *\n");
							arrView.add(2, "*********\n");
							arrView.add(3, "\n");
							
							arrAtom.add(0, "\n\n*********\n");
							arrAtom.add(1, "* Facts *\n");
							arrAtom.add(2, "*********\n");
							arrAtom.add(3, "\n");
//						}
						
						for (String s : arrView) stringDlgpWriter.append(s);
						for (String s : arrAtom) stringDlgpWriter.append(s);
						
						sources.add(new InputStreamSource(new ByteArrayInputStream(stringDlgpWriter.toString().getBytes(StandardCharsets.UTF_8)), true, factbase + " (facts)"));
						stringDlgpWriter.getBuffer().setLength(0);
					}
					else IGRepl.writeIfVerbose("Warning: fact base \"" + factbase + "\" does not exist. Skipping...\n", PrintLevel.WARNING);
				}
				for (String rulebase : this.ruleBases) {
					if (Checks.rulebaseExists(rulebase)) {
						ArrayList<String> arrRule = new ArrayList<>();
						for (FORule r : ComplexEnvironmentCommands.env.getRuleBaseByName(rulebase).getRules()) {
							dlgpW.write(r);
							arrRule.add(stringDlgpWriter.toString().replace("\n", "") + "\n");
							stringDlgpWriter.getBuffer().setLength(0);
						}
						arrRule.sort(null);
						if (arrRule.size() == 0) arrRule.add("(Empty)");
						arrRule.add(0, "*********\n");
						arrRule.add(1, "* Rules *\n");
						arrRule.add(2, "*********\n");
						arrRule.add(3, "\n");
						for (String s : arrRule) stringDlgpWriter.append(s);
						sources.add(new InputStreamSource(new ByteArrayInputStream(stringDlgpWriter.toString().getBytes(StandardCharsets.UTF_8)), true, rulebase + " (rules)"));
						stringDlgpWriter.getBuffer().setLength(0);
					}
					else IGRepl.writeIfVerbose("Warning: rule base \"" + rulebase + "\" does not exist. Skipping...\n", PrintLevel.WARNING);
				}

				for (String querybase : this.queryBases) {
					if (Checks.querybaseExists(querybase)) {
						ArrayList<String> arrQuery = new ArrayList<>();
						for (Query q : ComplexEnvironmentCommands.env.getQueryBaseByName(querybase)) {
							dlgpW.write(q);
							arrQuery.add(stringDlgpWriter.toString().replace("\n", "") + "\n");
							stringDlgpWriter.getBuffer().setLength(0);
						}
						arrQuery.sort(null);
						if (arrQuery.size() == 0) arrQuery.add("(Empty)");
						arrQuery.add(0, "***********\n");
						arrQuery.add(1, "* Queries *\n");
						arrQuery.add(2, "***********\n");
						arrQuery.add(3, "\n");
						for (String s : arrQuery) stringDlgpWriter.append(s);
						sources.add(new InputStreamSource(new ByteArrayInputStream(stringDlgpWriter.toString().getBytes(StandardCharsets.UTF_8)), true, querybase + " (queries)"));
						stringDlgpWriter.getBuffer().setLength(0);
					}
					else IGRepl.writeIfVerbose("Warning: query base \"" + querybase + "\" does not exist. Skipping...\n", PrintLevel.WARNING);
				}

				dlgpW.close();
				less.printLineNumbers = true;
				less.run(sources);

			}
			catch (IOException | InterruptedException e) {
				IGRepl.systemRegistry.trace(e);
			}
		}
	}


	/**
	 * Command class for the "list" command.
	 */
	@Command(mixinStandardHelpOptions = true, usageHelpAutoWidth = true, name = "list",
			description = "Lists the base(s) in memory.")
	static class ListBaseCommand implements Runnable {

		@Option(names = {"-f", "--fact"}, description = "desc")
		private boolean factBase;
		@Option(names = {"-r", "--rule"}, description = "desc")
		private boolean ruleBase;
		@Option(names = {"-q", "--query"}, description = "desc")
		private boolean queryBase;

		public void run() {
			if (!(this.factBase || this.ruleBase || this.queryBase)) {
				this.factBase = true;
				this.ruleBase = true;
				this.queryBase = true;
			}

			// TODO: this code is very similar to the one in "peek", it would be good to make a generic method that is called by these two commands.
			List<List<String>> matrix = new ArrayList<>();

			String factBaseTitle = "Fact bases";
			String ruleBaseTitle = "Rule bases";
			String queryBaseTitle = "Query bases";
			int longestFact = factBaseTitle.length();
			int longestRule = ruleBaseTitle.length();
			int longestQuery = queryBaseTitle.length();

			if (this.factBase) {
				List<String> arrFact = new ArrayList<>();
				for (String f : ComplexEnvironmentCommands.env.listFactBases()) {
					if (f.equals(ComplexEnvironmentCommands.env.getCurrentFactBase())) f = "* " + f;
					else f = "- " + f;
					if (f.length() > longestFact) longestFact = f.length();
					arrFact.add(f);
				}
				arrFact.sort(Comparator.comparing(o -> o.substring(1)));
				arrFact.addFirst(factBaseTitle);
				arrFact.add(1, "-".repeat(longestFact));
				matrix.add(arrFact);
			}
			if (this.ruleBase) {
				List<String> arrRule = new ArrayList<>();
				for (String r : ComplexEnvironmentCommands.env.listRuleBases()) {
					if (r.equals(ComplexEnvironmentCommands.env.getCurrentRuleBase())) r = "* " + r;
					else r = "- " + r;
					if (r.length() > longestRule) longestRule = r.length();
					arrRule.add(r);
				}
				arrRule.sort(Comparator.comparing(o -> o.substring(1)));
				arrRule.addFirst(ruleBaseTitle);
				arrRule.add(1, "-".repeat(longestRule));
				matrix.add(arrRule);
			}
			if (this.queryBase) {
				List<String> arrQuery = new ArrayList<>();
				for (String q : ComplexEnvironmentCommands.env.listQueryBases()) {
					if (q.equals(ComplexEnvironmentCommands.env.getCurrentQueryBase())) q = "* " + q;
					else q = "- " + q;
					if (q.length() > longestQuery) longestQuery = q.length();
					arrQuery.add(q);
				}
				arrQuery.sort(Comparator.comparing(o -> o.substring(1)));
				arrQuery.addFirst(queryBaseTitle);
				arrQuery.add(1, "-".repeat(longestQuery));
				matrix.add(arrQuery);
			}

			HashMap<String, Integer> mapSize = new HashMap<>();
			mapSize.put(factBaseTitle, longestFact);
			mapSize.put(ruleBaseTitle, longestRule);
			mapSize.put(queryBaseTitle, longestQuery);
			int baseMaxSize = 0;
			for (List<String> a : matrix) {
				baseMaxSize = Math.max(baseMaxSize, a.size());
			}

			StringBuilder sepLine = new StringBuilder("+");
			for (List<String> col : matrix) {
				if (col.getFirst().equals(factBaseTitle)) {
					sepLine.append("-".repeat(Math.max(0, longestFact + 2)));
				} else if (col.getFirst().equals(ruleBaseTitle)) {
					sepLine.append("-".repeat(Math.max(0, longestRule + 2)));
				} else {
					sepLine.append("-".repeat(Math.max(0, longestQuery + 2)));
				}
				sepLine.append("+");
			}

			StringBuilder finalString = new StringBuilder(sepLine.toString());
			finalString.append("\n");
			for (int i = 0; i < baseMaxSize; i++) {
				finalString.append("|");
				for (List<String> col : matrix) {
					if (i < col.size()) {
						finalString.append(" ");
						finalString.append(col.get(i));
						finalString.append(" ".repeat(Math.max(0, mapSize.get(col.getFirst()) - col.get(i).length())));
					} else {
						finalString.append(" ".repeat(Math.max(0, mapSize.get(col.getFirst()) + 1)));
					}
					finalString.append(" |");
				}
				finalString.append("\n");
			}
			finalString.append(sepLine);
			finalString.append("\n\n");

			IGRepl.writeIfVerbose(finalString.toString(), PrintLevel.MINIMAL);
		}
	}


	/**
	 * Command class for the "set" command.
	 */
	@Command(mixinStandardHelpOptions = true, usageHelpAutoWidth = true, name = "set",
			description = "Sets the current base(s).")
	static class SetBaseCommand implements Runnable {

		@Parameters(paramLabel = "BASENAME", arity = "0..1")
		String baseName;
		@Option(names = {"-f", "--fact"}, arity = "1", description = "desc")
		private String factBase;
		@Option(names = {"-r", "--rule"}, arity = "1", description = "desc")
		private String ruleBase;
		@Option(names = {"-q", "--query"}, arity = "1", description = "desc")
		private String queryBase;

		@Override
		public void run() {

			if (baseName != null) {
				this.factBase = baseName;
				this.ruleBase = baseName;
				this.queryBase = baseName;
			}

			IGRepl.writeIfVerbose("Setting:\n", PrintLevel.MAXIMAL);
			if (this.factBase != null) {
				if (!Checks.factbaseExists(this.factBase)) {
					IGRepl.writeIfVerbose("Warning: there is no \"" + this.factBase + "\" fact base in memory.\n",	PrintLevel.WARNING);
					IGRepl.writeIfVerbose("  \"" + ComplexEnvironmentCommands.env.getCurrentFactBase() + "\" as current fact base.\n",
							PrintLevel.MAXIMAL);
				} else {
					ComplexEnvironmentCommands.env.setCurrentFactBase(this.factBase);
					IGRepl.writeIfVerbose("  \"" + this.factBase + "\" as current fact base.\n", PrintLevel.MAXIMAL);
				}
			}

			if (this.ruleBase != null) {
				if (!Checks.rulebaseExists(this.ruleBase)) {
					IGRepl.writeIfVerbose("Warning: there is no \"" + this.ruleBase + "\" rule base in memory.\n", PrintLevel.WARNING);
					IGRepl.writeIfVerbose("  \"" + ComplexEnvironmentCommands.env.getCurrentRuleBase() + "\" as current rule base.\n",
							PrintLevel.MAXIMAL);
				} else {
					ComplexEnvironmentCommands.env.setCurrentRuleBase(this.ruleBase);
					IGRepl.writeIfVerbose("  \"" + this.ruleBase + "\" as current rule base.\n", PrintLevel.MAXIMAL);
				}
			}

			if (this.queryBase != null) {
				if (!Checks.querybaseExists(this.queryBase)) {
					IGRepl.writeIfVerbose("Warning: there is no \"" + this.queryBase + "\" query base in memory).\n", PrintLevel.WARNING);
					IGRepl.writeIfVerbose("  \"" + ComplexEnvironmentCommands.env.getCurrentQueryBase() + "\" as current query base.\n",
							PrintLevel.MAXIMAL);
				} else {
					ComplexEnvironmentCommands.env.setCurrentQueryBase(this.queryBase);
					IGRepl.writeIfVerbose("  \"" + this.queryBase + "\" as current query base.\n", PrintLevel.MAXIMAL);
				}
			}

			IGRepl.writeIfVerbose("Done setting base(s)!\n\n", PrintLevel.MINIMAL);
		}
	}

	/**
	 * Command class for the "create" command.
	 */
	@Command(mixinStandardHelpOptions = true, usageHelpAutoWidth = true, name = "create",
			description = "Creates a base (or clears it if already existing).")
	static class CreateBaseCommand implements Runnable {

		@Parameters(paramLabel = "BASENAME", arity = "0..*")
		private String[] baseNames = null;
		@Option(names = {"-f", "--fact"}, arity = "1..*",  description = "desc")
		private String[] factBases = null;
		@Option(names = {"-r", "--rule"}, arity = "1..*",  description = "desc")
		private String[] ruleBases = null;
		@Option(names = {"-q", "--query"}, arity = "1..*", description = "desc")
		private String[] queryBases;
		@Option(names = {"-m", "--mode"}, description = "The writing mode to use if the base receiving the result is not empty (${COMPLETION-CANDIDATES}).")
		private WriteMode mode = WriteMode.PROTECTED;

		@Override
		public void run() {

			if (baseNames == null && this.factBases == null && this.ruleBases == null && this.queryBases == null) {
				IGRepl.writeIfVerbose("Nothing to create! Aborting...\n\n", PrintLevel.MINIMAL);
				return;
			}

			if (baseNames != null) {
				this.factBases = baseNames;
				this.ruleBases = baseNames;
				this.queryBases = baseNames;
			}

			if (this.factBases == null) this.factBases = new String[]{};
			if (this.ruleBases == null)	this.ruleBases = new String[]{};
			if (this.queryBases == null) this.queryBases = new String[]{};

			IGRepl.writeIfVerbose("Creating (or clearing)", PrintLevel.MAXIMAL);
			IGRepl.writeIfVerbose(":\n", PrintLevel.MAXIMAL);

			if (this.factBases.length > 0) {
				ArrayList<String> createdBases = new ArrayList<>();
				boolean shouldCreate = true;
				for (String factBase : this.factBases) {
					if(Checks.factbaseExists(factBase) && !Checks.factbaseIsEmpty(factBase)) {
						IGRepl.writeIfVerbose("Warning: fact base \"" + factBase + "\" is not empty. ", PrintLevel.WARNING);
						switch (this.mode) {
							case OVERWRITE:
								IGRepl.writeIfVerbose("Overwriting...\n", PrintLevel.WARNING);
								break;
							case APPEND:
								IGRepl.writeIfVerbose("Doing nothing (append has no sense here)...\n", PrintLevel.WARNING);
								shouldCreate = false;
								break;
							case PROTECTED:
								IGRepl.writeIfVerbose("Doing nothing...\n", PrintLevel.WARNING);
								shouldCreate = false;
								break;
						}
					}
					if (shouldCreate) {
						ComplexEnvironmentCommands.env.createFactBase(factBase, this.mode);
						createdBases.add(factBase);
					}
				}

				if (!createdBases.isEmpty())
					IGRepl.writeIfVerbose("  \"" + String.join("\", \"", createdBases) + "\" fact base(s)...\n", PrintLevel.MAXIMAL);
				else
					IGRepl.writeIfVerbose("  no fact base...\n", PrintLevel.MAXIMAL);
			}

			if (this.ruleBases.length > 0) {
				ArrayList<String> createdBases = new ArrayList<>();
				boolean shouldCreate = true;
				for (String ruleBase : this.ruleBases) {
					if(Checks.rulebaseExists(ruleBase) && !Checks.rulebaseIsEmpty(ruleBase)) {
						IGRepl.writeIfVerbose("Warning: rule base \"" + ruleBase + "\" is not empty. ", PrintLevel.WARNING);
						switch (this.mode) {
							case OVERWRITE:
								IGRepl.writeIfVerbose("Overwriting...\n", PrintLevel.WARNING);
								break;
							case APPEND:
								IGRepl.writeIfVerbose("Doing nothing (append has no sense here)...\n", PrintLevel.WARNING);
								shouldCreate = false;
								break;
							case PROTECTED:
								IGRepl.writeIfVerbose("Doing nothing...\n", PrintLevel.WARNING);
								shouldCreate = false;
								break;
						}
					}
					if (shouldCreate) {
						ComplexEnvironmentCommands.env.createRuleBase(ruleBase, this.mode);
						createdBases.add(ruleBase);
					}
				}

				if (!createdBases.isEmpty())
					IGRepl.writeIfVerbose("  \"" + String.join("\", \"", createdBases) + "\" rule base(s)...\n", PrintLevel.MAXIMAL);
				else
					IGRepl.writeIfVerbose("  no rule base...\n", PrintLevel.MAXIMAL);
			}


			if (this.queryBases.length > 0) {
				ArrayList<String> createdBases = new ArrayList<>();
				boolean shouldCreate = true;
				for (String queryBase : this.queryBases) {
					if(Checks.querybaseExists(queryBase) && !Checks.querybaseIsEmpty(queryBase)) {
						IGRepl.writeIfVerbose("Warning: query base \"" + queryBase + "\" is not empty. ", PrintLevel.WARNING);
						switch (this.mode) {
							case OVERWRITE:
								IGRepl.writeIfVerbose("Overwriting...\n", PrintLevel.WARNING);
								break;
							case APPEND:
								IGRepl.writeIfVerbose("Doing nothing (append has no sense here)...\n", PrintLevel.WARNING);
								shouldCreate = false;
								break;
							case PROTECTED:
								IGRepl.writeIfVerbose("Doing nothing...\n", PrintLevel.WARNING);
								shouldCreate = false;
								break;
						}
					}
					if (shouldCreate) {
						ComplexEnvironmentCommands.env.createQueryBase(queryBase, this.mode);
						createdBases.add(queryBase);
					}
				}

				if (!createdBases.isEmpty())
					IGRepl.writeIfVerbose("  \"" + String.join("\", \"", createdBases) + "\" query base(s)...\n", PrintLevel.MAXIMAL);
				else
					IGRepl.writeIfVerbose("  no query base...\n", PrintLevel.MAXIMAL);
			}


			IGRepl.writeIfVerbose("Done creating base(s)!\n\n", PrintLevel.MINIMAL);
		}
	}

	/**
	 * Command class for the "remove" command.
	 */
	@Command(mixinStandardHelpOptions = true, usageHelpAutoWidth = true, name = "remove",
			description = "Removes a base.")
	static class RemoveBaseCommand implements Runnable {

		@Parameters(paramLabel = "BASENAME", arity = "0..*")
		private String[] baseNames = null;
		@Option(names = {"-f", "--fact"}, arity = "0..*",  description = "desc")
		private String[] factBases = null;
		@Option(names = {"-r", "--rule"}, arity = "0..*",  description = "desc")
		private String[] ruleBases = null;
		@Option(names = {"-q", "--query"}, arity = "0..*", description = "desc")
		private String[] queryBases;

		@Override
		public void run() {

			if (baseNames != null) {
				this.factBases = baseNames;
				this.ruleBases = baseNames;
				this.queryBases = baseNames;
			}

			if (this.factBases == null) this.factBases = new String[]{};
			else if (this.factBases.length == 0) this.factBases = new String[]{ComplexEnvironmentCommands.env.getCurrentFactBase()};
			if (this.ruleBases == null)	this.ruleBases = new String[]{};
			else if (this.ruleBases.length == 0) this.ruleBases = new String[]{ComplexEnvironmentCommands.env.getCurrentRuleBase()};
			if (this.queryBases == null) this.queryBases = new String[]{};
			else if (this.queryBases.length == 0) this.queryBases = new String[]{ComplexEnvironmentCommands.env.getCurrentQueryBase()};

			// TODO : forbid the removal when base is not empty (with option to force -F)?
			for (String factBase : this.factBases) {
				if (!Checks.factbaseExists(factBase)) {
					IGRepl.writeIfVerbose("Warning: there is no \"" + factBase + "\" fact base currently in memory.\n", PrintLevel.WARNING);
				} else if (factBase.equals("default")) {
					IGRepl.writeIfVerbose("Warning: the default fact base cannot be removed. Clearing instead...\n", PrintLevel.WARNING);
					ComplexEnvironmentCommands.env.createFactBase(factBase, WriteMode.OVERWRITE);
				} else {
					String currentFactBase = ComplexEnvironmentCommands.env.getCurrentFactBase();
					IGRepl.writeIfVerbose("Removing fact base \"" + factBase + "\"... ", PrintLevel.MAXIMAL);
					ComplexEnvironmentCommands.env.removeFactBase(factBase);
					if (factBase.equals(currentFactBase)) {
						ComplexEnvironmentCommands.env.setCurrentFactBase("default");
						IGRepl.writeIfVerbose("\"" + factBase + "\" was the current fact base. Setting new current fact base to default...\n", PrintLevel.MAXIMAL);
					} else {
						IGRepl.writeIfVerbose("\n", PrintLevel.MAXIMAL);
					}
				}
			}

			for (String ruleBase : this.ruleBases) {
				if (!Checks.rulebaseExists(ruleBase)) {
					IGRepl.writeIfVerbose("Warning: there is no \"" + ruleBase + "\" rule base currently in memory.\n", PrintLevel.WARNING);
				} else if (ruleBase.equals("default")) {
					IGRepl.writeIfVerbose("Warning: The default rule base cannot be removed. Clearing instead...\n", PrintLevel.WARNING);
					ComplexEnvironmentCommands.env.createRuleBase(ruleBase, WriteMode.OVERWRITE);
				} else {
					String currentRuleBase = ComplexEnvironmentCommands.env.getCurrentRuleBase();
					IGRepl.writeIfVerbose("Removing rule base \"" + ruleBase + "\"... ", PrintLevel.MAXIMAL);
					ComplexEnvironmentCommands.env.removeRuleBase(ruleBase);
					if (ruleBase.equals(currentRuleBase)) {
						ComplexEnvironmentCommands.env.setCurrentRuleBase("default");
						IGRepl.writeIfVerbose("\"" + ruleBase + "\" was the current rule base. Setting new current rule base to default...\n", PrintLevel.MAXIMAL);
					} else {
						IGRepl.writeIfVerbose("\n", PrintLevel.MAXIMAL);
					}
				}
			}

			for (String queryBase : this.queryBases) {
				if (!Checks.querybaseExists(queryBase)) {
					IGRepl.writeIfVerbose("Warning: there is no \"" + queryBase + "\" query base currently in memory.\n", PrintLevel.WARNING);
				} else if (queryBase.equals("default")) {
					IGRepl.writeIfVerbose("Warning: the default query base cannot be removed. Clearing instead...\n", PrintLevel.WARNING);
					ComplexEnvironmentCommands.env.createQueryBase(queryBase, WriteMode.OVERWRITE);
				} else {
					String currentQueryBase = ComplexEnvironmentCommands.env.getCurrentQueryBase();
					IGRepl.writeIfVerbose("Removing query base \"" + queryBase + "\"... ", PrintLevel.MAXIMAL);
					ComplexEnvironmentCommands.env.removeQueryBase(queryBase);
					if (queryBase.equals(currentQueryBase)) {
						ComplexEnvironmentCommands.env.setCurrentQueryBase("default");
						IGRepl.writeIfVerbose("\"" + queryBase + "\" was the current query base. Setting new current query base to default...\n", PrintLevel.MAXIMAL);
					} else {
						IGRepl.writeIfVerbose("\n", PrintLevel.MAXIMAL);
					}
				}
			}

			IGRepl.writeIfVerbose("Done removing base(s)!\n\n", PrintLevel.MINIMAL);
		}
	}

	/**
	 * Command class for the "assert" command.
	 */
	@Command(mixinStandardHelpOptions = true, usageHelpAutoWidth = true, name = "assert",
			description = "Add some facts, rules and/or queries to the current base(s).")
	static class AssertCommand implements Runnable {

		@Parameters(paramLabel = "DLGP", arity = "1")
		private String dlgp;
		@Option(names = {"-f", "--fact"}, arity="0..1", description = "desc")
		private String factBase = null;
		@Option(names = {"-r", "--rule"}, arity="0..1", description = "desc")
		private String ruleBase = null;
		@Option(names = {"-q", "--query"}, arity="0..1", description = "desc")
		private String queryBase = null;
		@Option(names = {"-s", "--set"}, description = "Set the created base(s) as current base(s).")
		private boolean set;
		@Option(names = {"-m", "--mode"}, description = "The writing mode to use if the base receiving the result is not empty (${COMPLETION-CANDIDATES}).")
		private WriteMode mode = WriteMode.APPEND;

		@Override
		public void run() {

			if (this.factBase == null && this.ruleBase == null && this.queryBase == null) {
				this.factBase = "";
				this.ruleBase = "";
				this.queryBase = "";
			}

			IGRepl.writeIfVerbose("Asserting:\n", PrintLevel.MAXIMAL);
			// TODO: not so great actually: by doing this this way, we parse at worse 3 times the same string (we could do it in one go...)

			String factBaseName;
			if (this.factBase != null) {
				if (!this.factBase.isEmpty()) factBaseName = this.factBase;
				else factBaseName = ComplexEnvironmentCommands.env.getCurrentFactBase();
				if (this.set) ComplexEnvironmentCommands.env.setCurrentFactBase(factBaseName);

				boolean shouldAssert = true;
				if (!Checks.factbaseExists(factBaseName)) {
					IGRepl.writeIfVerbose("Warning: fact base \"" + factBaseName + "\" does not exist. Creating...\n", PrintLevel.WARNING);
					ComplexEnvironmentCommands.env.createFactBase(factBaseName, WriteMode.OVERWRITE);
				} else if (!Checks.factbaseIsEmpty(factBaseName)) {
					IGRepl.writeIfVerbose("Warning: target fact base is not empty. ", PrintLevel.WARNING);
					shouldAssert = Checks.checkWriteModeAndWarn(this.mode);
				}

				int newFacts = 0;
				// TODO: it would be nice to catch the exception in IGRepl rather than here, but I don't know how to do that inside the run() method 
				try {
					if (shouldAssert) newFacts = ComplexEnvironmentCommands.env.addFactsToBase(factBaseName, this.dlgp);
				} catch (ParseException e) {
					IGRepl.systemRegistry.trace(e);
				}

				IGRepl.writeIfVerbose("  " + newFacts + " facts to \"" + factBaseName + "\" fact base...\n", PrintLevel.MAXIMAL);
			}

			String ruleBaseName;
			if (this.ruleBase != null) {
				if (!this.ruleBase.isEmpty()) ruleBaseName = this.ruleBase;
				else ruleBaseName = ComplexEnvironmentCommands.env.getCurrentRuleBase();
				if (this.set) ComplexEnvironmentCommands.env.setCurrentRuleBase(ruleBaseName);

				boolean shouldAssert = true;
				if (!Checks.rulebaseExists(ruleBaseName)) {
					IGRepl.writeIfVerbose("Warning: rule base \"" + ruleBaseName + "\" does not exist. Creating...\n", PrintLevel.WARNING);
					ComplexEnvironmentCommands.env.createRuleBase(ruleBaseName, WriteMode.OVERWRITE);
				} else if (!Checks.rulebaseIsEmpty(ruleBaseName)) {
					IGRepl.writeIfVerbose("Warning: target rule base is not empty. ", PrintLevel.WARNING);
					shouldAssert = Checks.checkWriteModeAndWarn(this.mode);
				}

				int newRules = 0;
				// TODO: it would be nice to catch the exception in IGRepl rather than here, but I don't know how to do that inside the run() method 
				try {
					if (shouldAssert) newRules = ComplexEnvironmentCommands.env.addRulesToBase(ruleBaseName, this.dlgp);
				} catch (ParseException e) {
					IGRepl.systemRegistry.trace(e);
				}

				IGRepl.writeIfVerbose("  " + newRules + " rules to \"" + ruleBaseName + "\" rule base...\n", PrintLevel.MAXIMAL);
			}

			String queryBaseName;
			if (this.queryBase != null) {
				if (!this.queryBase.isEmpty()) queryBaseName = this.queryBase;
				else queryBaseName = ComplexEnvironmentCommands.env.getCurrentQueryBase();
				if (this.set) ComplexEnvironmentCommands.env.setCurrentQueryBase(queryBaseName);

				boolean shouldAssert = true;
				if (!Checks.querybaseExists(queryBaseName)) {
					IGRepl.writeIfVerbose("Warning: query base \"" + queryBaseName + "\" does not exist. Creating...\n", PrintLevel.WARNING);
					ComplexEnvironmentCommands.env.createQueryBase(queryBaseName, WriteMode.OVERWRITE);
				} else if (!Checks.querybaseIsEmpty(queryBaseName)) {
					IGRepl.writeIfVerbose("Warning: target query base is not empty. ", PrintLevel.WARNING);
					shouldAssert = Checks.checkWriteModeAndWarn(this.mode);
				}

				int newQueries = 0;
				// TODO: it would be nice to catch the exception in IGRepl rather than here, but I don't know how to do that inside the run() method 
				try {
					if (shouldAssert) newQueries = ComplexEnvironmentCommands.env.addQueriesToBase(queryBaseName, this.dlgp);
				} catch (ParseException e) {
					IGRepl.systemRegistry.trace(e);
				}

				IGRepl.writeIfVerbose("  " + newQueries + " queries to \"" + queryBaseName + "\" query base...\n", PrintLevel.MAXIMAL);
			}


			IGRepl.writeIfVerbose("Done asserting!\n\n", PrintLevel.MINIMAL);
		}
	}


	/**
	 * Command class for the "retract" command.
	 */
	@Command(mixinStandardHelpOptions = true, usageHelpAutoWidth = true, name = "retract",
			description = "Removes some facts, rules and/or queries from the current base(s).")
	static class RetractCommand implements Runnable {

		@Parameters(paramLabel = "DLGP", arity = "1")
		private String dlgp;
		@Option(names = {"-f", "--fact"}, arity="0..1", description = "desc")
		private String factBase = null;
		@Option(names = {"-r", "--rule"}, arity="0..1", description = "desc")
		private String ruleBase = null;
		@Option(names = {"-q", "--query"}, arity="0..1", description = "desc")
		private String queryBase = null;

		@Override
		public void run() {

			if (this.factBase == null && this.ruleBase == null && this.queryBase == null) {
				this.factBase = "";
				this.ruleBase = "";
				this.queryBase = "";
			}

			IGRepl.writeIfVerbose("Retracting:\n", PrintLevel.MAXIMAL);

			String factBaseName;
			if (this.factBase != null) {
				if (!this.factBase.isEmpty()) factBaseName = this.factBase;
				else factBaseName = ComplexEnvironmentCommands.env.getCurrentFactBase();
				if (!Checks.factbaseExists(factBaseName)) {
					IGRepl.writeIfVerbose("Warning: fact base \"" + factBaseName + "\" does not exist.\n", PrintLevel.WARNING);
				} else {
					try {
						IGRepl.writeIfVerbose("  " + ComplexEnvironmentCommands.env.removeFactsFromBase(factBaseName, this.dlgp) +
								" facts from \"" + factBaseName + "\" fact base...\n", PrintLevel.MAXIMAL);
					} catch (ParseException e) {
						IGRepl.systemRegistry.trace(e);
					}
				}
			}

			String ruleBaseName;
			if (this.ruleBase != null) {
				if (!this.ruleBase.isEmpty()) ruleBaseName = this.ruleBase;
				else ruleBaseName = ComplexEnvironmentCommands.env.getCurrentRuleBase();
				if (!Checks.rulebaseExists(ruleBaseName)) {
					IGRepl.writeIfVerbose("Warning: rule base \"" + ruleBaseName + "\" does not exist.\n", PrintLevel.WARNING);
				} else {
					try {
						IGRepl.writeIfVerbose("  " + ComplexEnvironmentCommands.env.removeRulesFromBase(ruleBaseName, this.dlgp) +
								" rules from \"" + ruleBaseName + "\" rule base...\n", PrintLevel.MAXIMAL);
					} catch (ParseException e) {
						IGRepl.systemRegistry.trace(e);
					}
				}
			}

			String queryBaseName;
			if (this.queryBase != null) {
				if (!this.queryBase.isEmpty()) queryBaseName = this.queryBase;
				else queryBaseName = ComplexEnvironmentCommands.env.getCurrentQueryBase();
				if (!Checks.querybaseExists(queryBaseName)) {
					IGRepl.writeIfVerbose("Warning: query base \"" + queryBaseName + "\" does not exist.\n", PrintLevel.WARNING);
				} else {
					try {
						IGRepl.writeIfVerbose("  " + ComplexEnvironmentCommands.env.removeQueriesFromBase(queryBaseName, this.dlgp) +
								" queries from \"" + queryBaseName + "\" query base...\n", PrintLevel.MAXIMAL);
					} catch (ParseException e) {
						IGRepl.systemRegistry.trace(e);
					}
				}
			}

			IGRepl.writeIfVerbose("Done retracting!\n\n", PrintLevel.MINIMAL);
		}
	}


	/**
	 * Command class for the "mapping" command.
	 */
	@Command(mixinStandardHelpOptions = true, usageHelpAutoWidth = true, name = "mapping",
			description = "Loads a federation using a mapping file")
	static class MappingCommand implements Runnable {

		@Parameters(paramLabel = "MAPPING", arity = "1", description = "Path to the mapping.json file")
		private String mappingFilePath;

		@Override
		public void run() {

			IGRepl.writeIfVerbose("Loading mapping base from " + mappingFilePath + " ...\n", PrintLevel.MINIMAL);

			String filename = Path.of(this.mappingFilePath).getFileName().toString();

			if(Checks.factbaseExists(filename)) {
				IGRepl.writeIfVerbose("Warning: fact base \"" + filename + "\" already exists. Overriding\n", PrintLevel.WARNING);
			}

			FederatedFactBase federation;

			try {
				federation = new FederatedFactBase(StorageBuilder.defaultStorage(), ViewBuilder.createFactBases(this.mappingFilePath));
			} catch (ViewBuilder.ViewBuilderException e) {
				IGRepl.writeIfVerbose("An error occurred while creating the federation using the view definition file.\n", PrintLevel.MAXIMAL);
				IGRepl.writeIfVerbose(e.getMessage(), PrintLevel.MAXIMAL);
				return;
			}

			ComplexEnvironmentCommands.env.unsafePutFactBase(filename, federation, WriteMode.OVERWRITE);
			ComplexEnvironmentCommands.env.setCurrentFactBase(filename);
			IGRepl.writeIfVerbose("Mapping base " + filename + " loaded!\n\n", PrintLevel.MINIMAL);
		}
	}


}
