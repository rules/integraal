package fr.boreal.api.integraal_repl;

import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.boreal.api.integraal_repl.CECommands.ComplexEnvironmentCommands;
import fr.boreal.api.integraal_repl.ComplexEnvironment.WriteMode;
import fr.boreal.api.integraal_repl.IGRepl.PrintLevel;
import fr.boreal.io.api.ParseException;
import fr.boreal.io.dlgp.Directive;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.io.dlgp.DlgpUtil;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.storage.builder.StorageBuilder;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;
import fr.boreal.views.FederatedFactBase;
import fr.boreal.views.builder.ViewBuilder;
import fr.boreal.views.builder.ViewBuilder.ViewBuilderException;

/**
 * An object that represents an environment for a logic-based system.
 * 
 * ComplexEnvironment handles multiple bases of facts, rules and queries and provides functions to access and modify these bases.
 *
 * @author Pierre Bisquert
 */
public class ComplexEnvironment {

	private TermFactory termFactory = SameObjectTermFactory.instance();
    private PredicateFactory predicateFactory = SameObjectPredicateFactory.instance();
    private final HashMap<String, FactBase> factbases;
	private final HashMap<String, RuleBase> rulebases;
	private final HashMap<String, Collection<Query>> querybases;
	private String currentFactBase;
	private String currentRuleBase;
	private String currentQueryBase;
	public enum WriteMode { APPEND, PROTECTED, OVERWRITE }

	public ComplexEnvironment() {
		this(new HashMap<>(), new HashMap<>(), new HashMap<>());
		this.factbases.put("default", new SimpleInMemoryGraphStore());
		this.rulebases.put("default", new RuleBaseImpl());
		this.querybases.put("default", new HashSet<>());
		this.setCurrentBases("default");
	}

	public ComplexEnvironment(FactBase factbase, RuleBase rulebase, Collection<Query> queries) {
		this(new HashMap<>(), new HashMap<>(), new HashMap<>());
		this.factbases.put("default", factbase);
		this.rulebases.put("default", rulebase);
		this.querybases.put("default", queries);
		this.setCurrentBases("default");
	}

	private ComplexEnvironment(HashMap<String, FactBase> factbases, HashMap<String, RuleBase> rulebases, HashMap<String, Collection<Query>> querybases) {
		this.factbases = factbases;
		this.rulebases = rulebases;
		this.querybases = querybases;
	}

	public TermFactory getTermFactory() {
		return termFactory;
	}

	public void setTermFactory(TermFactory termFactory) {
		this.termFactory = termFactory;
	}

	public PredicateFactory getPredicateFactory() {
		return predicateFactory;
	}

	public void setPredicateFactory(PredicateFactory predicateFactory) {
		this.predicateFactory = predicateFactory;
	}


	public String getCurrentFactBase() {
		return currentFactBase;
	}

	public void setCurrentFactBase(String currentFactBase) {
		this.currentFactBase = currentFactBase;
	}

	public Set<String> listFactBases() {
		return this.factbases.keySet();
	}

	public boolean containsFactBase(String baseName) {
		return this.factbases.containsKey(baseName);
	}

	public FactBase getFactBaseByName(String baseName) {
		return this.factbases.get(baseName);
	}

	public FactBase getUnionOfFactBasesByNames(List<String> baseNames) {
		// TODO: something to note, we make the conscious choice to not be smart and call getFactBaseByName when baseNames contains
		// only one base to allow the removal of redundancies.
		SimpleInMemoryGraphStore factBaseUnion = new SimpleInMemoryGraphStore();
		for (String factBase : baseNames) {
			for (Atom atom : (Iterable<Atom>) this.getFactBaseByName(factBase).getAtoms()::iterator) {
				// since we can have redundancy from the (different) base(s), we remove them
				if (!factBaseUnion.contains(atom)) {
					factBaseUnion.add(atom);
				}
			}
		}
		return factBaseUnion;
	}

	public FactBase putFactBase(String baseName, FactBase factbase, WriteMode mode) {
		if (this.factbases.containsKey(baseName) && this.getFactBaseByName(baseName).getAtoms().findAny().isPresent()) {
			switch (mode) {
			case OVERWRITE:
				this.factbases.put(baseName, factbase);
				break;
			case APPEND:
				for (Atom atom : (Iterable<Atom>)factbase.getAtoms()::iterator) {
					this.getFactBaseByName(baseName).add(atom);
				}
				break;
			case PROTECTED:
				return null;
			}
		} else {
			this.factbases.put(baseName, factbase);
		}
		return this.getFactBaseByName(baseName);
	}
	
	/**
	 * This is not a safe method.
	 * This method should only be used for factbases where atoms cannot be checked such as mapping datasources.
	 * @param baseName the name of the new factbase
	 * @param factbase the enw factbase
	 * @param mode the write mode of the factbase
	 * @return the final factbase stored
	 */
	public FactBase unsafePutFactBase(String baseName, FactBase factbase, WriteMode mode) {
		if (this.factbases.containsKey(baseName)) {
			switch (mode) {
			case OVERWRITE:
				this.factbases.put(baseName, factbase);
				break;
			case APPEND:
				return null;
			case PROTECTED:
				return null;
			}
		} else {
			this.factbases.put(baseName, factbase);
		}
		return this.getFactBaseByName(baseName);
	}

	// TODO: not sure we really need this method...
	public FactBase createFactBase(String baseName, WriteMode mode) {
		return this.putFactBase(baseName, new SimpleInMemoryGraphStore(), mode);
	}

	public void removeFactBase(String baseName) {
		this.factbases.remove(baseName);
	}

	public int addFactsToBase(String baseName, String facts) throws ParseException {
		Set<Atom> parsedAtoms = DlgpUtil.parseFacts(facts);
		this.getFactBaseByName(baseName).addAll(parsedAtoms);
		return parsedAtoms.size();
	}

	public int removeFactsFromBase(String baseName, String facts) throws ParseException {
		Set<Atom> parsedAtoms = DlgpUtil.parseFacts(facts);
		this.getFactBaseByName(baseName).removeAll(parsedAtoms);
		return parsedAtoms.size();
	}

	/* TODO: 
	 * - what should we do if oldName is not in the map? Print an error? Or do nothing?
	 * For now, let's print an error message
	 */
	public FactBase renameFactBase(String oldName, String newName) {
		if (this.factbases.containsKey(oldName)) {
			FactBase factbase = this.putFactBase(newName, this.getFactBaseByName(oldName), WriteMode.PROTECTED);
			if (factbase != null) {
				this.factbases.remove(oldName);
				if (oldName.equals(this.getCurrentFactBase())) {
					this.setCurrentFactBase(newName);
				}
			}
			return factbase;
		} else {
			System.out.println("Warning: fact base \"" + oldName + "\" is unknown.");
			return null;
		}
	}


	/**************/
	/* Rule bases */
	/**************/

	//	public HashMap<String, RuleBase> getRuleBases() {
	//		return rulebases;
	//	}

	//	public void setRuleBases(HashMap<String, RuleBase> rulebases) {
	//		this.rulebases = rulebases;
	//	}

	public String getCurrentRuleBase() {
		return currentRuleBase;
	}

	public void setCurrentRuleBase(String currentRuleBase) {
		this.currentRuleBase = currentRuleBase;
	}

	public Set<String> listRuleBases() {
		return this.rulebases.keySet();
	}

	public boolean containsRuleBase(String baseName) {
		return this.rulebases.containsKey(baseName);
	}

	public RuleBase getRuleBaseByName(String baseName) {
		return this.rulebases.get(baseName);
	}

	public RuleBase getUnionOfRuleBasesByNames(List<String> baseNames) {
		// TODO: something to note, we make the conscious choice to not be smart and call getRuleBaseByName when baseNames contains
		// only one base to allow the removal of redundancies.
		RuleBaseImpl ruleBaseUnion = new RuleBaseImpl();
		for (String ruleBase : baseNames) {
			for (FORule rule : this.getRuleBaseByName(ruleBase).getRules()) {
				// since we can have redundancy from the (different) base(s), we remove them
				if (!ruleBaseUnion.getRules().contains(rule)) {
					ruleBaseUnion.add(rule);
				}
			}
		}
		return ruleBaseUnion;
	}

	public RuleBase putRuleBase(String baseName, RuleBase rulebase, WriteMode mode) {
		if (this.rulebases.containsKey(baseName) && !this.getRuleBaseByName(baseName).getRules().isEmpty()) {
			switch (mode) {
			case OVERWRITE:
				this.rulebases.put(baseName, rulebase);
				break;
			case APPEND:
				for (FORule rule : rulebase.getRules()) {
					this.getRuleBaseByName(baseName).add(rule);
				}
				break;
			case PROTECTED:
				return null;
			}
		} else {
			this.rulebases.put(baseName, rulebase);
		}
		return this.getRuleBaseByName(baseName);
	}

	// TODO: not sure we really need this method...
	public RuleBase createRuleBase(String baseName, WriteMode mode) {
		return this.putRuleBase(baseName, new RuleBaseImpl(), mode);
	}

	public void removeRuleBase(String baseName) {
		this.rulebases.remove(baseName);
	}

	// TODO: we lack a addAll here
	public int addRulesToBase(String baseName, String rules) throws ParseException {
		Set<FORule> parsedRules = DlgpUtil.parseRules(rules);
		RuleBase rulebase = this.getRuleBaseByName(baseName);
		for (FORule rule : parsedRules) {
			rulebase.add(rule);
		}
		return parsedRules.size();
	}

	// TODO: we lack a removeAll here
	public int removeRulesFromBase(String baseName, String rules) throws ParseException {
		Set<FORule> parsedRules = DlgpUtil.parseRules(rules);
		RuleBase rulebase = this.getRuleBaseByName(baseName);
		for (FORule rule : parsedRules) {
			rulebase.remove(rule);
		}
		return parsedRules.size();
	}

	/* TODO: 
	 * - what should we do if oldName is not in the map? Print an error? Or do nothing?
	 * For now, let's print an error message
	 */
	public RuleBase renameRuleBase(String oldName, String newName, WriteMode mode) {
		if (this.rulebases.containsKey(oldName)) {
			RuleBase rulebase = this.putRuleBase(newName, this.getRuleBaseByName(oldName), WriteMode.PROTECTED);
			if (rulebase != null) {
				this.rulebases.remove(oldName);
				if (oldName.equals(this.getCurrentRuleBase())) {
					this.setCurrentRuleBase(newName);
				}
			}
			return rulebase;
		} else {
			System.out.println("Warning: rule base \"" + oldName + "\" is unknown.");
			return null;
		}
	}


	/***************/
	/* Query bases */
	/***************/

	//	public HashMap<String, Collection<FOQuery>> getQueryBases() {
	//		return querybases;
	//	}

	//	public void setQueryBases(HashMap<String, Collection<FOQuery>> querybases) {
	//		this.querybases = querybases;
	//	}

	public String getCurrentQueryBase() {
		return currentQueryBase;
	}

	public void setCurrentQueryBase(String currentQueryBase) {
		this.currentQueryBase = currentQueryBase;
	}

	public Set<String> listQueryBases() {
		return this.querybases.keySet();
	}

	public boolean containsQueryBase(String baseName) {
		return this.querybases.containsKey(baseName);
	}

	public Collection<Query> getQueryBaseByName(String baseName) {
		return this.querybases.get(baseName);
	}

	public Collection<Query> getUnionOfQueryBasesByNames(List<String> baseNames) {
		// TODO: something to note, we make the conscious choice to not be smart and call getQueryBaseByName when baseNames contains
		// only one base to allow the removal of redundancies.
		Collection<Query> queryBaseUnion = new HashSet<>();
		for (String queryBase : baseNames) {
			for (Query query: this.getQueryBaseByName(queryBase)) {
				// since we can have redundancy from the (different) base(s), we remove them
				if (!queryBaseUnion.contains(query)) {
					queryBaseUnion.add(query);
				}
			}
		}
		return queryBaseUnion;
	}

	public Collection<Query> putQueryBase(String baseName, Collection<Query> querybase, WriteMode mode) {
		if (this.querybases.containsKey(baseName) && !this.getQueryBaseByName(baseName).isEmpty()) {
			switch (mode) {
			case OVERWRITE: 
				this.querybases.put(baseName, querybase);
				break;
			case APPEND:
				for (Query query: querybase) {
					this.getQueryBaseByName(baseName).add(query);
				}
				break;
			case PROTECTED:
				return null;
			}
		} else {
			this.querybases.put(baseName, querybase);
		}
		return this.getQueryBaseByName(baseName);
	}

	// TODO: not sure we really need this method...
	public Collection<Query> createQueryBase(String baseName, WriteMode mode) {
		return this.putQueryBase(baseName, new HashSet<>(), mode);
	}

	public void removeQueryBase(String baseName) {
		this.querybases.remove(baseName);
	}

	public int addQueriesToBase(String baseName, String queries) throws ParseException {
		Collection<Query> parsedQueries = DlgpUtil.parseQueries(queries);
		this.getQueryBaseByName(baseName).addAll(parsedQueries);
		return parsedQueries.size();
	}

	public int removeQueriesFromBase(String baseName, String queries) throws ParseException {
		Set<Query> parsedQueries = DlgpUtil.parseQueries(queries);
		this.getQueryBaseByName(baseName).removeAll(parsedQueries);
		return parsedQueries.size();
	}

	/* TODO: 
	 * - what should we do if oldName is not in the map? Print an error? Or do nothing?
	 * For now, let's print an error message
	 */
	public Collection<Query>  renameQueryBase(String oldName, String newName, WriteMode mode) {
		if (this.querybases.containsKey(oldName)) {
			Collection<Query> querybase = this.putQueryBase(newName, this.getQueryBaseByName(oldName), WriteMode.PROTECTED);
			if (querybase != null) {
				this.querybases.remove(oldName);
				if (oldName.equals(this.getCurrentQueryBase())) {
					this.setCurrentQueryBase(newName);
				}
			}
			return querybase;
		} else {
			System.out.println("Warning: query base \"" + oldName + "\" is unknown.");
			return null;
		}
	}


	/*************/
	/* All bases */
	/*************/

	public void setCurrentBases(String currentBases) {
		this.currentFactBase = currentBases;
		this.currentRuleBase = currentBases;
		this.currentQueryBase = currentBases;
	}

	public void setCurrentBases(String currentFactBase, String currentRuleBase, String currentQueryBase) {
		this.currentFactBase = currentFactBase;
		this.currentRuleBase = currentRuleBase;
		this.currentQueryBase = currentQueryBase;
	}

	/* TODO:
	 * - we are not taking care of redundancies here; should we?
	 * - add warning if one of the "bases" is empty in the dlgp file ?
	 */
	// UNUSED
//	public void loadDLGPFiles(String factBaseName, String ruleBaseName, String queryBaseName, InputStream[] dlgpList, WriteMode mode) throws ParseException {
//		FactBase factbase = null;
//		RuleBase rulebase = null;
//		Collection<Query> querybase = null;
//
//		if (factBaseName != null) {
//			factbase = this.createFactBase(factBaseName, mode);
//		}
//		if (ruleBaseName != null ) {
//			rulebase = this.createRuleBase(ruleBaseName, mode);
//		}
//		if (queryBaseName != null) {
//			querybase = this.createQueryBase(queryBaseName, mode);
//		}
//
//		// TODO: the factories will probably annoy us because they won't allow for the same predicates with different arities over different bases
//		// That might actually be desirable, if we need to mix and match different bases
//		//		try {
//
//		for (InputStream dlgp : dlgpList) {
//			DlgpParser dlgp_parser = new DlgpParser(dlgp);
//			while (dlgp_parser.hasNext()) {
//				Object result = dlgp_parser.next();
//				if (factbase != null && result instanceof Atom) {
//					factbase.add((Atom) result);
//				} else if (rulebase != null && result instanceof FORule) {
//					rulebase.add((FORule) result);
//				} else if (querybase != null && result instanceof FOQuery) {
//					querybase.add((Query) result);
//				}
//			}
//			dlgp_parser.close();
//		}
//	}

	// UNUSED
//	public void loadDLGPFile(String factBaseName, String ruleBaseName, String queryBaseName, InputStream dlgp, WriteMode mode) throws ParseException {
//		this.loadDLGPFiles(factBaseName, ruleBaseName, queryBaseName, new InputStream[] {dlgp}, mode);
//	}
	
	public void loadDLGPFile(String factBaseName, String ruleBaseName, String queryBaseName, InputStream dlgp, Path dlgpFileSourcePath, WriteMode mode) throws ParseException {
		FactBase factbase = null;
		RuleBase rulebase = null;
		Collection<Query> querybase = null;

		if (factBaseName != null) {
			factbase = this.createFactBase(factBaseName, mode);
		}
		if (ruleBaseName != null ) {
			rulebase = this.createRuleBase(ruleBaseName, mode);
		}
		if (queryBaseName != null) {
			querybase = this.createQueryBase(queryBaseName, mode);
		}

		DlgpParser dlgp_parser = new DlgpParser(dlgp);
		while (dlgp_parser.hasNext()) {
			Object result = dlgp_parser.next();
			if (factbase != null && result instanceof Atom) {
				factbase.add((Atom) result);
			} else if (rulebase != null && result instanceof FORule) {
				rulebase.add((FORule) result);
			} else if (querybase != null && result instanceof FOQuery) {
				querybase.add((Query) result);
			} else if (result instanceof Directive) {
				// TODO: we should probably have a subroutine that is called here and in the mapping command
				if (dlgpFileSourcePath != null) {
					// TODO: For now, VD file, if present, is only loaded if fileSourcePath is not null
					// It can be considered as a flag meaning we should load the VD file, in addition to provide the path (of the dlgp file)
					if (((Directive) result).type() == Directive.Type.VIEW) {
						String value = (String) ((Directive) result).value();
						// If the path is not absolute, we use the dlgp file path as origin
						Path vdPath = Paths.get(value);
						if (!vdPath.isAbsolute()) {
							vdPath = dlgpFileSourcePath.getParent().resolve(vdPath);
						}

						FederatedFactBase federation;
						if (factbase == null) factbase = new SimpleInMemoryGraphStore();
						try {
							federation = new FederatedFactBase(factbase, ViewBuilder.createFactBases(vdPath.toString()));
							this.putFactBase(factBaseName, federation, mode);
						} catch (ViewBuilderException e) {
							IGRepl.writeIfVerbose("An error occurred while parsing the view definition file. Skipping.\n", PrintLevel.MAXIMAL);
							IGRepl.writeIfVerbose(e.getMessage(), PrintLevel.MAXIMAL);
						}
					}		
				}
			}
		}
		dlgp_parser.close();
	}
}
