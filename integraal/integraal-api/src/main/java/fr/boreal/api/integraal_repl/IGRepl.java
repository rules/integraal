package fr.boreal.api.integraal_repl;

import java.io.IOException;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.function.Supplier;

import org.fusesource.jansi.AnsiConsole;
import org.jline.console.SystemRegistry;
import org.jline.console.impl.SystemRegistryImpl;
import org.jline.keymap.KeyMap;
import org.jline.reader.Binding;
import org.jline.reader.EndOfFileException;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.reader.MaskingCallback;
import org.jline.reader.Parser;
import org.jline.reader.Reference;
import org.jline.reader.UserInterruptException;
import org.jline.reader.impl.DefaultParser;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;
import org.jline.widget.TailTipWidgets;

import fr.boreal.api.integraal_repl.CECommands.ComplexEnvironmentCommands;
import fr.boreal.api.integraal_repl.IGCommands.InteGraalCommands;
import fr.boreal.io.dlgp.DlgpWriter;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.shell.jline3.PicocliCommands;
import picocli.shell.jline3.PicocliCommands.PicocliCommandsFactory;

/**
 * The main class defining the interactive tool.
 * <br/>
 * It basically initializes the JLine REPL with commands relating to InteGraal
 * 
 * @author Pierre Bisquert
 */
@Command(name = "IGRepl")
public class IGRepl {

	/* TODO:
	 * Something to remind, the new JLine version requires to add the JVM option --add-opens java.base/java.lang=ALL-UNNAMED
	 * For the jar, this can be done within the MANIFEST file: "Add-Opens: java.base/java.lang" (and this probably can be automated through Maven).
	 */

	@Option(names = {"-l", "--load"}, arity="1..*", description = "desc")
	private String[] fileToLoad = null;
	@Option(names = {"-v", "--verbose"}, description = "desc")
	//	protected static Boolean verbosity = Boolean.parseBoolean("false");
	//	private boolean[] verbosity = null;

	protected static PrintLevel verbosityLevel = PrintLevel.MINIMAL;
	protected static DlgpWriter dlgpWriter = new DlgpWriter();
	protected static SystemRegistry systemRegistry;

	public enum PrintLevel { 
		MINIMAL(1),
		WARNING(2),
		MAXIMAL(3);
		private final int priority;

		PrintLevel(int priority) {
			this.priority = priority;
		}

		public int getPriority() {
			return this.priority;
		}
	}

	public static void writeIfVerbose(Object toWrite, PrintLevel messageLevel) {
		if (messageLevel.getPriority() <= IGRepl.verbosityLevel.getPriority()) {
			try {
				IGRepl.dlgpWriter.write(toWrite);
				IGRepl.dlgpWriter.flush();
			} catch (IOException e) {
				IGRepl.systemRegistry.trace(e);
			}
		}
	}

	public static void main(String[] args) {
		AnsiConsole.systemInstall();
		IGRepl iGRepl = new IGRepl();
		new CommandLine(iGRepl).parseArgs(args);

		try {
			Supplier<Path> workDir = () -> Path.of(System.getProperty("user.dir"));

			// TODO: should the env be a static parameter also?
			ComplexEnvironment env = new ComplexEnvironment();
			PicocliCommandsFactory factory = new PicocliCommandsFactory();

			InteGraalCommands inteGraalCommands = new InteGraalCommands(env);
			CommandLine igCLI = new CommandLine(inteGraalCommands, factory);
			igCLI.setAbbreviatedOptionsAllowed(true);
			igCLI.setCaseInsensitiveEnumValuesAllowed(true);
			PicocliCommands picocliIGCommands = new PicocliCommands(igCLI);
			picocliIGCommands.name("InteGraal");

			ComplexEnvironmentCommands complexEnvironmentCommands = new ComplexEnvironmentCommands(env);
			CommandLine ceCLI = new CommandLine(complexEnvironmentCommands, factory);
			ceCLI.setAbbreviatedOptionsAllowed(true);
			ceCLI.setCaseInsensitiveEnumValuesAllowed(true);
			PicocliCommands picocliCECommands = new PicocliCommands(ceCLI);
			picocliCECommands.name("Environment");


			Parser parser = new DefaultParser();
			try (Terminal terminal = TerminalBuilder.builder().build()) {
				//TODO: the following could help setting up signal catching
				//			try (Terminal terminal = TerminalBuilder.builder().signalHandler(Terminal.SignalHandler.SIG_IGN).build()) {
				//				SignalHandler intHandler = terminal.handle(Signal.INT, s -> {
				//					System.out.println("interrupted");
				//				});
				//				System.out.println(intHandler.toString());

				// TODO: the verbosity stuff needs to be tinkered, meanwhile we use the regular SystemRegistry to make the verbose command disappear
				systemRegistry = new SystemRegistryImpl(parser, terminal, workDir, null);
				systemRegistry.setCommandRegistries(picocliIGCommands, picocliCECommands);


				LineReader reader = LineReaderBuilder.builder()
						.terminal(terminal)
						.completer(systemRegistry.completer())
						.parser(parser)
						.variable(LineReader.LIST_MAX, 50)   // max tab completion candidates
						.variable(LineReader.HISTORY_FILE, Path.of(System.getProperty("java.io.tmpdir"), "IGRepl_history")) // history file in tmp directory
						.variable(LineReader.HISTORY_FILE_SIZE, 100) // history entries
						.build();

				inteGraalCommands.setLineReader(reader);
				complexEnvironmentCommands.setLineReader(reader);
				factory.setTerminal(terminal);
				TailTipWidgets widgets = new TailTipWidgets(reader, systemRegistry::commandDescription, 7, TailTipWidgets.TipType.COMPLETER);
				widgets.enable();
				KeyMap<Binding> keyMap = reader.getKeyMaps().get("main");
				keyMap.bind(new Reference("tailtip-toggle"), KeyMap.alt("s"));

				String prompt = "?- ";

				// TODO: the verbosity stuff needs to be tinkered, meanwhile we set to maximal by default
				IGRepl.verbosityLevel = PrintLevel.MAXIMAL;

				String line = "";
				if (iGRepl.fileToLoad != null) {
					line = "load " + String.join(" ", iGRepl.fileToLoad);
					systemRegistry.execute(line);
				}


				while (true) {
					try {
						systemRegistry.cleanUp();
						String rightPrompt = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date(System.currentTimeMillis()));
						line = reader.readLine(prompt, rightPrompt, (MaskingCallback) null, null);
						if (line.startsWith("?")) {
							line = "evaluate " + line.split("\\?")[1];
						}
						systemRegistry.execute(line);
					} catch (UserInterruptException e) {
						// Ignore
					} catch (EndOfFileException e) {
						return;
					} catch (Exception e) {
						systemRegistry.trace(e);
					} finally {
						reader.getHistory().save();
					}
				}
			}
		} catch (Throwable t) {
			t.printStackTrace();
		} finally {
			AnsiConsole.systemUninstall();
		}
	}
}