package fr.boreal.api.integraal_repl;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.jline.reader.LineReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.Parameter;

import fr.boreal.api.high_level_api.EndUserAPI;
import fr.boreal.api.high_level_api.HybridRuleBase;
import fr.boreal.api.integraal_repl.ComplexEnvironment.WriteMode;
import fr.boreal.api.integraal_repl.IGRepl.PrintLevel;
import fr.boreal.component_builder.externalHaltingConditions.ExternalHaltingCondition;
import fr.boreal.component_builder.externalHaltingConditions.ExternalHaltingConditionFactory;
import fr.boreal.configuration.keywords.InteGraalKeywords;
import fr.boreal.configuration.keywords.InteGraalKeywords.Algorithms.Parameters.Compilation;
import fr.boreal.io.api.ParseException;
import fr.boreal.io.dlgp.DlgpUtil;
import fr.boreal.io.dlgp.DlgpWriter;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Help;
import picocli.CommandLine.ITypeConverter;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.ParentCommand;
import picocli.CommandLine.ScopeType;

/**
 * An object that gathers REPL commands relating to the {@link EndUserAPI}.
 *
 * @author Pierre Bisquert
 */
public class IGCommands {

	private static final Logger LOG = LoggerFactory.getLogger(IGCommands.class);

	/**
	 * Top-level command.
	 */
	@Command(name = "", usageHelpAutoWidth = true, footer = { "", "Press Ctrl-D to exit." }, subcommands = {
			SaturateCommand.class, RewriteCommand.class, EvaluateCommand.class, UnfoldCommand.class,
			DecomposeCommand.class, ExtractCommand.class, AnalyzeCommand.class, HybridizeCommand.class,
			IsHybridCommand.class })
	static class InteGraalCommands implements Runnable {
		PrintWriter out;
		static ComplexEnvironment env;

		InteGraalCommands(ComplexEnvironment env) {
			InteGraalCommands.env = env;
		}

		public void setLineReader(LineReader reader) {
			out = reader.getTerminal().writer();
		}

		/**
		 * Prints the substitutions from the iterator.
		 *
		 * @param subIt  The iterator of substitutions containing the query results.
		 * @param vars	 The variables to print; this provides the order of the variables
		 * @param writer The DlgpWriter object used to write the results.break;
		 * @param max    The maximum number of substitutions to print.
		 */
		// TODO: This method should return a string instead
		public static void printSubstitutions(Iterator<Substitution> subIt, Collection<Variable> varsOrder, DlgpWriter writer, Integer max) {
			boolean stop = false;
			if (max != null && max == 0)
				stop = true;
			
			// First, let's see if we have no answer (in that case, the query is false)
			if (!subIt.hasNext()) {
				IGRepl.writeIfVerbose("False\n", PrintLevel.MINIMAL);
			} else { // Here we have at least one answer
				// Let's check if the first answer is "empty" (in that case, there is only constant and
				// no substitution: the query is true)
				// Note that if the user ask for 0 answer, this will behave as if the substitution is empty
				Substitution sub = subIt.next();
				if (sub.isEmpty() || stop) {
					IGRepl.writeIfVerbose("True\n", PrintLevel.MINIMAL);
					// Since it is true, there is no need to continue
//					return;
				} else {
					// We have some substitutions, so some variables, hence let's print them
					// TODO: here print the header of the "table" with only the variables
//					String varStr = varsOrder.stream().map(Variable::toString).collect(Collectors.joining(" | "));
//					String varSep = "-".repeat(varStr.length());
//					IGRepl.writeIfVerbose("|" + varStr + "|\n", PrintLevel.MINIMAL);
//					IGRepl.writeIfVerbose("|" + varSep + "|\n", PrintLevel.MINIMAL);
											
					// Then print the first substitution that has already been extracted
					Map<Variable, Term> subMap = sub.toMap();
					StringJoiner str = new StringJoiner(", ");
					for (Variable var : varsOrder) {
						str.add(var + " : " + subMap.get(var).toString());
					}
					IGRepl.writeIfVerbose(str.toString() + "\n", PrintLevel.MINIMAL);
				
					// Then, let's continue with the remainder					
					while (subIt.hasNext() && !stop) {
						subMap = subIt.next().toMap();
						str = new StringJoiner(", ");
						for (Variable var : varsOrder) {
							str.add(var + " : " + subMap.get(var).toString());
						}
						IGRepl.writeIfVerbose(str.toString() + "\n", PrintLevel.MINIMAL);
						
						if (max != null && max-- == 0) {
								stop = true;
						}
					}
				} 
			}
		}

		public static Collection<Atom> yieldFacts(String[] factsStr) throws ParseException {
			// TODO: we could have specific treatment here (like in yieldQueries)
			// for now, it is (almost) just a wrapper for parseFact
			StringBuilder factsConstruct = new StringBuilder();
			for (String factStr : factsStr) {
				factsConstruct.append(factStr);
				if (!factStr.endsWith("."))
					factsConstruct.append(".");
			}
			return DlgpUtil.parseFacts(factsConstruct.toString());
		}

		public static Collection<FOQuery<?>> yieldQueries(String[] queriesStr) throws ParseException {
			StringBuilder queriesConstruct = new StringBuilder();
			for (String queryStr : queriesStr) {
				for (String q : queryStr.split("\\.")) {
					if (q.contains(":-")) {
						queriesConstruct.append(q).append(".");
					} else {
						queriesConstruct.append("?(").append(String.join(",", InteGraalCommands.findAnswerVariables(q)))
								.append("):-").append(q).append(".");
					}
				}
			}
			// TODO : change method return signature to Query if appropriate, and modify
			// below accordingly.
			List<FOQuery<?>> filteredQueries = DlgpUtil.parseQueries(queriesConstruct.toString()).stream()
					.filter(query -> query instanceof FOQuery<?>).map(query -> (FOQuery<?>) query)
					.collect(Collectors.toList());

			return filteredQueries;
		}

		public static HashSet<String> findAnswerVariables(String query) {
			Pattern upperCase = Pattern.compile("\\((.*?)\\)");
			Matcher matcher = upperCase.matcher(query);
			HashSet<String> results = new HashSet<>();
			while (matcher.find()) {
				// Suboptimal I guess...
				for (String term : matcher.group().replaceAll("[()]", "").split(",")) {
					if (Character.isUpperCase(term.charAt(0)))
						results.add(term);
				}
			}
			return results;
		}

		public static String translateAnswerVariables(String query) {
			Pattern upperCase = Pattern.compile("\\((.*?)\\)");
			Matcher matcher = upperCase.matcher(query);
			HashSet<String> results = new HashSet<>();
			while (matcher.find()) {
				// Suboptimal I guess...
				for (String term : matcher.group().replaceAll("[()]", "").split(",")) {
					if (Character.isUpperCase(term.charAt(0)))
						results.add(term);
				}
			}
			return "?(" + String.join(",", results) + "):-" + query;
		}

		public static FactBase concatInputFacts(String[] factBases, String[] factStr) {
			int factBasesSize = factBases == null ? 0 : factBases.length;
			int factStrSize = factStr == null ? 0 : factStr.length;
			if (factBasesSize == 1 && factStrSize == 0 && Checks.factbaseExists(factBases[0])) {
				// case where we just one to concat the input of a single factbase already
				// existing
				// This case is needed to shortcut the checks and reading of the facts in the
				// case of a mapping
				// Most of the standard usages will be handled by this case
				return InteGraalCommands.env.getFactBaseByName(factBases[0]);
			}

			FactBase factToUse = new SimpleInMemoryGraphStore();
			if (factStr != null) {
				Collection<Atom> yieldedFacts;
				try {
					yieldedFacts = InteGraalCommands.yieldFacts(factStr);
					factToUse.addAll(yieldedFacts);
				} catch (ParseException e) {
					IGRepl.writeIfVerbose("\nError on the provided facts \"" + String.join(" ", factStr) + "\":\n",
							PrintLevel.WARNING);
					IGRepl.writeIfVerbose(e.getClass().getSimpleName() + " " + e.getMessage() + "\n",
							PrintLevel.MAXIMAL);
					IGRepl.writeIfVerbose("Skipping the provided facts...\n", PrintLevel.WARNING);
				}
			}
			if (factBases != null) {
				ArrayList<String> consideredFactbases = new ArrayList<>();
				for (String factbase : factBases) {
					if (Checks.factbaseExists(factbase)) {
						consideredFactbases.add(factbase);
					} else
						IGRepl.writeIfVerbose("Warning: fact base \"" + factbase + "\" does not exist. Skipping...\n",
								PrintLevel.WARNING);
				}
				factToUse.addAll(InteGraalCommands.env.getUnionOfFactBasesByNames(consideredFactbases).getAtoms()
						.collect(Collectors.toList()));
			}
			return factToUse;
		}

		public static RuleBase concatInputRules(String[] ruleBases) { // , String[] ruleStr) {
			RuleBase ruleToUse = new RuleBaseImpl();
			if (ruleBases != null) {
				ArrayList<String> consideredRulebases = new ArrayList<>();
				for (String rulebase : ruleBases) {
					if (Checks.rulebaseExists(rulebase)) {
						consideredRulebases.add(rulebase);
					} else
						IGRepl.writeIfVerbose("Warning: rule base \"" + rulebase + "\" does not exist. Skipping...\n",
								PrintLevel.WARNING);
				}
				for (FORule rule : InteGraalCommands.env.getUnionOfRuleBasesByNames(consideredRulebases).getRules())
					ruleToUse.add(rule);
			}
			return ruleToUse;
		}

		public static Collection<Query> concatInputQueries(String[] queryBases, String[] queriesStr) {
			Collection<Query> queriesToUse = new HashSet<>();
			if (queriesStr != null) {
				Collection<FOQuery<?>> yieldedQueries;
				try {
					yieldedQueries = InteGraalCommands.yieldQueries(queriesStr);
					queriesToUse.addAll(yieldedQueries);
				} catch (ParseException e) {
					IGRepl.writeIfVerbose("\nError on the provided queries \"" + String.join(" ", queriesStr) + "\":\n",
							PrintLevel.WARNING);
					IGRepl.writeIfVerbose(e.getClass().getSimpleName() + " " + e.getMessage() + "\n",
							PrintLevel.MAXIMAL);
					IGRepl.writeIfVerbose("Skipping the provided queries...\n", PrintLevel.WARNING);
				}
			}
			if (queryBases != null) {
				ArrayList<String> consideredQuerybases = new ArrayList<>();
				for (String querybase : queryBases) {
					if (Checks.querybaseExists(querybase)) {
						consideredQuerybases.add(querybase);
					} else
						IGRepl.writeIfVerbose("Warning: query base \"" + querybase + "\" does not exist. Skipping...\n",
								PrintLevel.WARNING);
				}
				queriesToUse.addAll(InteGraalCommands.env.getUnionOfQueryBasesByNames(consideredQuerybases));
			}
			return queriesToUse;
		}

		public void run() {
			out.println(new CommandLine(this).getUsageMessage());
		}
	}

	static class FactBaseCompletion implements Iterable<String> {
		@Override
		public Iterator<String> iterator() {
			return InteGraalCommands.env.listFactBases().iterator();
		}
	}

	static class RuleBaseCompletion implements Iterable<String> {
		@Override
		public Iterator<String> iterator() {
			return InteGraalCommands.env.listRuleBases().iterator();
		}
	}

	static class QueryBaseCompletion implements Iterable<String> {
		@Override
		public Iterator<String> iterator() {
			return InteGraalCommands.env.listQueryBases().iterator();
		}
	}

	static class FactBaseConverter implements ITypeConverter<FactBase> {
		@Override
		public FactBase convert(String value) throws Exception {
			FactBase fb = InteGraalCommands.env.getFactBaseByName(value);
			if (fb == null) {
				throw new IllegalArgumentException("Factbase " + value + " does not exist !");
			}
			return fb;
		}
	}

	static class RuleBaseConverter implements ITypeConverter<RuleBase> {
		@Override
		public RuleBase convert(String value) throws Exception {
			return InteGraalCommands.env.getRuleBaseByName(value);
		}
	}

	static class OutFactBaseConverter implements ITypeConverter<FactBase> {
		@Override
		public FactBase convert(String value) throws Exception {
			if (!Checks.factbaseExists(value)) {
				IGRepl.writeIfVerbose("Info: fact base \"" + value + "\" does not exist. Creating...\n",
						PrintLevel.MINIMAL);
				return InteGraalCommands.env.createFactBase(value, WriteMode.PROTECTED);
			} else {
				return InteGraalCommands.env.getFactBaseByName(value);
			}
		}
	}

	/**
	 * Command class for the "saturate" command. TODO handling the case where
	 * baseName does not correspond to anything
	 */
	@Command(mixinStandardHelpOptions = true, usageHelpAutoWidth = true, name = "saturate", description = "Saturates the current facts. Add saturated facts to the current fact base.")
	static class SaturateCommand implements Runnable {

		@Parameters(paramLabel = "FACT", arity = "0..*", description = "Extra fact to saturate in addition to the factbases")
		private String[] factsStr = null;
		@Option(names = { "-f",
				"--fact" }, arity = "1..*", description = "The input factbases", completionCandidates = FactBaseCompletion.class)
		private String[] factBases = null;
		@Option(names = { "-r", "--rule" }, arity = "1..*", description = "The input rulebases")
		private String[] ruleBases = null;
		@Option(names = { "-o",
				"--out" }, arity = "1", description = "The fact base that will receive the saturated facts (an existing base will be overwritten).")
		private String outFactBase = null;
		@Option(names = { "-R",
				"--rank" }, description = "Maximum number of steps for the saturation algorithm. If the option is unspecified or -1 is given, the algorithm will continue until there is nothing more to produce.")
		private Integer rank = null;
		@Option(names = { "-T",
				"--type" }, defaultValue = "SEMI_OBLIVIOUS", showDefaultValue = Help.Visibility.ALWAYS, description = "Condition for trigger applicability (Possible options are ${COMPLETION-CANDIDATES})")
		private InteGraalKeywords.Algorithms.Parameters.Chase.Checker chaseType;
		@Option(names = { "-t", "--timeout" }, description = "Timeout in seconds for the saturation process")
		private Integer timeout = null;
		@Option(names = { "-S",
				"--stratified" }, description = "If this flag is set and multiple rulebases are given, each will be used independently, in the given order."
						+ "If this flag is not specified, the default behaviour is to use all the given rules together.")
		private boolean stratified;
		@Option(names = { "-m",
				"--mode" }, defaultValue = "APPEND", showDefaultValue = Help.Visibility.ALWAYS, description = "The writing mode to use if the base receiving the result is not empty (${COMPLETION-CANDIDATES}).")
		private WriteMode mode;

		@Override
		public void run() {

			if (this.factBases == null)
				factBases = new String[] { InteGraalCommands.env.getCurrentFactBase() };
			if (this.ruleBases == null)
				ruleBases = new String[] { InteGraalCommands.env.getCurrentRuleBase() };

			String targetFactBase = InteGraalCommands.env.getCurrentFactBase();
			if (this.outFactBase != null)
				targetFactBase = this.outFactBase;

			if (!InteGraalCommands.env.containsFactBase(targetFactBase)) {
				IGRepl.writeIfVerbose("Warning: fact base \"" + targetFactBase + "\" does not exist. Creating...\n",
						PrintLevel.WARNING);
			}

			IGRepl.writeIfVerbose("Writing saturated facts to \"" + targetFactBase + "\" fact base...\n",
					PrintLevel.WARNING);

			boolean shouldSaturate = true;
			if (this.outFactBase != null) {
				if (!Arrays.asList(this.factBases).contains(targetFactBase) && Checks.factbaseExists(targetFactBase)
						&& !Checks.factbaseIsEmpty(targetFactBase)) {
					IGRepl.writeIfVerbose("Warning: target fact base is not empty. ", PrintLevel.WARNING);
					shouldSaturate = Checks.checkWriteModeAndWarn(this.mode);
				}
			}

			FactBase factsToUse;
			if (shouldSaturate) {
				factsToUse = SaturateCommand.saturateFromInput(this.factBases, this.factsStr, this.ruleBases, this.rank,
						this.chaseType, this.timeout, this.stratified);
				InteGraalCommands.env.putFactBase(targetFactBase, factsToUse, this.mode);
				IGRepl.writeIfVerbose("Done saturating!\n\n", PrintLevel.MAXIMAL);
			}
		}

		public static FactBase saturateFromInput(String[] factBases, String[] factsStr, String[] ruleBases,
				Integer rank, InteGraalKeywords.Algorithms.Parameters.Chase.Checker type, Integer timeout,
				boolean stratified) {
			FactBase factsToUse = InteGraalCommands.concatInputFacts(factBases, factsStr);
			RuleBase rulesToUse = InteGraalCommands.concatInputRules(ruleBases);
			
			if (rulesToUse.getRules().isEmpty()) {
				IGRepl.writeIfVerbose("Warning: there is no rule to saturate with", PrintLevel.MAXIMAL);
				if (factsToUse.size() == 0) {
					IGRepl.writeIfVerbose(" (and no fact either)", PrintLevel.MAXIMAL);
				}
				IGRepl.writeIfVerbose("! Stopping...\n\n", PrintLevel.MAXIMAL);
				return factsToUse;
			} else if (factsToUse.size() == 0) {
				IGRepl.writeIfVerbose(
						"Warning: there is no local fact to saturate! (Trying to saturate anyway, in case there are empty-body rules or some facts are in some external source.)\n",
						PrintLevel.WARNING);
			}
			IGRepl.writeIfVerbose("\n", PrintLevel.MINIMAL);

			String factBaseName = String.join(" U ", factBases);
			String ruleBaseName;
			if (!stratified)
				ruleBaseName = String.join(" U ", ruleBases);
			else {
				ruleBaseName = String.join(" > ", ruleBases);
				IGRepl.writeIfVerbose(
						"Warning: for now, stratification is emulated from within the interactive tool; eventually, InteGraal should do it itself.\n\n",
						PrintLevel.WARNING);
			}

			IGRepl.writeIfVerbose("Saturating facts from \"" + factBaseName + "\" ", PrintLevel.MAXIMAL);
			if (factsStr != null)
				IGRepl.writeIfVerbose("(and the provided facts) ", PrintLevel.MAXIMAL);
			IGRepl.writeIfVerbose(
					"with" + (stratified ? " stratified " : " ") + "rules from \"" + ruleBaseName + "\"...\n",
					PrintLevel.MAXIMAL);

			StringBuilder timeoutMode = new StringBuilder();
			if (timeout == null && rank == null) {
				if (EndUserAPI.isFesOld(rulesToUse)) {
					timeoutMode.append(
							"\n[Secure mode: disabled] The rule set is recognized as terminating in forward chaining (FES). No max rank set.\n\n");
				} else {
					rank = 10;
					timeoutMode.append(
							"\n[Secure mode: enabled] The rule set is not recognized as terminating in forward chaining. Max rank set by default to ")
							.append(rank).append(" steps.\n")
							.append("(If reached, we cannot be sure saturation is actually finished.)\n\n");
				}
			}
			IGRepl.writeIfVerbose(timeoutMode.toString(), PrintLevel.MAXIMAL);

			ArrayList<ExternalHaltingCondition> haltingConds = new ArrayList<ExternalHaltingCondition>();
			if (timeout != null) haltingConds.add(ExternalHaltingConditionFactory.getTimeout("PT" + timeout + "S"));
			if (rank != null) haltingConds.add(ExternalHaltingConditionFactory.getRank(rank));

			if (!stratified) {
				EndUserAPI.saturateOld(factsToUse, rulesToUse, type, haltingConds.toArray(ExternalHaltingCondition[]::new));
			} else {
				for (String base : ruleBases) {
					RuleBase currentStrata = InteGraalCommands.env.getRuleBaseByName(base);
					EndUserAPI.saturateOld(factsToUse, currentStrata, type, haltingConds.toArray(ExternalHaltingCondition[]::new));
				}
			}
			return factsToUse;
		}
	}

	/**
	 * Command class for the "rewrite" command.
	 */
	@Command(mixinStandardHelpOptions = true, usageHelpAutoWidth = true, name = "rewrite", description = "Rewrites the (current or provided) queries. Add the rewritten queries to the current query base.")
	static class RewriteCommand implements Runnable {

		@Parameters(paramLabel = "QUERY", arity = "0..*", description = "Extra query to saturate in addition to the qquerybases")
		private String[] queriesStr = null;
		@Option(names = { "-r",
				"--rule" }, arity = "1..*", description = "The input rulebases", completionCandidates = RuleBaseCompletion.class)
		private String[] ruleBases = null;
		@Option(names = { "-q",
				"--query" }, arity = "1..*", description = "The input querybases", completionCandidates = QueryBaseCompletion.class)
		private String[] queryBases = null;
		@Option(names = { "-o",
				"--out" }, arity = "1", description = "The query base that will receive the rewritten queries (an existing base will be overwritten).", completionCandidates = QueryBaseCompletion.class)
		private String outQueryBase = null;
		@Option(names = { "-c",
				"--compile" }, defaultValue = "NO_COMPILATION", description = "Type of compilation (${COMPLETION-CANDIDATES}). Default: ${DEFAULT-VALUE}")
		private InteGraalKeywords.Algorithms.Parameters.Compilation compilationType;
		@Option(names = { "-t", "--timeout" }, description = "Timeout in seconds for the rewriting process (unsupported for now)")
		private Integer timeout = null;
		@Option(names = { "-R", "--rank" }, description = "Maximum number of steps for the rewriting algorithm (unsupported for now)")
		private Integer rank = null;
		@Option(names = { "-m",
				"--mode" }, description = "The writing mode to use if the base receiving the result is not empty (${COMPLETION-CANDIDATES}).")
		private WriteMode mode = WriteMode.APPEND;

		@Override
		public void run() {
			if (this.timeout != null) {
				IGRepl.writeIfVerbose(
						"Warning: timeout option not implemented yet. The option will not be applied (sorry).\n",
						PrintLevel.WARNING);
			}
			if (this.rank != null) {
				IGRepl.writeIfVerbose(
						"Warning: rank option not implemented yet. The option will not be applied (sorry).\n",
						PrintLevel.WARNING);
			}

			if (this.ruleBases == null)
				this.ruleBases = new String[] { InteGraalCommands.env.getCurrentRuleBase() };
			if (this.queryBases == null)
				this.queryBases = new String[] { InteGraalCommands.env.getCurrentQueryBase() };

			String targetQueryBase = InteGraalCommands.env.getCurrentQueryBase();
			if (this.outQueryBase != null)
				targetQueryBase = this.outQueryBase;

			if (!InteGraalCommands.env.containsQueryBase(targetQueryBase)) {
				IGRepl.writeIfVerbose("Warning: query base \"" + targetQueryBase + "\" does not exist. Creating...\n",
						PrintLevel.WARNING);
			}

			IGRepl.writeIfVerbose("Writing rewritten queries to \"" + targetQueryBase + "\" query base...\n",
					PrintLevel.MAXIMAL);

			boolean shouldRewrite = true;
			if (this.outQueryBase != null) {
				if (!Arrays.asList(this.queryBases).contains(targetQueryBase) && Checks.querybaseExists(targetQueryBase)
						&& !Checks.querybaseIsEmpty(targetQueryBase)) {
					IGRepl.writeIfVerbose("Target query base is not empty. ", PrintLevel.WARNING);
					shouldRewrite = Checks.checkWriteModeAndWarn(this.mode);
				}
			}

			if (shouldRewrite) {
				Collection<Query> rewritings = RewriteCommand.rewriteFromInput(queryBases, queriesStr, ruleBases,
						compilationType, rank, timeout);
				InteGraalCommands.env.putQueryBase(targetQueryBase, rewritings, this.mode);
				IGRepl.writeIfVerbose("Done rewriting!\n\n", PrintLevel.MINIMAL);
			}

		}

	
		// TODO: not quite sure this is the prettiest to allow other commands to call the evaluation
		public static Collection<Query> rewriteFromInput(String[] queryBases, String[] queriesStr, String[] ruleBases, Compilation compile, Integer rank, Integer timeout) {
			RuleBase rulesToUse = InteGraalCommands.concatInputRules(ruleBases);
			Collection<Query> queriesToUse = InteGraalCommands.concatInputQueries(queryBases, queriesStr);
			
			if (queriesToUse.isEmpty()) {
				IGRepl.writeIfVerbose("Warning: there is no query to rewrite", PrintLevel.MAXIMAL);
				if (rulesToUse.getRules().isEmpty()) {
					IGRepl.writeIfVerbose(" (and no rule either)", PrintLevel.MAXIMAL);	
				}
				IGRepl.writeIfVerbose("! Stopping...\n\n", PrintLevel.MAXIMAL);
				return queriesToUse;
			} else {
				if (rulesToUse.getRules().isEmpty()) {
					IGRepl.writeIfVerbose("Warning: there is no rule to rewrite with! (Trying to rewrite anyway, to compute the cover and core of the queries.)\n", PrintLevel.WARNING);
				}
				IGRepl.writeIfVerbose("\n", PrintLevel.MINIMAL);
			
				String ruleBaseName = String.join(" U ", ruleBases);
				String queryBaseName = String.join(" U ", queryBases);
	
				// TODO: add the timeout here (when implemented); see saturateFromInput
	
				IGRepl.writeIfVerbose("Rewriting queries from \"" + queryBaseName + "\" ", PrintLevel.MAXIMAL);
				if (queriesStr != null) IGRepl.writeIfVerbose("(and the provided queries) ", PrintLevel.MAXIMAL);
				IGRepl.writeIfVerbose("with rules from \"" + ruleBaseName + "\"...\n", PrintLevel.MAXIMAL);
	
				ArrayList<ExternalHaltingCondition> haltingConds = new ArrayList<ExternalHaltingCondition>();
				if (timeout != null) haltingConds.add(ExternalHaltingConditionFactory.getTimeout("PT" + timeout + "S"));
				if (rank != null) haltingConds.add(ExternalHaltingConditionFactory.getRank(rank));
				
				return EndUserAPI.rewriteOld(rulesToUse, queriesToUse, compile, haltingConds.toArray(ExternalHaltingCondition[]::new));
			}
		}
	}

	/**
	 * Command class for the "evaluate" command.
	 */
	@Command(mixinStandardHelpOptions = true, usageHelpAutoWidth = true, name = "evaluate", description = "Evaluates the (current or provided) queries on the current fact base. Prints the substitutions.", subcommands = {
			EvaluateWithSaturationCommand.class, EvaluateWithRewritingCommand.class })
	static class EvaluateCommand implements Runnable {

		@Parameters(paramLabel = "QUERY", arity = "0..*", description = "desc", scope = ScopeType.INHERIT)
		private String[] queriesStr = null;
		@Option(names = { "-f",
				"--fact" }, arity = "1..*", description = "desc", scope = ScopeType.INHERIT, completionCandidates = FactBaseCompletion.class)
		private String[] factBases = null;
		@Option(names = { "-q",
				"--query" }, arity = "1..*", description = "desc", scope = ScopeType.INHERIT, completionCandidates = QueryBaseCompletion.class)
		private String[] queryBases = null;
		@Option(names = { "-u", "--ucq" }, description = "desc", scope = ScopeType.INHERIT)
		private boolean ucq;
		@Option(names = { "-n", "--nbResults" }, description = "desc", scope = ScopeType.INHERIT)
		private Integer max;

		@Override
		public void run() {
			if (this.factBases == null)
				this.factBases = new String[] { InteGraalCommands.env.getCurrentFactBase() };
			if (this.queryBases == null)
				this.queryBases = new String[] { InteGraalCommands.env.getCurrentQueryBase() };

			String factBaseName = String.join(" U ", factBases);
			String queryBaseName = String.join(" U ", queryBases);

			IGRepl.writeIfVerbose("Evaluating queries from \"" + queryBaseName + "\" ", PrintLevel.MAXIMAL);

			if (this.queriesStr != null) IGRepl.writeIfVerbose("(and the provided queries) ", PrintLevel.MAXIMAL);
			IGRepl.writeIfVerbose("on facts from \"" + factBaseName + "\"...\n", PrintLevel.MAXIMAL);


			FactBase factsToUse = InteGraalCommands.concatInputFacts(this.factBases, null);
			Collection<Query> queriesToUse = InteGraalCommands.concatInputQueries(this.queryBases, this.queriesStr);

			EvaluateCommand.queryFromInput(factsToUse, queriesToUse, ucq, max);
		}

		// TODO: not quite sure it is the prettiest way of allowing other commands to
		// call the evaluation...
		public static void queryFromInput(FactBase factsToUse, Collection<Query> queriesToUse, boolean ucq,
				Integer max) {
			if (queriesToUse.isEmpty()) {
				IGRepl.writeIfVerbose("Warning: there is no query to evaluate", PrintLevel.MAXIMAL);
				if (factsToUse.size() == 0) {
					IGRepl.writeIfVerbose(" (and no fact either)", PrintLevel.MAXIMAL);
				}
				IGRepl.writeIfVerbose("! Stopping...\n\n", PrintLevel.MAXIMAL);
			} else {
				if (factsToUse.size() == 0) {
					IGRepl.writeIfVerbose(
							"Warning: there is no local fact on which to evaluate! (Trying to evaluate anyway, just in case some facts are in some external source.)\n",
							PrintLevel.WARNING);
				}
				IGRepl.writeIfVerbose("\n", PrintLevel.MINIMAL);
				if (!ucq) {					
					int index = 1;
					for (Query query : queriesToUse) {
						IGRepl.writeIfVerbose("Query " + index++ + ": " + query + "\n", PrintLevel.MINIMAL);
						InteGraalCommands.printSubstitutions(EndUserAPI.evaluateOld(factsToUse, query), query.getAnswerVariables(), IGRepl.dlgpWriter,
								max);
						IGRepl.writeIfVerbose("\n", PrintLevel.MINIMAL);
					}
				} 
				else {
					// Here, we basically reconstruct the string that represents the UCQ to have
					// something pretty
					// (something like ?(X,...,Z) :- pred1(X,...) | pred2(Y,...) )

					// First, let's convert the Queries into FOQueries
					// (and if a query is not instance of FOQueries, it is skipped) 
					List<FOQuery<?>> queriesToUseTyped = new ArrayList<FOQuery<?>>();
					for (Query q : queriesToUse) {
						if (q instanceof FOQuery<?> qq) {
							queriesToUseTyped.add(qq);
						} else {
							LOG.error("Wrong query type on " + q + " when reconstructing the UCQ from a set. Skipped");
						}
					}	

					// Then, let's obtain the answer variables (and their order) that are needed 
					// both for printing and evaluating
					Collection<Variable> ansVarsOrder = new LinkedHashSet<Variable>();
					
					StringJoiner formulasJoiner = new StringJoiner(" | ");
					for (FOQuery q : queriesToUseTyped) {
						ansVarsOrder.addAll(q.getAnswerVariables());
						formulasJoiner.add(q.getFormula().toString());
					}
					
					String ucqStr = "UCQ: " + "?(" + 
					ansVarsOrder.stream().map(Variable::toString).collect(Collectors.joining(",")) +
					") :- " + formulasJoiner.toString() + ".";
					IGRepl.writeIfVerbose(ucqStr + "\n", PrintLevel.MINIMAL);
					
					InteGraalCommands.printSubstitutions(EndUserAPI.evaluateOld(factsToUse, queriesToUse),
							ansVarsOrder, IGRepl.dlgpWriter, max);
					IGRepl.writeIfVerbose("\n", PrintLevel.MINIMAL);
				}
			}
		}
	}

	@Command(mixinStandardHelpOptions = true, usageHelpAutoWidth = true, name = "withSaturation", aliases = {
			"sat" }, description = ".")
	static class EvaluateWithSaturationCommand implements Runnable {

		@ParentCommand
		EvaluateCommand parent;

		@Option(names = { "-r", "--rule" }, arity = "1..*", description = "desc")
		private String[] ruleBases = null;
		@Option(names = { "-R", "--rank" }, description = "Rank")
		private Integer rank = null;
		@Option(names = { "-T", "--type" }, description = "Type of saturation  (${COMPLETION-CANDIDATES})")
		private InteGraalKeywords.Algorithms.Parameters.Chase.Checker chaseType = InteGraalKeywords.Algorithms.Parameters.Chase.Checker.SEMI_OBLIVIOUS;

		@Option(names = { "-t", "--timeout" }, description = "Timeout")
		private Integer timeout = null;
		@Option(names = { "-S", "--stratified" }, description = "desc")
		private boolean stratified;

		@Override
		public void run() {
			if (parent.factBases == null)
				parent.factBases = new String[] { InteGraalCommands.env.getCurrentFactBase() };
			if (this.ruleBases == null)
				this.ruleBases = new String[] { InteGraalCommands.env.getCurrentRuleBase() };
			if (parent.queryBases == null)
				parent.queryBases = new String[] { InteGraalCommands.env.getCurrentQueryBase() };

			FactBase factsToUse = SaturateCommand.saturateFromInput(parent.factBases, null, this.ruleBases, this.rank,
					this.chaseType, this.timeout, this.stratified);

			String queryBaseName = String.join(" U ", parent.queryBases);
			IGRepl.writeIfVerbose("Evaluating queries from \"" + queryBaseName + "\" ", PrintLevel.MAXIMAL);
			if (parent.queriesStr != null) {
				IGRepl.writeIfVerbose("(and the provided queries) ", PrintLevel.MAXIMAL);
			}
			IGRepl.writeIfVerbose("on the saturated facts...\n", PrintLevel.MAXIMAL);

			Collection<Query> queriesToUse = InteGraalCommands.concatInputQueries(parent.queryBases, parent.queriesStr);

			EvaluateCommand.queryFromInput(factsToUse, queriesToUse, parent.ucq, parent.max);
		}
	}

	@Command(mixinStandardHelpOptions = true, usageHelpAutoWidth = true, name = "withRewriting", aliases = {
			"rew" }, description = ".")
	static class EvaluateWithRewritingCommand implements Runnable {

		@ParentCommand
		EvaluateCommand parent;

		@Option(names = { "-r", "--rule" }, arity = "1..*", description = "desc")
		private String[] ruleBases = null;
		@Option(names = { "-c", "--compile" }, description = "Type of compilation (${COMPLETION-CANDIDATES})")
		private InteGraalKeywords.Algorithms.Parameters.Compilation compilationType = InteGraalKeywords.Algorithms.Parameters.Compilation.NO_COMPILATION;
		@Option(names = { "-t", "--timeout" }, description = "desc")
		private Integer timeout = null;
		@Option(names = { "-R", "--rank" }, description = "desc")
		private Integer rank = null;

		@Override
		public void run() {
			if (this.timeout != null) {
				IGRepl.writeIfVerbose("Timeout option not implemented yet. The option will not be applied (sorry).",
						PrintLevel.MINIMAL);
			}
			if (this.rank != null) {
				IGRepl.writeIfVerbose("Rank option not implemented yet. The option will not be applied (sorry).",
						PrintLevel.MINIMAL);
			}

			if (parent.factBases == null)
				parent.factBases = new String[] { InteGraalCommands.env.getCurrentFactBase() };
			if (this.ruleBases == null)
				this.ruleBases = new String[] { InteGraalCommands.env.getCurrentRuleBase() };
			if (parent.queryBases == null)
				parent.queryBases = new String[] { InteGraalCommands.env.getCurrentQueryBase() };

			String factBaseName = String.join(" U ", parent.factBases);
			String ruleBaseName = String.join(" U ", ruleBases);
			String queryBaseName = String.join(" U ", parent.queryBases);

			IGRepl.writeIfVerbose("Rewriting queries from \"" + queryBaseName + "\" ", PrintLevel.MAXIMAL);
			if (parent.queriesStr != null) {
				IGRepl.writeIfVerbose("(and the provided queries) ", PrintLevel.MAXIMAL);
			}
			IGRepl.writeIfVerbose("with rules from \"" + ruleBaseName + "\"...\n", PrintLevel.MAXIMAL);

			RuleBase rulesToUse = InteGraalCommands.concatInputRules(ruleBases);
			Collection<Query> queriesToUse = InteGraalCommands.concatInputQueries(parent.queryBases, parent.queriesStr);

			FactBase factsToUse = InteGraalCommands.concatInputFacts(parent.factBases, null);

			if (parent.ucq) {
				IGRepl.writeIfVerbose("\n[Using UCQ to simulate disjunction of rewritten queries.]\n\n",
						PrintLevel.MAXIMAL);
				Collection<Query> rewritings = EndUserAPI.rewriteOld(rulesToUse, queriesToUse, this.compilationType);

				IGRepl.writeIfVerbose("Evaluating rewritten queries ", PrintLevel.MAXIMAL);
				IGRepl.writeIfVerbose("on the facts from \"" + factBaseName + "\"...\n", PrintLevel.MAXIMAL);

				EvaluateCommand.queryFromInput(factsToUse, rewritings, true, parent.max);
			} else {
				int index = 1;
				for (Query q : queriesToUse) {
					IGRepl.writeIfVerbose("Query " + index++ + ": ", PrintLevel.MINIMAL);
					IGRepl.writeIfVerbose(q, PrintLevel.MINIMAL);

					Collection<Query> rewritings = EndUserAPI.rewriteOld(rulesToUse, Set.of(q), this.compilationType);
					EvaluateCommand.queryFromInput(factsToUse, rewritings, false, parent.max);
				}
			}
		}
	}

	/**
	 * Command class for the "unfold" command.
	 */
	@Command(mixinStandardHelpOptions = true, usageHelpAutoWidth = true, name = "unfold", description = "Unfolds the current query using compilation. Prints the unfolded queries.")
	static class UnfoldCommand implements Runnable {

		@Option(names = { "-c", "--compile" }, description = "desc")
		@Parameter(names = "--compile", description = "Type of compilation (${COMPLETION-CANDIDATES})")
		private InteGraalKeywords.Algorithms.Parameters.Compilation compilationType = InteGraalKeywords.Algorithms.Parameters.Compilation.NO_COMPILATION;
		@Option(names = { "-p", "--print" }, description = "desc")
		private boolean print;
		@Option(names = { "-m",
				"--mode" }, description = "The writing mode to use if the base receiving the result is not empty (${COMPLETION-CANDIDATES}).")
		private WriteMode mode = WriteMode.APPEND;

		@Override
		public void run() {

			String ruleBaseName = InteGraalCommands.env.getCurrentRuleBase();
			String queryBaseName = InteGraalCommands.env.getCurrentQueryBase();

			RuleBase rulesToUse = InteGraalCommands.env.getRuleBaseByName(ruleBaseName);
			Collection<Query> queriesToUse = InteGraalCommands.env.getQueryBaseByName(queryBaseName);

			IGRepl.writeIfVerbose("Unfolding queries from \"" + queryBaseName + "\"... ", PrintLevel.MAXIMAL);
			Collection<Query> unfoldings = EndUserAPI.unfoldOld(rulesToUse, queriesToUse, this.compilationType);

			if (print) {
				IGRepl.writeIfVerbose("Unfolded queries: \n\n", PrintLevel.MAXIMAL);
				for (Query q : unfoldings) {
					IGRepl.writeIfVerbose(q, PrintLevel.MINIMAL);
				}
				IGRepl.writeIfVerbose("\n", PrintLevel.MINIMAL);
			} else {
				InteGraalCommands.env.putQueryBase(queryBaseName, unfoldings, this.mode);
				IGRepl.writeIfVerbose("Done unfolding!\n\n", PrintLevel.MINIMAL);
			}
		}
	}

	/**
	 * Command class for the "decompose" command.
	 */
	@Command(mixinStandardHelpOptions = true, usageHelpAutoWidth = true, name = "decompose", description = "Decomposes and prints the current rules to single piece rules.")
	static class DecomposeCommand implements Runnable {

		@Option(names = { "-p", "--print" }, description = "desc")
		private boolean print;
		@Option(names = { "-m",
				"--mode" }, description = "The writing mode to use if the base receiving the result is not empty (${COMPLETION-CANDIDATES}).")
		private WriteMode mode = WriteMode.APPEND;

		@Override
		public void run() {

			String ruleBaseName = InteGraalCommands.env.getCurrentRuleBase();
			RuleBase decomposedRules = EndUserAPI.decomposeOld(InteGraalCommands.env.getRuleBaseByName(ruleBaseName));

			if (print) {
				if (decomposedRules.getRules().isEmpty()) {
					IGRepl.writeIfVerbose("Warning: nothing to decompose!\n", PrintLevel.WARNING);
				} else {
					IGRepl.writeIfVerbose("Decomposed rules:\n", PrintLevel.MAXIMAL);
					for (FORule r : decomposedRules.getRules()) {
						IGRepl.writeIfVerbose(r, PrintLevel.MINIMAL);
					}
					IGRepl.writeIfVerbose("\n", PrintLevel.MINIMAL);
				}
			} else {
				IGRepl.writeIfVerbose("Decomposing rules from \"" + ruleBaseName + "\"... ", PrintLevel.MAXIMAL);
				InteGraalCommands.env.putRuleBase(ruleBaseName, decomposedRules, this.mode);
				IGRepl.writeIfVerbose("Done decomposing!\n\n", PrintLevel.MINIMAL);
			}
		}
	}

	/**
	 * Command class for the "extract" command.
	 */
	@Command(mixinStandardHelpOptions = true, usageHelpAutoWidth = true, name = "extract", description = "Prints compilable or non-compilable rules from the current rule base.")
	static class ExtractCommand implements Runnable {

		@Option(names = { "-c", "--compile" }, description = "Type of compilation (${COMPLETION-CANDIDATES})")
		private InteGraalKeywords.Algorithms.Parameters.Compilation compilationType = InteGraalKeywords.Algorithms.Parameters.Compilation.NO_COMPILATION;

		@Option(names = { "-C", "--compilable" }, description = "desc")
		private boolean compilable = true;

		@Override
		public void run() {
			// TODO: We should have a print option, and write to target base

			String ruleBaseName = InteGraalCommands.env.getCurrentRuleBase();

			RuleBase extractedRules;
			if (this.compilable) {
				extractedRules = EndUserAPI.extractCompilableRulesOld(
						InteGraalCommands.env.getRuleBaseByName(ruleBaseName), this.compilationType);
				IGRepl.writeIfVerbose("Compilable rules:\n", PrintLevel.MINIMAL);
			} else {
				extractedRules = EndUserAPI.extractNonCompilableRulesOld(
						InteGraalCommands.env.getRuleBaseByName(ruleBaseName), this.compilationType);
				IGRepl.writeIfVerbose("Non compilable rules:\n", PrintLevel.MINIMAL);
			}

			if (extractedRules.getRules().isEmpty()) {
				IGRepl.writeIfVerbose("Warning: there is no rules to extract!\n\n", PrintLevel.WARNING);
			} else {
				for (FORule rule : extractedRules.getRules()) {
					// TODO: do a method for print or use StringBuilder
					IGRepl.writeIfVerbose(rule, PrintLevel.MAXIMAL);
				}
				IGRepl.writeIfVerbose("\n\n", PrintLevel.MAXIMAL);
			}
		}
	}

	/**
	 * Command class for the "analyze" command.
	 */
	@Command(mixinStandardHelpOptions = true, usageHelpAutoWidth = true, name = "analyze", description = "Checks that the given rule base satisfies some properties.")
	static class AnalyzeCommand implements Runnable {

		@Option(names = { "-e", "--fes" }, description = "desc")
		private boolean fes;
		@Option(names = { "-u", "--fus" }, description = "desc")
		private boolean fus;
		@Option(names = { "-D", "--decidable" }, description = "desc")
		private boolean decidable;
		@Option(names = { "-y", "--hybridable" }, description = "desc")
		private boolean hybridable;

		@Override
		public void run() {

			String ruleBaseName = InteGraalCommands.env.getCurrentRuleBase();
			RuleBase ruleBase = InteGraalCommands.env.getRuleBaseByName(ruleBaseName);

			if (!(this.fus || this.fes || this.decidable || this.hybridable)) {
				this.fus = true;
				this.fes = true;
				this.decidable = true;
				this.hybridable = true;
			}

			if (this.fus) {
				if (EndUserAPI.isFusOld(ruleBase)) {
					IGRepl.writeIfVerbose("Given the rule base " + ruleBaseName
							+ ", the rewriting process will stop for every query.\n", PrintLevel.MINIMAL);
				} else {
					IGRepl.writeIfVerbose(
							"Given the rule base " + ruleBaseName
									+ ", it is possible that the rewriting process does not stop for a given query.\n",
							PrintLevel.MINIMAL);
				}
			}

			if (this.fes) {
				if (EndUserAPI.isFesOld(ruleBase)) {
					IGRepl.writeIfVerbose("Given the rule base " + ruleBaseName
							+ ", the saturation process will stop for every factbase.\n", PrintLevel.MINIMAL);
				} else {
					IGRepl.writeIfVerbose("Given the rule base " + ruleBaseName
							+ ",it is possible that the saturation process does not stop for a given factbase.\n",
							PrintLevel.MINIMAL);
				}
			}

			if (this.decidable) {
				if (EndUserAPI.isDecidableOld(ruleBase)) {
					IGRepl.writeIfVerbose("The \"" + ruleBaseName
							+ "\" rule base *is* recognized as Query/Answering decidable for any factbase and any query.\n",
							PrintLevel.MINIMAL);
				} else {
					IGRepl.writeIfVerbose("The \"" + ruleBaseName
							+ "\" rule base *is not* recognized as Query/Answering decidable for any factbase and any query.\n",
							PrintLevel.MINIMAL);
				}
			}

			if (this.hybridable) {
				if (EndUserAPI.isHybridableOld(ruleBase)) {
					IGRepl.writeIfVerbose("The \"" + ruleBaseName + "\" rule base *is* recognized as hybridable.\n",
							PrintLevel.MINIMAL);
				} else {
					IGRepl.writeIfVerbose("The \"" + ruleBaseName + "\" rule base *is not* recognized as hybridable.\n",
							PrintLevel.MINIMAL);
				}
			}
			IGRepl.writeIfVerbose("\n", PrintLevel.MINIMAL);
		}
	}

	/**
	 * Command class for the "hybridize" command.
	 */
	@Command(mixinStandardHelpOptions = true, usageHelpAutoWidth = true, name = "hybridize", description = "Computes and prints a HybridRuleBase maximizing Forward Chaining or Backward Chaining.")
	static class HybridizeCommand implements Runnable {

		@Option(names = { "-T", "--type" }, description = "Type of hybridisation technique (${COMPLETION-CANDIDATES})")
		private InteGraalKeywords.Algorithms.Parameters.HybridTypes hybridType = InteGraalKeywords.Algorithms.Parameters.HybridTypes.MAX_FES;

		@Override
		public void run() {

			String ruleBaseName = InteGraalCommands.env.getCurrentRuleBase();

			Optional<HybridRuleBase> result = EndUserAPI
					.hybridizeOld(InteGraalCommands.env.getRuleBaseByName(ruleBaseName), this.hybridType);

			if (result.isPresent()) {
				IGRepl.writeIfVerbose("Hybridisation solution:\n", PrintLevel.MINIMAL);
				IGRepl.writeIfVerbose("# Alpha rules\n", PrintLevel.MINIMAL);
				IGRepl.writeIfVerbose(result.get().getAlphaRules(), PrintLevel.MINIMAL);
				IGRepl.writeIfVerbose("\n", PrintLevel.MINIMAL);
				IGRepl.writeIfVerbose("# Beta rules\n", PrintLevel.MINIMAL);
				IGRepl.writeIfVerbose(result.get().getBetaRules(), PrintLevel.MINIMAL);
				IGRepl.writeIfVerbose("\n", PrintLevel.MINIMAL);
			} else {
				IGRepl.writeIfVerbose(
						"No hybridisation solution was found (or the rule base contains a BTS component).\n",
						PrintLevel.MINIMAL);
			}
		}
	}

	/**
	 * Command class for the "isHybrid" command.
	 */
	@Command(mixinStandardHelpOptions = true, usageHelpAutoWidth = true, name = "isHybrid", description = "Tests whether the two specified rule bases form an hybrid rule base.")
	static class IsHybridCommand implements Runnable {

		@Parameters(paramLabel = "BASENAME1", arity = "1", description = "desc")
		private String ruleBaseName1;
		@Parameters(paramLabel = "BASENAME2", arity = "1", description = "desc")
		private String ruleBaseName2;

		@Override
		public void run() {

			boolean isHybridResult = EndUserAPI.isHybridOld(InteGraalCommands.env.getRuleBaseByName(ruleBaseName1),
					InteGraalCommands.env.getRuleBaseByName(ruleBaseName2));

			if (isHybridResult) {
				IGRepl.writeIfVerbose("\"" + ruleBaseName1 + "\" and \"" + ruleBaseName2
						+ "\" *are indeed* a valid hybrid rule base.\n", PrintLevel.MINIMAL);
			} else {
				IGRepl.writeIfVerbose(
						"\"" + ruleBaseName1 + "\" and \"" + ruleBaseName2 + "\" *are not* a valid hybrid rule base.\n",
						PrintLevel.MINIMAL);
			}
		}
	}

}
