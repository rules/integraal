package fr.boreal.api.integraal_commander;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import com.beust.jcommander.DefaultUsageFormatter;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

import fr.boreal.api.high_level_api.EndUserAPI;
import fr.boreal.api.high_level_api.Environment;
import fr.boreal.api.high_level_api.HybridRuleBase;
import fr.boreal.configuration.keywords.InteGraalKeywords;
import fr.boreal.io.dlgp.DlgpWriter;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.Query;
import fr.lirmm.boreal.util.enumerations.EnumUtils;

/**
 * CLI Client for a user wanting to easily use InteGraal for reasoning.
 */
public class InteGraalCommander {

	private static final String severalCommands = "several-commands";
	private static final String noCommand = "no-command";
	private static final String help = "help";
	private static final HelpCommand helpCommand = new HelpCommand();

	private static final String saturate = "saturate";
	private static final SaturateCommand saturateCommand = new SaturateCommand();
	private static final String rewrite = "rewrite";
	private static final RewriteCommand rewriteCommand = new RewriteCommand();
	private static final String evaluate = "evaluate";
	private static final EvaluateCommand evaluateCommand = new EvaluateCommand();
	private static final String unfold = "unfold";
	private static final UnfoldCommand unfoldCommand = new UnfoldCommand();
	private static final String decompose = "decompose";
	private static final DecomposeCommand decomposeCommand = new DecomposeCommand();
	private static final String extract = "extract";
	private static final ExtractCommand extractCommand = new ExtractCommand();
	private static final String analyze = "analyze";
	private static final AnalyzeCommand analyzeCommand = new AnalyzeCommand();
	private static final String hybridize = "hybridize";
	private static final HybridizeCommand hybridizeCommand = new HybridizeCommand();
	private static final String isHybrid = "isHybrid";
	private static final isHybridCommand isHybridCommand = new isHybridCommand();

	private static final String queryWithSaturation = "queryWithSaturation";
	private static final QueryWithSaturationCommand queryWithSaturationCommand = new QueryWithSaturationCommand();
	private static final String queryWithRewriting = "queryWithRewriting";
	private static final QueryWithRewritingCommand queryWithRewritingCommand = new QueryWithRewritingCommand();

	/**
	 * Entry point of the application.
	 *
	 * @param args Command-line arguments.
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		JCommander jCommander = JCommander.newBuilder().addCommand(helpCommand).addCommand(saturateCommand)
				.addCommand(rewriteCommand).addCommand(evaluateCommand).addCommand(unfoldCommand)
				.addCommand(decomposeCommand).addCommand(extractCommand).addCommand(analyzeCommand)
				.addCommand(hybridizeCommand).addCommand(isHybridCommand).addCommand(queryWithSaturationCommand)
				.addCommand(queryWithRewritingCommand).build();
		jCommander.setUsageFormatter(new SimpleUsageFormatter(jCommander));
		jCommander.setColumnSize(160);

		String parsedCommand = getCommand(jCommander, args);
		Map<String, JCommander> commands = jCommander.getCommands();

		try (DlgpWriter writer = new DlgpWriter()) {
			switch (parsedCommand) {
			case (saturate) -> saturateCommand.execute(commands.get(saturate), writer);
			case (rewrite) -> rewriteCommand.execute(commands.get(rewrite), writer);
			case (evaluate) -> evaluateCommand.execute(commands.get(evaluate), writer);
			case (unfold) -> unfoldCommand.execute(commands.get(unfold), writer);
			case (decompose) -> decomposeCommand.execute(commands.get(decompose), writer);
			case (extract) -> extractCommand.execute(commands.get(extract), writer);
			case (queryWithSaturation) -> queryWithSaturationCommand.execute(commands.get(queryWithSaturation), writer);
			case (queryWithRewriting) -> queryWithRewritingCommand.execute(commands.get(queryWithRewriting), writer);
			case (analyze) -> analyzeCommand.execute(commands.get(analyze));
			case (hybridize) -> hybridizeCommand.execute(commands.get(hybridize), writer);
			case (isHybrid) -> isHybridCommand.execute(commands.get(isHybrid), writer);
			case (help) -> jCommander.usage();
			case (noCommand) -> {
				System.err.println("Expected a command, none was given.\n");
				jCommander.usage();
			}
			case (severalCommands) -> {
				System.err.println("Expected only one command, several were given. \n");
				jCommander.usage();
			}
			default -> {
				System.err.println("Expected a parameter, got : " + parsedCommand + ".\n");
				jCommander.usage();
			}
			}
		}

	}

	/**
	 * Get the command given by the user <br/>
	 * severalCommands if multiple are present noCommand if none is given help if an
	 * error occur during command parsing
	 *
	 * @param jCommander the jcommander instance
	 * @param args       the command line arguments
	 * @return the parsed command by jcommander
	 */
	private static String getCommand(JCommander jCommander, String[] args) {
		HashSet<String> argsSet = new HashSet<>(Arrays.asList(args));
		Set<String> commandKeys = jCommander.getCommands().keySet();
		commandKeys.retainAll(argsSet);

		String parsedCommand;
		try {
			if (commandKeys.size() > 1)
				parsedCommand = severalCommands;
			else {
				jCommander.parse(args);
				parsedCommand = jCommander.getParsedCommand();
				if (parsedCommand == null) {
					parsedCommand = noCommand;
				}
			}
		} catch (ParameterException ex) {
			String[] message = ex.getMessage().split("'");
			if (message.length == 3)
				parsedCommand = message[1];
			else {
				System.err.println(ex.getMessage() + "\n");
				parsedCommand = help;
			}
		}
		return parsedCommand;
	}

	/**
	 * Prints the substitutions from the iterator.
	 *
	 * @param subIt  The iterator of substitutions containing the query results.
	 * @param writer The DlgpWriter object used to write the results.break;
	 * @param max    The maximum number of substitutions to print.
	 * @throws IOException If an error occurs while writing the results to the
	 *                     writer.
	 */
	private static void printSubstitutions(Iterator<Substitution> subIt, DlgpWriter writer, Integer max)
			throws IOException {

		if (subIt.hasNext()) {
			while (subIt.hasNext()) {
				if (max != null && max == 0) {
					return;
				}
				writer.write(subIt.next().toString());
				writer.write("\n");
				writer.flush();
				if (max != null) {
					max--;
				}
			}
		} else {
			writer.write("No answer \n");
			writer.flush();
		}
	}

	/**
	 * Command class for the "saturate" command.
	 */
	@Parameters(commandNames = {
			saturate }, commandDescription = "Perform saturation on the knowledge base. Return the saturated fact base.")
	private static class SaturateCommand {

		@Parameter(names = "--help", help = true)
		private boolean help;

		@Parameter(names = "--rank")
		private Integer rank = null;

		@Parameter(names = "--type")
		private String type = null;

		@Parameter(names = "--timeout")
		private Integer timeout = null;

		private InteGraalKeywords.Algorithms.Parameters.Chase.Checker chaseType = InteGraalKeywords.Algorithms.Parameters.Chase.Checker.SEMI_OBLIVIOUS;

		public void execute(JCommander jCommander, DlgpWriter writer) throws IOException {
			if (this.help) {
				jCommander.usage();
			} else {
				Environment kb = new Environment(System.in);

				if (type != null) {
					chaseType = EnumUtils.findEnumFromString(type,
							InteGraalKeywords.Algorithms.Parameters.Chase.Checker.class);
				}
				EndUserAPI.saturateOld(kb.getFactBase(), kb.getRuleBase(), chaseType);
				writer.write(kb.getFactBase());
			}
		}
	}

	/**
	 * Command class for the "rewrite" command.
	 */
	@Parameters(commandNames = {
			rewrite }, commandDescription = "Perform rewriting of queries. Return the rewritten queries.")
	private static class RewriteCommand {

		@Parameter(names = "--help", help = true)
		private boolean help;

		@Parameter(names = "--query")
		private String query;

		@Parameter(names = "--compile")
		private String compile = null;

		private InteGraalKeywords.Algorithms.Parameters.Compilation compilationType = InteGraalKeywords.Algorithms.Parameters.Compilation.NO_COMPILATION;

		@Parameter(names = "--rank")
		private Integer rank = null;

		@Parameter(names = "--timeout")
		private Integer timeout = null;

		public void execute(JCommander jCommander, DlgpWriter writer) throws IOException {
			if (this.help) {
				jCommander.usage();
			} else {
				Environment kb = new Environment(System.in);
				if (this.timeout != null) {
					System.err.println("Timeout option not implemented. The option is not applied.");
				}
				if (this.rank != null) {
					System.err.println("Rank option not implemented. The option is not applied.");
				}
				if (this.query != null) {
					kb.setQueries(this.query);
				}
				if (this.compile != null) {
					compilationType = EnumUtils.findEnumFromString(compile,
							InteGraalKeywords.Algorithms.Parameters.Compilation.class);
				}

				Collection<Query> rewritings = EndUserAPI.rewriteOld(kb.getRuleBase(), kb.getQueries(),
						this.compilationType);
				for (Query q : rewritings) {
					writer.write(q);
				}
			}
		}
	}

	/**
	 * Command class for the "evaluate" command.
	 */
	@Parameters(commandNames = {
			evaluate }, commandDescription = "Perform evaluation of the queries on the knowledge base. Return the substitutions by queries.")
	private static class EvaluateCommand {

		@Parameter(names = "--help", help = true)
		private boolean help;

		@Parameter(names = "--query")
		private String query;

		@Parameter(names = "--ucq")
		private boolean ucq;

		@Parameter(names = "--timeout")
		private Integer timeout = null;

		@Parameter(names = { "--nbResults", "-nb" })
		private Integer max;

		public void execute(JCommander jCommander, DlgpWriter writer) throws IOException {
			if (this.help) {
				jCommander.usage();
			} else {
				Environment kb = new Environment(System.in);
				if (this.query != null) {
					kb.setQueries(this.query);
				}
				if (this.timeout != null) {
					System.err.println("Timeout option not implemented. The option is not applied.");
				}

				if (this.ucq) {
					for (Query q : kb.getQueries()) {
						writer.write(q);
					}
					printSubstitutions(EndUserAPI.evaluateOld(kb.getFactBase(), kb.getQueries()), writer, this.max);
				} else {
					for (Query q : kb.getQueries()) {
						writer.write(q);
						printSubstitutions(EndUserAPI.evaluateOld(kb.getFactBase(), q), writer, this.max);
					}
				}
			}
		}
	}

	/**
	 * Command class for the "unfold" command.
	 */
	@Parameters(commandNames = {
			unfold }, commandDescription = "Perform unfolding with compilation on the rules. Return the unfolded queries.")
	private static class UnfoldCommand {

		@Parameter(names = "--help", help = true)
		private boolean help;

		@Parameter(names = "--compile")
		private String compile = null;

		private InteGraalKeywords.Algorithms.Parameters.Compilation compilationType = InteGraalKeywords.Algorithms.Parameters.Compilation.NO_COMPILATION;

		@Parameter(names = "--query")
		private String query;

		public void execute(JCommander jCommander, DlgpWriter writer) throws Exception {
			if (this.help) {
				jCommander.usage();
			} else {
				Environment kb = new Environment(System.in);
				if (this.query != null) {
					kb.setQueries(this.query);
				}
				if (this.compile != null) {
					compilationType = EnumUtils.findEnumFromString(compile,
							InteGraalKeywords.Algorithms.Parameters.Compilation.class);
				}

				Collection<Query> unfolding = EndUserAPI.unfoldOld(kb.getRuleBase(), kb.getQueries(),
						this.compilationType);
				for (Query q : unfolding) {
					writer.write(q);
				}
			}
		}
	}

	/**
	 * Command class for the "decompose" command.
	 */
	@Parameters(commandNames = {
			decompose }, commandDescription = "Rearrange the rule base into pieces or atomic fragments. Return the transformed rule base.")
	private static class DecomposeCommand {

		@Parameter(names = "--help", help = true)
		private boolean help;

		public void execute(JCommander jCommander, DlgpWriter writer) throws IOException {
			if (this.help) {
				jCommander.usage();
			} else {
				Environment kb = new Environment(System.in);
				kb.setRuleBase(EndUserAPI.decomposeOld(kb.getRuleBase()));
				writer.write(kb.getRuleBase());
			}
		}
	}

	/**
	 * Command class for the "extract" command.
	 */
	@Parameters(commandNames = {
			extract }, commandDescription = "Extract non-compilable rules from the rule base or the compilable ones iff compilable option is called. Return the extracted rule base.")
	private static class ExtractCommand {

		@Parameter(names = "--help", help = true)
		private boolean help;

		@Parameter(names = "--compile")
		private String compile = null;

		private InteGraalKeywords.Algorithms.Parameters.Compilation compilationType = InteGraalKeywords.Algorithms.Parameters.Compilation.NO_COMPILATION;

		@Parameter(names = "--compilable")
		private boolean compilable;

		public void execute(JCommander jCommander, DlgpWriter writer) throws IOException {
			if (this.help) {
				jCommander.usage();
			} else {
				Environment kb = new Environment(System.in);
				if (this.compile != null) {
					compilationType = EnumUtils.findEnumFromString(compile,
							InteGraalKeywords.Algorithms.Parameters.Compilation.class);
				}
				if (this.compilable) {
					writer.write(EndUserAPI.extractCompilableRulesOld(kb.getRuleBase(), this.compilationType));
				} else {
					writer.write(EndUserAPI.extractNonCompilableRulesOld(kb.getRuleBase(), this.compilationType));
				}
			}
		}
	}

	/**
	 * Command class for the "queryWithSaturation" command.
	 */
	@Parameters(commandNames = {
			queryWithSaturation }, commandDescription = "Perform the evaluation of queries with saturation on the knowledge base. Return the substitutions by queries.")
	private static class QueryWithSaturationCommand {

		@Parameter(names = "--help", help = true)
		private boolean help;

		@Parameter(names = "--rank")
		private Integer rank = null;

		@Parameter(names = "--type")
		private String type = null;

		private InteGraalKeywords.Algorithms.Parameters.Chase.Checker chaseType = InteGraalKeywords.Algorithms.Parameters.Chase.Checker.SEMI_OBLIVIOUS;

		@Parameter(names = "--timeout")
		private Integer timeout = null;

		@Parameter(names = "--query")
		private String query;

		@Parameter(names = "--ucq")
		private boolean ucq;

		@Parameter(names = { "--nbResults", "-nb" })
		private Integer nbResults;

		public void execute(JCommander jCommander, DlgpWriter writer) throws IOException {
			if (this.help) {
				jCommander.usage();
			} else {
				Environment kb = new Environment(System.in);
				if (this.query != null) {
					kb.setQueries(this.query);
				}

				if (this.type != null) {
					this.chaseType = EnumUtils.findEnumFromString(this.type,
							InteGraalKeywords.Algorithms.Parameters.Chase.Checker.class);
				}
				EndUserAPI.saturateOld(kb.getFactBase(), kb.getRuleBase(), this.chaseType);

				if (this.ucq) {
					for (Query q : kb.getQueries()) {
						writer.write(q);
					}
					printSubstitutions(EndUserAPI.evaluateOld(kb.getFactBase(), kb.getQueries()), writer, this.nbResults);
				} else {
					for (Query q : kb.getQueries()) {
						writer.write(q);
						printSubstitutions(EndUserAPI.evaluateOld(kb.getFactBase(), q), writer, this.nbResults);
					}
				}
			}
		}
	}

	/**
	 * Command class for the "queryWithRewriting" command.
	 */
	@Parameters(commandNames = {
			queryWithRewriting }, commandDescription = "Perform the evaluation of queries with rewriting on the rule base. Return the substitutions by queries.")
	private static class QueryWithRewritingCommand {

		@Parameter(names = "--help", help = true)
		private boolean help;

		@Parameter(names = "--query")
		private String query;

		@Parameter(names = "--compile")
		private String compile = null;

		private InteGraalKeywords.Algorithms.Parameters.Compilation compilationType = InteGraalKeywords.Algorithms.Parameters.Compilation.NO_COMPILATION;

		@Parameter(names = "--ucq")
		private boolean ucq;

		@Parameter(names = "--rank")
		private Integer rank = null;

		@Parameter(names = "--timeout")
		private Integer timeout = null;

		@Parameter(names = { "--nbResults", "-nb" })
		private Integer nbResults;

		public void execute(JCommander jCommander, DlgpWriter writer) throws IOException {
			if (this.help) {
				jCommander.usage();
			} else {
				Environment kb = new Environment(System.in);
				if (this.timeout != null) {
					System.err.println("Timeout option not implemented. The option is not applied.");
				}
				if (this.rank != null) {
					System.err.println("Rank option not implemented. The option is not applied.");
				}

				if (this.query != null) {
					kb.setQueries(this.query);
				}
				if (this.compile != null) {
					compilationType = EnumUtils.findEnumFromString(compile,
							InteGraalKeywords.Algorithms.Parameters.Compilation.class);
				}

				kb.setQueries(EndUserAPI.rewriteOld(kb.getRuleBase(), kb.getQueries(), this.compilationType));

				if (this.ucq) {
					for (Query q : kb.getQueries()) {
						writer.write(q);
					}
					printSubstitutions(EndUserAPI.evaluateOld(kb.getFactBase(), kb.getQueries()), writer, this.nbResults);
				} else {
					for (Query q : kb.getQueries()) {
						writer.write(q);
						printSubstitutions(EndUserAPI.evaluateOld(kb.getFactBase(), q), writer, this.nbResults);
					}
				}
			}
		}
	}

	/**
	 * Command class for the "analyze" command.
	 */
	@Parameters(commandNames = {
			analyze }, commandDescription = "Verifies that the given rule base satisfies the characteristics specified in the options.")
	private static class AnalyzeCommand {

		@Parameter(names = "--help", help = true)
		private boolean help;

		@Parameter(names = "--fes")
		private boolean fes;

		@Parameter(names = "--fus")
		private boolean fus;

		@Parameter(names = "--decidable")
		private boolean decidable;

		@Parameter(names = "--hybridable")
		private boolean hybridable;

		public void execute(JCommander jCommander) throws IOException {
			if (this.help) {
				jCommander.usage();
			} else {
				Environment kb = new Environment(System.in);

				if (!(this.fus || this.fes || this.decidable || this.hybridable)) {
					this.fus = true;
					this.fes = true;
					this.decidable = true;
					this.hybridable = true;
				}

				if (this.fus) {
					if (EndUserAPI.isFusOld(kb.getRuleBase())) {
						System.out.println("Given this rule base, the rewriting process will stop for every query.");
					} else {
						System.out.println(
								"Given this rule base, it is possible that the rewriting process does not stop for a given query.");
					}
				}

				if (this.fes) {
					if (EndUserAPI.isFesOld(kb.getRuleBase())) {
						System.out
								.println("Given this rule base, the saturation process will stop for every factbase.");
					} else {
						System.out.println(
								"Given this rule base, it is possible that the saturation process does not stop for a given factbase.");
					}
				}

				if (this.decidable) {
					if (EndUserAPI.isDecidableOld(kb.getRuleBase())) {
						System.out.println(
								"The rule base is recognized as Query/Answering decidable for any factbase and any query.");
					} else {
						System.out.println(
								"The rule base is not recognized as Query/Answering decidable for any factbase and any query.");
					}
				}

				if (this.hybridable) {
					if (EndUserAPI.isHybridableOld(kb.getRuleBase())) {
						System.out.println("The rule base is recognized as hybridable.");
					} else {
						System.out.println("The rule base is not recognized as hybridable.");
					}
				}
			}
		}
	}

	/**
	 * Command class for the "hybridize" command.
	 */
	@Parameters(commandNames = {
			hybridize }, commandDescription = "Computes a Hybrid RuleBase maximizing Forward Chaining or Backward Chaining.")
	private static class HybridizeCommand {

		@Parameter(names = "--help")
		private boolean help;

		@Parameter(names = "--type")
		private String type;

		private InteGraalKeywords.Algorithms.Parameters.HybridTypes hybridType = InteGraalKeywords.Algorithms.Parameters.HybridTypes.MAX_FES;

		public void execute(JCommander jCommander, DlgpWriter writer) throws IOException {
			if (this.help) {
				jCommander.usage();
			} else {
				Environment kb = new Environment(System.in);

				if (this.type != null) {
					this.hybridType = EnumUtils.findEnumFromString(this.type,
							InteGraalKeywords.Algorithms.Parameters.HybridTypes.class);
				}

				Optional<HybridRuleBase> result = EndUserAPI.hybridizeOld(kb.getRuleBase(), this.hybridType);
				if (result.isPresent()) {
					writer.write("# alpha rules\n");
					writer.write(result.get().getAlphaRules());
					writer.write("# beta rules\n");
					writer.write(result.get().getBetaRules());
				} else {
					System.err.println("No combination solution was found or it contains a BTS component.");
				}
			}
		}
	}

	/**
	 * Command class for the "isHybrid" command.
	 */
	@Parameters(commandNames = {
			isHybrid }, commandDescription = "Tests whether the two rule bases form an hybrid rule base.")
	private static class isHybridCommand {

		@Parameter(names = "--help")
		private boolean help;

		public void execute(JCommander jCommander, DlgpWriter writer) throws IOException {
			if (this.help) {
				jCommander.usage();
			} else {
				Environment kb = new Environment(System.in);
				writer.write(EndUserAPI.isHybridOld(kb.getRuleBase(), kb.getRuleBase()));
			}
		}
	}

	/**
	 * Command class for the "help" command.
	 */
	@Parameters(commandNames = { help }, commandDescription = "Prints this help")
	private static class HelpCommand {

		@Parameter(names = "--help", help = true)
		private boolean help;
	}

	/**
	 * Simple JCommander formatter to print shorter help when no command is given
	 *
	 * @author Pierre Bisquert
	 *
	 */
	public static class SimpleUsageFormatter extends DefaultUsageFormatter {

		private final JCommander commander;

		/**
		 * Default constructor
		 * 
		 * @param commander the associated jcommander instance
		 */
		public SimpleUsageFormatter(JCommander commander) {
			super(commander);
			this.commander = commander;
		}

		public void appendCommands(StringBuilder out, int indentCount, int descriptionIndent, String indent) {
			boolean hasOnlyHiddenCommands = true;
			for (Map.Entry<JCommander.ProgramName, JCommander> commands : commander.getRawCommands().entrySet()) {
				Object arg = commands.getValue().getObjects().getFirst();
				Parameters p = arg.getClass().getAnnotation(Parameters.class);

				if (p == null || !p.hidden())
					hasOnlyHiddenCommands = false;
			}

			if (hasOnlyHiddenCommands)
				return;

			out.append(indent).append("  Commands:\n");

			for (Map.Entry<JCommander.ProgramName, JCommander> commands : commander.getRawCommands().entrySet()) {
				Object arg = commands.getValue().getObjects().getFirst();
				Parameters p = arg.getClass().getAnnotation(Parameters.class);

				if (p == null || !p.hidden()) {
					JCommander.ProgramName progName = commands.getKey();
					String displayName = progName.getDisplayName();
					String description = indent + s(4) + displayName + s(6) + getCommandDescription(progName.getName());
					wrapDescription(out, indentCount + descriptionIndent, description);
					out.append("\n");

					commander.findCommandByAlias(progName.getName());
				}
			}
			out.append("\n");
			out.append("For more information on a specific command:\n");
			out.append("Usage: <main class> [command] --help");
		}
	}

}
