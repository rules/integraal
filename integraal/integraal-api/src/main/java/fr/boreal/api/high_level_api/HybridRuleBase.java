package fr.boreal.api.high_level_api;

import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;

/**
 * The HybridRuleBase class represents a hybrid rule base consisting of an alpha rule set to use for the saturation process and a beta rule set to use for the rewriting process.
 */
public class HybridRuleBase {

    private final RuleBase alphaRuleSet;
    private final RuleBase betaRuleSet;

    /**
     * Constructs a new HybridRuleBase object with the given alpha and beta rule bases.
     *
     * @param alpha The alpha rule base.
     * @param beta  The beta rule base.
     */
    public HybridRuleBase(RuleBase alpha, RuleBase beta) {
        alphaRuleSet = new RuleBaseImpl(alpha.getRules()); // or just alphaRuleSet=alpha ?
        betaRuleSet = new RuleBaseImpl(beta.getRules()); // or just alphaRuleSet=beta ?
    }

    /**
     * Retrieves the alpha rule base.
     *
     * @return The alpha rule base.
     */
    public RuleBase getAlphaRules() {
        return alphaRuleSet;
    }

    /**
     * Retrieves the beta rule base.
     *
     * @return The beta rule base.
     */
    public RuleBase getBetaRules() {
        return betaRuleSet;
    }
}
