package fr.boreal.api.integraal_repl;

import fr.boreal.api.integraal_repl.CECommands.ComplexEnvironmentCommands;
import fr.boreal.api.integraal_repl.ComplexEnvironment.WriteMode;
import fr.boreal.api.integraal_repl.IGRepl.PrintLevel;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.views.FederatedFactBase;

public class Checks {
	
	private static final ComplexEnvironment env = ComplexEnvironmentCommands.env;
	
	public static boolean factbaseExists(String name) {
		return env.containsFactBase(name);
	}
	
	public static boolean rulebaseExists(String name) {
		return env.containsRuleBase(name);
	}
	
	public static boolean querybaseExists(String name) {
		return env.containsQueryBase(name);
	}
	
	/**
	 * 
	 * @param name
	 * @return true iff the factbase exists and is empty
	 */
	public static boolean factbaseIsEmpty(String name) {
		if(Checks.factbaseExists(name)) {
			FactBase fb = env.getFactBaseByName(name);
			if(fb instanceof FederatedFactBase) {
				return false;
			} else {
				return fb.getAtoms().findAny().isEmpty();
			}
		}
		return false;
	}
	
	public static boolean rulebaseIsEmpty(String name) {
		return Checks.rulebaseExists(name) && env.getRuleBaseByName(name).getRules().isEmpty();
	}
	
	public static boolean querybaseIsEmpty(String name) {
		return Checks.querybaseExists(name) && env.getQueryBaseByName(name).isEmpty();
	}
	
	public static boolean checkWriteModeAndWarn(WriteMode mode) {
        return switch (mode) {
            case OVERWRITE -> {
                IGRepl.writeIfVerbose("Overwriting...\n", PrintLevel.WARNING);
                yield true;
            }
            case APPEND -> {
                IGRepl.writeIfVerbose("Appending...\n", PrintLevel.WARNING);
                yield true;
            }
            case PROTECTED -> {
                IGRepl.writeIfVerbose("\n", PrintLevel.WARNING);
                yield false;
            }
        };
	}

}
