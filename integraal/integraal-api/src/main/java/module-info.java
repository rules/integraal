/**
 * High level API module for the main functionalities of InteGraal
 * 
 * @author Sarah Michel
 *
 */
open module fr.boreal.api {

	requires transitive fr.boreal.model;

	requires fr.boreal.storage;
	requires fr.boreal.io;
	requires fr.boreal.query_evaluation;
	requires fr.boreal.backward_chaining;
	requires fr.boreal.forward_chaining;
	requires fr.lirmm.boreal.util;
	requires fr.lirmm.integraal.rule_analysis;
	requires fr.boreal.redundancy;
	requires fr.boreal.forgetting;
	requires fr.boreal.views;
    requires transitive fr.boreal.component;
	requires fr.boreal.configuration;


	requires org.hsqldb;

	requires jcommander;
	requires org.apache.commons.lang3;

	requires org.jline;
	requires info.picocli;
	requires picocli.shell.jline3;
	requires org.fusesource.jansi;

    requires org.slf4j;
	requires com.google.common;

    exports fr.boreal.api.high_level_api;

}