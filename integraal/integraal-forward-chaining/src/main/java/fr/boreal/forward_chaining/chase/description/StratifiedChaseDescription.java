package fr.boreal.forward_chaining.chase.description;

import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.forward_chaining.chase.ChaseBuilder;
import fr.boreal.forward_chaining.chase.halting_condition.HaltingCondition;
import fr.boreal.forward_chaining.chase.treatment.EndTreatment;
import fr.boreal.forward_chaining.chase.treatment.Pretreatment;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;

import java.util.Collection;
import java.util.List;

public record StratifiedChaseDescription(
        FactBase factBase,
        List<RuleBase> strata,
        Chase chase,
        ChaseBuilder chaseBuilder,
        Collection<HaltingCondition> haltingConditions,
        Collection<Pretreatment> globalPretreatments,
        Collection<Pretreatment> stepPretreatments,
        Collection<EndTreatment> globalEndTreatments,
        Collection<EndTreatment> endOfStepTreatments,
        int stepNumber
) implements IChaseDescription {

    // Convert to manually built JSON string
    public String toJson() {
        StringBuilder sb = new StringBuilder();
        sb.append("{\n");

        sb.append("  \"factBaseSize\": ").append(factBase.size()).append(",\n");

        // Strata as an array
        sb.append("  \"strata\": [\n");
        for (int i = 0; i < strata.size(); i++) {
            sb.append("    { \"stratumIndex\": ").append(i)
                    .append(", \"ruleCount\": ").append(strata.get(i).getRules().size()).append(" }");
            if (i < strata.size() - 1) sb.append(",");
            sb.append("\n");
        }
        sb.append("  ],\n");

        sb.append("  \"currentStepNumber\": ").append(stepNumber).append(",\n");
        sb.append("  \"currentChase\": \"").append(chase != null ? chase.describe() : "None").append("\",\n");

        appendJsonArray(sb, "haltingConditions", haltingConditions);
        appendJsonArray(sb, "globalPretreatments", globalPretreatments);
        appendJsonArray(sb, "stepPretreatments", stepPretreatments);
        appendJsonArray(sb, "globalEndTreatments", globalEndTreatments);
        appendJsonArray(sb, "endOfStepTreatments", endOfStepTreatments, false);

        sb.append("\n}");
        return sb.toString();
    }

    // Convert to human-readable structured string
    public String toPrettyString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StratifiedChase\n");
        sb.append("├── FactBase (size): ").append(factBase.size()).append("\n");

        sb.append("├── Strata:\n");
        for (int i = 0; i < strata.size(); i++) {
            sb.append("│   ├── Stratum ").append(i).append(" (size): ").append(strata.get(i).getRules().size()).append("\n");
        }

        sb.append("├── Current Step Number: ").append(stepNumber).append("\n");
        sb.append("├── Current Chase: ").append(chase != null ? chase.describe() : "None").append("\n");

        appendPrettySection(sb, "HaltingConditions", haltingConditions);
        appendPrettySection(sb, "GlobalPretreatments", globalPretreatments);
        appendPrettySection(sb, "StepPretreatments", stepPretreatments);
        appendPrettySection(sb, "GlobalEndTreatments", globalEndTreatments);
        appendPrettySection(sb, "EndOfStepTreatments", endOfStepTreatments);

        return sb.toString();
    }

    // Helper method for JSON arrays
    private static <T> void appendJsonArray(StringBuilder sb, String key, Collection<T> collection) {
        appendJsonArray(sb, key, collection, true);
    }

    private static <T> void appendJsonArray(StringBuilder sb, String key, Collection<T> collection, boolean addComma) {
        sb.append("  \"").append(key).append("\": [");
        if (collection.isEmpty()) {
            sb.append("],\n");
        } else {
            sb.append("\n");
            for (T item : collection) {
                sb.append("    \"").append(item.toString()).append("\",\n");
            }
            sb.setLength(sb.length() - 2); // Remove last comma
            sb.append("\n  ]");
            if (addComma) sb.append(",");
            sb.append("\n");
        }
    }

    // Helper method for structured string sections
    private static <T> void appendPrettySection(StringBuilder sb, String title, Collection<T> collection) {
        sb.append("├── ").append(title).append(":\n");
        if (collection.isEmpty()) {
            sb.append("│   ├── None\n");
        } else {
            for (T item : collection) {
                sb.append("│   ├── ").append(item.toString()).append("\n");
            }
        }
    }
}
