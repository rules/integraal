package fr.boreal.forward_chaining.chase.rule_applier;

import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.forward_chaining.chase.RuleApplicationStepResult;
import fr.boreal.forward_chaining.chase.rule_applier.body_to_query_transformer.AllTransformer;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.TriggerApplierImpl;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.facts_handler.DirectApplication;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.renamer.FreshRenamer;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_checker.ObliviousChecker;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_computer.NaiveTriggerComputer;
import fr.boreal.model.kb.api.DatalogDelegable;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.query_evaluation.generic.FOQueryEvaluatorWithDBMSDelegation;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 * The source delegated rule application applies the rules by
 * * Delegating the application to the factbase if it can handle it
 * * Using a fallback method is it can't
 */
public class SourceDelegatedDatalogRuleApplier implements RuleApplier {

	private final RuleApplier fallback;

	/**
	 * DEfault constructor with a oblivious/naive chase as fallback
	 */
	public SourceDelegatedDatalogRuleApplier() {
		this(new BreadthFirstTriggerRuleApplier(
				new AllTransformer(),
				new NaiveTriggerComputer(FOQueryEvaluatorWithDBMSDelegation.defaultInstance()),
				new ObliviousChecker(),
				new TriggerApplierImpl(
						new FreshRenamer(SameObjectTermFactory.instance()),
						new DirectApplication())
				));		
	}

	/**
	 * Constructor with that applied the given chase as fallback
	 * @param fallback fallback application
	 */
	public SourceDelegatedDatalogRuleApplier(RuleApplier fallback) {
		this.fallback = fallback;
	}


	@Override
	public void init(Chase c) {
		// nothing to do
	}

	@Override
	public RuleApplicationStepResult apply(Collection<FORule> rules, FactBase fb) {
		if(fb instanceof DatalogDelegable storage) {
            Collection<FORule> existential_rules = new HashSet<>();
			Collection<FORule> range_restricted_rules = new HashSet<>();

			rules.forEach(rule -> {
				if(rule.getExistentials().isEmpty()) {
					range_restricted_rules.add(rule);
				} else {
					existential_rules.add(rule);
				}
			});

			boolean changed = false;
			try {
				changed = storage.delegate(range_restricted_rules);
			} catch (Exception e) {
				e.printStackTrace();
			}

			RuleApplicationStepResult existential_result = this.fallback.apply(existential_rules, fb);

			range_restricted_rules.addAll(existential_result.applied_rules());
			Collection<Atom> facts = existential_result.created_facts();

			if(facts.isEmpty()) {
				if(changed) {
					return new RuleApplicationStepResult(range_restricted_rules, null);
				} else {
					return new RuleApplicationStepResult(range_restricted_rules, Collections.emptyList());
				}
			} else {
				return new RuleApplicationStepResult(range_restricted_rules, facts);
			}
		}
		return this.fallback.apply(rules, fb);
	}

	@Override
	public String describe() {
		return this.getClass().getSimpleName() + " with fallback " + this.fallback.describe();
	}

	@Override
	public String describeJSON() {
		return "{\n" +
			   "  \"class\": \"" + this.getClass().getSimpleName() + "\",\n" +
			   "  \"fallback\": \"" + this.fallback.describe() + "\"\n" +
			   "}";
	}

}
