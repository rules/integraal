package fr.boreal.forward_chaining.chase.metachase.stratified;

import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.forward_chaining.chase.ChaseBuilder;
import fr.boreal.forward_chaining.chase.halting_condition.HaltingCondition;
import fr.boreal.forward_chaining.chase.halting_condition.LimitNumberOfStep;
import fr.boreal.forward_chaining.chase.treatment.Debug;
import fr.boreal.forward_chaining.chase.treatment.EndTreatment;
import fr.boreal.forward_chaining.chase.treatment.PredicateFilterEndTreatment;
import fr.boreal.forward_chaining.chase.treatment.Pretreatment;
import fr.boreal.grd.api.GraphOfFORuleDependencies;
import fr.boreal.grd.impl.GRDImpl;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.logicalElements.api.Predicate;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Builder for creating a StratifiedChase.
 * Allows setting various configurations and choosing different stratification methods.
 */
public class StratifiedChaseBuilder {

    private ChaseBuilder chaseBuilder;
    private FactBase fb;
    private RuleBase rb;
    private List<RuleBase> strata = new ArrayList<>();
    private final Collection<HaltingCondition> haltingConditions = new ArrayList<>();
    private final Collection<Pretreatment> globalPretreatments = new ArrayList<>();
    private final Collection<Pretreatment> stepPretreatments = new ArrayList<>();
    private final Collection<EndTreatment> globalEndTreatments = new ArrayList<>();
    private final Collection<EndTreatment> endOfStepTreatments = new ArrayList<>();
    private boolean debug = false;
    private StratificationMethod stratificationMethod = StratificationMethod.NONE;
    private List<Predicate> finalPredicates;
    private final Map<Integer, Set<Predicate>> predicatesToRemoveByStep = new HashMap<>();

    /**
     * Default constructor.
     */
    public StratifiedChaseBuilder() {
    }

    /**
     * Constructor with a ChaseBuilder instance.
     *
     * @param chaseBuilder the ChaseBuilder instance to use.
     */
    public StratifiedChaseBuilder(ChaseBuilder chaseBuilder) {
        this.chaseBuilder = chaseBuilder;
    }

    /**
     * Returns a default StratifiedChaseBuilder initialized with the given FactBase and RuleBase.
     *
     * @param fb the FactBase.
     * @param rb the RuleBase.
     * @return the default StratifiedChaseBuilder initialized with the given parameters.
     */
    public static StratifiedChaseBuilder defaultBuilder(FactBase fb, RuleBase rb) {
        return new StratifiedChaseBuilder(new ChaseBuilder())
                .setFactBase(fb)
                .setRuleBase(rb);
    }

    /**
     * Sets the ChaseBuilder instance.
     *
     * @param chaseBuilder the ChaseBuilder instance to use.
     * @return this StratifiedChaseBuilder instance.
     */
    public StratifiedChaseBuilder setChaseBuilder(ChaseBuilder chaseBuilder) {
        this.chaseBuilder = chaseBuilder;
        return this;
    }

    /**
     * Sets the FactBase.
     *
     * @param fb the FactBase.
     * @return this StratifiedChaseBuilder instance.
     */
    public StratifiedChaseBuilder setFactBase(FactBase fb) {
        this.fb = fb;
        return this;
    }

    /**
     * Sets the RuleBase.
     *
     * @param rb the RuleBase.
     * @return this StratifiedChaseBuilder instance.
     */
    public StratifiedChaseBuilder setRuleBase(RuleBase rb) {
        this.rb = rb;
        return this;
    }

    /**
     * Sets the strata (list of RuleBase).
     * If this method is used, no stratification method will be applied.
     *
     * @param strata the list of RuleBase.
     * @return this StratifiedChaseBuilder instance.
     */
    public StratifiedChaseBuilder setStrata(List<RuleBase> strata) {
        this.strata = strata;
        this.stratificationMethod = StratificationMethod.NONE;
        return this;
    }

    /**
     * Uses the pseudo-minimal stratification method.
     * Ensures each rule is evaluated at least once, minimizing the number of strata.
     *
     * @return this StratifiedChaseBuilder instance.
     */
    public StratifiedChaseBuilder usePseudoMinimalStratification() {
        this.stratificationMethod = StratificationMethod.PSEUDO_MINIMAL;
        return this;
    }

    /**
     * Uses the default stratification method (by strongly connected components).
     * Ensures correct rule application by grouping strongly connected components.
     *
     * @return this StratifiedChaseBuilder instance.
     */
    public StratifiedChaseBuilder useStratification() {
        this.stratificationMethod = StratificationMethod.DEFAULT;
        return this;
    }

    /**
     * Uses the single evaluation stratification method.
     * Ensures each rule is evaluated exactly once, assuming the graph is acyclic.
     *
     * @return this StratifiedChaseBuilder instance.
     */
    public StratifiedChaseBuilder useSingleEvaluationStratification() {
        this.stratificationMethod = StratificationMethod.SINGLE_EVALUATION;
        return this;
    }

    /**
     * Adds halting conditions.
     *
     * @param conditions the halting conditions to add.
     * @return this StratifiedChaseBuilder instance.
     */
    public StratifiedChaseBuilder addHaltingConditions(HaltingCondition... conditions) {
        this.haltingConditions.addAll(Arrays.asList(conditions));
        return this;
    }

    /**
     * Adds global pretreatments.
     *
     * @param treatments the global pretreatments to add.
     * @return this StratifiedChaseBuilder instance.
     */
    public StratifiedChaseBuilder addGlobalPretreatments(Pretreatment... treatments) {
        this.globalPretreatments.addAll(Arrays.asList(treatments));
        return this;
    }

    /**
     * Adds step pretreatments.
     *
     * @param treatments the step pretreatments to add.
     * @return this StratifiedChaseBuilder instance.
     */
    public StratifiedChaseBuilder addStepPretreatments(Pretreatment... treatments) {
        this.stepPretreatments.addAll(Arrays.asList(treatments));
        return this;
    }

    /**
     * Adds global end treatments.
     *
     * @param treatments the global end treatments to add.
     * @return this StratifiedChaseBuilder instance.
     */
    public StratifiedChaseBuilder addGlobalEndTreatments(EndTreatment... treatments) {
        this.globalEndTreatments.addAll(Arrays.asList(treatments));
        return this;
    }

    /**
     * Adds end of step treatments.
     *
     * @param treatments the end of step treatments to add.
     * @return this StratifiedChaseBuilder instance.
     */
    public StratifiedChaseBuilder addEndOfStepTreatments(EndTreatment... treatments) {
        this.endOfStepTreatments.addAll(Arrays.asList(treatments));
        return this;
    }

    /**
     * Sets the list of final predicates to keep in the fact base.
     *
     * @param predicates the list of predicates to keep.
     * @return this StratifiedChaseBuilder instance.
     */
    public StratifiedChaseBuilder setFinalPredicates(List<Predicate> predicates) {
        this.finalPredicates = predicates;
        return this;
    }

    /**
     * Enables debug mode.
     *
     * @return this StratifiedChaseBuilder instance.
     */
    public StratifiedChaseBuilder debug() {
        this.debug = true;
        return this;
    }

    /**
     * Builds the StratifiedChase instance.
     *
     * @return an Optional containing the StratifiedChase if successfully built, otherwise an empty Optional.
     */
    public Optional<Chase> build() {
        if (chaseBuilder == null) {
            chaseBuilder = new ChaseBuilder();
        }

        if (fb == null) {
            return Optional.empty();
        }

        if (rb == null && strata.isEmpty()) {
            return Optional.empty();
        }

        chaseBuilder.setFactBase(fb);

        // Apply the selected stratification method
        if (strata.isEmpty()) {
            if (rb == null) {
                return Optional.empty();
            }

            GraphOfFORuleDependencies grd = new GRDImpl(rb);

            switch (stratificationMethod) {
                case PSEUDO_MINIMAL:
                    strata = grd.getPseudoMinimalStratification()
                            .orElseThrow(() -> new IllegalArgumentException("The rule base must be stratifiable"));
                    break;
                case DEFAULT:
                    strata = grd.getStratification();
                    break;
                case SINGLE_EVALUATION:
                    strata = grd.getSingleEvaluationStratification()
                            .orElseThrow(() -> new IllegalArgumentException("The rule base must be stratifiable"));
                    this.chaseBuilder.addHaltingConditions(new LimitNumberOfStep(1));
                    break;
                case NONE:
                    break;
            }
        }

        // Calculate predicates to remove at each step
        if (finalPredicates != null) {
            calculatePredicatesToRemove();
            this.addEndOfStepTreatments(new PredicateFilterEndTreatment(predicatesToRemoveByStep));
        }

        if (debug) {
            this.addEndOfStepTreatments(new Debug());
        }

        return Optional.of(new StratifiedChase(
                chaseBuilder,
                fb,
                strata,
                haltingConditions,
                globalPretreatments,
                stepPretreatments,
                globalEndTreatments,
                endOfStepTreatments
        ));
    }

    private void calculatePredicatesToRemove() {
        // Step 1: Calculate predicates used in the rule bodies of each stratum
        List<Set<Predicate>> bodyPredicatesByStratum = new ArrayList<>();
        for (RuleBase ruleBase : strata) {
            Set<Predicate> bodyPredicates = ruleBase.getRules().stream()
                    .flatMap(rule -> rule.getBody().getPredicates().stream())
                    .collect(Collectors.toSet());
            bodyPredicatesByStratum.add(bodyPredicates);
        }

        // Step 2: Calculate predicates that need to be retained at the end of each step
        List<Set<Predicate>> requiredPredicatesByStratum = new ArrayList<>(Collections.nCopies(strata.size(), new HashSet<>()));
        Set<Predicate> accumulatedPredicates = new HashSet<>(finalPredicates);

        for (int i = strata.size() - 1; i >= 0; i--) {
            Set<Predicate> requiredPredicates = new HashSet<>(accumulatedPredicates);
            requiredPredicatesByStratum.set(i, requiredPredicates);
            accumulatedPredicates.addAll(bodyPredicatesByStratum.get(i));
        }

        // Step 3: Calculate predicates to remove for each stratum
        Set<Predicate> producedPredicates = new HashSet<>();
        for (int i = 0; i < strata.size(); i++) {
            final int finalI = i;
            strata.get(i).getRules().stream()
                    .flatMap(rule -> rule.getHead().getPredicates().stream())
                    .forEach(producedPredicates::add);
            Set<Predicate> predicatesToRemove = producedPredicates.stream()
                    .filter(predicate -> !requiredPredicatesByStratum.get(finalI).contains(predicate))
                    .collect(Collectors.toSet());

            predicatesToRemoveByStep.put(i+1, predicatesToRemove);
        }
    }


    /**
     * Enumeration for selecting the stratification method.
     */
    private enum StratificationMethod {
        NONE,
        PSEUDO_MINIMAL,
        DEFAULT,
        SINGLE_EVALUATION
    }
}
