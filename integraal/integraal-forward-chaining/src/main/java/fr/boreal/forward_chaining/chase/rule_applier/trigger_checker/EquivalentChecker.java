package fr.boreal.forward_chaining.chase.rule_applier.trigger_checker;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import fr.boreal.model.formula.FOFormulas;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.query_evaluation.generic.GenericFOQueryEvaluator;

/**
 * Checks if the factbase created with the given trigger would be equivalent to the current factbase
 */
public class EquivalentChecker implements TriggerChecker {

	private final FOQueryEvaluator<FOFormula> evaluator;

	/**
	 * Default constructor using the generic query evaluator
	 */
	public EquivalentChecker() {
		this(GenericFOQueryEvaluator.defaultInstance());
	}

	/**
	 * Constructor using the given query evaluator
	 * @param evaluator the query evaluator to use
	 */
	public EquivalentChecker(FOQueryEvaluator<FOFormula> evaluator) {
		this.evaluator = evaluator;
	}

	@Override
	public boolean check(FORule rule, Substitution substitution, FactBase fb) {
		FOFormulaConjunction piece = getPiecesInFactBase(rule, substitution, fb);
		FOFormulaConjunction complete_formula = FOFormulaFactory.instance().createOrGetConjunction(
				piece,
				FOFormulas.createImageWith(rule.getHead(), substitution));
        FOQuery<FOFormulaConjunction> query = FOQueryFactory.instance().createOrGetQuery(complete_formula, null, null);
        return !this.evaluator.existHomomorphism(query, fb);
    }

	private FOFormulaConjunction getPiecesInFactBase(FORule r, Substitution h, FactBase factBase) {
		Collection<Variable> vars = FOFormulas.createImageWith(r.getBody(), h).getVariables();
		Set<Variable> addedVars = new HashSet<>();

		Collection<FOFormula> result = new HashSet<>();

		while (!vars.isEmpty()) {
			Variable v = vars.iterator().next();
			vars.remove(v);
			addedVars.add(v);

			Iterator<Atom> it = factBase.getAtoms(v).iterator();
			while (it.hasNext()) {
				Atom a = it.next();
				result.add(a);

				for (Variable av : a.getVariables()) {
					if (!addedVars.contains(av)) {
						vars.add(av);
					}
				}
			}
		}

		return FOFormulaFactory.instance().createOrGetConjunction(result);
	}
}