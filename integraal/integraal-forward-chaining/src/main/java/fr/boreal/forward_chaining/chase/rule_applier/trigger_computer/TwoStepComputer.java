package fr.boreal.forward_chaining.chase.rule_applier.trigger_computer;

import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.query_evaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Computes only triggers that has not been computed at a previous step
 * <p>
 * Send an atom on the new facts and extends the homomorphism to the factbase
 */
public class TwoStepComputer implements TriggerComputer {

	private final FOQueryEvaluator<FOFormula> evaluator;
	private Chase chase;

	/**
	 * Default constructor using the generic query evaluator
	 */
	public TwoStepComputer() {
		this(GenericFOQueryEvaluator.defaultInstance());
	}

	/**
	 * Constructor using the given query evaluator
	 * @param evaluator the query evaluator to use
	 */
	public TwoStepComputer(FOQueryEvaluator<FOFormula> evaluator) {
		this.evaluator = evaluator;
	}

	@Override
	public void init(Chase c) {
		this.chase = c;
	}

	@Override
	public Iterator<Substitution> compute(FOQuery<?> body, FactBase fb) {
		Collection<Atom> last_step_facts = this.chase.getLastStepResults().created_facts();

		// First step
		if(last_step_facts == null) {
			return this.evaluator.homomorphism(body, fb);
		}

		FactBase last_step_factbase;
		last_step_factbase = new SimpleInMemoryGraphStore(
				last_step_facts.stream()
				.map(Atom::asAtomSet)
				.flatMap(Collection::stream)
				.collect(Collectors.toSet()));

		Collection<Atom> body_atoms = body.getFormula().asAtomSet();

		// atomic body
		if(body_atoms.size() == 1) {
			return this.evaluator.homomorphism(body, last_step_factbase);
		}

		Map<Atom,Set<Substitution>> partialS = new HashMap<>();

		for(Atom a : body_atoms) {
			partialS.put(a, new HashSet<>());
			FOQuery<Atom> a_query = FOQueryFactory.instance().createOrGetQuery(a, a.getVariables(), null);
			Iterator<Substitution> sub = this.evaluator.homomorphism(a_query, last_step_factbase);
			sub.forEachRemaining(partialS.get(a)::add);
		}

		LinkedList<Iterator<Substitution>> finalResult = new LinkedList<>();

		for (Atom a : partialS.keySet()) {
			for (Substitution s : partialS.get(a)) {
				finalResult.add(StreamSupport.stream(Spliterators.spliteratorUnknownSize(
						this.evaluator.homomorphism(body, fb, s), Spliterator.ORDERED), false)
						.map(subs -> s.merged(subs).orElseThrow())
						.iterator());
			}
		}

		return finalResult.stream()
				.flatMap(i -> StreamSupport.stream(Spliterators.spliteratorUnknownSize(i, Spliterator.ORDERED), false))
				.distinct()
				.iterator();
	}
}