package fr.boreal.forward_chaining.chase.rule_applier.trigger_checker;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.rule.api.FORule;

/**
 * Accept the given trigger if it is the first time.
 * This is the criteria for the oblivious chase
 */
public class ObliviousChecker implements TriggerChecker {
	
	final Map<FORule, Set<Substitution>> alreadyTreated = new HashMap<>();

	@Override
	public boolean check(FORule rule, Substitution substitution, FactBase fb) {
		Set<Substitution> rule_substitutions = this.alreadyTreated.get(rule);
		if(rule_substitutions == null) {
			rule_substitutions = new HashSet<>();
		} else if(rule_substitutions.contains(substitution)) {
			return false;
		}
		
		rule_substitutions.add(substitution);
		this.alreadyTreated.put(rule, rule_substitutions);
		return true;
	}

}
