package fr.boreal.forward_chaining.chase.description;

import fr.boreal.configuration.parameters.IGParameter;
import fr.boreal.forward_chaining.chase.RuleApplicationStepResult;
import fr.boreal.forward_chaining.chase.halting_condition.HaltingCondition;
import fr.boreal.forward_chaining.chase.rule_applier.RuleApplier;
import fr.boreal.forward_chaining.chase.rule_scheduler.RuleScheduler;
import fr.boreal.forward_chaining.chase.treatment.EndTreatment;
import fr.boreal.forward_chaining.chase.treatment.Pretreatment;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.configuration.keywords.InteGraalKeywords;

import java.util.Collection;
import java.util.List;

public record ChaseDescription(
        FactBase factBase,
        RuleBase ruleBase,
        RuleScheduler ruleScheduler,
        RuleApplier ruleApplier,
        RuleApplicationStepResult lastStepResult,

        int stepNumber,
        Collection<HaltingCondition> haltingConditions,
        Collection<Pretreatment> globalPretreatments,
        Collection<Pretreatment> stepPretreatments,
        Collection<EndTreatment> globalEndTreatments,
        Collection<EndTreatment> endOfStepTreatments
) implements IChaseDescription {

    public Collection<IGParameter<InteGraalKeywords,?>> getCorrespondingParameters(){
        //TODO add the other items of the configuration
        return List.of(ruleScheduler.getCorrespondingParameter());
    }
    // Convert to a manually built JSON string
    public String toJson() {
        StringBuilder sb = new StringBuilder();
        sb.append("{\n");

        sb.append("  \"factBaseSize\": ").append(factBase.size()).append(",\n");
        sb.append("  \"ruleBaseSize\": ").append(ruleBase.getRules().size()).append(",\n");
        sb.append("  \"ruleScheduler\": \"").append(ruleScheduler.describe()).append("\",\n");
        sb.append("  \"ruleApplier\": \"").append(ruleApplier.describeJSON()).append("\",\n");
        sb.append("  \"lastStepResult\": \"").append(lastStepResult.describe()).append("\",\n");
        sb.append("  \"stepNumber\": ").append(stepNumber).append(",\n");

        appendJsonArray(sb, "haltingConditions", haltingConditions);
        appendJsonArray(sb, "globalPretreatments", globalPretreatments);
        appendJsonArray(sb, "stepPretreatments", stepPretreatments);
        appendJsonArray(sb, "globalEndTreatments", globalEndTreatments);
        appendJsonArray(sb, "endOfStepTreatments", endOfStepTreatments, false);

        sb.append("\n}");
        return sb.toString();
    }

    // Convert to a human-readable structured string
    public String toPrettyString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ChaseImpl\n");
        sb.append("├── FactBase (size): ").append(factBase.size()).append("\n");
        sb.append("├── RuleBase (size): ").append(ruleBase.getRules().size()).append("\n");
        sb.append("├── RuleScheduler: ").append(ruleScheduler.describe()).append("\n");
        sb.append("├── RuleApplier: ").append(ruleApplier.describe()).append("\n");
        sb.append("├── LastStepResult: ").append(lastStepResult.describe()).append("\n");
        sb.append("├── StepNumber: ").append(stepNumber).append("\n");

        appendPrettySection(sb, "HaltingConditions", haltingConditions);
        appendPrettySection(sb, "GlobalPretreatments", globalPretreatments);
        appendPrettySection(sb, "StepPretreatments", stepPretreatments);
        appendPrettySection(sb, "GlobalEndTreatments", globalEndTreatments);
        appendPrettySection(sb, "EndOfStepTreatments", endOfStepTreatments);

        return sb.toString();
    }

    // Helper method for JSON arrays
    private static <T> void appendJsonArray(StringBuilder sb, String key, Collection<T> collection) {
        appendJsonArray(sb, key, collection, true);
    }

    private static <T> void appendJsonArray(StringBuilder sb, String key, Collection<T> collection, boolean addComma) {
        sb.append("  \"").append(key).append("\": [");
        if (collection.isEmpty()) {
            sb.append("]");
            if (addComma) sb.append(",");
            sb.append("\n");
        } else {
            sb.append("\n");
            for (T item : collection) {
                sb.append("    \"").append(item.toString()).append("\",\n");
            }
            sb.setLength(sb.length() - 2); // Remove last comma
            sb.append("\n  ]");
            if (addComma) sb.append(",");
            sb.append("\n");
        }
    }

    // Helper method for structured string sections
    private static <T> void appendPrettySection(StringBuilder sb, String title, Collection<T> collection) {
        sb.append("├── ").append(title).append(":\n");
        if (collection.isEmpty()) {
            sb.append("│   ├── None\n");
        } else {
            for (T item : collection) {
                sb.append("│   ├── ").append(item.toString()).append("\n");
            }
        }
    }
}
