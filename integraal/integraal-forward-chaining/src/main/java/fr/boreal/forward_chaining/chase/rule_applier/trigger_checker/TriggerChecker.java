package fr.boreal.forward_chaining.chase.rule_applier.trigger_checker;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.rule.api.FORule;

/**
 * Checks if the given trigger respect a criteria
 */
public interface TriggerChecker {

	/**
	 * @param rule the rule that is triggered
	 * @param substitution the trigger
	 * @param fb the factbase
	 * @return true iff the trigger respect the criteria
	 */
    boolean check(FORule rule, Substitution substitution, FactBase fb);


	/**
	 * Defautl method to describe the rule scheduler
	 */
	default String describe(){return this.getClass().getSimpleName();}
}
