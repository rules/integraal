package fr.boreal.forward_chaining.chase.rule_applier;

import fr.boreal.forward_chaining.chase.RuleApplicationStepResult;
import fr.boreal.forward_chaining.chase.rule_applier.body_to_query_transformer.BodyToQueryTransformer;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.TriggerApplier;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_checker.TriggerChecker;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_computer.TriggerComputer;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.rule.api.FORule;
import org.jgrapht.alg.util.Pair;

import java.util.*;
import java.util.concurrent.*;

/**
 * Multithreaded Rule Applier with a worker for each rule.
 * @author Guillaume Pérution-Kihli
 */
public class MultiThreadRuleApplier extends AbstractRuleApplier {

    private Map<FORule, BlockingQueue<Substitution>> checkingQueues; // Queues for checking per rule
    private Map<FORule, BlockingQueue<Substitution>> applicationQueues;  // Queues for application per rule
    private Map<FORule, Boolean> producerFinished; // Track when each producer has finished for each rule
    private Map<FORule, Boolean> checkingFinished; // Track when each checking process has finished for each rule
    private static final int MAX_WORKERS = 32;

    public MultiThreadRuleApplier(BodyToQueryTransformer transformer, TriggerComputer computer, TriggerChecker checker, TriggerApplier applier) {
        super(transformer, computer, checker, applier);
    }

    @Override
    public RuleApplicationStepResult apply(Collection<FORule> rules, FactBase fb) {
        ExecutorService producerExecutor = Executors.newCachedThreadPool();  // 1 thread per body
        // Pool for checking
        ExecutorService checkingWorkerExecutor = Executors.newFixedThreadPool(Math.min(MAX_WORKERS, rules.size())); // Pool for checking per rule
        // Pool for application
        ExecutorService applicationWorkerExecutor = Executors.newFixedThreadPool(Math.min(MAX_WORKERS, rules.size()));  // Pool for application per rule

        this.checkingQueues = new ConcurrentHashMap<>();
        this.applicationQueues = new ConcurrentHashMap<>();
        this.producerFinished = new ConcurrentHashMap<>();
        this.checkingFinished = new ConcurrentHashMap<>();

        // Group rules by body (FOQuery)
        Map<FOQuery<?>, Collection<FORule>> rulesByBody = this.groupRulesByBodyQuery(rules);

        // Concurrent sets to store applied rules and created facts
        Set<FORule> appliedRules = ConcurrentHashMap.newKeySet();
        Set<Atom> createdFacts = ConcurrentHashMap.newKeySet();

        // Initialize a queue for checking and application per rule, and set producer/checking not finished
        for (FORule rule : rules) {
            checkingQueues.put(rule, new LinkedBlockingQueue<>()); // Queue for checking
            applicationQueues.put(rule, new LinkedBlockingQueue<>());  // Queue for application
            producerFinished.put(rule, false); // Initially, the producer has not finished
            checkingFinished.put(rule, false); // Initially, checking is not finished
        }

        // Start producers: 1 thread per body (FOQuery)
        for (FOQuery<?> body : rulesByBody.keySet()) {
            producerExecutor.submit(() -> processBody(body, fb, rulesByBody.get(body)));
        }

        // Start worker threads for checking and application for each rule
        for (FORule rule : rules) {
            checkingWorkerExecutor.submit(() -> processChecking(fb, rule));
            applicationWorkerExecutor.submit(() -> processApplication(fb, createdFacts, appliedRules, rule));
        }

        // Shutdown the checking and application worker executors
        shutdownAndAwaitTermination(producerExecutor);
        shutdownAndAwaitTermination(checkingWorkerExecutor);
        shutdownAndAwaitTermination(applicationWorkerExecutor);

        // Return the final result
        return new RuleApplicationStepResult(appliedRules, createdFacts);
    }

    /**
     * Producer thread: Computes the substitutions (homomorphisms) for a given body (FOQuery)
     * and enqueues operations to the checking queue of each rule.
     */
    private void processBody(FOQuery<?> body, FactBase fb, Collection<FORule> associatedRules) {
        try {
            Iterator<Substitution> substitutions = this.computer.compute(body, fb);
            substitutions.forEachRemaining(substitution -> {
                for (FORule rule : associatedRules) {
                    try {
                        checkingQueues.get(rule).put(substitution); // Enqueue checking task for each rule
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        } catch (Exception e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(
                    String.format("[%s::apply] Error during the application of the rules - " +
                                    "error while computing the homomorphisms for the body: %s",
                            this.getClass(), body),
                    e);
        }

        // Mark the producer as finished for all rules associated with this body
        for (FORule rule : associatedRules) {
            producerFinished.put(rule, true);
        }
    }

    /**
     * Process checking of rules.
     */
    private void processChecking(FactBase fb, FORule rule) {
        BlockingQueue<Substitution> queue = checkingQueues.get(rule); // Get the queue for this rule
        Substitution homomorphism = null;
        while (true) {
            try {
                homomorphism = queue.poll(100, TimeUnit.MILLISECONDS); // Poll with timeout
                if (homomorphism == null) {
                    // Check if the producer is finished and the queue is empty
                    if (producerFinished.get(rule) && queue.isEmpty()) {
                        checkingFinished.put(rule, true); // Mark checking as finished for this rule
                        break; // Stop processing
                    }
                    continue;
                }

                // Perform the checking operation
                if (this.checker.check(rule, homomorphism, fb)) {
                    applicationQueues.get(rule).put(homomorphism);  // Enqueue application task
                }

            } catch (Exception e) {
                Thread.currentThread().interrupt();
                throw new RuntimeException(
                        String.format("[%s::apply] Error during the application of the rules - " +
                                        "error while checking the rule %s with homomorphism: %s",
                                this.getClass(), rule, homomorphism),
                        e);
            }
        }
    }

    /**
     * Process application of facts.
     */
    private void processApplication(FactBase fb, Set<Atom> createdFacts, Set<FORule> appliedRules, FORule rule) {
        BlockingQueue<Substitution> queue = applicationQueues.get(rule); // Get the queue for this rule
        Substitution homomorphism = null;
        while (true) {
            try {
                homomorphism = queue.poll(100, TimeUnit.MILLISECONDS); // Poll with timeout
                if (homomorphism == null) {
                    // Check if the checking process is finished and the queue is empty
                    if (checkingFinished.get(rule) && queue.isEmpty()) {
                        break; // Stop processing
                    }
                    continue;
                }

                // Perform the application operation
                FOFormula applicationFacts;
                synchronized (this.applier) {
                    applicationFacts = this.applier.apply(rule, homomorphism, fb);
                }
                if (applicationFacts != null) {
                    createdFacts.addAll(applicationFacts.asAtomSet());
                    appliedRules.add(rule);  // Add the rule to applied rules only if facts were created
                }

            } catch (Exception e) {
                Thread.currentThread().interrupt();
                throw new RuntimeException(
                        String.format("[%s::apply] Error during the application of the rules - " +
                                        "error while applying the rule %s with homomorphism: %s",
                                this.getClass(), rule, homomorphism),
                        e);
            }
        }
    }

    /**
     * Helper to shut down and wait for executor termination.
     */
    private void shutdownAndAwaitTermination(ExecutorService executorService) {
        executorService.shutdown();
        try {
            // Wait until all tasks are finished
            if (!executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS)) {
                throw new RuntimeException(String.format("[%s::apply] Error during the application of the rules: " +
                        "timeout waiting for executors to finish.", this.getClass()));
            }
        } catch (Exception e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(String.format("[%s::apply] Error during the application of the rules.",
                    this.getClass()), e);
        }
    }
}


