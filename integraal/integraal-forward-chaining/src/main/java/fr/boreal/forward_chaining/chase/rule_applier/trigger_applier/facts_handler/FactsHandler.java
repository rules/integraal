package fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.facts_handler;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;

/**
 * Handles newly created facts
 */
public interface FactsHandler {

	/**
	 * Adds the facts to the factbase
	 * @param new_facts fatcs to add
	 * @param fb factbase
	 * @return the effectively added facts
	 */
    FOFormula add(FOFormula new_facts, FactBase fb);

}
