package fr.boreal.forward_chaining.chase.halting_condition;

import fr.boreal.forward_chaining.chase.Chase;

/**
 * The Halting condition computes if there is a need for a next chase step or if it stops
 */
public interface HaltingCondition {

	/**
	 * Initialize the halting condition for the given chase
	 * @param c the Chase object
	 */
    void init(Chase c);
	
	/**
	 * @return true iff the condition is respected
	 */
    boolean check();

	/**
	 * Defautl method to describe the rule scheduler
	 */
	default String describe(){return this.getClass().getSimpleName();}

}
