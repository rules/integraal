package fr.boreal.forward_chaining.chase.description;

import fr.boreal.configuration.keywords.InteGraalKeywords;
import fr.boreal.configuration.parameters.IGParameter;

import java.util.Collection;

public interface IChaseDescription {

    /**
     *
     * @return a string description of the chase configuration and status
     */
    public String toJson();

    /**
     *
     * @return a JSON string description of the chase configuration and status
     */
    public String toPrettyString();

    /**
     *
     * @return the set of parameters corresponding to this chase configuration
     */
    default Collection<IGParameter<InteGraalKeywords,?>> getCorrespondingParameters(){
        return null;
    }
}
