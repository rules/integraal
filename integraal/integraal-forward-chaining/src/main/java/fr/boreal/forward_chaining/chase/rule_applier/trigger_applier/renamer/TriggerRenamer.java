package fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.renamer;

import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.rule.api.FORule;

/**
 * Renames the existentials on the homomorphism from the given rules
 */
public interface TriggerRenamer {
	
	/**
	 * Is expected to NOT change the given substitution
	 * @param rule rule that created the trigger
	 * @param substitution the trigger
	 * @return a substitution made from all the entries of the initial substitution
	 * and an additional entry for each existential of the rule
	 * that is not in the initial substitution
	 */
    Substitution renameExitentials(FORule rule, final Substitution substitution);

}
