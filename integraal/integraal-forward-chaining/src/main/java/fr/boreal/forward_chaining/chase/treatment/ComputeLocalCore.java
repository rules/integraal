package fr.boreal.forward_chaining.chase.treatment;

import fr.boreal.core.CoreProcessor;
import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Variable;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Treatment that computes the local core of the current fact base
 * This treatment consists of only remove all the redundant atoms created at the last step
 *
 * @author Guillaume Pérution-Kihli
 */
public class ComputeLocalCore implements EndTreatment {
    private Chase chase;
    final private CoreProcessor processor;
    Set<Variable> frozenVariables;

    public ComputeLocalCore(CoreProcessor processor)
    {
        this.processor = processor;
    }

    @Override
    public void init(Chase c) {
        this.chase = c;
        frozenVariables = this.chase.getFactBase().getVariables().collect(Collectors.toSet());
    }

    @Override
    public void apply() {
        this.processor.computeCore(this.chase.getFactBase(), frozenVariables);
        this.chase.getFactBase().getVariables().forEach(frozenVariables::add);

        List<Atom> toRemove = this.chase.getLastStepResults()
                .created_facts()
                .stream()
                .filter(a -> !this.chase.getFactBase().contains(a))
                .toList();

        this.chase.getLastStepResults().created_facts().removeAll(toRemove);
    }
}