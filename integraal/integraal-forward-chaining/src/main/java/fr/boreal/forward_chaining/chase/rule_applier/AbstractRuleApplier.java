package fr.boreal.forward_chaining.chase.rule_applier;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;

import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.forward_chaining.chase.rule_applier.body_to_query_transformer.BodyToQueryTransformer;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.TriggerApplier;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_checker.TriggerChecker;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_computer.TriggerComputer;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.rule.api.FORule;

/**
 * @author Florent Tornil
 *
 * Abstract class
 */
public abstract class AbstractRuleApplier implements RuleApplier {

	protected Logger LOG;

	/**
	 * Transformation of the rule's body to a query
	 */
	protected final BodyToQueryTransformer transformer;

	/**
	 * How to evaluate the rule's body
	 */
	protected final TriggerComputer computer;

	/**
	 * How to check the trigger should be applied
	 */
	protected final TriggerChecker checker;

	/**
	 * How to effectively apply the trigger
	 */
	protected final TriggerApplier applier;

	/**
	 * Defautl method to describe the rule scheduler
	 */
	public String describe(){
			return "\n" +
				   "│   ├── TriggerComputer: " + computer.describe() + "\n" +
				   "│   ├── TriggerChecker: " + checker.describe() + "\n" +
				   "│   ├── TriggerApplier: " + applier.describe() + "\n" +
				   "│   └── BodyToQueryTransformer: " + transformer.describe();
	}

	@Override
	public String describeJSON() {
		return "{\n" +
			   "  \"TriggerComputer\": \"" + computer.describe() + "\",\n" +
			   "  \"TriggerChecker\": \"" + checker.describe() + "\",\n" +
			   "  \"TriggerApplier\": \"" + applier.describe() + "\",\n" +
			   "  \"BodyToQueryTransformer\": \"" + transformer.describe() + "\"\n" +
			   "}";
	}

	/**
	 * Constructor
	 * 
	 * @param transformer the BodyToQueryTransformer
	 * @param computer the TriggerComputer
	 * @param checker the TriggerChecker
	 * @param applier the TriggerApplier
	 */
	public AbstractRuleApplier(BodyToQueryTransformer transformer, TriggerComputer computer, TriggerChecker checker, TriggerApplier applier) {
		this.transformer = transformer;
		this.computer = computer;
		this.checker = checker;
		this.applier = applier;
	}

	@Override
	public void init(Chase c) {
		this.computer.init(c);		
	}

	/**
	 *  Re-groups the rules with the same body
	 *  
	 * @param rules rules to re-group
	 * @return the map of query to rules
	 */
	protected Map<FOQuery<?>, Collection<FORule>> groupRulesByBodyQuery(Collection<FORule> rules) {
		Map<FOQuery<?>, Collection<FORule>> map = new HashMap<>();

		for(FORule rule : rules) {
			FOQuery<?> ruleQuery = this.transformer.transform(rule);
			if(!map.containsKey(ruleQuery)) {
				map.put(ruleQuery, new ArrayList<>());
			}
			map.get(ruleQuery).add(rule);
		}
		return map;
	}

}
