package fr.boreal.forward_chaining.chase.rule_applier;

import java.util.Collection;

import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.forward_chaining.chase.RuleApplicationStepResult;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.rule.api.FORule;

/**
 * A rule applier applies the given rules of the given factbase
 */
public interface RuleApplier {
	
	/**
	 * Initialize the rule applier for the given chase
	 * @param c the chase object
	 */
    void init(Chase c);
	
	/**
	 * Applies the given rules on the given factbase an returns the result (see {@link RuleApplicationStepResult})
	 * @param rules the rules to apply
	 * @param fb  the factbase to apply the rules on
	 * @return the result of one application step
	 */
    RuleApplicationStepResult apply(Collection<FORule> rules, FactBase fb);

	/**
	 *
	 * @return a description of the rule applier
	 */
	String describe();

	/**
	 *
	 * @return a JSON description of the rule applier
	 */
	String describeJSON();
}
