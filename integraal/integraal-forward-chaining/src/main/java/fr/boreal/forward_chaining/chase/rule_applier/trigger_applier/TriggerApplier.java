package fr.boreal.forward_chaining.chase.rule_applier.trigger_applier;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.rule.api.FORule;

/**
 * A trigger applier applies an homomorphism to the rule's head and add the resulting atoms to the factbase
 */
public interface TriggerApplier {

	/**
	 * Applies the given substitution to the rule's head and add the result to the factbase
	 * @param rule the rule to apply
	 * @param substitution the trigger of the rule
	 * @param fb the factbase
	 * @return the effectively added facts
	 */
    FOFormula apply(FORule rule, Substitution substitution, FactBase fb);


	/**
	 * Defautl method to describe the rule scheduler
	 */
	default String describe(){return this.getClass().getSimpleName();}
}
