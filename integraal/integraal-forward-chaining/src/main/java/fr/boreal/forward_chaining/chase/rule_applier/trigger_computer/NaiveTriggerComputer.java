package fr.boreal.forward_chaining.chase.rule_applier.trigger_computer;

import java.util.Iterator;

import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.query_evaluation.generic.GenericFOQueryEvaluator;

/**
 * This computing method consist of only evaluating the rule's body on the factbase.
 */
public class NaiveTriggerComputer implements TriggerComputer {

	private final FOQueryEvaluator<FOFormula> evaluator;

	/**
	 * Default constructor using the generic query evaluator
	 */
	public NaiveTriggerComputer() {
		this.evaluator = GenericFOQueryEvaluator.defaultInstance();
	}

	/**
	 * Constructor using the given query evaluator
	 * @param evaluator the query evaluator to use
	 */
	public NaiveTriggerComputer(FOQueryEvaluator<FOFormula> evaluator) {
		this.evaluator = evaluator;
	}

	@Override
	public void init(Chase c) {
		// Do nothing
	}

	@Override
	public Iterator<Substitution> compute(FOQuery<?> body, FactBase fb) {
		return this.evaluator.homomorphism(body, fb);
	}

}
