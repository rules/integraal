package fr.boreal.forward_chaining.chase.halting_condition;

import fr.boreal.forward_chaining.chase.Chase;

/**
 * This halting condition stops the chase if no rules need to be applied according to the scheduler
 */
public class HasRulesToApply implements HaltingCondition {

	Chase c;

	@Override
	public boolean check() {
		return ! this.c.getRuleScheduler().getRulesToApply(this.c.getLastStepResults().applied_rules()).isEmpty();
	}

	@Override
	public void init(Chase c) {
		this.c = c;
	}

}
