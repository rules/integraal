package fr.boreal.forward_chaining.chase.halting_condition;

import fr.boreal.forward_chaining.chase.Chase;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * This halting condition stops the chase if the boolean is set to true
 */
public class ExternalInterruption implements HaltingCondition {

	private AtomicBoolean stop;

	/**
	 * @param stop the boolean to observe
	 */
	public ExternalInterruption(AtomicBoolean stop) {
		this.stop = stop;
	}

	@Override
	public boolean check() {
		return this.stop.get();
	}

	@Override
	public void init(Chase c) {
	}
}

