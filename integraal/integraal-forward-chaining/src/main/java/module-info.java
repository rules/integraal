/**
 * Module for forward chaining elements of InteGraal
 * 
 * @author Florent Tornil
 *
 */
module fr.boreal.forward_chaining {

	requires fr.boreal.query_evaluation;
	requires fr.boreal.storage;
	requires fr.boreal.grd;
	requires fr.boreal.core;
	requires fr.boreal.io;

	requires  org.slf4j;
    requires org.jgrapht.core;
    requires com.fasterxml.jackson.databind;
	requires fr.boreal.configuration;
	requires rdf4j.queryalgebra.model;

	exports fr.boreal.forward_chaining.api;
	exports fr.boreal.forward_chaining.chase;
	exports fr.boreal.forward_chaining.chase.description;
	exports fr.boreal.forward_chaining.chase.halting_condition;
	exports fr.boreal.forward_chaining.chase.rule_applier;
	exports fr.boreal.forward_chaining.chase.rule_applier.body_to_query_transformer;
	exports fr.boreal.forward_chaining.chase.rule_applier.trigger_applier;
	exports fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.facts_handler;
	exports fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.renamer;
	exports fr.boreal.forward_chaining.chase.rule_applier.trigger_checker;
	exports fr.boreal.forward_chaining.chase.rule_applier.trigger_computer;
	exports fr.boreal.forward_chaining.chase.rule_scheduler;
	exports fr.boreal.forward_chaining.chase.treatment;
}