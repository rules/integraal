package fr.boreal.forward_chaining.chase.test.treatment;

import fr.boreal.core.*;
import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.forward_chaining.chase.ChaseBuilder;
import fr.boreal.forward_chaining.chase.treatment.ComputeLocalCore;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.query_evaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class ComputeLocalCoreTest {
    private static final FOQueryEvaluator<FOFormula> evaluator = GenericFOQueryEvaluator.defaultInstance();

    static Stream<Object[]> provideData() {
        return Arrays.stream(new CoreProcessor[] {
                        new MultiThreadsByPieceCoreProcessor(
                                16, MultiThreadsByPieceCoreProcessor.Variant.BY_SPECIALISATION),
                        new MultiThreadsByPieceCoreProcessor(
                                16, MultiThreadsByPieceCoreProcessor.Variant.BY_DELETION),
                        new MultiThreadsByPieceCoreProcessor(
                                16, MultiThreadsByPieceCoreProcessor.Variant.EXHAUSTIVE),
                        new ByPieceCoreProcessor(ByPieceCoreProcessor.Variant.BY_SPECIALISATION),
                        new ByPieceCoreProcessor(ByPieceCoreProcessor.Variant.BY_DELETION),
                        new ByPieceCoreProcessor(ByPieceCoreProcessor.Variant.EXHAUSTIVE),
                        new NaiveCoreProcessor(),
                        new ByPieceAndVariableCoreProcessor()
                })
                .flatMap(processor -> provideTestCases(processor).stream())
                .flatMap(ComputeLocalCoreTest::addChase);
    }

    private static Chase createChase (ChaseBuilder cb, Object[] testCase) {
        return cb.addStepEndTreatments(new ComputeLocalCore((CoreProcessor) testCase[2]))
                .setFactBase(new SimpleInMemoryGraphStore(Objects
                        .requireNonNull(DlgpParser.parseFile((String) testCase[0])).atoms()))
                .setRuleBase(new RuleBaseImpl(Objects
                        .requireNonNull(DlgpParser.parseFile((String) testCase[0])).rules()))
                .build().get();
    }

    private static Stream<Object[]> addChase (Object[] testCase) {
        return Arrays.stream(new Object[][]{
                        {createChase(new ChaseBuilder().useParallelApplier(), testCase),
                                new SimpleInMemoryGraphStore(
                                        Objects.requireNonNull(DlgpParser.parseDLGPString((String) testCase[1])).atoms())},
                        {createChase(new ChaseBuilder().useBreadthFirstApplier(), testCase),
                                new SimpleInMemoryGraphStore(
                                        Objects.requireNonNull(DlgpParser.parseDLGPString((String) testCase[1])).atoms())}

                }
        );
    }

    private static List<Object[]> provideTestCases(CoreProcessor processor) {
        String folder = "src/test/resources/knowledge_bases/";
        return Arrays.asList(new Object[][] {
                {folder + "localCoreTerminates.dlgp", "p(a,b), p(b,b).", processor},
                {folder + "nonLinearLocalCoreTerminates.dlgp", "p(a,b), p(b,c), p(b,b), p(c,c), p(a,c).", processor},
        });
    }

    @ParameterizedTest
    @MethodSource("provideData")
    public void computeCoreTest(Chase chase, SimpleInMemoryGraphStore expected) {
        chase.execute();
        Assertions.assertTrue(isACore(chase.getFactBase()));
        Assertions.assertTrue(isEquivalent(chase.getFactBase(), expected));
    }

    private static boolean isACore(FactBase fb) {
        // If a fact base is a core, then, all endomorphisms are isomorphisms
        FOQuery<?> q = FOQueryFactory.instance().createOrGetQuery(
                FOFormulaFactory.instance().createOrGetConjunction(fb),
                List.of());
        Iterator<Substitution> it = evaluator.evaluate(q, fb);
        while (it.hasNext()) {
            Substitution s = it.next();
            if (fb.getAtomsInMemory().stream()
                    .map(s::createImageOf)
                    .distinct()
                    .count() != fb.size()) {
                return false;
            }
        }
        return true;
    }

    private static boolean isEquivalent(FactBase fb1, FactBase fb2) {
        FOQuery<?> q1 = FOQueryFactory.instance().createOrGetQuery(
                FOFormulaFactory.instance().createOrGetConjunction(fb1),
                List.of());
        FOQuery<?> q2 = FOQueryFactory.instance().createOrGetQuery(
                FOFormulaFactory.instance().createOrGetConjunction(fb2),
                List.of());

        return evaluator.existHomomorphism(q1, fb2) && evaluator.existHomomorphism(q2, fb1);
    }
}
