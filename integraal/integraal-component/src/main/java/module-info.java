/**
 * High level components for the main functionalities of InteGraal
 * 
 * @author Federico
 *
 */
open module fr.boreal.component {

	requires transitive fr.boreal.model;

	requires transitive fr.boreal.query_evaluation;
	requires transitive fr.boreal.backward_chaining;
	requires transitive fr.boreal.forward_chaining;

	requires fr.boreal.storage;
	requires fr.boreal.io;
	requires transitive fr.boreal.views;
	requires radicchio;
	requires org.slf4j;
	requires fr.lirmm.boreal.util;
	requires fr.boreal.configuration;

	exports fr.boreal.component_builder;
	exports fr.boreal.component_builder.api;
	exports fr.boreal.component_builder.api.algorithm;
	exports fr.boreal.component_builder.api.scenario;
	exports fr.boreal.component_builder.components;
	exports fr.boreal.component_builder.externalHaltingConditions;
    exports fr.boreal.component_builder.evaluators.generic;
	exports fr.boreal.component_builder.evaluators.query_rewriting;
	exports fr.boreal.component_builder.evaluators.query_evaluation;
	exports fr.boreal.component_builder.evaluators.query_evaluation.auxiliary;
	exports fr.boreal.component_builder.evaluators.query_rewriting.auxiliary;
	exports fr.boreal.component_builder.evaluators.generic.processors;

}