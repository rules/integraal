package fr.boreal.component_builder.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import fr.boreal.component_builder.InputDataScenario.MW;

/**
 * Utility class used to print the content of a object manipulated by the
 * component builder
 */
public class StringUtils {

	public static String buildLogMessage(int numberOfStars, String message) {
		return "\n\n" + " ***\n".repeat(Math.max(0, numberOfStars)) + " ***\t" + message + "\n"
				+ " ***\n".repeat(Math.max(0, numberOfStars));
	}

	public static String buildBoxedLogMessage(String message, int padding) {
		// Adjusting messageLength directly might not be universally applicable
		// The original calculation aims to create a box that dynamically fits the
		// message and specified padding
		int messageLength = message.length();
		int totalWidth = messageLength + (padding * 2) + 4; // 2 sides of the box, plus left and right padding

		StringBuilder sb = new StringBuilder("\n\n ");

		// Top border
		sb.append("*".repeat(Math.max(0, totalWidth)));
		sb.append("\n ");

		// Padding lines above the message
		String repeat = " ".repeat(Math.max(0, totalWidth - 2));
		for (int i = 0; i < padding; i++) {
			sb.append("*");
			sb.append(repeat);
			sb.append("*\n ");
		}

		// Message line
		sb.append("*");
		sb.append(" ".repeat(Math.max(0, padding + 1)));
		sb.append(message);
		sb.append(" ".repeat(Math.max(0, padding + 1)));
		sb.append("*\n ");

		// Padding lines below the message
		for (int i = 0; i < padding; i++) {
			sb.append("*");
			sb.append(repeat);
			sb.append("*\n ");
		}

		// Bottom border
		sb.append("*".repeat(Math.max(0, totalWidth)));

		sb.append("\n");
		return sb.toString();
	}

	public static String print(Object o) {
		StringBuilder sb = new StringBuilder("\n\n" + o.getClass().getSimpleName() + "\n");
		Map<String, String> prefixMap = new LinkedHashMap<>(); // Map prefixes to names

		Field[] fields = o.getClass().getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			Field field = fields[i];
			if (field.isSynthetic() || Modifier.isStatic(field.getModifiers())) {
				// Skip synthetic and static fields
				continue;
			}
			try {
				field.setAccessible(true);
				if ("log".equalsIgnoreCase(field.getName()) || field.getType().equals(boolean.class)
						|| field.getType().equals(Boolean.class)) {
					continue;
				}

				Object value = field.get(o);
				// Process Optional values
				if (value instanceof Optional<?> optional) {
					value = optional.isPresent() ? optional.get() : "Optional.empty";
				}

				// Determine the correct prefix
				String prefix = (i < fields.length - 2) ? "├─ " : "└─ ";

				// Special handling for Collection of Strings, assumed to be file paths
				if (value instanceof Collection<?> collection && !collection.isEmpty()
						&& collection.iterator().next() instanceof String) {
					StringBuilder pathsBuilder = new StringBuilder();
					for (Object pathObj : collection) {
						String path = (String) pathObj;
						String filename = Paths.get(path).getFileName().toString();
						String prefixPath = path.replace(filename, "");
						String prefixName = prefixMap.computeIfAbsent(prefixPath,
								k -> "path_" + (prefixMap.size() + 1));

						pathsBuilder.append(filename).append("  (").append(prefixName).append("), ");
					}
					// Remove the last comma and space
					if (!pathsBuilder.isEmpty()) {
						pathsBuilder.delete(pathsBuilder.length() - 2, pathsBuilder.length());
					}
					sb.append(prefix).append(field.getName()).append(": ").append(pathsBuilder).append("\n");
				} else if (value != null) {
					// Handle other non-Collection, non-null fields
					sb.append(prefix).append(field.getName()).append(": ").append(value).append("\n");
				}
			} catch (IllegalAccessException e) {
				e.printStackTrace(); // Replace with logger if available
			}
		}

		// Add prefix mappings at the end
		if (!prefixMap.isEmpty()) {
			sb.append("└─ Prefixes:\n");
			prefixMap.forEach(
					(prefix, name) -> sb.append("   ├─ ").append(name).append(": ").append(prefix).append("\n"));
			// Removing the last "├─ " and replacing it with "└─ " for the last prefix
			int lastPrefixIndex = sb.lastIndexOf("├─ ");
			if (lastPrefixIndex != -1) {
				sb.replace(lastPrefixIndex, lastPrefixIndex + 2, "└─ ");
			}
		}

		return sb.toString();
	}

	public static String printObjectWithModifiableWrapper(Object o) {
		StringBuilder sb = new StringBuilder("\n\n" + o.getClass().getSimpleName() + "\n");
		Map<String, String> prefixMap = new LinkedHashMap<>(); // Map prefixes to names

		// Collect all relevant fields
		Field[] allFields = o.getClass().getDeclaredFields();
		List<Field> fields = new ArrayList<>();
		for (Field field : allFields) {
			if (field.isSynthetic() || Modifier.isStatic(field.getModifiers())
					|| "log".equalsIgnoreCase(field.getName())) {
				// Skip synthetic, static, and 'log' fields
				continue;
			}
			fields.add(field);
		}

		int totalFields = fields.size();
		int printedFields = 0;

		for (Field field : fields) {
			try {
				field.setAccessible(true);

				Object value = field.get(o);
				if (value == null) {
					continue;
				}

				// Handle MW<T> by extracting the 'value' field
				if (value instanceof MW<?> mw) {

					if (!mw.isSet()) {
						continue;
					}

					value = mw.getValue();
				}

				// Process Optional values
				if (value instanceof Optional<?> optional) {
					value = optional.isPresent() ? optional.get() : "Optional.empty";
				}

				// Determine the correct prefix
				String prefix = (printedFields < totalFields - 1) ? "├─ " : "└─ ";

				if (value instanceof Collection<?> collection && !collection.isEmpty()
						&& collection.iterator().next() instanceof String) {
					// Special handling for Collection of Strings, assumed to be file paths
					StringBuilder pathsBuilder = new StringBuilder();
					for (Object pathObj : collection) {
						String path = (String) pathObj;
						String filename = Paths.get(path).getFileName().toString();
						String prefixPath = path.replace(filename, "");
						String prefixName = prefixMap.computeIfAbsent(prefixPath,
								k -> "path_" + (prefixMap.size() + 1));

						pathsBuilder.append(filename).append("  (").append(prefixName).append("), ");
					}
					// Remove the last comma and space
					if (pathsBuilder.length() > 0) {
						pathsBuilder.setLength(pathsBuilder.length() - 2);
					}
					sb.append(prefix).append(field.getName()).append(": ").append(pathsBuilder).append("\n");
				} else if (value instanceof Collection<?> collection) {
					// Handle other collections
					sb.append(prefix).append(field.getName()).append(": ").append(collection).append("\n");
				} else {
					// Handle other non-Collection, non-null fields
					sb.append(prefix).append(field.getName()).append(": ").append(value).append("\n");
				}
				printedFields++;
			} catch (IllegalAccessException e) {
				e.printStackTrace(); // Replace with logger if available
			}
		}

		// Add prefix mappings at the end
		if (!prefixMap.isEmpty()) {
			sb.append("└─ Prefixes:\n");
			int prefixCount = prefixMap.size();
			int count = 0;
			for (Map.Entry<String, String> entry : prefixMap.entrySet()) {
				String prefixSymbol = (++count < prefixCount) ? "   ├─ " : "   └─ ";
				sb.append(prefixSymbol).append(entry.getValue()).append(": ").append(entry.getKey()).append("\n");
			}
		}

		return sb.toString();
	}
}
