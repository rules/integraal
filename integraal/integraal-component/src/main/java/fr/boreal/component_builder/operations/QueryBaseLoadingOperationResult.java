package fr.boreal.component_builder.operations;

import java.io.Serializable;
import java.util.Collection;

import fr.boreal.component_builder.api.IOperationResult;
import fr.boreal.model.query.api.Query;

/**
 * Records the size of the query set
 * 
 * @param size
 */
public record QueryBaseLoadingOperationResult(long size) implements IOperationResult, Serializable {

	/**
	 * Records the size of the query set
	 * 
	 * @param queryBase
	 */
	public QueryBaseLoadingOperationResult(Collection<Query> queryBase) {
		this(queryBase.size());
	}

	public String serializationString() {
		return "Querybase size : " + size;
	}

}
