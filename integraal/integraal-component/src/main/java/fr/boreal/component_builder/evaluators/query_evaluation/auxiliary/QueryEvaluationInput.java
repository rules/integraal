package fr.boreal.component_builder.evaluators.query_evaluation.auxiliary;

import java.util.Objects;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.queryEvaluation.api.QueryEvaluator;

public record QueryEvaluationInput(Query query, FactBase factBase, QueryEvaluator<Query> queryEvaluator,
		boolean constantsOnly, boolean count) {

	public QueryEvaluationInput {
		Objects.requireNonNull(query, "query must not be null");
		Objects.requireNonNull(factBase, "factBase must not be null");
	}

	@Override
	public String toString() {
		long factBaseSize = factBase.size();

		String evaluatorClassName = queryEvaluator.getClass().getSimpleName();

		return String.format("QueryEvaluationInput{query=%s, factBase size=%d, evaluator class=%s}", query,
				factBaseSize, evaluatorClassName);
	}
}
