package fr.boreal.component_builder.externalHaltingConditions;

import fr.boreal.configuration.keywords.InteGraalKeywords.Algorithms.ExternalHaltingConditions;

/**
 * Factory for creating ExternalHaltingConditions
 */
public class ExternalHaltingConditionFactory {
	////////////////////////
		/// STATIC METHODS
		////////////////////////
		/**
		 * 
		 * @param timeout 
		 * @return an object with given timeout
		 */
		public static ExternalHaltingCondition getTimeout(Object timeout) {
			return new ExternalHaltingCondition(ExternalHaltingConditions.TIMEOUT, timeout);
		}
	
		/**
		 * @param rank 
		 * @return an object with given rank
		 */
		public static ExternalHaltingCondition getRank(Object rank) {
			return new ExternalHaltingCondition(ExternalHaltingConditions.RANK, rank);
		}
}

