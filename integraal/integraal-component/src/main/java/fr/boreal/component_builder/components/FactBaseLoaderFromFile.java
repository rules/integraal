package fr.boreal.component_builder.components;

import fr.boreal.component_builder.api.algorithm.IAlgorithmParameters;
import fr.boreal.component_builder.api.scenario.IInputDataScenario;
import fr.boreal.configuration.keywords.InteGraalKeywords.SupportedFileExtensions;
import fr.boreal.configuration.keywords.InteGraalKeywords.InternalStorageConfiguration.StorageLayout;
import fr.boreal.io.api.Parser;
import fr.boreal.io.csv.CSVLoader;
import fr.boreal.io.csv.CSVParser;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.io.dlgpe.DlgpeParser;
import fr.boreal.io.rdf.RDFParser;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.storage.builder.StorageBuilder;
import fr.boreal.views.FederatedFactBase;
import fr.boreal.views.builder.ViewBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import radicchio.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;

public class FactBaseLoaderFromFile {

	static final Logger LOG = LoggerFactory.getLogger(FactBaseLoaderFromFile.class);

	/**
	 *
	 * Loads all atoms in files specified by the knowlede base scenario in a storage
	 * which is defined by the algorithm parameters object.
	 *
	 * @param kbscenario
	 * @param integraalAlgorithmParameters
	 * @return a factbase corresponding to the give fact sets
	 */

	public static FactBase getFactbaseFor(IInputDataScenario kbscenario,
										  IAlgorithmParameters integraalAlgorithmParameters) {

		FactBase fact_storage = StorageComponent.prepareStorage(integraalAlgorithmParameters);
		for (String singleFile : kbscenario.getFactbasePaths().get()) {
			SupportedFileExtensions.Factbase extension = FileUtils.getExtension(singleFile, SupportedFileExtensions.Factbase.class);
			LOG.debug("loading [{}] file {}", extension.name(), singleFile);

			boolean encoding = isEncodeInput(integraalAlgorithmParameters);
			if (extension.equals(SupportedFileExtensions.Factbase.RLS)) {
				// try to use a CSV Loader
				CSVLoader.parseAndLoad(fact_storage, new File(singleFile), encoding);
			} else {
				try {
					Parser<?> parser = switch (extension) {
						case DLGP -> new DlgpParser(new File(singleFile));
						case DLGPE -> new DlgpeParser(new File(singleFile));
						case RDF -> new RDFParser(singleFile);
						case CSV -> new CSVParser(singleFile);
						case RLS -> null; // already covered before
					};

					fact_storage.addAll(parser.streamParsedObjects(Atom.class));
					parser.close();
				} catch (FileNotFoundException e) {
					LOG.error(e.getMessage());
					throw new RuntimeException(
							String.format("[FactBaseLoaderFromFile::getFactBaseFor] %s.", e.getMessage()), e);
				}
			}
		}
		return fact_storage;
	}

	/**
	 *
	 * @param kbscenario
	 * @param integraalAlgorithmParameters
	 * @return the federated factbase built from mapping files
	 */
	public static FactBase getFederatedFactbaseFor(IInputDataScenario kbscenario,
												   IAlgorithmParameters integraalAlgorithmParameters) {
		try {
			return new FederatedFactBase(StorageBuilder.defaultStorage(), ViewBuilder.createFactBases(kbscenario.getMappingbasePaths().get()));
		} catch (ViewBuilder.ViewBuilderException e) {
			LOG.error(e.getMessage());
			throw new RuntimeException(
					String.format(
							"[FactBaseLoaderFromFile::getFederatedFactBaseFor]" +
									"Parameters: %s, %s.",
							kbscenario, integraalAlgorithmParameters));
		}
	}

	/**
	 *
	 * @param integraalAlgorithmParameters
	 * @return true if the storage strategy requires an integer encoding of data
	 */

	private static boolean isEncodeInput(IAlgorithmParameters integraalAlgorithmParameters) {
		if (integraalAlgorithmParameters.getDBStrategy().isPresent()) {
			var strategy = integraalAlgorithmParameters.getDBStrategy().get();
			return strategy.equals(StorageLayout.EncodingAdHocSQL);
		}
		return false;
	}
}
