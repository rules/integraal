package fr.boreal.component_builder.utils;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.boreal.component_builder.evaluators.query_rewriting.auxiliary.RewritingOutput;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.component_builder.evaluators.query_evaluation.auxiliary.QueryEvaluationOutput;

public class ComponentPrinter {
	static final Logger LOG = LoggerFactory.getLogger(ComponentPrinter.class);

	public static void writeAnswersToLog(Iterable<QueryEvaluationOutput> result) {

		Objects.requireNonNull(result);

		result.forEach(record -> LOG.info(record.getPrintQueryAnswers()));
	}

	public static void writeUCQAnswersCountToLog(Iterable<QueryEvaluationOutput> ucq_result) {

		Objects.requireNonNull(ucq_result);

		long sum = StreamSupport.stream(ucq_result.spliterator(), false).mapToLong(QueryEvaluationOutput::result_size)
				.sum();

		String ucq = StreamSupport.stream(ucq_result.spliterator(), false).map(QueryEvaluationOutput::query)
				.map(Object::toString).collect(Collectors.joining(" OR "));

        LOG.debug("#anwers: {} for UCQ : {}", sum, printSubstring(0, 50, ucq));

	}

	public static void writeQueriesToLog(Iterable<RewritingOutput> result) {

		Objects.requireNonNull(result);

		result.forEach(record -> {

            LOG.debug("\n\n Query : {}\n========Query Rewritings========", record.query());

			LOG.debug(record.rewritings().toString());

		});

	}

	public static void writeFactbaseSizeToLog(FactBase factBase) {

		Objects.requireNonNull(factBase);

        LOG.info("\n\n========Chase Result========\n{}", factBase.size());

	}

	private static String printSubstring(int start, int end, String string) {

		return string.length() <= end ? string : string.substring(start, end) + "[... rest of the query cut]";

	}

}
