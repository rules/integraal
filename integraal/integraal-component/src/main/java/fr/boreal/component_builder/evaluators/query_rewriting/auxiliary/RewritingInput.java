package fr.boreal.component_builder.evaluators.query_rewriting.auxiliary;

import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.ruleCompilation.api.RuleCompilation;

public record RewritingInput(Query query, RuleBase rules, RuleCompilation compilation) {

}