package fr.boreal.component_builder.components;

import java.util.Collection;
import java.util.Set;

import fr.boreal.component_builder.api.algorithm.IAlgorithmParameters;
import fr.boreal.component_builder.evaluators.query_rewriting.QueryRewriterWitMultiEvaluator;
import fr.boreal.component_builder.externalHaltingConditions.ExternalAlgorithmHaltingConditions;
import fr.boreal.configuration.keywords.InteGraalKeywords.Algorithms;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.ruleCompilation.HierarchicalRuleCompilation;
import fr.boreal.model.ruleCompilation.NoRuleCompilation;
import fr.boreal.model.ruleCompilation.api.RuleCompilation;
import fr.boreal.model.ruleCompilation.id.IDRuleCompilation;

/**
 * Builds a custom query rewriter
 */
public class RewritingComponentBuilder {

	/**
	 * 
	 * @param queries
	 * @param rulebase
	 * @param rewritingParameters
	 * @return
	 */
	public static QueryRewriterWitMultiEvaluator prepareAndGetRewriterFrom(Collection<Query> queries, RuleBase rulebase,
																		   IAlgorithmParameters rewritingParameters) {

		// handle compilation
		Algorithms.Parameters.Compilation compil = rewritingParameters.getCompilation().isPresent()
				? rewritingParameters.getCompilation().get()
				: Algorithms.Parameters.Compilation.NO_COMPILATION;

		var c = switch (compil) {
		case HIERARCHICAL_COMPILATION -> new HierarchicalRuleCompilation();
		case ID_COMPILATION -> new IDRuleCompilation();
		default -> NoRuleCompilation.instance();

		};

		// halting conditions
		ExternalAlgorithmHaltingConditions hc = new ExternalAlgorithmHaltingConditions(
				rewritingParameters.getRank().isEmpty() ? null : rewritingParameters.getRank().get(),
				rewritingParameters.getTimeout().isEmpty() ? null : rewritingParameters.getTimeout().get());

		return prepareAndGetRewriterFrom(queries, rulebase, c, hc);

	}


	/**
	 * 
	 * @param query
	 * @param rulebase
	 * @param compilation
	 * @param rewritingParameters
	 * @return
	 */
	public static QueryRewriterWitMultiEvaluator prepareAndGetRewriterFrom(Query query, RuleBase rulebase, RuleCompilation compilation,
                                                                           IAlgorithmParameters rewritingParameters) {

		return prepareAndGetRewriterFrom(Set.of(query), rulebase, rewritingParameters);

	}
	
	/**
	 * @param queries
	 * @param rulebase
	 * @param compilation
	 * @param hc
	 * @return the query rewriter according to the configuration required
	 */
	public static QueryRewriterWitMultiEvaluator prepareAndGetRewriterFrom(Collection<Query> queries, RuleBase rulebase,
                                                                           RuleCompilation compilation, ExternalAlgorithmHaltingConditions hc) {

		return new QueryRewriterWitMultiEvaluator(queries, rulebase, compilation, hc);

	}

	/**
	 * @param query
	 * @param rulebase
	 * @param compilation
	 * @param hc
	 * @return the query rewriter according to the configuration required
	 */
	public static QueryRewriterWitMultiEvaluator prepareAndGetRewriterFrom(Query query, RuleBase rulebase, RuleCompilation compilation,
                                                                           ExternalAlgorithmHaltingConditions hc) {

		return prepareAndGetRewriterFrom(Set.of(query), rulebase, compilation, hc);

	}

}
