package fr.boreal.component_builder.api;

import java.io.Serializable;

/**
 * Records information about a system operation ; for debug/monitor purposes.
 * Examples include the creation of logical elements (facts, rules, queries) and
 * reasoning algorithms (chase, rewriting).
 */
public interface IOperationResult extends Serializable {

    /**
     * @return string representing info on the operation : result or error message
     */
	default String serializationString() {
		return this.toString();
	}

}
