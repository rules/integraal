
package fr.boreal.component_builder.evaluators.query_rewriting.auxiliary;

import java.util.Optional;
import java.util.function.BiFunction;

import fr.lirmm.boreal.util.object_analyzer.ObjectAnalizer;

/**
 * Creates a query rewriting output where the timeout is indicated
 */
public class DefaultQueryRewritingOutputIfTimeoutFunction
		implements BiFunction<RewritingInput, String, RewritingOutput> {

	@Override
	public RewritingOutput apply(RewritingInput t, String u) {

		return new RewritingOutput(t.query(), null,
				Optional.of("Query " + ObjectAnalizer.digest(t.query()) + " TIMEOUT after" + u + " seconds"));

	}

}
