package fr.boreal.component_builder.operations;

import java.io.Serializable;

import fr.boreal.component_builder.api.IOperationResult;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.views.FederatedFactBase;

/**
 * records the size of the loaded factbase
 * 
 * @param size
 * @param message
 */
public record FactBaseLoadingOperationResult(long size, String message) implements IOperationResult, Serializable {

	/**
	 * records the size of the loaded factbase
	 * 
	 * @param factbase
	 */
	public FactBaseLoadingOperationResult(FactBase factbase) {
		this(factbase.size(), message(factbase));
	}

	private static String message(FactBase factbase) {
		if (factbase instanceof FederatedFactBase) {
			return "Impossible to know the size of a federated factbase in the presence of views, just indicating the size of the local factbase. ";

		}
		return "";
	}

	public String serializationString() {
		return message + "Factbase size : " + size;
	}

}
