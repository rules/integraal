package fr.boreal.component_builder.api;

import java.util.Collection;
import java.util.Set;

import fr.boreal.component_builder.ComponentBuilder;
import fr.boreal.component_builder.api.algorithm.IAlgorithmParameters;
import fr.boreal.component_builder.api.scenario.IInputDataScenario;
import fr.boreal.component_builder.evaluators.query_evaluation.QueryEvaluatorWithMultiEvaluator;
import fr.boreal.component_builder.evaluators.query_rewriting.QueryRewriterWitMultiEvaluator;
import fr.boreal.component_builder.externalHaltingConditions.ExternalAlgorithmHaltingConditions;
import fr.boreal.component_builder.externalHaltingConditions.ExternalHaltingCondition;
import fr.boreal.configuration.keywords.InteGraalKeywords;
import fr.boreal.configuration.parameters.IGParameter;
import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.ruleCompilation.api.RuleCompilationResult;

/**
 * Interface for building various InteGraal components related to query
 * answering, rule-based reasoning, query rewriting, and rule compilation.
 * Provides methods to construct these components from input fact bases, rule
 * bases, and queries.
 */
public interface IComponentBuilder {

	///////////////////////////////
	/// STATIC METHODS WITH IGPARAMETERS
	///////////////////////////////

	/**
	 * Creates a query evaluator for answering a single query on a fact base.
	 *
	 * @param fb     the fact base to be used for query answering.
	 * @param query  a single query to be evaluated.
	 * @param params the parameters for storage and halting conditions
	 * @return an instance of {@link QueryEvaluatorWithMultiEvaluator} configured
	 *         for query answering.
	 */
	static QueryEvaluatorWithMultiEvaluator buildAndGetQueryEvaluator(FactBase fb, Query query,
			IGParameter<InteGraalKeywords, ?>... params) {
		return buildAndGetQueryEvaluator(fb, Set.of(query), params);
	}

	/**
	 * Creates a query evaluator for answering a set of queries on a fact base.
	 *
	 * @param fb      the fact base to be used for query answering.
	 * @param queries a set of queries to be evaluated.
	 * @param params  the parameters for storage and halting conditions
	 * @return an instance of {@link QueryEvaluatorWithMultiEvaluator} configured
	 *         for query answering.
	 */
	static QueryEvaluatorWithMultiEvaluator buildAndGetQueryEvaluator(FactBase fb, Set<Query> queries,
			IGParameter<InteGraalKeywords, ?>... params) {
		IInputDataScenario inputDBQuery = IInputDataScenario.QA(fb, queries);
		IAlgorithmParameters algoParam = IAlgorithmParameters.QA(params);
		IComponentBuilder builder = createBuilderFrom(inputDBQuery, algoParam);
		return builder.buildOrGetQueryAnsweringAlgorithm();
	}

	/**
	 * Creates a Chase-based reasoning component.
	 *
	 * @param fb     the fact base for the chase.
	 * @param rb     the rule base for the chase.
	 * @param params the parameters for storage and halting conditions
	 * @return an instance of {@link Chase} configured for reasoning, or
	 *         {@code null} if an error occurs.
	 */
	static Chase buildAndGetChase(FactBase fb, RuleBase rb, IGParameter<InteGraalKeywords, ?>... params) {
		IInputDataScenario inputKB = IInputDataScenario.KB(fb, rb);
		IAlgorithmParameters algoParam = IAlgorithmParameters.Chase(params);
		var builder = createBuilderFrom(inputKB, algoParam);
		return builder.buildOrGetChase();
	}

	/**
	 * Creates a query rewriter for Ontology-Mediated Query (OMQ) Rewriting.
	 *
	 * @param rb      the rule base for rewriting.
	 * @param queries a collection of queries to be rewritten.
	 * @param params  the parameters for storage and halting conditions
	 * @return an instance of {@link QueryRewriterWitMultiEvaluator} configured for query rewriting,
	 *         or {@code null} in case of an error.
	 */
	static QueryRewriterWitMultiEvaluator buildAndGetOMQRewriter(RuleBase rb, Collection<Query> queries,
																 IGParameter<InteGraalKeywords, ?>... params) {
		IInputDataScenario inputOMQ = IInputDataScenario.OMQ(rb, queries);
		IAlgorithmParameters algoParam = IAlgorithmParameters.Rewriting(params);
		var builder = createBuilderFrom(inputOMQ, algoParam);
		return builder.buildOrGetRewriter();
	}

	/**
	 * Creates a component builder for rule compilation.
	 *
	 * @param ruleBase the rule base for rewriting.
	 * @param params   the parameters for storage and halting conditions
	 * @return an instance of {@link RuleCompilationResult} configured for rule
	 *         compilation, or {@code null} in case of an error.
	 */
	static RuleCompilationResult buildAndGetRuleCompilation(RuleBase ruleBase,
			IGParameter<InteGraalKeywords, ?>... params) {
		IInputDataScenario inputRuleBase = IInputDataScenario.Compilation(ruleBase);
		IAlgorithmParameters algoParam = IAlgorithmParameters.Compilation(params);
		var builder = createBuilderFrom(inputRuleBase, algoParam);
		return builder.getRuleCompilationResult();
	}

	///////////////////////////////
	/// OLD STATIC METHODS
	///////////////////////////////
	/**
	 * Creates a query evaluator for answering a single query on a fact base.
	 *
	 * @param fb    the fact base to be used for query answering.
	 * @param query a single query to be evaluated.
	 * @param conds he external halting conditions (e.g., timeout, rank).
	 * @return an instance of {@link QueryEvaluatorWithMultiEvaluator} configured
	 *         for query answering.
	 */
	static QueryEvaluatorWithMultiEvaluator buildAndGetQueryEvaluator(FactBase fb, Query query,
			ExternalHaltingCondition... conds) {
		return buildAndGetQueryEvaluator(fb, Set.of(query), conds);
	}

	/**
	 * Creates a query evaluator for answering a set of queries on a fact base.
	 *
	 * @param fb      the fact base to be used for query answering.
	 * @param queries a set of queries to be evaluated.
	 * @param conds   he external halting conditions (e.g., timeout, rank).
	 * @return an instance of {@link QueryEvaluatorWithMultiEvaluator} configured
	 *         for query answering.
	 */
	static QueryEvaluatorWithMultiEvaluator buildAndGetQueryEvaluator(FactBase fb, Set<Query> queries,
			ExternalHaltingCondition... conds) {
		IInputDataScenario inputDBQuery = IInputDataScenario.QA(fb, queries);
		IAlgorithmParameters algoParam = IAlgorithmParameters.QA(new ExternalAlgorithmHaltingConditions(conds));
		IComponentBuilder builder = createBuilderFrom(inputDBQuery, algoParam);
		return builder.buildOrGetQueryAnsweringAlgorithm();
	}

	/**
	 * Creates a Chase-based reasoning component.
	 *
	 * @param fb        the fact base for the chase.
	 * @param rb        the rule base for the chase.
	 * @param chaseType the type of chase algorithm (optional).
	 * @param conds     he external halting conditions (e.g., timeout, rank).
	 * @return an instance of {@link Chase} configured for reasoning, or
	 *         {@code null} if an error occurs.
	 */
	static Chase buildAndGetChase(FactBase fb, RuleBase rb,
			InteGraalKeywords.Algorithms.Parameters.Chase.Checker chaseType, ExternalHaltingCondition... conds) {
		IInputDataScenario inputKB = IInputDataScenario.KB(fb, rb);
		IAlgorithmParameters algoParam = IAlgorithmParameters.Chase(chaseType,
				new ExternalAlgorithmHaltingConditions(conds));
		var builder = createBuilderFrom(inputKB, algoParam);
		return builder.buildOrGetChase();
	}

	/**
	 * Creates a Chase-based reasoning component.
	 *
	 * @param fb        the fact base for the chase.
	 * @param rb        the rule base for the chase.
	 * @param chaseType the type of chase algorithm (optional).
	 * @param params    he external halting conditions (e.g., timeout, rank).
	 * @return an instance of {@link Chase} configured for reasoning, or
	 *         {@code null} if an error occurs.
	 */
	static Chase buildAndGetChase(FactBase fb, RuleBase rb,
			InteGraalKeywords.Algorithms.Parameters.Chase.Checker chaseType, IGParameter<InteGraalKeywords, ?> params) {
		IInputDataScenario inputKB = IInputDataScenario.KB(fb, rb);
		IAlgorithmParameters algoParam = IAlgorithmParameters.Chase(chaseType, params);
		var builder = createBuilderFrom(inputKB, algoParam);
		return builder.buildOrGetChase();
	}

	/**
	 * Creates a query rewriter for Ontology-Mediated Query (OMQ) Rewriting.
	 *
	 * @param rb              the rule base for rewriting.
	 * @param queries         a collection of queries to be rewritten.
	 * @param ruleCompilation the rule compilation method (optional, null means no
	 *                        compilation).
	 * @param conds           he external halting conditions (e.g., timeout, rank).
	 * @return an instance of {@link QueryRewriterWitMultiEvaluator} configured for query rewriting,
	 *         or {@code null} in case of an error.
	 */
	static QueryRewriterWitMultiEvaluator buildAndGetOMQRewriter(RuleBase rb, Collection<Query> queries,
																 InteGraalKeywords.Algorithms.Parameters.Compilation ruleCompilation, ExternalHaltingCondition... conds) {
		IInputDataScenario inputOMQ = IInputDataScenario.OMQ(rb, queries);
		IAlgorithmParameters algoParam = IAlgorithmParameters.Rewriting(ruleCompilation,
				new ExternalAlgorithmHaltingConditions(conds));
		var builder = createBuilderFrom(inputOMQ, algoParam);
		return builder.buildOrGetRewriter();
	}

	/**
	 * Creates a query rewriter for Ontology-Mediated Query (OMQ) Rewriting.
	 *
	 * @param rb              the rule base for rewriting.
	 * @param queries         a collection of queries to be rewritten.
	 * @param ruleCompilation the rule compilation method (optional, null means no
	 *                        compilation).
	 * @param params          the external halting conditions (e.g., timeout, rank)
	 *                        and the compilation type
	 * @return an instance of {@link QueryRewriterWitMultiEvaluator} configured for query rewriting,
	 *         or {@code null} in case of an error.
	 */
	static QueryRewriterWitMultiEvaluator buildAndGetOMQRewriter(RuleBase rb, Collection<Query> queries,
																 InteGraalKeywords.Algorithms.Parameters.Compilation ruleCompilation,
																 IGParameter<InteGraalKeywords, ?>... params) {
		IInputDataScenario inputOMQ = IInputDataScenario.OMQ(rb, queries);
		IAlgorithmParameters algoParam = IAlgorithmParameters.Rewriting(params);
		var builder = createBuilderFrom(inputOMQ, algoParam);
		return builder.buildOrGetRewriter();
	}

	/**
	 * Creates a component builder for rule compilation.
	 *
	 * @param ruleBase        the rule base for rewriting.
	 * @param ruleCompilation the rule compilation method.
	 * @param conds           he external halting conditions (e.g., timeout, rank).
	 * @return an instance of {@link RuleCompilationResult} configured for rule
	 *         compilation, or {@code null} in case of an error.
	 */
	static RuleCompilationResult buildAndGetRuleCompilation(RuleBase ruleBase,
			InteGraalKeywords.Algorithms.Parameters.Compilation ruleCompilation, ExternalHaltingCondition... conds) {
		IInputDataScenario inputRuleBase = IInputDataScenario.Compilation(ruleBase);
		IAlgorithmParameters algoParam = IAlgorithmParameters.Compilation(ruleCompilation,
				new ExternalAlgorithmHaltingConditions(conds));
		var builder = createBuilderFrom(inputRuleBase, algoParam);
		return builder.getRuleCompilationResult();
	}

	
	/**
	 * Creates a default component builder.
	 *
	 * @param inputKB   the input data scenario for the builder.
	 * @param algoParam the algorithm parameters.
	 * @param init      whether to initialize the builder immediately. prefer
	 *                  setting it to false if the factbase is large, and then call
	 *                  method init()
	 * @return a {@link ComponentBuilder} configured based on the provided inputs.
	 */
	static ComponentBuilder createBuilderFrom(IInputDataScenario inputKB, IAlgorithmParameters algoParam,
			boolean init) {
		ComponentBuilder builder = new ComponentBuilder(inputKB, algoParam);
		if (init) {
			builder.init();
		}
		return builder;
	}

	/**
	 * Creates a default component builder. By default, it initializes all
	 * components by setting init=true.
	 *
	 * @param inputKB   the input data scenario for the builder.
	 * @param algoParam the algorithm parameters.
	 * @return a {@link ComponentBuilder} configured based on the provided inputs.
	 */
	static ComponentBuilder createBuilderFrom(IInputDataScenario inputKB, IAlgorithmParameters algoParam) {
		return createBuilderFrom(inputKB, algoParam, true);
	}

	///////////////////////////////
	/// PUBLIC METHODS
	///////////////////////////////

	/**
	 * Gets the fact base created by the component.
	 *
	 * @return the fact base created by the component.
	 */
	FactBase getFactbase();

	/**
	 * Gets the rule base created by the component.
	 *
	 * @return the rule base created by the component.
	 */
	RuleBase getRulebase();

	/**
	 * Gets the queries handled by the component.
	 *
	 * @return the collection of queries handled by the component.
	 */
	Collection<Query> getQueries();

	/**
	 * Gets the result of the rule compilation process.
	 *
	 * @return the result of the rule compilation.
	 */
	RuleCompilationResult getRuleCompilationResult();

	/**
	 * Builds or gets the Chase algorithm component.
	 *
	 * @return the {@link Chase} component.
	 */
	Chase buildOrGetChase();

	/**
	 * Builds or gets the query rewriter component.
	 *
	 * @return the {@link QueryRewriterWitMultiEvaluator} component.
	 */
	QueryRewriterWitMultiEvaluator buildOrGetRewriter();

	/**
	 * Builds or gets the query answering algorithm component.
	 *
	 * @return the {@link QueryEvaluatorWithMultiEvaluator} component.
	 */
	QueryEvaluatorWithMultiEvaluator buildOrGetQueryAnsweringAlgorithm();

	/**
	 * Initializes all components of the builder.
	 */
	void init();
}
