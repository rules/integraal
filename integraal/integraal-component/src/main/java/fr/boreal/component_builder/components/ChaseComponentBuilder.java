package fr.boreal.component_builder.components;

import fr.boreal.component_builder.api.algorithm.IAlgorithmParameters;
import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.forward_chaining.chase.ChaseBuilder;
import fr.boreal.forward_chaining.chase.halting_condition.CreatedFactsAtPreviousStep;
import fr.boreal.forward_chaining.chase.halting_condition.HasRulesToApply;
import fr.boreal.forward_chaining.chase.halting_condition.LimitNumberOfStep;
import fr.boreal.forward_chaining.chase.halting_condition.Timeout;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;

/**
 * Builds chase objects from configurations.
 */
public class ChaseComponentBuilder {

	/**
	 * Initializes the chase algorithm based on the current configuration. This
	 * method prepares the chase by loading data, setting up the rulebase, and
	 * configuring the chase algorithm.
	 * 
	 * @param fb          factbase
	 * @param rb          rulebase
	 * @param chaseParams chase parameters
	 *
	 * @return the prepared instance of the Chase algorithm.
	 */
	public static Chase prepareAndGetChaseFrom(FactBase fb, RuleBase rb, IAlgorithmParameters chaseParams) {

		ChaseBuilder builder = ChaseBuilder.defaultBuilder(fb, rb);

		// SCHEDULER

		if (chaseParams.getScheduler().isPresent())
			switch (chaseParams.getScheduler().get()) {
			case GRD -> builder.useGRDRuleScheduler();
			case NAIVE_SCHEDULER -> builder.useNaiveRuleScheduler();
			}

		// REDUNDANCY CHECKER

		if (chaseParams.getCriterion().isPresent())
			switch (chaseParams.getCriterion().get()) {
			case OBLIVIOUS -> builder.useObliviousChecker();
			case SEMI_OBLIVIOUS -> builder.useSemiObliviousChecker();
			case RESTRICTED -> builder.useRestrictedChecker();
			case EQUIVALENT -> builder.useEquivalentChecker();
			case TRUE -> builder.useAlwaysTrueChecker();
			}

		// COMPUTER
		if (chaseParams.getComputer().isPresent())
			switch (chaseParams.getComputer().get()) {
			case NAIVE_COMPUTER -> builder.useNaiveComputer();
			case SEMI_NAIVE -> builder.useSemiNaiveComputer();
			case TWO_STEP -> builder.useTwoStepComputer();
			}
		
		// APPLIER
		if (chaseParams.getRuleApplier().isPresent())
			switch (chaseParams.getRuleApplier().get()) {
			case SOURCE_DELEGATED_DATALOG -> builder.useSourceDelegatedDatalogApplier();
			case BREADTH_FIRST -> builder.useBreadthFirstApplier();
			case PARALLEL -> builder.useParallelApplier();
			case MULTI_THREAD_PARALLEL -> builder.useMultiThreadRuleApplier();
			}

		// NAMER
		if (chaseParams.getSkolemization().isPresent())
			switch (chaseParams.getSkolemization().get()) {
			case FRESH -> builder.useFreshNaming();
			case BODY -> builder.useBodySkolem();
			case FRONTIER -> builder.useFrontierSkolem();
			case FRONTIER_PIECE -> builder.useFrontierByPieceSkolem();
			}

		// prepare halting conditions
		builder.addHaltingConditions(new CreatedFactsAtPreviousStep(), new HasRulesToApply());

		var rank = chaseParams.getRank();
		var timeout = chaseParams.getTimeout();

		if (rank.isPresent() && rank.get() >= 0) {
			int steps = (rank.get() > Integer.MAX_VALUE) ? Integer.MAX_VALUE : (int) rank.get().intValue();
			builder.addHaltingConditions(new LimitNumberOfStep(steps));
		}
		if (timeout.isPresent() && timeout.get().toMillis() >= 0) {
			builder.addHaltingConditions(new Timeout(timeout.get().toMillis()));
		}

		Chase customizedChase = builder.build().get();

		return customizedChase;

	}


}
