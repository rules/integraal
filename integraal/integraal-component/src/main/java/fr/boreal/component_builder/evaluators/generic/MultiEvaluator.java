package fr.boreal.component_builder.evaluators.generic;

import java.util.Collection;
import java.util.function.BiFunction;
import java.util.function.Function;

import fr.boreal.component_builder.evaluators.generic.processors.BatchProcessor;
import fr.boreal.component_builder.evaluators.generic.processors.LazyIterator;
import org.slf4j.Logger;

import fr.boreal.component_builder.externalHaltingConditions.ExternalAlgorithmHaltingConditions;

public abstract class MultiEvaluator<InputType, OutputType> {

	protected static Logger LOG;

	private final Collection<InputType> input;
	private final Function<InputType, OutputType> batchFunction;
	private final Function<InputType, OutputType> lazyFunction;

	private final ExternalAlgorithmHaltingConditions externalHaltingConditions;

	private final BiFunction<InputType, String, OutputType> outputIfTimeout;

	/**
	 * @param input
	 * @param function
	 * @param externalHaltingConditions
	 * @param outputIfTimeout
	 */
	public MultiEvaluator(Collection<InputType> input, Function<InputType, OutputType> function,
			ExternalAlgorithmHaltingConditions externalHaltingConditions,
			BiFunction<InputType, String, OutputType> outputIfTimeout) {
		this.input = input;
		this.batchFunction = function;
		this.lazyFunction = function;
		this.outputIfTimeout = outputIfTimeout;
		this.externalHaltingConditions = externalHaltingConditions;
	}

	/**
	 * @param input
	 * @param batchFunction
	 * @param lazyFunction
	 * @param externalHaltingConditions
	 * @param outputIfTimeout
	 */
	public MultiEvaluator(Collection<InputType> input, Function<InputType, OutputType> batchFunction,
			Function<InputType, OutputType> lazyFunction, ExternalAlgorithmHaltingConditions externalHaltingConditions,
			BiFunction<InputType, String, OutputType> outputIfTimeout) {
		this.input = input;
		this.batchFunction = batchFunction;
		this.lazyFunction = lazyFunction;
		this.externalHaltingConditions = externalHaltingConditions;
		this.outputIfTimeout = outputIfTimeout;
	}

	/**
	 * Evaluates all queries in a batch and returns a list of QueryEvaluationOutput.
	 *
	 * @return A list of QueryEvaluationOutput, each containing a query and its
	 *         corresponding iterator of substitutions.
	 */
	public final Iterable<OutputType> batchEvaluate() {
		return new BatchProcessor<>(input, batchFunction, externalHaltingConditions, outputIfTimeout).processBatch();
	}

	/**
	 * Provides lazy evaluation for the queries, yielding QueryEvaluationOutput for
	 * each query.
	 *
	 * @return An iterable for lazy evaluation of query results, each as a
	 *         QueryEvaluationOutput.
	 */
	public final Iterable<OutputType> lazyEvaluate() {
		return new LazyIterator<>(input, lazyFunction);
	}

}
