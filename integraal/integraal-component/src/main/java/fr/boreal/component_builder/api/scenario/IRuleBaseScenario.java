package fr.boreal.component_builder.api.scenario;

import fr.boreal.model.kb.api.RuleBase;

import java.util.Collection;
import java.util.Optional;

/**
 * The {@code IRuleBaseScenario} interface defines methods for managing the rule
 * base in a knowledge base scenario.
 *
 */
interface IRuleBaseScenario {

	/**
	 * Retrieves the path(s) to the rule base file(s).
	 *
	 * @return an {@link Optional} containing the file paths, or empty if not set
	 */
	Optional<Collection<String>> getRulebasePath();

	/**
	 * Sets the path to the rule base file, if not already set.
	 *
	 * @param rulebasepath the path to the rule base file
	 */
	void setRulebasePaths(String... rulebasepath);

	/**
	 * Retrieves the rule base object.
	 *
	 * @return an {@link Optional} containing the {@link RuleBase}, or empty if not
	 *         set
	 */
	Optional<RuleBase> getRuleBase();

	/**
	 * Sets the rule base object, if not already set.
	 *
	 * @param rulebase the {@link RuleBase} object to set
	 */
	void setRulebase(RuleBase rulebase);
	
	/**
	 * Sets the path for the file holding the hynrid rule base, but only if the
	 * rulebase has not been already set.
	 *
	 * @param rulebasepath_sat for saturation
	 * @param rulebasepath_rew for rewriting
	 *
	 */

	void setHybridRulebasePaths(Collection<String> rulebasepath_sat, Collection<String> rulebasepath_rew);

	/**
	 * Sets the hybrid rule base fact base object, but only if the rulebase has not
	 * been already set.
	 *
	 * @param rb_sat rule base for saturation
	 * @param rb_rew rule base for rewriting
	 *
	 */

	void setHybridRulebase(RuleBase rb_sat, RuleBase rb_rew);

	/**
	 * Gets the hybrid rule base for saturation.
	 * 
	 * @return the rulebase for saturation
	 *
	 */

	Optional<RuleBase> getHybridRulebaseSat();

	/**
	 * Gets the hybrid rule base for saturation.
	 * 
	 * @return the rulebase for saturation
	 *
	 */

	Optional<RuleBase> getHybridRulebaseRew();

	/**
	 * Gets the paths to the files defining the hybrid rule base for saturation.
	 * 
	 * @return the rulebase for saturation
	 *
	 */

	Optional<Collection<String>> getHybridRulebaseSatPaths();

	/**
	 * Gets the paths to the files defining the hybrid rule base for rewriting.
	 * 
	 * @return the rulebase for rewriting
	 *
	 */

	Optional<Collection<String>> getHybridRulebaseRewPaths();

}
