package fr.boreal.component_builder.api.algorithm;

import java.util.Map;
import java.util.Optional;

import fr.boreal.configuration.keywords.InteGraalKeywords.InternalStorageConfiguration;

/**
 * Interface for storage parameters.
 */
interface IStorageParameters {

	boolean usesStorage();

	Optional<InternalStorageConfiguration.DBType> getStorageType();

	IAlgorithmParameters setStorageType(InternalStorageConfiguration.DBType storageName);

	Optional<InternalStorageConfiguration.DriverType> getDBDriver();

	IAlgorithmParameters setDBDriver(InternalStorageConfiguration.DriverType dbDriverName);

	Optional<Map<InternalStorageConfiguration.DBMSDriverParameters, String>> getDBMSDriverParameters();

	IAlgorithmParameters setDBMSDriverParameters(InternalStorageConfiguration.DBMSDriverParameters paramName,
			String value);

	Optional<InternalStorageConfiguration.StorageLayout> getDBStrategy();

	IAlgorithmParameters setDBStrategy(InternalStorageConfiguration.StorageLayout strategyName);
}
