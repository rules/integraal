package fr.boreal.component_builder.evaluators.query_rewriting.auxiliary;

import java.util.Optional;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.boreal.backward_chaining.api.BackwardChainingAlgorithm;
import fr.boreal.backward_chaining.pure.PureRewriter;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.impl.UnionFOQuery;

/**
 * A class to encapsulate the function of rewriting a query against a ruleset
 * and via a certain algorithm and producing a RewritingResult.
 */
public class DefaultRewritingFunction implements Function<RewritingInput, RewritingOutput> {

	static final Logger LOG = LoggerFactory.getLogger(DefaultRewritingFunction.class);

	@Override
	public RewritingOutput apply(RewritingInput rewritingInput) {

		LOG.debug("rewriting : {}", rewritingInput);

		BackwardChainingAlgorithm algorithm = new PureRewriter(rewritingInput.compilation());

		UnionFOQuery rewritings =

				switch (rewritingInput.query()) {

				case FOQuery<?> q -> algorithm.rewrite(q, rewritingInput.rules());

				case UnionFOQuery q-> algorithm.rewrite(q, rewritingInput.rules());
				
				default -> throw new IllegalArgumentException("Unsupported Query Type: " + rewritingInput.query());
				
				}

		;
		return new RewritingOutput(rewritingInput.query(), rewritings, Optional.empty());
	}

}
