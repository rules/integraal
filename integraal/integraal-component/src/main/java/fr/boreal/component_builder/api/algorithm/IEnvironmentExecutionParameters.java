package fr.boreal.component_builder.api.algorithm;

import java.time.Duration;
import java.util.Optional;

import fr.boreal.component_builder.externalHaltingConditions.ExternalAlgorithmHaltingConditions;

interface IEnvironmentExecutionParameters {

	ExternalAlgorithmHaltingConditions getExternalHaltingConditions();

	IAlgorithmParameters setExternalHaltingConditions(ExternalAlgorithmHaltingConditions hc);

	IAlgorithmParameters setMax(Integer rank) ;
	IAlgorithmParameters setRank(Integer rank) ;
	IAlgorithmParameters setTimeout(Duration timeout) ;

	Optional<Integer> getMax();
	Optional<Integer> getRank();
	Optional<Duration> getTimeout();


}
