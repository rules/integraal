package fr.boreal.component_builder.api.algorithm;

import fr.boreal.component_builder.AlgorithmParameters;
import fr.boreal.component_builder.externalHaltingConditions.ExternalAlgorithmHaltingConditions;
import fr.boreal.configuration.keywords.InteGraalKeywords;
import fr.boreal.configuration.keywords.InteGraalKeywords.Algorithms.Parameters.Compilation;
import fr.boreal.configuration.parameters.IGParameter;
import fr.lirmm.boreal.util.enumerations.EnumUtils;

/**
 * Contains parameters for an InteGraal algorithm or service.
 */
public interface IAlgorithmParameters extends IStorageParameters, IAnswerType, IForwardChainingParameters,
		IBackwardChainingParameters, IHybridReasoningParameters, IEnvironmentExecutionParameters {

	/**
	 * @return a description of the algorithm configuration.
	 */
	String getName();

	/**
	 * @return the algorithm type.
	 */
	InteGraalKeywords.Algorithms getAlgorithm();

	/////////////////////////////////////////
	//// STATIC METHODS (IGParameters)
	/////////////////////////////////////////

	/**
	 * Creates an {@code IAlgorithmParameters} instance for query answering with a
	 * specified timeout.
	 *
	 * @param params the conditions to force algorithm termination or storage type
	 * @return A new {@code IAlgorithmParameters} instance configured for query
	 *         answering.
	 */
	public static IAlgorithmParameters QA(IGParameter<InteGraalKeywords, ?>... params) {
		return new AlgorithmParameters("QA", InteGraalKeywords.Algorithms.QUERY_ANSWERING, params);
	}

	/**
	 * Creates an {@code IAlgorithmParameters} instance for a Chase-based reasoning
	 * algorithm with the specified parameters.
	 *
	 * @param params for the chase, storage and forcing termination
	 * @return A new {@code IAlgorithmParameters} instance configured for Chase
	 *         reasoning.
	 */
	public static IAlgorithmParameters Chase(IGParameter<InteGraalKeywords, ?>... params) {
		return new AlgorithmParameters("Chase", InteGraalKeywords.Algorithms.KB_CHASE, params);
	}

	/**
	 * Creates an {@code IAlgorithmParameters} instance for OMQA Chase-based
	 * reasoning with the specified parameters.
	 *
	 * @param params for the chase, storage, query answering, and forcing
	 *               termination
	 * @return A new {@code IAlgorithmParameters} instance configured for OMQA Chase
	 *         reasoning.
	 */
	public static IAlgorithmParameters OMQAChase(IGParameter<InteGraalKeywords, ?>... params) {
		return new AlgorithmParameters("OMQA Chase", InteGraalKeywords.Algorithms.OMQA_CHASE, params);
	}

	/**
	 * Creates an {@code IAlgorithmParameters} instance for rewriting-based
	 * reasoning with the specified parameters.
	 *
	 * @param params for the rewriting and forcing termination
	 * @return A new {@code IAlgorithmParameters} instance configured for rewriting.
	 */
	public static IAlgorithmParameters Rewriting(IGParameter<InteGraalKeywords, ?>... params) {
		return new AlgorithmParameters("Query Rewriting", InteGraalKeywords.Algorithms.OMQ_REWRITING, params);
	}

	/**
	 * Creates an {@code IAlgorithmParameters} instance for OMQA rewriting with the
	 * specified parameters.
	 *
	 * @param params for the rewriting, storage, query answering, and forcing
	 *               termination
	 * @return A new {@code IAlgorithmParameters} instance configured for OMQA
	 *         rewriting.
	 */
	public static IAlgorithmParameters OMQARewriting(IGParameter<InteGraalKeywords, ?>... params) {
		return new AlgorithmParameters("OMQA Rewriting", InteGraalKeywords.Algorithms.OMQA_REW, params);
	}

	/**
	 * Creates an {@code IAlgorithmParameters} instance for hybrid reasoning with
	 * the specified parameters.
	 *
	 * @param params for the rewriting, chase, storages, and forcing termination
	 * @return A new {@code IAlgorithmParameters} instance configured for hybrid
	 *         reasoning.
	 */
	public static IAlgorithmParameters OMQAHybrid(IGParameter<InteGraalKeywords, ?>... params) {
		return new AlgorithmParameters("OMQA Hybrid", InteGraalKeywords.Algorithms.QUERY_ANSWERING_VIA_HYBRID_STRATEGY,
				params);
	}

	/**
	 * Creates an {@code IAlgorithmParameters} instance for rule compilation with
	 * the specified parameters.
	 *
	 * @param params for the compilation and forcing termination
	 * @return A new {@code IAlgorithmParameters} instance configured for rule
	 *         compilation.
	 */
	public static IAlgorithmParameters Compilation(IGParameter<InteGraalKeywords, ?>... params) {
		return new AlgorithmParameters("Compilation", InteGraalKeywords.Algorithms.RULE_COMPILATION, params);
	}

	/**
	 * Creates an {@code IAlgorithmParameters} instance for query explanation with
	 * the specified parameters.
	 *
	 * @param params for the explanation process and forcing termination
	 * @return A new {@code IAlgorithmParameters} instance configured for query
	 *         explanation.
	 */
	public static IAlgorithmParameters Explanation(IGParameter<InteGraalKeywords, ?>... params) {
		return new AlgorithmParameters("Explanation", InteGraalKeywords.Algorithms.QUERY_EXPLANATION, params);
	}

	/////////////////////////////////////////
	/////////////////////////////////////////
	////
	////
	////
	////
	//// OLD STATIC METHODS
	////
	////
	////
	////
	/////////////////////////////////////////
	/////////////////////////////////////////

	/**
	 * Creates an {@code IAlgorithmParameters} instance for query answering with a
	 * specified timeout.
	 *
	 * @param hc the conditions to force algorithm termination
	 * @return A new {@code IAlgorithmParameters} instance configured for query
	 *         answering.
	 */
	public static IAlgorithmParameters QA(ExternalAlgorithmHaltingConditions hc) {
		return new AlgorithmParameters("QA", InteGraalKeywords.Algorithms.QUERY_ANSWERING)
				.setExternalHaltingConditions(hc);
	}

	/**
	 * Creates an {@code IAlgorithmParameters} instance for a Chase-based reasoning
	 * algorithm with the specified parameters.
	 *
	 * @param chaseType The type of Chase checker.
	 * @param hc        the conditions to force algorithm termination
	 * @return A new {@code IAlgorithmParameters} instance configured for Chase
	 *         reasoning.
	 */
	public static IAlgorithmParameters Chase(InteGraalKeywords.Algorithms.Parameters.Chase.Checker chaseType,
			ExternalAlgorithmHaltingConditions hc) {

		IAlgorithmParameters p = switch (chaseType) {
		case null -> new AlgorithmParameters("Default Chase", InteGraalKeywords.Algorithms.KB_CHASE);
		default -> new AlgorithmParameters("Chase with " + chaseType, InteGraalKeywords.Algorithms.KB_CHASE)
				.setCriterion(chaseType);
		};

		return p.setExternalHaltingConditions(hc);

	}

	/**
	 * Creates an {@code IAlgorithmParameters} instance for a Chase-based reasoning
	 * algorithm with the specified parameters.
	 *
	 * @param chaseType The type of Chase checker.
	 * @param params    the conditions to force algorithm termination
	 * @return A new {@code IAlgorithmParameters} instance configured for Chase
	 *         reasoning.
	 */
	public static IAlgorithmParameters Chase(InteGraalKeywords.Algorithms.Parameters.Chase.Checker chaseType,
			IGParameter<InteGraalKeywords, ?> params) {

		return switch (chaseType) {
		case null -> new AlgorithmParameters("Default Chase", InteGraalKeywords.Algorithms.KB_CHASE, params);
		default -> new AlgorithmParameters("Chase with " + chaseType, InteGraalKeywords.Algorithms.KB_CHASE, params)
				.setCriterion(chaseType);
		};

	}

	/**
	 * Creates an {@code IAlgorithmParameters} instance for a Chase-based reasoning
	 * algorithm with the specified parameters.
	 *
	 * @param chaseType The type of Chase checker.
	 * @param hc        the conditions to force algorithm termination
	 * @return A new {@code IAlgorithmParameters} instance configured for Chase
	 *         reasoning.
	 */
	public default IAlgorithmParameters Chase(String chaseType, ExternalAlgorithmHaltingConditions hc) {

		var criterion = EnumUtils.getEnumerationFromString(chaseType,
				InteGraalKeywords.Algorithms.Parameters.Chase.Checker.class);

		return Chase(criterion, hc);

	}

	/**
	 * Creates an {@code IAlgorithmParameters} instance for OMQA Chase-based
	 * reasoning with the specified parameters.
	 *
	 * @param chaseType The type of Chase checker as a string.
	 * @param hc        the conditions to force algorithm termination
	 * @return A new {@code IAlgorithmParameters} instance configured for OMQA Chase
	 *         reasoning.
	 */
	public static IAlgorithmParameters OMQAChase(String chaseType, ExternalAlgorithmHaltingConditions hc) {

		var criterion = EnumUtils.getEnumerationFromString(chaseType,
				InteGraalKeywords.Algorithms.Parameters.Chase.Checker.class);

		return OMQAChase(criterion, hc);
	}

	/**
	 * Creates an {@code IAlgorithmParameters} instance for OMQA Chase-based
	 * reasoning with the specified parameters.
	 *
	 * @param chaseType The type of Chase checker as a string.
	 * @param hc        the conditions to force algorithm termination
	 * @return A new {@code IAlgorithmParameters} instance configured for OMQA Chase
	 *         reasoning.
	 */
	public static IAlgorithmParameters OMQAChase(InteGraalKeywords.Algorithms.Parameters.Chase.Checker chaseType,
			ExternalAlgorithmHaltingConditions hc) {

		IAlgorithmParameters p = switch (chaseType) {
		case null -> new AlgorithmParameters("Default OMQA Chase ", InteGraalKeywords.Algorithms.OMQA_CHASE);
		default -> new AlgorithmParameters("OMQA Chase with  " + chaseType, InteGraalKeywords.Algorithms.OMQA_CHASE)
				.setCriterion(chaseType);
		};

		return p.setExternalHaltingConditions(hc);
	}

	/**
	 * Creates an {@code IAlgorithmParameters} instance for rewriting-based
	 * reasoning with the specified parameters.
	 *
	 * @param ruleCompilation The compilation method for rewriting.
	 * @param hc              the conditions to force algorithm termination
	 * @return A new {@code IAlgorithmParameters} instance configured for rewriting.
	 */
	public static IAlgorithmParameters Rewriting(InteGraalKeywords.Algorithms.Parameters.Compilation ruleCompilation,
			ExternalAlgorithmHaltingConditions hc) {
		IAlgorithmParameters p;
		if (ruleCompilation == null
				|| ruleCompilation.equals(InteGraalKeywords.Algorithms.Parameters.Compilation.NO_COMPILATION)) {
			p = new AlgorithmParameters("Rewriting without compilation", InteGraalKeywords.Algorithms.OMQ_REWRITING);
		} else {
			p = new AlgorithmParameters("Rewriting with " + ruleCompilation,
					InteGraalKeywords.Algorithms.OMQ_REWRITING);
			p.setCompilation(ruleCompilation);
		}
		p.setExternalHaltingConditions(hc);
		return p;
	}

	/**
	 * Creates an {@code IAlgorithmParameters} instance for OMQA rewriting with the
	 * specified parameters.
	 *
	 * @param compilation The compilation method for rewriting.
	 * @param hc          the conditions to force algorithm termination
	 * @return A new {@code IAlgorithmParameters} instance configured for OMQA
	 *         rewriting.
	 */
	public static IAlgorithmParameters OMQARewriting(Compilation compilation, ExternalAlgorithmHaltingConditions hc) {
		IAlgorithmParameters p;

		if (compilation == null || compilation == Compilation.NO_COMPILATION) {
			p = new AlgorithmParameters("OMQA via Rewriting without compilation",
					InteGraalKeywords.Algorithms.OMQA_REW);
		} else {
			p = new AlgorithmParameters("OMQA via Rewriting with " + compilation,
					InteGraalKeywords.Algorithms.OMQA_REW);
			p.setCompilation(compilation);
		}

		p.setExternalHaltingConditions(hc);
		return p;
	}

	/**
	 * Creates an {@code IAlgorithmParameters} instance for OMQA rewriting with the
	 * specified parameters.
	 *
	 * @param ruleCompilation The compilation method for rewriting.
	 * @param hc              the conditions to force algorithm termination
	 * @return A new {@code IAlgorithmParameters} instance configured for OMQA
	 *         rewriting.
	 */
	public static IAlgorithmParameters OMQARewriting(String ruleCompilation, ExternalAlgorithmHaltingConditions hc) {
		var compil = EnumUtils.getEnumerationFromString(ruleCompilation,
				InteGraalKeywords.Algorithms.Parameters.Compilation.class);
		return OMQARewriting(compil, hc);
	}

	/**
	 * Creates an {@code IAlgorithmParameters} instance for hybrid reasoning with
	 * the specified parameters.
	 *
	 * @param chaseType       The type of Chase checker.
	 * @param compilationType The type of rule compilation.
	 * @param hc              the conditions to force algorithm termination
	 * @return A new {@code IAlgorithmParameters} instance configured for hybrid
	 *         reasoning.
	 */
	public static IAlgorithmParameters OMQAHybrid(String chaseType, String compilationType,
			ExternalAlgorithmHaltingConditions hc) {
		IAlgorithmParameters p;
		p = new AlgorithmParameters("OMQA Hybrid", InteGraalKeywords.Algorithms.QUERY_ANSWERING_VIA_HYBRID_STRATEGY);

		p.setCompilation(EnumUtils.findEnumFromString(compilationType,
				InteGraalKeywords.Algorithms.Parameters.Compilation.class));
		p.setCriterion(
				EnumUtils.findEnumFromString(chaseType, InteGraalKeywords.Algorithms.Parameters.Chase.Checker.class));

		p.setExternalHaltingConditions(hc);
		return p;
	}

	/**
	 * Creates an {@code IAlgorithmParameters} instance for rule compilation with
	 * the specified parameters.
	 *
	 * @param ruleCompilation The compilation method for rule compilation.
	 * @param hc              the conditions to force algorithm termination
	 * @return A new {@code IAlgorithmParameters} instance configured for rule
	 *         compilation.
	 */
	public static IAlgorithmParameters Compilation(InteGraalKeywords.Algorithms.Parameters.Compilation ruleCompilation,
			ExternalAlgorithmHaltingConditions hc) {
		return new AlgorithmParameters("Compilation", InteGraalKeywords.Algorithms.RULE_COMPILATION)
				.setCompilation(ruleCompilation).setExternalHaltingConditions(hc);
	}

	/**
	 * Creates an {@code IAlgorithmParameters} instance for query explanation with
	 * the specified parameters.
	 *
	 * @param hc the conditions to force algorithm termination
	 * @return A new {@code IAlgorithmParameters} instance configured for query
	 *         explanation.
	 */
	public static IAlgorithmParameters Explanation(ExternalAlgorithmHaltingConditions hc) {
		IAlgorithmParameters p = new AlgorithmParameters("Explanation", InteGraalKeywords.Algorithms.QUERY_EXPLANATION);
		p.setExternalHaltingConditions(hc);
		return p;
	}

	/**
	 * Sets the value of a parameter based on the provided enum constant.
	 *
	 * @param enumValue The enum constant to be set as a parameter value.
	 * @return
	 */
	IAlgorithmParameters setParameter(Enum<?> enumValue);

	/**
	 * Sets the value of a parameter based on the provided class, property name, and
	 * value.
	 *
	 * @param className    The class type of the enum which defines the property
	 *                     name.
	 * @param propertyName The name of the property to be set.
	 * @param value        The value to be assigned to the property.
	 */
	IAlgorithmParameters setOpenParameterFromEnumeration(Class<? extends Enum<?>> className, String propertyName,
			String value);

	/**
	 * Sets the value of a parameter based on the provided enumeration, property
	 * name, and value.
	 *
	 * @param className    The parent enum which defines the property name.
	 * @param propertyName The name of the property to be set.
	 * @param value        The value to be assigned to the property.
	 */
	IAlgorithmParameters setOpenParameterFromEnumeration(Enum<?> className, String propertyName, String value);

	/**
	 * Sets the value of an open parameter which is not defined within a parent
	 * enumeration
	 *
	 * @param k     The property name
	 * @param value The value to be assigned to the property.
	 */
	IAlgorithmParameters setOpenParameter(InteGraalKeywords k, Object value);

}
