package fr.boreal.component_builder.api.algorithm;

import fr.boreal.configuration.keywords.InteGraalKeywords;
import fr.boreal.configuration.keywords.InteGraalKeywords.Algorithms;
import fr.boreal.configuration.keywords.InteGraalKeywords.InternalStorageConfiguration;
import fr.boreal.configuration.keywords.InteGraalKeywords.MonitoringOperations;
import fr.boreal.configuration.keywords.InteGraalKeywords.SupportedFileExtensions;
import fr.boreal.configuration.keywords.InteGraalKeywords.Algorithms.*;
import fr.boreal.configuration.keywords.InteGraalKeywords.Algorithms.Parameters.*;
import fr.boreal.configuration.parameters.IGParameter;

import java.time.Duration;

public class ParameterExamples {
    // -------------------------------------------------------
    // CLOSED PARAMETERS (enum-based)
    // -------------------------------------------------------

    public static final IGParameter<InteGraalKeywords, Algorithms> ALGORITHMS_PARAM =
            new IGParameter<>(InteGraalKeywords.ALGORITHMS, Algorithms.KB_CHASE);

    public static final IGParameter<InteGraalKeywords, Answers> ANSWERS_PARAM_COUNT_ONLY =
            new IGParameter<>(InteGraalKeywords.ANSWERS, Answers.COUNT_ONLY);

    public static final IGParameter<InteGraalKeywords, Images> IMAGES_ALL =
            new IGParameter<>(InteGraalKeywords.IMAGES, Images.ALL);

    public static final IGParameter<InteGraalKeywords, Images> IMAGES_CONSTANT =
            new IGParameter<>(InteGraalKeywords.IMAGES, Images.CONSTANT);

    public static final IGParameter<InteGraalKeywords, Compilation> COMPILATION_PARAM_ID =
            new IGParameter<>(InteGraalKeywords.COMPILATION, Compilation.ID_COMPILATION);

    public static final IGParameter<InteGraalKeywords, Chase.Scheduler> SCHEDULER_PARAM =
            new IGParameter<>(InteGraalKeywords.SCHEDULER, Chase.Scheduler.NAIVE_SCHEDULER);

    public static final IGParameter<InteGraalKeywords, Chase.Evaluator> EVALUATOR_PARAM =
            new IGParameter<>(InteGraalKeywords.EVALUATOR, Chase.Evaluator.GENERIC);

    public static final IGParameter<InteGraalKeywords, Chase.Applier> APPLIER_PARAM =
            new IGParameter<>(InteGraalKeywords.APPLIER, Chase.Applier.BREADTH_FIRST);

    public static final IGParameter<InteGraalKeywords, Chase.Transformer> TRANSFORMER_PARAM =
            new IGParameter<>(InteGraalKeywords.TRANSFORMER, Chase.Transformer.ALL);

    public static final IGParameter<InteGraalKeywords, Chase.Computer> COMPUTER_PARAM =
            new IGParameter<>(InteGraalKeywords.COMPUTER, Chase.Computer.SEMI_NAIVE);

    public static final IGParameter<InteGraalKeywords, Chase.Checker> CHECKER_PARAM_OBL =
            new IGParameter<>(InteGraalKeywords.CHECKER, Chase.Checker.OBLIVIOUS);

//    public static final IGParameter<InteGraalKeywords, Chase.Application> APPLICATION_PARAM =
//            new IGParameter<>(InteGraalKeywords.APPLICATION, Chase.Application.DIRECT);

    public static final IGParameter<InteGraalKeywords, Chase.Namer> SKOLEM_PARAM =
            new IGParameter<>(InteGraalKeywords.NAMER, Chase.Namer.FRESH);

    public static final IGParameter<InteGraalKeywords, Algorithms.Parameters.HybridTypes> HYBRIDTYPES_PARAM =
            new IGParameter<>(InteGraalKeywords.HYBRIDTYPES, Algorithms.Parameters.HybridTypes.MAX_FES);

    public static final IGParameter<InteGraalKeywords, InternalStorageConfiguration.DBType> DBTYPE_PARAM_SQL =
            new IGParameter<>(InteGraalKeywords.DBTYPE, InternalStorageConfiguration.DBType.SQL);

    public static final IGParameter<InteGraalKeywords, InternalStorageConfiguration.DriverType> DRIVERTYPE_PARAM =
            new IGParameter<>(InteGraalKeywords.DRIVERTYPE, InternalStorageConfiguration.DriverType.PostgreSQL);

    public static final IGParameter<InteGraalKeywords, InternalStorageConfiguration.StorageLayout> STORAGELAYOUT_PARAM =
            new IGParameter<>(InteGraalKeywords.STORAGELAYOUT, InternalStorageConfiguration.StorageLayout.AdHocSQL);

    public static final IGParameter<InteGraalKeywords, SupportedFileExtensions.Factbase> FACTBASE_PARAM =
            new IGParameter<>(InteGraalKeywords.FACTBASE, SupportedFileExtensions.Factbase.DLGP);

    public static final IGParameter<InteGraalKeywords, SupportedFileExtensions.Querybase> QUERYBASE_PARAM =
            new IGParameter<>(InteGraalKeywords.QUERYBASE, SupportedFileExtensions.Querybase.DLGPE);

    public static final IGParameter<InteGraalKeywords, SupportedFileExtensions.Rulebase> RULEBASE_PARAM =
            new IGParameter<>(InteGraalKeywords.RULEBASE, SupportedFileExtensions.Rulebase.OWL);

    public static final IGParameter<InteGraalKeywords, SupportedFileExtensions.Mappingbase> MAPPINGBASE_PARAM =
            new IGParameter<>(InteGraalKeywords.MAPPINGBASE, SupportedFileExtensions.Mappingbase.VD);

    public static final IGParameter<InteGraalKeywords, MonitoringOperations> MONITORINGOPERATIONS_PARAM =
            new IGParameter<>(InteGraalKeywords.MONITORINGOPERATIONS, MonitoringOperations.LOAD_FACTBASE);

    // -------------------------------------------------------
    // OPEN PARAMETERS
    // -------------------------------------------------------

    // Reusable constants
    public static final int MAX_VAL = 100;
    public static final int RANK_VAL = 42;
    public static final Duration TIMEOUT_VAL = Duration.ofSeconds(60);

    // IGParameter declarations
    public static final IGParameter<InteGraalKeywords, Integer> MAX_PARAM =
            new IGParameter<>(InteGraalKeywords.MAX, MAX_VAL);

    public static final IGParameter<InteGraalKeywords, Integer> RANK_PARAM =
            new IGParameter<>(InteGraalKeywords.RANK, RANK_VAL);

    public static final IGParameter<InteGraalKeywords, Duration> TIMEOUT_PARAM =
            new IGParameter<>(InteGraalKeywords.TIMEOUT, TIMEOUT_VAL);

    // -------------------------------------------------------
    // OPEN PARAMETERS DEFINED FROM A PARENT ENUMERATION
    // -------------------------------------------------------

    // Reusable strings
    public static final String DB_URL_VALUE      = "jdbc://my-host:5432/myDb";
    public static final String DB_PORT_VALUE     = "5432";
    public static final String DB_NAME_VALUE     = "myDb";
    public static final String DB_USER_VALUE     = "dbuser";
    public static final String DB_PASSWORD_VALUE = "secret";
    public static final String DB_CLEAR_DB_VALUE = "true";

    public static final IGParameter<InteGraalKeywords, String> DB_URL =
            new IGParameter<>(InteGraalKeywords.URL, DB_URL_VALUE);

}
