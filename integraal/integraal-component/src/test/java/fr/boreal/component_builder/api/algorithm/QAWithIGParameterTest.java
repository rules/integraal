package fr.boreal.component_builder.api.algorithm;

import fr.boreal.component_builder.AlgorithmParameters;
import fr.boreal.configuration.keywords.InteGraalKeywords;
import fr.boreal.configuration.keywords.InteGraalKeywords.Algorithms;
import fr.boreal.configuration.parameters.IGParameter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.runners.Parameterized.Parameters;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

/**
 * Demonstrates a single Parameterized Test focusing on the IAlgorithmParameters.QA(IGParameter)
 * static method. Each Arguments entry has two parts:
 * <ul>
 *   <li>The first argument is an {@link AlgorithmParameters} (expected result)</li>
 *   <li>The second argument is an array of {@code IGParameter<InteGraalKeywords, ?>}</li>
 * </ul>
 */
public class QAWithIGParameterTest {

    @Parameters
    static Stream<Arguments> dataQA() {

        AlgorithmParameters targetConfiguration = new AlgorithmParameters("QA", Algorithms.QUERY_ANSWERING);
        targetConfiguration.setResultType(Algorithms.Answers.COUNT_ONLY);
        targetConfiguration.setStorageType(InteGraalKeywords.InternalStorageConfiguration.DBType.SQL);
        targetConfiguration.setOpenParameterFromEnumeration(InteGraalKeywords.URL.getParentEnum().get(),InteGraalKeywords.URL.toString(),ParameterExamples.DB_URL_VALUE);
        targetConfiguration.setTimeout(ParameterExamples.TIMEOUT_VAL);

        IGParameter<InteGraalKeywords, ?>[] inputParams = new IGParameter[] {
                ParameterExamples.ANSWERS_PARAM_COUNT_ONLY,
                ParameterExamples.DBTYPE_PARAM_SQL,
                ParameterExamples.DB_URL,
                ParameterExamples.TIMEOUT_PARAM
        };

        // 3) Return both in an Arguments.of(...) call
        return Stream.of(
                Arguments.of(targetConfiguration, inputParams)
        );
    }

    @Parameters
    static Stream<Arguments> dataChaseOMQA() {

        AlgorithmParameters targetConfiguration = new AlgorithmParameters("OMQA Chase", Algorithms.OMQA_CHASE);
        targetConfiguration.setResultType(Algorithms.Answers.COUNT_ONLY);
        targetConfiguration.setCriterion(Algorithms.Parameters.Chase.Checker.OBLIVIOUS);
        targetConfiguration.setStorageType(InteGraalKeywords.InternalStorageConfiguration.DBType.SQL);
        targetConfiguration.setOpenParameterFromEnumeration(InteGraalKeywords.URL.getParentEnum().get(),InteGraalKeywords.URL.toString(),ParameterExamples.DB_URL_VALUE);
        targetConfiguration.setTimeout(ParameterExamples.TIMEOUT_VAL);

        IGParameter<InteGraalKeywords, ?>[] inputParams = new IGParameter[] {
                ParameterExamples.ANSWERS_PARAM_COUNT_ONLY,
                ParameterExamples.CHECKER_PARAM_OBL,
                ParameterExamples.DBTYPE_PARAM_SQL,
                ParameterExamples.DB_URL,
                ParameterExamples.TIMEOUT_PARAM
        };

        // 3) Return both in an Arguments.of(...) call
        return Stream.of(
                Arguments.of(targetConfiguration, inputParams)
        );
    }


    @Parameters
    static Stream<Arguments> dataRewOMQA() {

        AlgorithmParameters targetConfiguration = new AlgorithmParameters("OMQA Rewriting", Algorithms.OMQA_REW);
        targetConfiguration.setResultType(Algorithms.Answers.COUNT_ONLY);
        targetConfiguration.setCompilation(Algorithms.Parameters.Compilation.ID_COMPILATION);
        targetConfiguration.setStorageType(InteGraalKeywords.InternalStorageConfiguration.DBType.SQL);
        targetConfiguration.setOpenParameterFromEnumeration(InteGraalKeywords.URL.getParentEnum().get(),InteGraalKeywords.URL.toString(),ParameterExamples.DB_URL_VALUE);
        targetConfiguration.setTimeout(ParameterExamples.TIMEOUT_VAL);

        IGParameter<InteGraalKeywords, ?>[] inputParams = new IGParameter[] {
                ParameterExamples.ANSWERS_PARAM_COUNT_ONLY,
                ParameterExamples.COMPILATION_PARAM_ID,
                ParameterExamples.DBTYPE_PARAM_SQL,
                ParameterExamples.DB_URL,
                ParameterExamples.TIMEOUT_PARAM
        };

        // 3) Return both in an Arguments.of(...) call
        return Stream.of(
                Arguments.of(targetConfiguration, inputParams)
        );
    }


    /**
     * Test for QA configurations
     * @param expected    The manually created AlgorithmParameters we expect
     * @param inputParams The IGParameter(s) used as input
     */
    @DisplayName("testQAWithIGParameter()")
    @ParameterizedTest(name = "[{index}] => expected={0}, inputParams={1}")
    @MethodSource("dataQA")
    public void testQAWithIGParameter(AlgorithmParameters expected,
                                      IGParameter<InteGraalKeywords, ?>... inputParams) {

        var actual = IAlgorithmParameters.QA(inputParams);

        Assertions.assertTrue(
                AlgorithmParametersComparator.haveSameParameterValues(actual, expected),
                () -> "Expected " + expected + " but got " + actual
        );
    }


    /**
     * Test for OMQA Chase configurations
     * @param expected    The manually created AlgorithmParameters we expect
     * @param inputParams The IGParameter(s) used as input
     */
    @DisplayName("testChaseOMQQAWithIGParameter()")
    @ParameterizedTest(name = "[{index}] => expected={0}, inputParams={1}")
    @MethodSource("dataChaseOMQA")
    public void testChaseOMQQAWithIGParameter(AlgorithmParameters expected,
                                      IGParameter<InteGraalKeywords, ?>... inputParams) {

        var actual = IAlgorithmParameters.OMQAChase(inputParams);

        Assertions.assertTrue(
                AlgorithmParametersComparator.haveSameParameterValues(actual, expected),
                () -> "Expected " + expected + " but got " + actual
        );
    }


    /**
     * Test for OMQA Rew configurations
     * @param expected    The manually created AlgorithmParameters we expect
     * @param inputParams The IGParameter(s) used as input
     */
    @DisplayName("testRewOMQQAWithIGParameter()")
    @ParameterizedTest(name = "[{index}] => expected={0}, inputParams={1}")
    @MethodSource("dataRewOMQA")
    public void testRewOMQQAWithIGParameter(AlgorithmParameters expected,
                                              IGParameter<InteGraalKeywords, ?>... inputParams) {

        var actual = IAlgorithmParameters.OMQARewriting(inputParams);

        Assertions.assertTrue(
                AlgorithmParametersComparator.haveSameParameterValues(actual, expected),
                () -> "Expected " + expected + " but got " + actual
        );
    }
}
