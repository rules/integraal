# Changelog

## Legend
- :boom: : Breaking change
- :rocket: : Feature
- :bug: : Fix
- :arrow_double_up: : Dependency upgrade

## 1.6.3
- added configuration module
- improved integraal keywords 
- improved CLI

## 1.6.2

### Added
- :bug: : fix on homomorphism and core 
- :bug: : fix on component builder
- :rocket: IGParameters for keyword-based configuration

## 1.6.1

### Added

- :rocket: : shortcut methods for component builder

## 1.6.0

### Changed

- :boom: Refactor typing from `FOQuery` to `Query` 

## 1.5.1

### Added

### Changed

- :boom: Refactor `FOQuery` to remove the hierarchy by introducing genericity as a type parameter
- :boom: Refactor `FOQuery` to remove the initial substitution and replace it by a pertition of terms representing the equalities between variables of the query and other variables of the query or constants of the vocabulary
- :boom: Refactor `QueryEvaluator` to take into account a pre homomorphism. This is to replace the old behavior of the intiial substitution in the query
- :boom: Changed `ViewBuilder` to create the collection of view wrappers instead of the federation

####

- :bug: Fix javadoc comments
- :bug: Fix grammar and typo
- :bug: Improve code readability
- :bug: Migrate some old code to Java 21 standards

## 1.5.0

### Added

- :rocket: Added jar generation for integraal-commander and integraal-cli during CI/CD process
- :rocket: Added `UnionFOQuery` to represent UCQs in the correct way


### Changed

- :bug: Fix `EndUserAPI#rewrite` returning duplicate rewritings
- :bug: Changed `FederatedFactBase#size` method to return the size of the local storage instead of being unsupported as the size was needed in the component builder
- :boom: Update `EndUserAPI` to use the enumeration instead of String
- :bug: Fix method `EndUserAPI#unfold` to unfold the queries using the compilation instead of rewriting the rules
- :boom: Removed rewritings toward FOQueryDisjunction as it was not the correct usage for UCQs. This will be added back in a later version
- :bug: Update `InteGraalCommander` to use the correct version of the API with constants
- :bug: Update CLI to use the correct version of the API with constants
- :rocket: Added keywords for hybrid types
- :bug: Removed Logging by default when building applications
- :bug: Added compilation parameter to unfold component building
- :bug: Correctly print possible values of the enumeration when the given value is not found
- :bug: Added values of the initial substitution of a query back to the result of the evaluation (in case of query where the rewriting process affected a constant to an answer variable
- :bug: Changed disjonction evaluator to evaluate `UnionFOQuery` instead
- :rocket: Removed duplicate between interfaces and implementation for FOQuery

####

- :bug: Fix some javadoc comments
- :bug: Improve code readability

## 1.4.0

### Added

- :rocket: Added new module `integraal-core` with support for computing core of conjunctions
- :rocket: Added support for core treatment on chase
- :bug: Added unit tests for chase and core

### Changed

- :bug: Fix some typo in comments
- :bug: Improve code readability

## 1.3.0

### Added

- :rocket: Added new module `integraal-component` with support for high level benchmarking
- :rocket: Added support for logging tool


### Changed

- :bug: Fix issue with Source-Target one step rewritting
- :bug: Fix support for both query evaluation and homomorphism computing

## 1.2.1

### Added

- :bug: Added unit test for dlgp import using views
- :bug: Added unit test for set/list semantic for query evaluation

### Changed

- :arrow_double_up: Upgrade `dlgp-parser` version from `1.1.0` to `1.2.0`

## 1.2.0

### Added

- :rocket: Added support for new `mapping-parser` elements including WebApi access using JSONPath queries
- :rocket: Added support for new `dlgp-parser` `@view` directive

### Changed

- :rocket: Improved documentation

####

- :arrow_double_up: Upgrade `mapping-parser` version from `0.1.4` to `0.3.0`
- :arrow_double_up: Upgrade `dlgp-parser` version from `1.0.0` to `1.1.0`

## 1.1.0

### Added

- :rocket: Added new module : Component builder for high level creation of objects

####

- :rocket: Added isomorphism computing
- :rocket: Added CSV parsing
- :rocket: Added CSV direct loading into an RDBMS

### Changed

- :boom: Upgrade required Java version from `11` to `21`

####

- :boom: Changed default behaviour of the `FOQueryEvaluator.evaluate(FOQuery, FactBase)` method to be closer to domain query evaluation. Previous behaviour is available through the `FOQueryEvaluator.homomorphism(FOQuery, FactBase)` method.

####

- :arrow_double_up: Upgrade all dependencies to their latest version.

## 1.0.0

Undocumented changes ...

## 0.0.1-SNAPSHOT - Unreleased : CSV Parsing and Data encoding

### Added

- :rocket: Added new input source : CSV files and RLS configuration file
- :rocket: Added new functionality : Pre-compute the encoding of CSV files

####

- :arrow_double_up: Upgrade Collections4 dependencies from `4.1` to `4.4`

### Changed

- :boom:

## 0.0.1-SNAPSHOT - Unreleased : API, Forgetting and Rule refactoring

### Changed

- :boom: Refactoring of the `FORule` : the body is now a `FOFormula` instead of a `FOQuery`

### Added

- :rocket: Added new module : api for high level user interface
- :rocket: Added new module : redundancy for detection and removal of dependencies between rules
- :rocket: Added new module : forgetting for the process of forgetting ; remove some rules from the rulebase
- :rocket: Added new module : graal-ruleset-analysis for analysis of the rulebase (this is only an import from graal's version intended for internal use only)

####

- :rocket: Added new jar : integraal-commander for a high level usage of the main functionalities of InteGraal in a single executable jar

####

- :rocket: Added new component to the Chase : You can now choose what variables to keep as answer variables when evaluating a rule's body during the Chase

## 0.0.1-SNAPSHOT - Unreleased : Storage refactoring

### Changed

- :boom: Refactoring of the storage module. It is now splitted into two modules : storage and views
- :boom: Renamed interface `DatalogRuleDelegatable` to `DatalogDelegable` in integraal-model
- :boom: Splited interface `FactBase` into two interfaces : `Readable` and `Writable`

### Added

- :boom: Added a new module : views for interaction with datasources accessed using relational views
- :rocket: Added a new class `MatchFilter` implementing a lazy post filtering of an iterator following a call to the `match` method in integraal-util
- :rocket: Added automatic generation of the full jar during CI/CD
- :rocket: Added method `RuleBase#add(FORule)` to add a rule to the rulebase
- :bug: Added keyword `synchronized` to term and predicate factories

### Removed

- :boom: Removed abstract class `AbstractStorageWrapper` (see new class `MatchFilter` for it's `post_filter` method)
- :boom: Removed methods specific to a `FactBase` from view datasources wrappers
- :boom: Removed classes `TermFactoryImpl` and `PredicateFactoryImpl` from integraal-model
- :boom: Removed interface and class `AtomicFOFormula` from integraal-model : Now it is directly an `Atom`

## 0.0.1-SNAPSHOT - Unreleased : Formula (small) refactoring

### Changed

- :boom: Removed type parameter from formula. It is now only formula of atoms
- :boom: Renamed method `FOFormula#flatten` to `FOFormula#asAtomSet`
- :boom: Removed Optional typing from `FOFormulaFactory#copy` and `FOFormulaFactory#createOrGet...`
- :boom: Removed methods `FOFormulaUtil#get...`. The equivalent method is available as `FOFormula#get...`

####

- :boom: Removed integraal-app module from InteGraal. It is now a dedicated module outside of the library

## 0.0.1-SNAPSHOT - Unreleased

### Added

- :boom: Better handling of dependencies using java modules with `module-info.java` files in all integraal modules
- :rocket: Improve dependency handling in `pom.xml` in all integraal modules

####

- :rocket: Added method `Term#isFrozen(Substitution)` to check if a term is frozen according to a given substitution
- :rocket: Added version number for maven plugins in `pom.xml` files
- :rocket: Added this CHANGELOG file

### Changed

- :boom: Changed package name for dlgp parser in integraal-io from `fr.boreal.io.dlgp.impl.builtin` to `fr.boreal.io.dlgp`
- :boom: Changed package name for query evaluation in integraal-query-evaluation from `fr.boreal.queryEvaluation` to `fr.boreal.query_evaluation`
- :boom: Changed `HsqlException` to `SQLException` in integraal-storage 

####

- :arrow_double_up: Upgrade RDF4J dependencies from `3.7.3` to `4.3.0-M1`
- :arrow_double_up: Upgrade Junit dependencies from `5.8.2` to `5.9.2`
- :arrow_double_up: Upgrade SQLite dependencies from `3.34.0` to `3.41.2.1`
- :arrow_double_up: Upgrade MongoDB dependencies from `4.6.0` to `4.9.1`

####

- Moved unit test to a `test` package for every module
- Moved example files from the default package to `examples` in integraal-app
- Cleanup example files in integraal-app

####

- :rocket: Incrementally improve the documentation and code

### Fixed

- :bug: Fixed a bug where unifiers could be aggregated without a needed renaming
- :bug: Fixed a bug where partition did not keep the correct size attribute used for comparison.

### Removed

- :boom: Removed unused class `EmptyCloseableIteratorWithoutException` in integraal-util
- :boom: Removed unused class `SingletonCloseableIteratorWithoutException` in integraal-util
- :boom: Removed unused class `UniqIterator` in integraal-util
- :boom: Removed unused interface `Stream` in integraal-util
- :boom: Removed unused interface `Writer` in integraal-util
- :boom: Removed the old implementation of the dlgp parser in integraal-io

####

- Removed `Basic` folder and files in integraal-app
- Removed `public` keyword from unit test classes
- Removed unused dependencies from `pom.xml` in every module
